##-*- coding: utf-8 -*-
##########   IBRAHIM BILADIN | ibradiiin@gmail.com | 083871909782   ##########
import time
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp.tools.translate import _
    
class sale_order(osv.osv):
    _inherit = "sale.order"

    def _amount_line_tax(self, cr, uid, line, context=None):
        val = 0.0
        price_unit = line.price_unit * (1-(line.discount or 0.0)/100.0)
        product_qty = line.product_uom_qty
        if line.discount_method == "fixed":
            price_unit = (line.product_uom_qty * line.price_unit) - line.discount 
            product_qty = 1.0 
        for c in self.pool.get('account.tax').compute_all(cr, uid, line.tax_id, price_unit, product_qty, line.product_id, line.order_id.partner_id)['taxes']:
            val += c.get('amount', 0.0)
        return val
    
    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_untaxed': 0.0,
                'amount_discount': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0,
            }
            val = val1 = val2 = val3 = 0.0
            cur = order.pricelist_id.currency_id
            for line in order.order_line:
                val1 += line.price_subtotal
                val += self._amount_line_tax(cr, uid, line, context=context)
            if order.discount_method=='fixed':
                val2 = order.discount
                val = val - ((val / val1) * order.discount)
            if order.discount_method=='percent':
                val2 = val1 * ((order.discount or 0.0)/100.0)
                val = val - (val * ((order.discount or 0.0)/100.0))
                
            res[order.id]['amount_tax'] = cur_obj.round(cr, uid, cur, val)
            res[order.id]['amount_untaxed'] = cur_obj.round(cr, uid, cur, val1)
            res[order.id]['amount_discount'] = cur_obj.round(cr, uid, cur, val2)
            res[order.id]['amount_total'] = (res[order.id]['amount_untaxed'] - res[order.id]['amount_discount']) + res[order.id]['amount_tax']
        return res
     
    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('sale.order.line').browse(cr, uid, ids, context=context):
            result[line.order_id.id] = True
        return result.keys()
    
    _columns = {
        'discount_method': fields.selection([('percent','Percentage'),('fixed','Fixed Amount')], 'Discount Method', 
                readonly=True, states={'draft': [('readonly', False)],'sent': [('readonly', False)]}),
        'discount': fields.float('Discount', digits_compute= dp.get_precision('Account'), 
                readonly=True, states={'draft': [('readonly', False)],'sent': [('readonly', False)]}),
        'amount_untaxed': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Untaxed Amount',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_method','discount'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The amount without tax.", track_visibility='always'),
        'amount_discount': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Discount',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_method','discount'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The discount amount.", track_visibility='always'),
        'amount_tax': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Taxes',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_method','discount'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The tax amount.", track_visibility='always'),
        'amount_total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Total',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_method','discount'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The total amount.", track_visibility='always'),
        'pos_line': fields.one2many('sale.pos.line', 'order_id', 'POS Lines'),
    }
    
    _defaults = {
        'discount': 0.0,
    }
    
    def onchange_discount_method(self, cr, uid, ids, method, context=None):
        warning = {}
        val = {'discount_method': False, 'discount': 0.0,}
        for order in self.browse(cr, uid, ids, context=context):
            if method in ('percent','fixed'):
                val.update({'discount_method' : method, 'discount': 0.0})
                for line in order.order_line:
                    if line.discount <> 0.0 or line.discount_method in ('percent','fixed'):
                        val.update({'discount_method' : '' or False, 'discount': 0.0})
                        warning = {'title': _('Error!!!'),
                            'message' : _("Salah satu produk/item sudah ditetapkan diskon (persentase atau statis), untuk menggunakan 'metode diskon total (diluar produk)' maka kosongkan semua diskon persentase/fixed didalam produk.")}
        return {'value': val, 'warning': warning}
    
    def unlink(self, cr, uid, ids, context=None):
        sale_orders = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []
        for s in sale_orders:
            if s['state'] in ['draft', 'cancel']:
                unlink_ids.append(s['id'])
                cr.execute('delete from sale_pos_line where order_id=%s', (s['id'],))
                cr.execute('update sales_pos_order set ordered=%s, sale_id=NULL where sale_id=%s', (False,s['id'],))
            else:
                raise osv.except_osv(_('Invalid Action!'), _('In order to delete a confirmed sales order, you must cancel it before!'))
        return super(sale_order, self).unlink(cr, uid, unlink_ids, context=context)
    
    def action_cancel(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        sale_order_line_obj = self.pool.get('sale.order.line')
        account_invoice_obj = self.pool.get('account.invoice')
        for sale in self.browse(cr, uid, ids, context=context):
            for inv in sale.invoice_ids:
                if inv.state not in ('draft', 'cancel'):
                    raise osv.except_osv(
                        _('Cannot cancel this sales order!'),
                        _('First cancel all invoices attached to this sales order.'))
                inv.signal_workflow('invoice_cancel')
            line_ids = [l.id for l in sale.order_line if l.state != 'cancel']
            sale_order_line_obj.button_cancel(cr, uid, line_ids, context=context)
            cr.execute('update sales_pos_order set ordered=%s, sale_id=NULL where sale_id=%s', (False,sale.id,))
        self.write(cr, uid, ids, {'state': 'cancel'})
        return True
    
    
    
class sale_order_line(osv.osv):
    _inherit = "sale.order.line"
    
    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        context = context or {}
        for line in self.browse(cr, uid, ids, context=context):
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            if line.discount_method=="fixed":
                price = line.price_unit - line.discount
            taxes = self.pool.get('account.tax').compute_all(cr, uid, line.tax_id, price, line.product_uom_qty, line.product_id, line.order_id.partner_id)
            cur = line.order_id.pricelist_id.currency_id
            val = taxes['total']
            #if line.discount_method=="fixed":
                #val = (taxes['total'] - line.discount)
            res[line.id] = self.pool.get('res.currency').round(cr, uid, cur, val)
        return res
    
    def _amount_discount_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        val_discount = 0.0
        for line in self.browse(cr, uid, ids, context=None):
            cur = line.order_id.pricelist_id.currency_id
            if line.discount_method=="fixed":
                val_discount = line.discount
            if line.discount_method=="percent":
                val_discount = (line.product_uom_qty * line.price_unit) * (line.discount / 100.0)
            res[line.id] = self.pool.get('res.currency').round(cr, uid, cur, val_discount)
        return res
    
    _inherit = 'sale.order.line'
    _columns = {
        'discount_method': fields.selection([('percent','Percentage'),('fixed','Fixed Amount')], 'Discount Method', 
                readonly=True, states={'draft': [('readonly', False)],'sent': [('readonly', False)]}),
        'discount': fields.float('Discount', digits_compute= dp.get_precision('Discount'), 
                readonly=True, states={'draft': [('readonly', False)],'sent': [('readonly', False)]}),
        'price_subtotal': fields.function(_amount_line, string='Subtotal', digits_compute= dp.get_precision('Account')),
        'subtotal_discount': fields.function(_amount_discount_line, string='Subtotal Discount', digits_compute= dp.get_precision('Account')),
    }
    
    def invoice_line_create(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        create_ids = []
        sales = set()
        for line in self.browse(cr, uid, ids, context=context):
            account = False
            if line.name == 'Cash': 
                account_ids = self.pool.get('account.account').search(cr, uid, [('code','=','240000.00.01'), ('name','like','%Cash%')], context=context)
                account = account_ids and account_ids[0]
            elif line.name == 'Card': 
                account_ids = self.pool.get('account.account').search(cr, uid, [('code','=','240000.00.02'), ('name','like','%Card%')], context=context)
                account = account_ids and account_ids[0]
            elif line.name == 'Prepaid': 
                account_ids = self.pool.get('account.account').search(cr, uid, [('code','=','240000.00.05'), ('name','like','%Prepaid%')], context=context)
                account = account_ids and account_ids[0]
            vals = self._prepare_order_line_invoice_line(cr, uid, line, account, context)
            if vals:
                inv_id = self.pool.get('account.invoice.line').create(cr, uid, vals, context=context)
                self.write(cr, uid, [line.id], {'invoice_lines': [(4, inv_id)]}, context=context)
                sales.add(line.order_id.id)
                create_ids.append(inv_id)
        # Trigger workflow events
        for sale_id in sales:
            workflow.trg_write(uid, 'sale.order', sale_id, cr)
        return create_ids
    
    def onchange_discount(self, cr, uid, ids, method, parent_disc, context=None):
        warning = {}
        val = {'discount_method': method, 'discount': 0.0,}
        if parent_disc in ('percent','fixed'):
            val.update({'discount_method': False, 'discount': 0.0})
            warning = {'title': _('Error!!!'),
                    'message' : _("Sudah ada perhitungan diskon diluar per produk (diskon persentase atau statis), silahkan periksa kembali dokumen ini.")}        
        return {'value': val, 'warning': warning}

        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
