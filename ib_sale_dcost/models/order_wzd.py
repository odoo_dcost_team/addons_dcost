##-*- coding: utf-8 -*-
##########   IBRAHIM BILADIN | ibradiiin@gmail.com | 083871909782   ##########

from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)


class order_transfer_atonce(osv.osv_memory):
    _name = "order.transfer.detail.atonce"

    def _check_order_list(self, cr, uid, context):
        if context.get('active_model', '') in ('sale.order','purchase.order'):
            ids = context['active_ids']
            order_obj = self.pool.get('sale.order')
            if context['active_model'] == 'purchase.order':
                order_obj = self.pool.get('purchase.order')
            user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
            if ids or (len(ids) > 1):
                orders = order_obj.read(cr, uid, ids, ['state','company_id',])
                for o in orders:
                    if o['state'] not in ('approved', 'except_invoice', 'progress', 'manual', 'invoice_except'):
                    #if o['state'] in ('draft','sent','bid','cancel','done'):
                        raise orm.except_orm(_('Warning!'), _('At least one of the selected order is %s! \nAllowed order status is order confirmed or in progress.') % o['state'])
                    if o['company_id'][0] <> user.company_id.id:
                        raise orm.except_orm(_('Warning!'), _('Not all Orders are in the same company as the user!'))
        return {}

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        if context is None:
            context = {}
        res = super(order_transfer_atonce, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=False)
        self._check_order_list(cr, uid, context)
        return res

    def action_view_order_lines(self, cr, uid, ids, context=None):
        order_obj = self.pool.get('sale.order')
        if context.get('active_model', '') == 'sale.order':
            order_obj = self.pool.get('sale.order')
        elif context.get('active_model', '') == 'purchase.order':
            order_obj = self.pool.get('purchase.order')
        order_ids = context.get('active_ids', [])

        mod_obj = self.pool.get('ir.model.data')
        dummy, action_id = tuple(mod_obj.get_object_reference(cr, uid, 'stock', 'action_picking_tree'))
        result = self.pool.get('ir.actions.act_window').read(cr, uid, action_id, context=context)
        #compute the number of pickings to display
        todo_list = []
        for order in order_obj.browse(cr, uid, order_ids, context=context):
            todo_list += [picking.id for picking in order.picking_ids]

        result['context'] = {}
        if len(todo_list)>1:
            result['domain'] = "[('id','in',["+','.join(map(str, todo_list))+"])]"
        else:
            res = mod_obj.get_object_reference(cr, uid, 'stock', 'view_picking_form')
            result['views'] = [(res and res[1] or False, 'form')]
            result['res_id'] = todo_list and todo_list[0] or False
        return result


class stock_picking_transfer_atonce(osv.osv_memory):
    _name = "stock.picking.transfer.at.once"

    def _check_picking_list(self, cr, uid, context):
        if context.get('active_model', '') == 'stock.picking':
            ids = context['active_ids']
            user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
            if ids or (len(ids) > 1):
                pickings = self.pool.get('stock.picking').read(cr, uid, ids, ['state','company_id',])
                for p in pickings:
                    if p['state'] not in ('assigned','partially_available'):
                        raise orm.except_orm(_('Warning!'), _('At least one of the selected picking is %s! \nAllowed picking status is Partially Available or Ready to Transfer.') % p['state'])
                    if p['company_id'][0] <> user.company_id.id:
                        raise orm.except_orm(_('Warning!'), _('Not all picking are in the same company as the user!'))
        return {}

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        if context is None:
            context = {}
        res = super(stock_picking_transfer_atonce, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=False)
        self._check_picking_list(cr, uid, context)
        return res

    def action_merge_transfer(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        else:
            context = context.copy()
        picking_ids = context and context.get('active_ids', [])
        # picking_ids = ids or []
        # if context and context.get('active_ids', False):
        #     picking_ids = context['active_ids']
        context.update({
            'active_model': 'stock.picking',
            'active_ids': picking_ids,
            'active_id': len(picking_ids) and picking_ids[0] or False
        })
        created_id = self.pool.get('stock.transfer_details').create(cr, uid, {
            'picking_id': len(picking_ids) and picking_ids[0] or False}, context)
        return self.pool.get('stock.transfer_details').wizard_view(cr, uid, created_id, context)



class confirm_order_atonce(osv.osv_memory):
    _name = "confirm.order.atonce"

    def _check_purchase_order_list(self, cr, uid, context):
        if context.get('active_model', '') == 'purchase.order':
            ids = context['active_ids']
            order_obj = self.pool.get('purchase.order')
            user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
            if ids or (len(ids) > 1):
                orders = order_obj.read(cr, uid, ids, ['state','company_id',])
                for o in orders:
                    if o['state'] not in ('draft','sent','bid'):
                        raise orm.except_orm(_('Warning!'), _('At least one of the selected order is %s! \nAllowed order status is Draft PO, RFQ or Bid Received.') % o['state'])
                    if o['company_id'][0] <> user.company_id.id:
                        raise orm.except_orm(_('Warning!'), _('Not all Orders are in the same company as the user!'))
        return {}

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        if context is None:
            context = {}
        res = super(confirm_order_atonce, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=False)
        self._check_purchase_order_list(cr, uid, context)
        return res

    def confirm_all_order(self, cr, uid, ids, context=None):
        purchase_obj = self.pool.get('purchase.order')
        purchase_ids = context.get('active_ids', [])
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        if purchase_ids and (len(purchase_ids) > 0):
            cr.execute('''SELECT uid AS user FROM res_groups_users_rel 
                WHERE gid IN (SELECT x.id FROM res_groups AS x LEFT JOIN ir_module_category AS y ON x.category_id=y.id 
                            WHERE (x.name like '%Manager%' or x.name like '%User%') AND  y.name like 'Purchases')
                GROUP BY uid''')
            allow_users = []
            for dt in cr.dictfetchall():
                if dt['user'] not in allow_users:
                    allow_users.append(dt['user'])

            if (context.get('active_model', '')=='purchase.order'):
                if user.id not in allow_users:
                    raise osv.except_osv(_('Error!'), _('You are not allowed to confirm the purchase.'))
                else:
                    purchase_obj.wkf_confirm_order(cr, uid, purchase_ids, context=context)
                    purchase_obj.signal_workflow(cr, uid, purchase_ids, 'purchase_confirm')
                    purchase_obj.wkf_approve_order(cr, uid, purchase_ids, context=context)
        return {'type': 'ir.actions.act_window_close'}


