##-*- coding: utf-8 -*-
##########   IBRAHIM BILADIN | ibradiiin@gmail.com | 083871909782   ##########
import time
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp


class mrp_bom(osv.osv):
    _inherit = 'mrp.bom'

    _columns = {
        'product_qty': fields.float('Product Quantity', required=True, digits_compute=dp.get_precision('BoM')),
    }



class mrp_bom_line(osv.osv):
    _inherit = 'mrp.bom.line'

    _columns = {
        'product_qty': fields.float('Product Quantity', required=True, digits_compute=dp.get_precision('BoM')),
    }

