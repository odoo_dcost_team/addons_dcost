##-*- coding: utf-8 -*-
##########   IBROHIM BILADIN | ibradiiin@gmail.com | 083871909782   ##########
import time
import os
import glob
import csv
import psycopg2

from psycopg2 import OperationalError
from openerp import models, api, fields as Fields
from openerp.osv import osv, fields, orm, expression
#from openerp import tools
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp import SUPERUSER_ID
from datetime import datetime
from openerp.exceptions import except_orm, Warning
import openerp

ZONE_SELECTION = [('zona_1', 'Zona 1'), ('zona_2', 'Zona 2'), ('zona_dd', 'Zona DD'),
                  ('zona_vip', 'Zona VIP'), ('zona_warung', 'Zona Warung')]
    
class stock_warehouse(osv.osv):
    _inherit = "stock.warehouse"
    _columns = {
        'code': fields.char('Warehouse Code', size=10, help="Short name used to identify your warehouse"),
        'outlet': fields.char(string='Outlet'),
        'outlet_id': fields.char('Outlet ID', required=True), #harus generate auto ID
        'active': fields.boolean('Active'),
        'zone_store': fields.selection(ZONE_SELECTION, 'Zona', help="Kelompok zona outlet sesuai lokasi."),
        'kota_id': fields.related('partner_id', 'kota_id', type='many2one', relation='res.partner.kota',
                    string='Kota / Wilayah', store=True),
    }
    _defaults = {
        'active': True,
    }
    _sql_constraints = [
        ('warehouse_outlet_id_uniq', 'unique(outlet_id, company_id)', 'ID outlet or warehouse must be unique per company!'),
    ]

    def name_search(self, cr, user, name, args=None, operator='ilike', context=None, limit=80):
        if not args:
            args = []
        if operator in expression.NEGATIVE_TERM_OPERATORS:
            domain = [('code', operator, name), ('name', operator, name), ('outlet', operator, name)]
        else:
            domain = ['|','|',('code', operator, name),('name', operator, name),('outlet', operator, name)]
        ids = self.search(cr, user, expression.AND([domain, args]), limit=limit, context=context)
        return self.name_get(cr, user, ids, context=context)

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        result = []
        for record in self.browse(cr, uid, ids, context=context):
            name = record.outlet  #outlet_name:DCOST SUNTER ,etc
            if record.code:
                name = "[%s] %s" % (record.code, name)
            if context.get('show_complete'):
                if record.zone_store:
                    name = name + "\n %s" % (str(record.zone_store).replace("_", " ").title())
                if record.kota_id:
                    kota_kab = self.pool.get('res.partner.kota').browse(cr, uid, record.kota_id.id).name
                    name = name + ", %s" % (kota_kab)
            result.append((record.id, name))
        return result

    
class product_template(osv.osv):
    _inherit = "product.template"
    _columns = {
        'menu_id': fields.char('Menu ID'),  #,required=True
    }
    

class account_invoice(osv.osv):
    _inherit = "account.invoice"

    def _get_warehouse(self, cr, uid, context=None):
        usr_obj = self.pool.get('res.users')
        user = usr_obj.browse(cr, uid, uid, context=context)
        if user.warehouse_id:
            return user.warehouse_id.id
        return False

    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse'),
        'pos_ids': fields.many2many('sales.pos.order', 'sales_pos_order_invoice_rel', 'invoice_id', 'pos_id',
                                        'Data POS', readonly=True, copy=False),
    }

    origin = Fields.Char(string='Source Document', size=512,
                         help="Reference of the document that produced this invoice.",
                         readonly=True, states={'draft': [('readonly', False)]})

    _defaults = {
        'warehouse_id': _get_warehouse,
    }


class sale_order_pos_line(osv.osv):
    _name = "sale.pos.line"
    _columns = {
        'order_id': fields.many2one('sale.order', 'Sale Order', ondelete='cascade', select=True),
    }
                
                
class analytic_cabang(osv.osv):
    _inherit = "analytic.cabang"
    

class purchase_requisition(osv.osv):
    _inherit = "purchase.requisition" 
    
    
class purchase_requisition_line(osv.osv):
    _inherit = "purchase.requisition.line"
        
        
class stock_move(osv.osv):
    _inherit = "stock.move"


class LogHistoryMenuRanking(models.Model):
    _name = 'log.history.menu'
    _order = 'date desc'
    _description = 'Log History - Menu Ranking'

    name = Fields.Char('Subject', required=True)
    date = Fields.Datetime('Date')
    body = Fields.Text('Summary')


class sales_pos_order(osv.osv):
    _name = "sales.pos.order"
    _description = "POS DCost for Sales"
    _inherit = ['mail.thread']

    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        res = {}
        context = context or {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = {'subtotal': 0.0, 'amount_tax': 0.0, 'amount_untaxed': 0.0}
            cur = user.company_id.currency_id
            total = (line.cash_amount + line.card_amount + line.prepaid_amount + line.discount + line.delivery_amount)
            if line.name and line.name.outlet_id in ('80','119','133') and line.name.code in ('vipbli','vipabm','vipbgm'):
                total = (line.cash_amount + line.card_amount + line.prepaid_amount + line.delivery_amount - line.discount)
            res[line.id]['subtotal'] = self.pool.get('res.currency').round(cr, uid, cur, total)
            res[line.id]['amount_untaxed'] = self.pool.get('res.currency').round(cr, uid, cur, total/1.1)
            res[line.id]['amount_tax'] = self.pool.get('res.currency').round(cr, uid, cur, (total/1.1 * 0.1))
        return res

    def _get_warehouse(self, cr, uid, ids, name, arg, context=None):
        res = {}
        context = context or {}
        for pos in self.browse(cr, uid, ids, context=context):
            cr.execute('''SELECT id FROM stock_warehouse   
                WHERE outlet_id=%s AND outlet like %s AND active=%s 
                LIMIT 1''', (str(pos.branch_id),_("%")+str(pos.branch_name)+_("%"),True))
            whs = cr.fetchone()
            res[pos.id] = (whs and whs[0]) or False
        return res

    def _invoice_exists(self, cursor, user, ids, name, arg, context=None):
        res = {}
        for pos in self.browse(cursor, user, ids, context=context):
            res[pos.id] = False
            if pos.invoice_ids:
                res[pos.id] = True
        return res

    def _invoiced(self, cursor, user, ids, name, arg, context=None):
        res = {}
        for pos in self.browse(cursor, user, ids, context=context):
            res[pos.id] = True
            invoice_existence = False
            for invoice in pos.invoice_ids:
                if invoice.state!='cancel':
                    invoice_existence = True
                    if invoice.state != 'paid':
                        res[pos.id] = False
                        break
            if not invoice_existence or pos.state == 'manual':
                res[pos.id] = False
        return res
    
    def _invoiced_search(self, cursor, user, obj, name, args, context=None):
        if not len(args):
            return []
        clause = ''
        pos_clause = ''
        no_invoiced = False
        for arg in args:
            if (arg[1] == '=' and arg[2]) or (arg[1] == '!=' and not arg[2]):
                clause += 'AND inv.state = \'paid\''
            else:
                clause += 'AND inv.state != \'cancel\' AND pos.state != \'cancel\'  AND inv.state <> \'paid\'  AND rel.pos_id = pos.id '
                pos_clause = ',  sales_pos_order AS pos '
                no_invoiced = True

        cursor.execute('SELECT rel.pos_id ' \
                'FROM sales_pos_order_invoice_rel AS rel, account_invoice AS inv '+ pos_clause + \
                'WHERE rel.invoice_id = inv.id ' + clause)
        res = cursor.fetchall()
        if no_invoiced:
            cursor.execute('SELECT pos.id ' \
                    'FROM sales_pos_order AS pos ' \
                    'WHERE pos.id NOT IN ' \
                        '(SELECT rel.pos_id ' \
                        'FROM sales_pos_order_invoice_rel AS rel) and pos.state != \'cancel\'')
            res.extend(cursor.fetchall())
        if not res:
            return [('id', '=', 0)]
        return [('id', 'in', [x[0] for x in res])]

    _columns = {
        'date': fields.date('Date', select=True, copy=False),
        'name': fields.function(_get_warehouse, type='many2one',
                relation='stock.warehouse', string='Outlet', store=True),
        'discount': fields.float('Discount', digits_compute=dp.get_precision('Account'), help="Potongan Penjualan Kotor (Cash)"),
        'cash_amount': fields.float('Cash Amount', digits_compute=dp.get_precision('Account'), help="Pendapatan Penjualan Tunai(Cash) Kotor"),
        'card_amount': fields.float('Card Amount', digits_compute=dp.get_precision('Account'), help="Pendapatan Penjualan Kartu (Debit/Credit Card) Kotor"),
        'prepaid_amount': fields.float('Prepaid Amount', digits_compute=dp.get_precision('Account'), help="Pendapatan Penjualan Prabayar (Prepaid) Kotor"),
        'ordered': fields.boolean('Ordered', help="This indicates that the POS data has at least one sales order."),
        'sale_id': fields.many2one('sale.order', 'Sales Order'),
        'subtotal': fields.function(_amount_line, string='Total', digits_compute=dp.get_precision('Account'),
            store={'sales.pos.order': (lambda self, cr, uid, ids, c={}: ids, ['cash_amount','card_amount','prepaid_amount','discount','delivery_amount'], 10),},
            multi='sums'),
        'branch_id': fields.integer('Branch ID'),
        'branch_name': fields.char('Branch Name'),
        'source_id': fields.float('Source Database ID', digits_compute=dp.get_precision('Account')),
        'src_mysql_id': fields.char('Source Database ID', required=True, help="ID Database MySQL POS"),
        'invoiced': fields.boolean('Invoice'),
        'invoice_ids': fields.many2many('account.invoice', 'sales_pos_order_invoice_rel', 'pos_id', 'invoice_id',
                        'Invoices', readonly=True, copy=False),
        'amount_tax': fields.function(_amount_line, string='Pajak Resto (PB1)', digits_compute=dp.get_precision('Account'),
            store={'sales.pos.order': (lambda self, cr, uid, ids, c={}: ids,['cash_amount','card_amount','prepaid_amount',
                                        'discount','delivery_amount'], 10),}, multi='sums'),
        'amount_untaxed': fields.function(_amount_line, string='Untaxed Amount', digits_compute=dp.get_precision('Account'),
            store={'sales.pos.order': (lambda self, cr, uid, ids, c={}: ids, ['cash_amount','card_amount','prepaid_amount',
                                        'discount','delivery_amount'], 10),}, multi='sums'),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('cancel', 'Cancelled'),
            ('progress', 'In Progress'),
            ('invoice_except', 'Invoice Exception'),
            ('done', 'Done'),
        ], 'Status', readonly=True, copy=False, select=True),
        'invoice_paid': fields.function(_invoiced, string='Invoice-Paid', fnct_search=_invoiced_search, type='boolean',
                        help="It indicates that an invoice has been paid."),
        'invoice_exists': fields.function(_invoice_exists, string='Invoiced', fnct_search=_invoiced_search, type='boolean',
                        help="It indicates that sales order has at least one invoice."),
        'delivery_amount': fields.float('Delivery Amount', digits_compute=dp.get_precision('Account'), help="Pendapatan Penjualan Delivery"),
        'voucher': fields.float('Voucher', digits_compute=dp.get_precision('Account')),
    }
    _defaults = {
        'discount': lambda *a: 0.0,
        'cash_amount': lambda *a: 0.0,
        'card_amount': lambda *a: 0.0,
        'prepaid_amount': lambda *a: 0.0,
        'ordered': False,
        'invoiced': False,
        'state': 'draft',
        'delivery_amount': lambda *a: 0.0,
        'voucher': lambda *a: 0.0,
    }

    def run(self, cr, uid, ids, autocommit=False, context=None):
        invoice_ids = False
        for pos_omzet_id in ids:
            omzet = self.browse(cr, uid, pos_omzet_id, context=context)  
            #rewrite UID ke user outlet not uid admin
            user = self.pool.get('res.users').search(cr, uid, [('login', 'like', 'adm%'), ('login', 'not like', '%admin%'),
                    ('warehouse_id', '=', omzet.name and omzet.name.id)], limit=1, order='login')[0] or SUPERUSER_ID
            if (not omzet.invoice_ids) or (not omzet.invoiced): #Create new invoice
                if (not omzet.cash_amount) and (not omzet.card_amount) and (not omzet.prepaid_amount) and (not omzet.delivery_amount):
                    raise except_orm(_('Peringatan !!!'),
                    _("Ada salah satu Data POS tidak mempunyai transaksi Cash/Card/Prepaid/Delivery (nol). Silahkan cek kembali..."))
                else:
                    if (not omzet.date):
                        raise except_orm(_('Peringatan !!!'),
                                         _("Ada salah satu Data POS yang tidak ada tanggal transaksinya. Silahkan cek kembali..."))
                    if (not omzet.invoiced):
                        try:
                            invoice_ids = self._run(cr, user, omzet, False, context=context or {}) #[omzet.id]
                            if invoice_ids:
                                self.write(cr, user, [omzet.id], {'invoiced': True}, context=context)
                                self._create_voucher_and_move(cr, user, invoice_ids, context=context or {})
                        except OperationalError:
                            if autocommit:
                                cr.rollback()
                                continue
                            else:
                                raise
            else:
                for inv in omzet.invoice_ids:   #Check existing invoice
                    if inv.state == 'draft':    #Edit draft Invoice
                        invoice_ids = self._run(cr, user, omzet, inv.id, context=context or {}) #[omzet.id]
                    else:
                        if not omzet.invoice_exists:#Create new invoice
                            invoice_ids = self._run(cr, user, omzet, False, context=context or {})
                    if invoice_ids:
                        self.write(cr, user, [omzet.id], {'invoiced': True}, context=context)
                        self._create_voucher_and_move(cr, user, invoice_ids or omzet.invoice_ids, context=context or {})
        return True

    def _create_invoice(self, cr, uid, partner, pos, context=None):
        res_partner = self.pool.get('res.partner')
        invoice_obj = self.pool.get('account.invoice')
        curr_obj = self.pool.get('product.pricelist')
        users_obj = self.pool.get('res.users')

        account_id = partner.property_account_receivable and partner.property_account_receivable.id or False
        customer = res_partner.browse(cr, uid, partner.id, context=context)
        company_id = users_obj.browse(cr, uid, uid, context=context).company_id
        pricelist = customer.property_product_pricelist.id
        currency = curr_obj.browse(cr, uid, pricelist, context=context).currency_id.id or False
        payment_term = customer.property_payment_term and customer.property_payment_term.id or False
        fpos = customer.property_account_position and customer.property_account_position.id or False
        users = users_obj.search(cr, uid, [('login', 'like', 'adm%'),('login', 'not like', '%admin%'),
                        ('warehouse_id', '=', pos.name and pos.name.id)], limit=1, order='login')
        user_id = users and users[0] or SUPERUSER_ID
        invoice_vals = {
            'name': _("Penjualan %s, Tanggal: %s [POS]") % (pos.name and pos.name.outlet, str(pos.date)),
            'type': 'out_invoice',
            'reference': "%s" % (_(int(pos.source_id)) or _('')),
            'account_id': account_id,
            'partner_id': customer.id,
            'currency_id' : currency or (company_id.currency_id and company_id.currency_id.id),
            'payment_term': payment_term,
            'fiscal_position': fpos,
            'user_id': user_id,
            'company_id': company_id and company_id.id,
            'date_invoice': pos.date or time.strftime('%Y-%m-%d'),
            'warehouse_id': pos.name and pos.name.id or False,
        }
        if (not pos.invoice_ids) and (not pos.invoiced):
            return invoice_obj.create(cr, uid, invoice_vals, context=context)
        else:
            return False

    def _create_invoice_line(self, cr, uid, data, context=None):
        account_obj = self.pool.get('account.account')
        acc_tax_obj = self.pool.get('account.tax')
        analytic_cbg_obj = self.pool.get('analytic.cabang')
        product_uom = False; account_id = False; account_analytic_id = False
        invoices = self.pool.get('account.invoice').browse(cr, uid, data['invoice_id'], context=context)

        cr.execute("SELECT id FROM product_uom WHERE name like %s or name like %s ORDER BY id ASC LIMIT 1",
                   ('%day%', '%Day%'))
        if cr.fetchall():
            for i in cr.fetchall():
                product_uom = i[0]
        else:
            cr.execute("SELECT id FROM product_uom WHERE name like %s or name like %s ORDER BY id ASC LIMIT 1",
                       ('%lot%', '%Lot%'))
            for i in cr.fetchall():
                product_uom = i[0]
        if data['name'] == 'Cash':
            account_ids = account_obj.search(cr, uid, [
                ('code','=','240000.00.01'), ('name','like','%Penjualan Cash%'),('active','=',True)], context=context)
            account_id = account_ids and account_ids[0] or 4630
        elif data['name'] == 'Card':
            account_ids = account_obj.search(cr, uid, [
                ('code','=','240000.00.02'),('name','like','%Penjualan Card%'),('active','=',True)], context=context)
            account_id = account_ids and account_ids[0] or 4631
        elif data['name'] == 'Prepaid':
            account_ids = account_obj.search(cr, uid, [
                ('code','=','240000.00.05'),('name','like','%Penjualan Prepaid%'),('active','=',True)], context=context)
            account_id = account_ids and account_ids[0] or 4634
        elif data['name'] == 'Discount':
            account_ids = account_obj.search(cr, uid, [
                ('code','=','240000.00.07'),('name','like','%Potongan Penjualan%'),('active','=',True)], context=context)
            account_id = account_ids and account_ids[0] or 4636
        elif data['name'] == 'Delivery':
            account_ids = account_obj.search(cr, uid, [
                ('code','=','240000.00.03'),('name','like','%Pendapatan Penjualan Delivery%'),('active','=',True)], context=context)
            account_id = account_ids and account_ids[0] or 4632

        if not account_id:
            prop = self.pool.get('ir.property').get(cr, uid,
                'property_account_income_categ', 'product.category', context=context)
            account_id = prop and prop.id

        tax_ids = []
        tax_list = acc_tax_obj.search(cr, uid, [('active','=',True),('type','=','percent'),('amount','=',0.1),
                                                 ('type_tax_use','=','sale')], context=context)
        if tax_list:
            tax_ids += [at.id for at in acc_tax_obj.browse(cr, uid, tax_list, context=context)]

        if account_id:
            analytic_list = analytic_cbg_obj.search(cr, uid,
                [('account_id','=',account_id),('name','=',invoices.warehouse_id and invoices.warehouse_id.id)],
                limit=1, context=context)
            for analytics in analytic_cbg_obj.browse(cr, uid, analytic_list, context=context):
                account_analytic_id = analytics.analytic_id and analytics.analytic_id.id
        vals = {
                'invoice_id': data['invoice_id'],
                'name': data['name'],
                'account_id': account_id,
                'price_unit': data['amount'] or 0.0,
                'quantity': 1.0,
                'uos_id': product_uom,
                'account_analytic_id': account_analytic_id,
        }
        if data['name'] in ('Cash','Card','Prepaid','Delivery'):
            vals.update({'invoice_line_tax_id': [(6, 0, tax_ids)]})
        return self.pool.get('account.invoice.line').create(cr, uid, vals, context=context)

    def _run(self, cr, uid, pos, inv_id, context=None): #run execute data pos
        context = context or {}
        res_partner = self.pool.get('res.partner')
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')
        inv_ids, pos_ids = [], []
        warehouses, discounts, ilines = {}, {}, {}
        key_partner = _("%CUSTOMER ") + str(pos.name.code).upper() +"%" if pos.name else _('%All Customer%')
        part_ids = res_partner.search(cr, uid, [('name', 'like', key_partner)], context=context)
        partner = res_partner.browse(cr, uid, part_ids[0] or 9732, context=context)
        ##for pos in self.browse(cr, uid, omzet_ids, context=context):
        if not inv_id:
            if (pos.name.id not in warehouses.keys()):
                warehouses[pos.name.id] = {}
            if (pos.date not in warehouses[pos.name.id].keys()):
                invoice_id = self._create_invoice(cr, uid, partner, pos, context=context)
                ilines[invoice_id] = {}
                if pos.discount and pos.discount > 0.0:
                    #if (pos.name.outlet_id in ('80','119','133')) or (pos.name.code in ('vipbli','vipabm','vipbgm')):
                    discounts.update({invoice_id: pos.discount})  #* -1
                    #else:
                        #ilines[invoice_id]['Discount'] = pos.discount * -1
                if pos.prepaid_amount and pos.prepaid_amount > 0.0:
                    ilines[invoice_id]['Prepaid'] = (pos.prepaid_amount/1.1)
                if pos.card_amount and pos.card_amount > 0.0:
                    ilines[invoice_id]['Card'] = (pos.card_amount/1.1)
                if pos.cash_amount and pos.cash_amount > 0.0:
                    ilines[invoice_id]['Cash'] = (pos.cash_amount/1.1) + (pos.discount/1.1)
                if pos.delivery_amount and pos.delivery_amount > 0.0:
                    ilines[invoice_id]['Delivery'] = (pos.delivery_amount/1.1)
                warehouses[pos.name.id][pos.date] = invoice_id or False
            else:
                invoice_id = warehouses[pos.name.id][pos.date]
                if pos.discount > 0.0:
                    #and ((pos.name.outlet_id in ('80','119','133')) or (pos.name.code in ('vipbli','vipabm','vipbgm'))):
                    if invoice_id in discounts.keys():
                        discounts.update({invoice_id: discounts[invoice_id] + pos.discount})
                    else:
                        discounts.update({invoice_id: pos.discount})
                if invoice_id not in ilines.keys():
                    ilines[invoice_id] = {}
                    if pos.prepaid_amount and pos.prepaid_amount > 0.0:
                        ilines[invoice_id]['Prepaid'] = (pos.prepaid_amount/1.1)
                    if pos.card_amount and pos.card_amount > 0.0:
                        ilines[invoice_id]['Card'] = (pos.card_amount/1.1)
                    if pos.cash_amount and pos.cash_amount > 0.0:
                        ilines[invoice_id]['Cash'] = (pos.cash_amount/1.1) + (pos.discount/1.1)
                    if pos.delivery_amount and pos.delivery_amount > 0.0:
                        ilines[invoice_id]['Delivery'] = (pos.delivery_amount/1.1)
                else:
                    if ('Prepaid' in ilines[invoice_id]) and pos.prepaid_amount > 0.0:
                        ilines[invoice_id]['Prepaid'] += (pos.prepaid_amount/1.1)
                    else:
                        if pos.prepaid_amount > 0.0:
                            ilines[invoice_id]['Prepaid'] = (pos.prepaid_amount/1.1)
                    if ('Card' in ilines[invoice_id]) and pos.card_amount > 0.0:
                        ilines[invoice_id]['Card'] += (pos.card_amount/1.1)
                    else:
                        if pos.card_amount > 0.0:
                            ilines[invoice_id]['Card'] = (pos.card_amount/1.1)
                    if ('Cash' in ilines[invoice_id]) and pos.cash_amount > 0.0:
                        ilines[invoice_id]['Cash'] += (pos.cash_amount/1.1) + (pos.discount/1.1)
                    else:
                        if pos.cash_amount > 0.0:
                            ilines[invoice_id]['Cash'] = (pos.cash_amount/1.1) + (pos.discount/1.1)
                    if ('Delivery' in ilines[invoice_id]) and pos.delivery_amount > 0.0:
                        ilines[invoice_id]['Delivery'] += (pos.delivery_amount/1.1)
                    else:
                        if pos.delivery_amount > 0.0:
                            ilines[invoice_id]['Delivery'] = (pos.delivery_amount/1.1)
        else:
            invoice_id = inv_id
            inv_line_desc = []
            invoices = invoice_obj.read(cr, uid, [invoice_id], ['state', 'discount', 'warehouse_id'], context=context)
            if invoice_id not in ilines.keys():
                ilines[invoice_id] = {}
            if pos.name.id not in warehouses.keys():
                warehouses[pos.name.id] = {}
            for i in invoices:
                if i['warehouse_id'] and i['warehouse_id'][1]:
                    warehouse_id = i['warehouse_id'][1]
                if i['state'] <> 'draft':
                    raise osv.except_osv(_('Error!'),
                        _('Invoice tidak valid karena berstatus %s. Data POS hanya dapat digabungkan ke dalam Invoice yang mempunyai status Draft.') % (i['state'],))
                if pos.name.id <> i['warehouse_id'][0]:
                    raise osv.except_osv(_('Error!'), _('ID Outlet/Cabang Data POS tidak sama dengan Invoice (%s),\nSilahkan cek kembali...\n[ID: %s; Outlet: %s; Date: %s]') % (
                        i['warehouse_id'][1], pos.id, pos.name and pos.name.name, (pos.date).strftime('%d-%m-%Y'),))
                if pos.discount > 0.0:
                    #and (pos.name.outlet_id in ('80','119','133')) or (pos.name.code in ('vipbli','vipabm','vipbgm')):
                    if i['discount']:
                        discounts.update({invoice_id: i['discount'] + pos.discount})
                    else:
                        if invoice_id in discounts.keys():
                            discounts.update({invoice_id: discounts[invoice_id] + pos.discount})
                        else:
                            discounts.update({invoice_id: pos.discount})
            inv_lines_ids = invoice_line_obj.search(cr, uid, [('invoice_id', '=', invoice_id)], context=context)
            for line in invoice_line_obj.browse(cr, uid, inv_lines_ids, context=context):
                if line.name == 'Delivery' and pos.delivery_amount > 0.0:
                    invoice_line_obj.write(cr, uid, [line.id], {'price_unit': (line.price_unit + (pos.delivery_amount/1.1))}, context=context)
                if line.name == 'Prepaid' and pos.prepaid_amount > 0.0:
                    invoice_line_obj.write(cr, uid, [line.id], {'price_unit': (line.price_unit + (pos.prepaid_amount/1.1))}, context=context)
                if line.name == 'Card' and pos.card_amount > 0.0:
                    invoice_line_obj.write(cr, uid, [line.id], {'price_unit': (line.price_unit + (pos.card_amount/1.1))},context=context)
                if line.name == 'Cash' and pos.cash_amount > 0.0:
                    invoice_line_obj.write(cr, uid, [line.id], {'price_unit': (line.price_unit + (pos.cash_amount/1.1))}, context=context) #+ (pos.discount/1.1)
                inv_line_desc.append(line.name)

            if ('Prepaid' in ilines[invoice_id].keys()):
                ilines[invoice_id]['Prepaid'] += (pos.prepaid_amount/1.1)
            else:
                if ('Prepaid' not in inv_line_desc) and pos.prepaid_amount > 0.0:
                    ilines[invoice_id]['Prepaid'] = (pos.prepaid_amount/1.1)
            if ('Card' in ilines[invoice_id].keys()):
                ilines[invoice_id]['Card'] += (pos.card_amount/1.1)
            else:
                if ('Card' not in inv_line_desc) and pos.card_amount > 0.0:
                    ilines[invoice_id]['Card'] = (pos.card_amount/1.1)
            if ('Cash' in ilines[invoice_id].keys()):
                ilines[invoice_id]['Cash'] += (pos.cash_amount/1.1) + (pos.discount/1.1)
            else:
                if ('Cash' not in inv_line_desc) and pos.cash_amount > 0.0:
                    ilines[invoice_id]['Cash'] = (pos.cash_amount/1.1) + (pos.discount/1.1)
            if ('Delivery' in ilines[invoice_id].keys()):
                ilines[invoice_id]['Delivery'] += (pos.delivery_amount/1.1)
            else:
                if ('Delivery' not in inv_line_desc) and pos.discount > 0.0:
                    ilines[invoice_id]['Delivery'] = (pos.delivery_amount/1.1)
            if (pos.name.id in warehouses.keys()) and (pos.date not in warehouses[pos.name.id].keys()):
                warehouses[pos.name.id][pos.date] = invoice_id

        if pos.id not in pos_ids:
            pos_ids.append(pos.id)
        if invoice_id:
            if invoice_id not in inv_ids:
                inv_ids.append(invoice_id)
            self.write(cr, uid, [pos.id], {'invoice_ids': [(4, invoice_id)],'invoiced': True,'state': 'progress'}, context=context)

        if inv_ids:
            for inv in inv_ids:
                if (inv in ilines.keys()):
                    invoice_lines = ilines[inv]
                    for desc in invoice_lines.keys():
                        self._create_invoice_line(cr, uid, {'invoice_id': inv, 'name': desc,
                                                            'amount': invoice_lines[desc]}, context=None)
                if inv in discounts.keys():
                    invoice_obj.write(cr, uid, [inv], {'discount_method': 'fixed', 'discount': discounts[inv]}, context=context)
            invoice_obj.button_reset_taxes(cr, uid, inv_ids, context=context)
            invoice_obj.signal_workflow(cr, uid, inv_ids, 'invoice_open')
        return inv_ids

    def _recompute_acc_voucher(self, cr, uid, ids, pid, jid, amt, cid, dt, ttype, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        voucher_obj = self.pool.get('account.voucher')
        res = voucher_obj.basic_onchange_partner(cr, uid, ids, pid, jid,
                    ttype, context=ctx)
        vals = voucher_obj.recompute_voucher_lines(cr, uid, ids, pid, jid, amt,
                    cid, ttype, dt, context=ctx)
        vals2 = voucher_obj.recompute_payment_rate(cr, uid, ids, vals, cid, dt,
                    ttype, jid, amt, context=ctx)
        for key in vals.keys():
            res[key].update(vals[key])
        for key in vals2.keys():
            res[key].update(vals2[key])
        return res['value'] or {}

    def _prepare_voucher_data(self, cr, uid, ids, invoice, data, context=None):
        analytic_id, analytic_disc_id = False, False
        account_obj = self.pool.get('account.account')
        journal_obj = self.pool.get('account.journal')
        analytic_cbg_obj = self.pool.get('analytic.cabang')
        vals = {
            'name': "%s" % (invoice.number or invoice.internal_number,),
            'payment_rate_currency_id': invoice.currency_id.id,
            'partner_id': data['partner_id'] or invoice.partner_id.id,
            'amount': invoice.type in ('out_refund', 'in_refund') and -invoice.residual or invoice.residual,
            'company_id': invoice.company_id and invoice.company_id.id,
            'type': invoice.type in ('out_invoice', 'out_refund') and 'receipt' or 'payment',
            'state': 'draft',
            'pay_now': 'pay_now',
            'period_id': invoice.period_id and invoice.period_id.id or data['periods'],
            'date': invoice.date_invoice or fields.date.context_today,
            'user_id': invoice.user_id and invoice.user_id.id,
            'warehouse_id': invoice.warehouse_id and invoice.warehouse_id.id,
        }
        journal_ids = journal_obj.search(cr, uid, [('code', 'like', '%' + data['journal_code'] + '%'),
                        ('name', 'like', '%' + data['journal_name'] + '%')], limit=1)
        journal = journal_obj.browse(cr, uid, journal_ids and journal_ids[0], context=context)
        if invoice.type in ('out_invoice', 'out_refund') and vals['type'] in ('sale', 'receipt'):
            account_id = journal.default_debit_account_id.id
        else:
            account_ids = []
            if data['line_desc'] in ('Cash','Delivery'):
                account_ids = account_obj.search(cr, uid, ['|', ('code', 'like', '%111111.00.01%'),
                        ('name', 'like', '%Kas Besar Outlet%')], limit=1)
            elif data['line_desc'] == 'Card':
                account_ids = account_obj.search(cr, uid, ['|', ('code', 'like', '%111140.00.02%'),
                        ('name', 'like', '%Tagihan Kartu Kredit dan Debit%')], limit=1)
            elif data['line_desc'] == 'Prepaid':
                account_ids = account_obj.search(cr, uid, ['|', ('code', 'like', '%111211.00.08%'),
                        ('name', 'like', '%Piutang Antar Kantor%')], limit=1)
            elif data['line_desc'] == 'Diskon':
                account_ids = account_obj.search(cr, uid, ['|', ('code', 'like', '%240000.00.07%'),
                        ('name', 'like', '%Potongan Penjualan%')], limit=1)
            # elif data['line_desc'] == 'Delivery':
            #     account_ids = account_obj.search(cr, uid, ['|', ('code', 'like', '%111111.00.03%'),
            #             ('name', 'like', '%Kas Besar Delivery%')], limit=1)
            account_id = account_ids and account_ids[0] or False

        analytic_cbg_ids = analytic_cbg_obj.search(cr, uid, [('account_id', '=', account_id),
                        ('name', '=', invoice.warehouse_id.id)], limit=1)
        if analytic_cbg_ids:
            analytic_id = analytic_cbg_obj.browse(cr, uid, analytic_cbg_ids[0]).analytic_id.id
        vals.update({
            'reference': _("%s, %s, %s") % (
                invoice.warehouse_id and invoice.warehouse_id.outlet, str(invoice.date_invoice), data['line_desc']),
            'journal_id': journal_ids and journal_ids[0] or journal.id or False,
            'account_id': account_id,
            'analytic_id': analytic_id,
            'currency_id': journal.currency and journal.currency.id or invoice.currency_id.id,
        })
        return vals

    def _autocreate_voucher_lines(self, cr, uid, ids, line_ids, voucher_id, context=None):
        for mv_line in self.pool.get('account.move.line').browse(cr, uid, line_ids, context=context):
            self.pool.get('account.voucher.line').create(cr, uid, {
                'voucher_id': voucher_id,
                'name': "%s" % _(mv_line.name or mv_line.move_id.ref),
                'account_id': mv_line.account_id and mv_line.account_id.id,
                'reconcile': True,
                'move_line_id': mv_line.id,
                'type': 'cr',
                'account_analytic_id': mv_line.analytic_account_id and mv_line.analytic_account_id.id,
                'currency_id': mv_line.currency_id.id,
            }, context=context)
        return True

    def _create_voucher_and_move(self, cr, uid, inv_ids, context=None): #auto-create voucher dr invoice data pos
        if context is None:
            context = {}
        partner_obj = self.pool.get('res.partner')
        voucher_obj = self.pool.get('account.voucher')
        invoice_obj = self.pool.get('account.invoice')
        acc_move_obj = self.pool.get('account.move')
        obj_move_line = self.pool.get('account.move.line')
        ctx = context.copy()

        voucher_ids, all_move_lines, move_receivable_lines = [], [], []
        for inv in invoice_obj.browse(cr, uid, inv_ids, context=context):
            partner = partner_obj.browse(cr, uid, inv.partner_id and inv.partner_id.id, context=context)
            user = inv.user_id and inv.user_id.id or uid or SUPERUSER_ID
            periods = self.pool.get('account.period').find(cr, user, context=context)
            journal_code = {'Cash': 'BNK1', 'Card': 'CARD', 'Prepaid': 'PREP', 'Diskon': 'Disk', 'Delivery': 'DELV'}
            partner_receivable_id = partner.property_account_receivable and partner.property_account_receivable.id
            ctx.update({
                'invoice_id': inv.id,
                'invoice_type': inv.type,
                'payment_expected_currency': inv.currency_id.id,
                'close_after_process': True, 'default_reference': inv.name,
                'type': inv.type in ('out_invoice', 'out_refund') and 'receipt' or 'payment',
                'default_type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
            })
            if inv.state == 'open':
                move_line_ids = obj_move_line.search(cr, uid, ['&',('move_id', '=', inv.move_id.id),'|',
                        ('account_id', '=', inv.account_id.id),('account_id', '=', partner_receivable_id)]) or []
                if not move_line_ids:
                    raise osv.except_osv(_('Error!'), _('Result:\n %s\n %s\n %s') %
                                         (inv.move_id.name,partner_receivable_id,move_line_ids))
                move_receivable_lines += move_line_ids
                total_pay = 0.0
                for line in inv.invoice_line:
                    if line.name in ('Cash','Card','Prepaid','Delivery'):
                        vals = self._prepare_voucher_data(cr, uid, inv_ids, inv, {
                            'partner_id': partner.id,
                            'periods': periods and periods[0],
                            'journal_code': str(journal_code[str(line.name).strip()]),
                            'journal_name': str(line.name).strip().upper(),
                            'line_desc': _(line.name),
                        }, context=ctx)
                        query = """ SELECT p.cash_amount, p.card_amount, p.prepaid_amount, p.delivery_amount, l.pos_id  
                                    FROM sales_pos_order p, sales_pos_order_invoice_rel l 
                                    WHERE l.invoice_id=%s and p.id = l.pos_id """
                        cr.execute(query, (inv.id,))
                        result = cr.fetchall()
                        if result:
                            for cash, card, prepaid, delivery, pos_id in result:
                                if line.name == 'Cash':
                                    vals.update({'amount': cash})
                                elif line.name == 'Card':
                                    vals.update({'amount': card})
                                elif line.name == 'Prepaid':
                                    vals.update({'amount': prepaid})
                                elif line.name == 'Delivery':
                                    vals.update({'amount': delivery})
                        else:
                            vals.update({'amount': line.price_unit + (line.price_unit * 0.1)})
                        if ('amount' in vals) and vals['amount'] > 0.0:
                            total_pay += vals['amount']
                        voucher_id = voucher_obj.create(cr, user, vals, context=ctx)
                        if voucher_id not in voucher_ids:
                            voucher_ids.append(voucher_id)
                        if voucher_id and move_line_ids:
                            self._autocreate_voucher_lines(cr, user, inv_ids, move_line_ids, voucher_id, context=ctx)
                if (inv.discount and inv.discount > 0.0) or ((not inv.discount) and (inv.amount_total - total_pay)>0.0):
                    discount_vals = self._prepare_voucher_data(cr, uid, inv_ids, inv, {
                        'partner_id': partner.id,
                        'periods': periods and periods[0],
                        'journal_code': _("DISK"),
                        'journal_name': _("Diskon"),
                        'line_desc': _(line.name),
                    }, context=ctx)
                    disc_amount = inv.discount or 0.0
                    if inv.amount_total - (inv.discount + total_pay) > 0.0:
                        disc_amount = inv.discount + (inv.amount_total - (inv.discount + total_pay))
                    discount_vals.update({
                        'amount': disc_amount,
                        'reference': _("%s, %s, %s") % (
                            inv.warehouse_id and inv.warehouse_id.outlet, str(inv.date_invoice), _("Diskon")),
                    })
                    vou_disc_id = voucher_obj.create(cr, user, discount_vals, context=ctx)
                    if vou_disc_id not in voucher_ids:
                        voucher_ids.append(vou_disc_id)
                    if vou_disc_id and move_line_ids:
                        self._autocreate_voucher_lines(cr, user, inv_ids, move_line_ids, vou_disc_id, context=ctx)

        if voucher_ids:
            all_moves = []
            voucher_obj.button_proforma_voucher(cr, user, voucher_ids, context=ctx) #confirm_voucher
            for new_vou in voucher_obj.browse(cr, uid, voucher_ids, context=context):
                if new_vou.move_id:
                    all_moves.append(new_vou.move_id.id)
                    for new_move in acc_move_obj.browse(cr, uid, [new_vou.move_id.id], context=context):
                        for move_line in new_move.line_id:
                            if move_line.id not in all_move_lines:
                                all_move_lines.append(move_line.id)
                            if (move_line.id not in move_receivable_lines) and (move_line.account_id.id==partner_receivable_id or move_line.account_id.id==inv.account_id.id):
                                move_receivable_lines.append(move_line.id)
            obj_move_line.reconcile_partial(cr, uid, move_receivable_lines, 'auto', context=context) #autocreate_reconcile_lines
            if all_moves and len(all_moves) > 0:
                acc_move_obj.post(cr, uid, all_moves, context=context) #post_journal_entry
        return True

    ### Run Scheduler - Data POS to Invoice ###
    def run_scheduler(self, cr, uid, use_new_cursor=False, date_start=False, date_stop=False, warehouse_id=False, context=None):
        if context is None:
            context = {}
        try:
            if use_new_cursor:
                cr = openerp.registry(cr.dbname).cursor()
            # Run confirmed entry Data POS
            cr.execute("SELECT DISTINCT id FROM stock_warehouse WHERE active=true")
            outlet_ids = [int(x[0]) for x in cr.fetchall()] or [1,2,3,4,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,309,300]
            dom = [('invoice_exists','=',False)]  #[('invoiced','=',False)]    #[('name','<>',False)('invoice_ids','=',[]),]
            if date_start and date_stop:
                dom += [('date','>=',date_start),('date','<=',date_stop)]
            else:
                #dom += [('date','>=',time.strftime('%Y-01-01'))]  #'%Y-%m-%d'
                dom += [('date', '>=',time.strftime('%Y-03-01')),('date', '<=',time.strftime('%Y-03-31'))]
            if warehouse_id:
                dom += [('name', '=', warehouse_id)]
            else:
                dom += [('name', 'in', tuple(outlet_ids))]
            prev_ids = []
            while True:
                ids = self.search(cr, SUPERUSER_ID, dom, context=context)
                if not ids or prev_ids == ids:
                    break
                else:
                    prev_ids = ids
                self.run(cr, SUPERUSER_ID, ids, autocommit=use_new_cursor, context=context)
                if use_new_cursor:
                    cr.commit()
        finally:
            if use_new_cursor:
                try:
                    cr.close()
                except Exception:
                    pass
        return {}

    def button_dummy(self, cr, uid, ids, context=None):
        return True

    def run_middleware_scheduler(self, cr, uid, use_new_cursor=False, context=None):
        try:
            if use_new_cursor:
                cr = openerp.registry(cr.dbname).cursor()
                if cr.dbname == 'PB1':
                    con = psycopg2.connect("dbname='PB1' user='odoo' password='Mys3@f00d' host='192.168.19.6'")
                    cr = con.cursor()
                elif cr.dbname == 'LCD':
                    connect = psycopg2.connect("dbname='LCD' user='odoo8' password='Mys3@f00d' host='192.168.19.7'")
                    cr = connect.cursor()                
            
            data_path = '/opt/odoo8/custom/addons_dcost/ib_sale_dcost/data'
            directory = os.path.join(data_path)
            for root, dirs, files in os.walk(directory):
                for file in files: #looping file csv in dir
                    if file.endswith(".csv") and file == 'tblomzet.csv':
                        with open(file, 'r') as f, open(data_path+"/"+"ResultOmzet.csv", 'w') as f1:
                            next(f)
                            for line in f:
                                f1.write(line)
                                
                        with open(data_path+"/"+"ResultOmzet.csv") as f2:
                            # same as readlines() but removes the \n from each line
                            data = f2.read().splitlines()
                            
                        new_data = data[1:-1]  # remove the first and last line

                        with open(data_path+"/"+"ResultOmzet.csv", 'w') as f3:
                            #add a newline character between every line, then write
                            f3.write('\n'.join(new_data))
            
            extension = 'csv'
            os.chdir(data_path)
            csv_files = [i for i in glob.glob('*.{}'.format(extension))]
            if 'ResultOmzet.csv' in csv_files: #check csv file availability [ResultOmzet.csv]
                cr.execute("SELECT outlet_id FROM stock_warehouse WHERE active=true")
                OUTLET_CODE = [x[0] for x in cr.fetchall()]  #filter Outlet utk 2 company (PEBO & LCD)
                QUERY = """SELECT id FROM sales_pos_order 
                            WHERE source_id=CAST(%s AS numeric) OR 
                            (branch_id=CAST(%s AS int) AND date=to_date(%s, 'DD-MM-YYYY'))"""
                with open("ResultOmzet.csv") as csvfile:
                    rows = csv.reader(csvfile, delimiter=';')
                    for row in rows:
                        params = (row[0], row[1], row[2])
                        cr.execute(QUERY, params)  # check existing data
                        results = cr.fetchone()  # cek data di db pebo
                        if len(row) > 1:  #len(row)>3 #cek kolom setiap line di csv file
                            order_date = datetime.strptime(row[2], "%d-%m-%Y")
                            outlet_name = str("DCOST ") + str(row[3]).strip().title()                            
                            if row[3] == 'BnB Hotel':
                                outlet_name = str("DCOST ") + str(row[3]).strip().upper()
                            elif row[3] == 'Blok M':
                                outlet_name = str("DCOST Blok M Square")
                            elif rows[3] == 'Armada Town Square':
                                namacabang = str("DCOST Magelang")
                            elif row[3] == 'AirPort':
                                outlet_name = str("DCOST Airport Hub")
                            elif row[3][0:4] == 'DVIP':
                                outlet_name = str(row[3])
                                if row[3] == 'DVIP BALI' or str(row[3][4:]).strip() == 'BALI':
                                    outlet_name = str("DVIP Bali Sunset Road")
                            elif row[3][0:3] in ('SGC', 'PGC', 'ITC', 'BSD'):
                                outlet_name = str("DCOST ") + str(row[3]).strip()
                            grand_total = float(row[4]) + float(row[5]) + float(row[7]) + float(row[8]) + float(row[9])
                            if row[3][0:4] == 'DVIP':
                                grand_total = float(row[4]) + float(row[5]) + float(row[7]) + float(row[8]) - float(row[9])
                            if (row[1] in OUTLET_CODE) and (not results):
                                cr.execute('''SELECT id FROM stock_warehouse WHERE (outlet_id like %s OR outlet like %s) 
                                            AND active=TRUE LIMIT 1''', (row[1], str("%DCOST ") + str(row[3]) + str("%")))
                                warehouse_id = cr.fetchone() and cr.fetchone()[0] or False
                                cr.execute("""INSERT INTO sales_pos_order(date, prepaid_amount, cash_amount, discount, card_amount, 
                                        delivery_amount, branch_id, branch_name, amount_tax, src_mysql_id, state, name, create_uid,
                                        voucher, subtotal, invoiced, create_date, write_date) VALUES(%s, %s, %s, %s, %s, %s, %s, 
                                            %s, %s, %s, %s, %s, %s, %s, %s, False, current_date, current_date)""", 
                                        (order_date, row[7], row[4], row[9], row[5], row[8], row[1], outlet_name, row[10],
                                            row[0], 'draft', warehouse_id, 1, row[6], grand_total))
                                cr.commit()
                            else:
                                if int(float(row[2][3:5])) > 1 and results:
                                    cr.execute("""UPDATE sales_pos_order SET delivery_amount=%s, voucher=%s, src_mysql_id=%s, 
                                            subtotal=%s, cash_amount=%s, card_amount=%s, discount=%s, prepaid_amount=%s,
                                            write_date=current_date WHERE id=%s""", (row[8], row[6], row[0], grand_total, 
                                                                                     row[4], row[5], row[9], row[7], results[0]))
                                cr.commit()
            # cr.close()
        finally:
            if use_new_cursor:
                try:
                    cr.close()
                except Exception:
                    pass
        return {}


class mrp_pos_order(osv.osv):
    _name = "mrp.pos.order"
    _description = "POS DCost for Manufacturing"
    _columns = {
        'date': fields.date('Date', select=True),
        'name': fields.char('Menu Name', size=512),
        'menu': fields.char('Menu ID', required=True),
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', required=True),
        'qty': fields.float('Quantity', digits_compute=dp.get_precision('Stock Weight'),
                            help="Jumlah porsi pemesanan."),
        'unit_price': fields.float('Unit Price', digits_compute=dp.get_precision('Product Price')),
        'amount_total': fields.float('Total', digits_compute=dp.get_precision('Account')),
        'branch_id': fields.integer('Branch ID'),
        'branch_name': fields.char('Branch Name'),
        'src_mysql_id': fields.char('Source Database ID', help="ID Database MySQL POS"),
        'work_order': fields.boolean('Work Order', help="This indicates that the POS data has at least one work order."),
        'work_order_id': fields.many2one('work.order', 'Work Order'),
    }
    _defaults = {
        'qty': lambda *a: 0.0,
        'unit_price': lambda *a: 0.0,
        'amount_total': lambda *a: 0.0,
        'work_order': False,
    }

    def run(self, cr, uid, ids, autocommit=False, context=None):
        failed1, failed2, failed3 = 0, 0, 0
        failed_product = _("Failed process from Order Menu:")
        failed_outlet = _("Failed process from Outlet ID:")
        failed_order_qty = _("Failed from order quantity:")
        for mrp_id in ids:
            mrp_pos = self.browse(cr, uid, mrp_id, context=context)
            # rewrite UID ke user outlet not uid admin
            user = self.pool.get('res.users').search(cr, uid, [('login', 'like', 'adm%'), ('login', 'not like', '%admin%'),
                ('warehouse_id', '=', mrp_pos.warehouse_id and mrp_pos.warehouse_id.id)], limit=1, order='login')[0] or SUPERUSER_ID
            product_tmpl_ids = self.pool.get('product.template').search(cr, uid,
                                [('name', 'like', '%' + str(mrp_pos.name).strip() + '%'),
                                    ('menu_id', '=', str(mrp_pos.menu).strip())], context=context) or False
            if not product_tmpl_ids:
                failed_product += "\n %s, %s, %s, %s" % (mrp_pos.src_mysql_id, mrp_pos.date,
                        _("["+ str(mrp_pos.branch_id) +"] "+mrp_pos.branch_name),_("["+mrp_pos.menu+"] "+mrp_pos.name))
                failed1 += 1
                #continue
            elif (not mrp_pos.warehouse_id):
                failed_outlet += "\n %s, %s, %s" % (mrp_pos.src_mysql_id, mrp_pos.date,
                                    _("[" + str(mrp_pos.branch_id) + "] " + mrp_pos.branch_name))
                failed2 += 1
            elif (mrp_pos.qty <= 0.0):
                failed_order_qty += "\n %s, %s, %s, %s" % (mrp_pos.src_mysql_id, mrp_pos.date,
                                    _("[" + str(mrp_pos.branch_id) + "] " + mrp_pos.branch_name),_(mrp_pos.qty))
                failed3 += 1
            else:
                if (not mrp_pos.work_order_id) or (not mrp_pos.work_order): #Create new work_order
                    if (not mrp_pos.name or not mrp_pos.menu) and (not mrp_pos.qty) and (not mrp_pos.unit_price):
                        raise except_orm(_('Peringatan !!!'),
                            _("Ada salah satu Data Menu Ranking tidak mempunyai menu-ID/menu-Name/qty/price (null). "
                              "Silahkan cek kembali..."))
                    else:
                        if (not mrp_pos.date):
                            raise except_orm(_('Peringatan !!!'),
                                             _("Ada salah satu Data Menu Ranking tidak mempunyai tanggal transaksi. "
                                               "Silahkan cek kembali..."))
                        if (not mrp_pos.work_order):
                            try:
                                work_order_id = self._run(cr, user, mrp_pos, False, context=context or {})
                                if work_order_id:
                                    self.write(cr, user, [mrp_pos.id], {'work_order': True}, context=context)
                            except OperationalError:
                                if autocommit:
                                    cr.rollback()
                                    continue
                                else:
                                    raise
                else:
                    if mrp_pos.work_order_id and mrp_pos.work_order_id.state=='draft':
                        work_order_id = self._run(cr, user, mrp_pos, mrp_pos.work_order_id.id, context=context or {})
                    else:
                        work_order_id = self._run(cr, user, mrp_pos, False, context=context or {})
                    if work_order_id:
                        self.write(cr, user, [mrp_pos.id], {'work_order': True}, context=context)

        if failed1 > 0 or failed2 > 0 or failed3 > 0:
            summary = '''Here is the duplication report:'''
            if failed1 > 0:
                summary += "\n%s" % failed_product
            if failed2 > 0:
                summary += "\n%s" % failed_outlet
            if failed3 > 0:
                summary += "\n%s" % failed_order_qty
            self.pool.get('log.history.menu').create(cr, uid, {
                'name': "Menu Ranking Logs ["+ _(time.strftime('%d-%m-%Y')) +"]",
                'date': time.strftime('%Y-%m-%d, %H:%M:%S'),
                'body': summary,
            }, context=context)
        return True

    def _autocreate_finished_product(self, cr, uid, ids, vals, loc_prod_id, tags, context=None):
        obj_picking = self.pool.get('stock.picking')
        product = self.pool.get('product.product').browse(cr, uid, vals['product_id'])
        warehouse = self.pool.get('stock.warehouse').browse(cr, uid, vals['warehouse_id'])
        loc_vendor_ids = self.pool.get('stock.location').search(cr, uid, [('active', '=', True),
                            ('usage', '=', 'supplier')], context=context)
        picking_id = obj_picking.create(cr, uid, {
            'origin': "%s" % _(vals['name'] or ""),
            'work_id': ids[0],
            'type_wo': 'fg',
            'picking_type_id': warehouse.int_type_id.id,
            'move_type': 'one',
            'date': vals['date'],
            'note': "%s" % _(vals['note'] or ""),
            'company_id': warehouse.company_id.id,
        })
        move_vals = { # create_MOVE_from_stock_to_production
            'date': vals['date'],
            'product_id': vals['product_id'] or product.id or False,
            'product_uom_qty': vals['product_qty'] or 0.0,
            'product_uom': vals['product_uom'],
            'state': 'draft',
            'origin': "%s" % _(vals['name'] or vals['note']),
            'warehouse_id': vals['warehouse_id'] or False,
            'picking_id': picking_id,
            'location_id': loc_prod_id,
            'location_dest_id': vals['location_src_id'] or False,
            'price_unit': product.product_tmpl_id.standard_price,
            'name': product.product_tmpl_id.name or product.name_template or _("Autocreate from WO - POS"),
        }
        if tags=='semifg':
            move_vals.update({
                'location_id': loc_vendor_ids and loc_vendor_ids[0] or 8,
                'location_dest_id': vals['location_src_id'] or False,
            })
        stock_move_id = self.pool.get('stock.move').create(cr, uid, move_vals, context=context)
        if picking_id and stock_move_id:
            obj_picking.action_confirm(cr, uid, [picking_id], context=context)
            obj_picking.force_assign(cr, uid, [picking_id], context=context)
            obj_picking.action_done(cr, uid, [picking_id], context=context)
            self.pool.get('work.order').write(cr, uid, ids, {'move_ids': [(4, stock_move_id)]}, context=context)
        return True

    def _run(self, cr, uid, mrp, wo_id, context=None):
        context = context or {}
        work_order_obj = self.pool.get('work.order')
        product_obj = self.pool.get('product.product')
        product_tmpl_obj = self.pool.get('product.template')
        location_obj = self.pool.get('stock.location')
        warehouse_obj = self.pool.get('stock.warehouse')
        raw_matl_obj = self.pool.get('raw.material.line')
        bom_obj = self.pool.get('mrp.bom')
        work_order_id = False
        product_tmpl_ids = product_tmpl_obj.search(cr, uid, [('name', 'like', '%' + str(mrp.name).strip() + '%'),
                        ('menu_id', '=', str(mrp.menu).strip())], context=context) or False
        product_ids = product_obj.search(cr, uid, [('product_tmpl_id', '=', product_tmpl_ids and product_tmpl_ids[0])], context=context) or False
        if not product_ids:
            raise osv.except_osv(_('Error!'), _('Product ID is Empty. \n%s\n%s') % (mrp.name,mrp.menu))
        if not mrp.warehouse_id:
            raise osv.except_osv(_('Error!'), _('Outlet/WH ID is Empty.'))

        product = product_obj.browse(cr, uid, product_ids and product_ids[0], context=context)
        outlet = warehouse_obj.browse(cr, uid, mrp.warehouse_id.id, context=context)
        bom_id = bom_obj._bom_find(cr, uid, product_tmpl_id=product.product_tmpl_id.id, product_id=product.id,
                                       properties=[], context=None) or False
        data_bom = bom_obj.browse(cr, uid, bom_id)
        location_fg_ids = location_obj.search(cr, uid, [('active','=',True),('usage','=','customer')], context=context)
        if not wo_id: #create new WO
            if product_ids:
                wo_vals = {
                    'date': mrp.date,
                    'product_id': product.id or False,
                    'product_qty': mrp.qty or 0.0,
                    'product_uom': product.product_tmpl_id and product.product_tmpl_id.uom_id
                                   and product.product_tmpl_id.uom_id.id,
                    'bom_id': bom_id,
                    'state': 'draft',
                    # 'material_lines': material,
                    'note': str(mrp.date) +", "+ str(mrp.warehouse_id.outlet) +", "+ str(mrp.src_mysql_id),
                    'warehouse_id': mrp.warehouse_id and mrp.warehouse_id.id or False,
                    'location_src_id': outlet.lot_stock_id and outlet.lot_stock_id.id,
                    'location_dest_id': location_fg_ids and location_fg_ids[0],
                }
                work_order_id = work_order_obj.create(cr, uid, wo_vals, context=context)
                if work_order_id and bom_id:
                    loc_production_ids = location_obj.search(cr, uid, [
                        ('usage','=','production'),('active','=',True)], context=context)
                    loc_production_id = loc_production_ids and loc_production_ids[0] or 7
                    for bom_line in data_bom.bom_line_ids:
                        raw_matl_obj.create(cr, uid, {
                            'work_id': work_order_id,
                            'product_id': bom_line.product_id and bom_line.product_id.id,
                            'product_qty': (bom_line.product_qty * mrp.qty) or 0.0,
                            'product_uom': bom_line.product_uom and bom_line.product_uom.id,
                            'location_src_id': outlet.lot_stock_id and outlet.lot_stock_id.id,
                            'location_dest_id': loc_production_id,
                            # 'location_dest_id': location_fg_ids and location_fg_ids[0],
                        }, context=context)
                    self._autocreate_finished_product(cr, uid, [work_order_id], wo_vals, loc_production_id, 'fg', context=context)
        else:
            work_order_id = wo_id
            work_orders = work_order_obj.read(cr, uid, [wo_id], ['state', 'warehouse_id'], context=context)
            for wo in work_orders:
                if wo['state']=='done':
                    raise osv.except_osv(_('Error!'), _('Work Order status is Done, and can not be edited.'))
                elif wo['state']=='draft':
                    work_order_obj.write(cr, uid, {
                        'date': mrp.date,
                        'product_id': product.id or False,
                        'product_qty': mrp.qty or 0.0,
                        'product_uom': product.product_tmpl_id and product.product_tmpl_id.uom_id and \
                                       product.product_tmpl_id.uom_id.id,
                        'warehouse_id': mrp.warehouse_id and mrp.warehouse_id.id,
                        'bom_id': bom_id,
                    }, context=context)

        if work_order_id:
            work_order_obj.work_validate(cr, uid, [work_order_id], context=context)
            self.write(cr, uid, [mrp.id], {'work_order': True, 'work_order_id': work_order_id}, context=context)
        return work_order_id

    ### Run Scheduler - Data POS menu ranking to Work Order ###
    def run_scheduler(self, cr, uid, use_new_cursor=False, date_start=False, date_stop=False, warehouse_id=False, context=None):
        if context is None:
            context = {}
        try:
            if use_new_cursor:
                cr = openerp.registry(cr.dbname).cursor()
            # Run confirmed entry Data POS menu ranking
            outlet_ids = [1, 2, 3, 4, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144]
            cr.execute("SELECT DISTINCT id FROM stock_warehouse WHERE active=true")
            res_outlet = cr.fetchall()
            if res_outlet:
                outlet_ids = [int(x[0]) for x in res_outlet]
            cr.execute("SELECT DISTINCT menu_id FROM product_template WHERE active=true and menu_id is not null")
            res_products = cr.fetchall()
            menu_ids = [y[0] for y in res_products] or []
            dom = [('work_order','=',False),('menu','in',tuple(menu_ids))]
            if date_start and date_stop:
                dom += [('date','>=',date_start),('date','<=',date_stop)]
            else:
                dom += [('date','>=',time.strftime('%Y-01-01'))]  #'%Y-%m-%d'
                #dom += [('date', '>=',time.strftime('%Y-01-01')),('date', '<=',time.strftime('%Y-10-31'))]
            if warehouse_id:
                dom += [('warehouse_id', '=', warehouse_id)]
            else:
                dom += [('warehouse_id','in',tuple(outlet_ids))]
            prev_ids = []
            while True:
                ids = self.search(cr, SUPERUSER_ID, dom, context=context)
                if not ids or prev_ids == ids:
                    break
                else:
                    prev_ids = ids
                self.run(cr, SUPERUSER_ID, ids, autocommit=use_new_cursor, context=context)
                if use_new_cursor:
                    cr.commit()
        finally:
            if use_new_cursor:
                try:
                    cr.close()
                except Exception:
                    pass
        return {}


class work_order(osv.osv):
    _inherit = "work.order"
    _order = 'date desc, name desc'
    _columns = {
        'parent_id': fields.many2one('work.order', 'Parent [WO]', ondelete='cascade'),
        'child_ids': fields.many2many('work.order', 'work_order_child_wo_rel', 'work_order_id', 'child_id',
                                        'Related Work Orders', readonly=True, copy=False),
        'move_ids': fields.many2many('stock.move', 'work_order_stock_move_rel', 'work_order_id', 'move_id',
                                      'Consumed Products', readonly=True, copy=False),
        'product_qty': fields.float('Quantity', digits_compute=dp.get_precision('Menu Ranking POS'), required=True,
                                    readonly=True, states={'draft': [('readonly', False)]}), #Product Unit of Measure
    }

    def work_validate(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids, context={})[0]

        obj_move = self.pool.get('stock.move')
        obj_picking = self.pool.get('stock.picking')

        material_id = obj_picking.create(cr, uid, {
            'origin': val.name,
            'work_id': val.id,
            'type_wo': 'rm',
            'picking_type_id': val.warehouse_id.int_type_id.id,
            'move_type': 'one',
            'date': val.date,
            'note': val.note or "",
            'company_id': val.warehouse_id.company_id.id,
        })
        goods_id, idg = False, False
        if (not val.parent_id):
            goods_id = obj_picking.create(cr, uid, {
                'origin': val.name,
                'work_id': val.id,
                'type_wo': 'fg',
                'picking_type_id': val.warehouse_id.int_type_id.id,
                'move_type': 'one',
                'date': val.date,
                'note': val.note or "",
                'company_id': val.warehouse_id.company_id.id,
            })

        picking_list = [material_id]
        if (not val.parent_id) and goods_id:
            picking_list.append(goods_id)
            idg = obj_move.create(cr, uid, {
                'name': val.product_id.partner_ref,
                'date': val.date,
                'product_id': val.product_id.id,
                'product_uom': val.product_uom.id,
                'product_uom_qty': val.product_qty,
                'location_id': val.location_src_id and val.location_src_id.id or 7,
                'location_dest_id': val.location_dest_id and val.location_dest_id.id or val.warehouse_id.lot_stock_id.id,
                'picking_id': goods_id,
                'origin': val.name or val.note,
                'warehouse_id': val.warehouse_id.id,
                'company_id': val.warehouse_id.company_id.id})

        sum = []
        for x in val.material_lines:
            sum.append(obj_move.create(cr, uid, {
                'name': x.product_id.partner_ref,
                'date': val.date,
                'product_id': x.product_id.id,
                'product_uom': x.product_uom.id,
                'product_uom_qty': x.product_qty,
                'location_id': x.location_src_id and x.location_src_id.id or val.warehouse_id.lot_stock_id.id,
                'location_dest_id': x.location_dest_id and x.location_dest_id.id or 7,
                'picking_id': material_id,
                'origin': val.name or val.note,
                'warehouse_id': val.warehouse_id.id,
                'company_id': val.warehouse_id.company_id.id}))

        obj_picking.action_confirm(cr, uid, picking_list, context=context)
        obj_picking.force_assign(cr, uid, picking_list, context=context)
        obj_picking.action_done(cr, uid, picking_list, context=context)

        cost = 0.0
        for s in obj_move.browse(cr, uid, sum):
            harga = s.price_unit
            if not harga:
                harga = 0
            cost += harga * s.product_uom_qty

        if val.product_qty > 0.0 and (not val.parent_id):
            obj_move.write(cr, uid, [idg], {'price_unit': float(cost) / float(val.product_qty)})
            for o in obj_move.browse(cr, uid, idg).quant_ids:
                self.pool.get('stock.quant').write(cr, uid, [o.id], {'cost': cost / val.product_qty})

        self.write(cr, uid, ids, {'state': 'done'})
        return True


class MaterialConsumptionWO(osv.osv):
    _inherit = "raw.material.line"

    _columns = {
        'product_qty': fields.float('Quantity', digits_compute=dp.get_precision('Menu Ranking POS'),
                                    required=True),
    }

    def create(self, cr, uid, values, context=None):
        bom_obj = self.pool.get('mrp.bom')
        work_order_obj = self.pool.get('work.order')
        product_obj = self.pool.get('product.product')
        location_obj = self.pool.get('stock.location')
        product_id = values.get('product_id', False)
        parent_id = values.get('work_id', False)
        result = super(MaterialConsumptionWO, self).create(cr, uid, values, context=context)

        if product_id and parent_id:
            loc_production_ids = location_obj.search(cr, uid, [('usage','=','production'),('active','=',True)], context=context)
            product = product_obj.browse(cr, uid, product_id)
            bom_id = bom_obj._bom_find(cr, uid, product_tmpl_id=product.product_tmpl_id.id, product_id=product.id,
                                       properties=[], context=None) or False
            parent = work_order_obj.browse(cr, uid, parent_id)
            if bom_id:
                bom = bom_obj.browse(cr, uid, bom_id)
                wo_child_vals = {
                    'date': parent.date,
                    'product_id': product.id or product_id or False,
                    'product_qty': values.get('product_qty') or 0.0,
                    'product_uom': product.product_tmpl_id and product.product_tmpl_id.uom_id
                                   and product.product_tmpl_id.uom_id.id,
                    'bom_id': bom_id,
                    'state': 'draft',
                    'note': parent.note or _(''),
                    'warehouse_id': parent.warehouse_id and parent.warehouse_id.id or False,
                    'location_src_id': parent.warehouse_id.lot_stock_id and parent.warehouse_id.lot_stock_id.id
                                       or parent.location_src_id.id,
                    'location_dest_id': loc_production_ids and loc_production_ids[0] or 7,
                    #parent.warehouse_id.lot_stock_id and parent.warehouse_id.lot_stock_id.id,
                    'parent_id': parent.id or parent_id or False,
                }
                wo_child_id = work_order_obj.create(cr, uid, wo_child_vals, context=context)
                if wo_child_id:
                    if bom.bom_line_ids:
                        for boml in bom.bom_line_ids:
                            self.create(cr, uid, {
                                'work_id': wo_child_id,
                                'product_id': boml.product_id and boml.product_id.id,
                                'product_qty': (boml.product_qty * values.get('product_qty')) or 0.0,
                                'product_uom': boml.product_uom and boml.product_uom.id,
                                'location_src_id': parent.warehouse_id.lot_stock_id and parent.warehouse_id.lot_stock_id.id
                                                   or parent.location_src_id.id,
                                'location_dest_id': loc_production_ids and loc_production_ids[0] or 7,
                            }, context=context)
                    # self.pool.get('mrp.pos.order')._autocreate_finished_product(cr, uid, [wo_child_id], wo_child_vals,
                    #         loc_production_ids and loc_production_ids[0] or 7, 'semifg', context=context)
                    work_order_obj.work_validate(cr, uid, [wo_child_id], context=context)
                    work_order_obj.write(cr, uid, [parent.id], {'child_ids': [(4, wo_child_id)]},context=context)
        return result

# class pos_x_manufacturing(osv.osv):
#     _name = "x.manufacturing"
#     _table = "x_manufacturing"
#     _columns = {
#         'work_order': fields.boolean('Work Order', help="This indicates that the POS data has at least one work order."),
#         'work_order_id': fields.many2one('work.order', 'Work Order'),
#     }
#     _defaults = {
#         'work_order' : True,
#     }

# class pos_x_omzet(osv.osv):
#     _name = "x.omzet"
#     _table = "x_omzet"
#     _columns = {
#         'ordered': fields.boolean('Sales Order', help="This indicates that the POS data has at least one sales order."),
#         'sale_id': fields.many2one('sale.order', 'Sales Order'),
#     }
#     _defaults = {
#         'ordered': True,
#     }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
