##-*- coding: utf-8 -*-
##########   IBRAHIM BILADIN | ibradiiin@gmail.com | 083871909782   ##########
import time
import openerp
from datetime import datetime
from psycopg2 import OperationalError
import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv, expression
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import SUPERUSER_ID, api
import logging

_logger = logging.getLogger(__name__)

class stock_move(osv.osv):
    _inherit = "stock.move"

    _columns = {
        'entries': fields.boolean('Entries'), #states={'done': [('readonly', True)]}
        'related_journal': fields.char('Journal Entry'), #states={'done': [('readonly', True)]}
        'price_unit': fields.float('Unit Price', states={'done': [('readonly', True)]},  
                                   help="Technical field used to record the product cost set by the user during a picking confirmation (when costing method used is 'average price' or 'real'). Value given in company currency and in product uom."),
        'origin': fields.char("Source", states={'done': [('readonly', True)]}),
    }
    _defaults = {
        'entries': False,
    }

    def get_price_unit(self, cr, uid, move, context=None):
        if move.purchase_line_id:
            return move.purchase_line_id.price_unit or move.price_unit

        return super(stock_move, self).get_price_unit(cr, uid, move, context=context)
    
    def action_cancel(self, cr, uid, ids, context=None):
        procurement_obj = self.pool.get('procurement.order')
        context = context or {}
        procs_to_check = set()
        for move in self.browse(cr, uid, ids, context=context):
            if move.state == 'done' and (move.location_id.usage<>"inventory" and move.location_dest_id.usage<>"inventory"):
                raise osv.except_osv(_('Operation Forbidden!'),
                        _('You cannot cancel a stock move that has been set to \'Done\'.'))
            if move.reserved_quant_ids:
                self.pool.get("stock.quant").quants_unreserve(cr, uid, move, context=context)
            if context.get('cancel_procurement'):
                if move.propagate:
                    procurement_ids = procurement_obj.search(cr, uid, [('move_dest_id', '=', move.id)], context=context)
                    procurement_obj.cancel(cr, uid, procurement_ids, context=context)
            else:
                if move.move_dest_id:
                    if move.propagate:
                        self.action_cancel(cr, uid, [move.move_dest_id.id], context=context)
                    elif move.move_dest_id.state == 'waiting':
                        #If waiting, the chain will be broken and we are not sure if we can still wait for it (=> could take from stock instead)
                        self.write(cr, uid, [move.move_dest_id.id], {'state': 'confirmed'}, context=context)
                if move.procurement_id:
                    # Does the same as procurement check, only eliminating a refresh
                    procs_to_check.add(move.procurement_id.id)

        res = self.write(cr, uid, ids, {'state': 'cancel', 'move_dest_id': False}, context=context)
        if procs_to_check:
            procurement_obj.check(cr, uid, list(procs_to_check), context=context)
        return res

    def action_done(self, cr, uid, ids, context=None):
        context = context or {}
        picking_obj = self.pool.get("stock.picking")
        quant_obj = self.pool.get("stock.quant")
        inv_lines_obj = self.pool.get('stock.inventory.line')
        todo = [move.id for move in self.browse(cr, uid, ids, context=context) if move.state == "draft"]
        if todo:
            ids = self.action_confirm(cr, uid, todo, context=context)
        pickings = set()
        procurement_ids = set()
        # Search operations that are linked to the moves
        operations = set()
        move_qty = {}
        for move in self.browse(cr, uid, ids, context=context):
            move_qty[move.id] = move.product_qty
            for link in move.linked_move_operation_ids:
                operations.add(link.operation_id)

        # Sort operations according to entire packages first, then package + lot, package only, lot only
        operations = list(operations)
        operations.sort(key=lambda x: ((x.package_id and not x.product_id) and -4 or 0) + (x.package_id and -2 or 0) + (
        x.lot_id and -1 or 0))
        #update sebelum done
        self.product_price_update_before_done(cr, uid, ids, context=context)
        for ops in operations:
            if ops.picking_id:
                pickings.add(ops.picking_id.id)
            main_domain = [('qty', '>', 0)]
            for record in ops.linked_move_operation_ids:
                move = record.move_id
                self.check_tracking(cr, uid, move, not ops.product_id and ops.package_id.id or ops.lot_id.id, context=context)
                prefered_domain = [('reservation_id', '=', move.id)]
                fallback_domain = [('reservation_id', '=', False)]
                fallback_domain2 = ['&', ('reservation_id', '!=', move.id), ('reservation_id', '!=', False)]
                prefered_domain_list = [prefered_domain] + [fallback_domain] + [fallback_domain2]
                dom = main_domain + self.pool.get('stock.move.operation.link').get_specific_domain(cr, uid, record, context=context)
                quants = quant_obj.quants_get_prefered_domain(cr, uid, ops.location_id, move.product_id, record.qty, domain=dom, 
                                                              prefered_domain_list=prefered_domain_list,
                                                              restrict_lot_id=move.restrict_lot_id.id,
                                                              restrict_partner_id=move.restrict_partner_id.id, context=context)
                if ops.product_id:
                    quant_dest_package_id  = ops.result_package_id.id
                    ctx = context
                else:
                    quant_dest_package_id = False
                    ctx = context.copy()
                    ctx['entire_pack'] = True
                quant_obj.quants_move(cr, uid, quants, move, ops.location_dest_id, location_from=ops.location_id, lot_id=ops.lot_id.id, owner_id=ops.owner_id.id, src_package_id=ops.package_id.id, dest_package_id=quant_dest_package_id, context=ctx)

                # Handle pack in pack
                if not ops.product_id and ops.package_id and ops.result_package_id.id != ops.package_id.parent_id.id:
                    self.pool.get('stock.quant.package').write(cr, SUPERUSER_ID, [ops.package_id.id], {'parent_id': ops.result_package_id.id}, context=context)
                if not move_qty.get(move.id):
                    raise osv.except_osv(_("Error"), _("The roundings of your Unit of Measures %s on the move vs. %s on the product don't allow to do these operations or you are not transferring the picking at once. ") % (move.product_uom.name, move.product_id.uom_id.name))
                move_qty[move.id] -= record.qty

        move_dest_ids = set()
        update_vals = {}
        for move in self.browse(cr, uid, ids, context=context):
            move_qty_cmp = float_compare(move_qty[move.id], 0, precision_rounding=move.product_id.uom_id.rounding)
            if move_qty_cmp > 0:  # (=In case no pack operations in picking)
                main_domain = [('qty', '>', 0)]
                prefered_domain = [('reservation_id', '=', move.id)]
                fallback_domain = [('reservation_id', '=', False)]
                fallback_domain2 = ['&', ('reservation_id', '!=', move.id), ('reservation_id', '!=', False)]
                prefered_domain_list = [prefered_domain] + [fallback_domain] + [fallback_domain2]
                self.check_tracking(cr, uid, move, move.restrict_lot_id.id, context=context)
                qty = move_qty[move.id]
                quants = quant_obj.quants_get_prefered_domain(cr, uid, move.location_id, move.product_id, qty, domain=main_domain, prefered_domain_list=prefered_domain_list, restrict_lot_id=move.restrict_lot_id.id, restrict_partner_id=move.restrict_partner_id.id, context=context)
                quant_obj.quants_move(cr, uid, quants, move, move.location_dest_id, lot_id=move.restrict_lot_id.id, owner_id=move.restrict_partner_id.id, context=context)

            # If the move has a destination, add it to the list to reserve
            if move.move_dest_id and move.move_dest_id.state in ('waiting', 'confirmed'):
                move_dest_ids.add(move.move_dest_id.id)

            if move.procurement_id:
                procurement_ids.add(move.procurement_id.id)

            if move.inventory_id:
                inv_line_ids = inv_lines_obj.search(cr, uid, [('inventory_id','=',move.inventory_id.id),
                                                              ('product_id','=',move.product_id.id)], context=context)
                inv_line = inv_lines_obj.browse(cr, uid, inv_line_ids, context=context)[0]
                update_vals.update({'date': move.inventory_id.date, 'date_expected': move.inventory_id.date})
                price_unit = inv_line_ids and inv_line.inventory_value or self.get_price_unit(cr, uid, move, context=context)
                #STOCK OPNAME base on LAST PRICELIST - batal
                # if (price_unit==0.0 or (not price_unit)) and move.product_id and move.product_uom and move.inventory_id.date:
                #     price_unit = self._get_move_price_from_pricelist(cr, uid, move, context=context)
                self.write(cr, uid, [move.id], {'price_unit': price_unit}, context=context)
            if move.picking_id and move.picking_id.work_id and move.picking_id.type_wo=='rm': #stock_move price base on pricelist
                price_unit = self._get_move_price_from_pricelist(cr, uid, move, context=context)
                self.write(cr, uid, [move.id], {'price_unit': price_unit}, context=context)

            #unreserve the quants and make them available for other operations/moves
            quant_obj.quants_unreserve(cr, uid, move, context=context)

        # Check the packages have been placed in the correct locations
        self._check_package_from_moves(cr, uid, ids, context=context)
        # set the move as done
        update_vals.update({'state': 'done'})
        self.write(cr, uid, ids, update_vals, context=context)
        ##'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)}, context=context)
        self.pool.get('procurement.order').check(cr, uid, list(procurement_ids), context=context)
        # assign destination moves
        if move_dest_ids:
            self.action_assign(cr, uid, list(move_dest_ids), context=context)
        # check picking state to set the date_done is needed
        done_picking = []
        for picking in picking_obj.browse(cr, uid, list(pickings), context=context):
            if picking.state == 'done' and not picking.date_done:
                done_picking.append(picking.id)
        if done_picking:
            picking_obj.write(cr, uid, done_picking, {'date_done': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)},
                              context=context)
        #update setelah move done
        self.product_price_update_after_done(cr, uid, ids, context=context)
        return True
    
    def _get_move_price_from_pricelist(self, cr, uid, move, context=None):
        price_unit = move.price_unit or move.product_id.standard_price
        if move.inventory_id:
            date = move.inventory_id.date
        else:
            date = move.date
        date_inventory_str = datetime.strptime(date, DEFAULT_SERVER_DATETIME_FORMAT).strftime(
            DEFAULT_SERVER_DATE_FORMAT) or False
        warehouse_id = move.warehouse_id and move.warehouse_id.id
        if (not warehouse_id):
            warehouse_id = self.pool.get("res.users").browse(cr, uid, uid, context=context).warehouse_id.id or False
        cr.execute("""SELECT pricelist_id FROM pricelist_warehouse_rel
                    WHERE warehouse_id=%s ORDER BY pricelist_id DESC LIMIT 1""", (warehouse_id,))
        pricelist_id = cr.fetchone() and cr.fetchone()[0] or False
        if pricelist_id:
            price_unit = self.pool.get("product.pricelist").price_get(cr, uid, [pricelist_id], move.product_id.id,
                                        move.product_uom_qty or 1.0, move.partner_id.id or False,
                                        {'uom': move.product_uom.id, 'date': date_inventory_str})[pricelist_id]
        return price_unit

    # def product_price_update_before_done(self, cr, uid, ids, context=None):
    #     product_obj = self.pool.get('product.product')
    #     # tmpl_dict = {}
    #     for move in self.browse(cr, uid, ids, context=context):
    #         #adapt standard price on incomming moves if the product cost_method is 'average'
    #         if ((move.location_id.usage == 'supplier') and (move.product_id.cost_method == 'average')) or \
    #                 ((move.location_id.usage == 'inventory') and (move.location_dest_id.usage == 'internal')):
    #             new_std_price = self.get_new_std_price(cr, uid, ids, context=context)
    #             product = move.product_id
    #             # prod_tmpl_id = move.product_id.product_tmpl_id.id
    #             # qty_available = move.product_id.product_tmpl_id.qty_available
    #             # if tmpl_dict.get(prod_tmpl_id):
    #             #     product_avail = qty_available + tmpl_dict[prod_tmpl_id]
    #             # else:
    #             #     tmpl_dict[prod_tmpl_id] = 0
    #             #     product_avail = qty_available
    #             # if product_avail <= 0:
    #             #     new_std_price = move.price_unit
    #             # else:
    #             #     amount_unit = product.standard_price
    #             #     new_std_price = ((amount_unit * product_avail) + (move.price_unit * move.product_qty)) / (product_avail + move.product_qty)
    #             # tmpl_dict[prod_tmpl_id] += move.product_qty
    #             ctx = dict(context or {}, force_company=move.company_id.id)
    #             product_obj.write(cr, SUPERUSER_ID, [product.id], {'standard_price': new_std_price}, context=ctx)
    #             if (move.location_id.usage == 'inventory') and (move.location_dest_id.usage == 'internal'):
    #                 self.write(cr, SUPERUSER_ID, [move.id], {'price_unit': new_std_price}, context=context)

    def product_price_update_after_done(self, cr, uid, ids, context=None):
        inv_lines_obj = self.pool['stock.inventory.line']
        for move in self.browse(cr, uid, ids, context=context):
            if move.product_id.cost_method == 'real' and move.location_dest_id.usage != 'internal':
                self._store_average_cost_price(cr, uid, move, context=context)
            if (move.location_id.usage == 'inventory') and (move.location_dest_id.usage == 'internal') and move.inventory_id:
                inv_line_ids = inv_lines_obj.search(cr, uid, [('inventory_id', '=', move.inventory_id.id),
                                                              ('product_id', '=', move.product_id.id)], context=context)
                inventory_line = inv_lines_obj.browse(cr, uid, inv_line_ids, context=context)[0]
                if move.quant_ids:
                    quants = set([q.id for q in move.quant_ids])
                    new_price = inv_line_ids and inventory_line.inventory_value or \
                                self.get_price_unit(cr, uid, move, context=context)
                    self.pool.get('stock.quant').write(cr, SUPERUSER_ID, quants, {'cost': new_price}, context=context)

    # def get_new_std_price(self, cr, uid, ids, context=None):
    #     tmpl_dict = {}
    #     for move in self.browse(cr, uid, ids, context=context):
    #         product = move.product_id
    #         prod_tmpl_id = move.product_id.product_tmpl_id.id
    #         qty_available = move.product_id.product_tmpl_id.qty_available
    #         if tmpl_dict.get(prod_tmpl_id):
    #             product_avail = qty_available + tmpl_dict[prod_tmpl_id]
    #         else:
    #             tmpl_dict[prod_tmpl_id] = 0
    #             product_avail = qty_available
    #         if product_avail <= 0:
    #             new_std_price = move.price_unit
    #         else:
    #             # Get the standard price
    #             amount_unit = product.standard_price
    #             new_std_price = ((amount_unit * product_avail) + (move.price_unit * move.product_qty)) / (product_avail + move.product_qty)
    #         tmpl_dict[prod_tmpl_id] += move.product_qty
    #     return new_std_price

    def run_schedule_acc_entry(self, cr, uid, ids, autocommit=False, context=None):
        user_obj = self.pool['res.users']
        quant_obj = self.pool['stock.quant']
        for move_id in ids:
            stk_move = self.browse(cr, uid, move_id, context=context)
            user_id = SUPERUSER_ID
            if stk_move.warehouse_id:
                users = user_obj.search(cr,uid,[('login', 'like', 'adm%'),('login', 'not like', '%admin%'),
                        ('warehouse_id', '=', stk_move.warehouse_id.id)], limit=1, order='login')
                user_id = users and users[0]
            elif stk_move.inventory_id:
                warehouse = self.pool['stock.warehouse'].search(cr, uid,
                            [('lot_stock_id', '=', stk_move.inventory_id.location_id.id)], limit=1)
                users = user_obj.search(cr,uid,[('login', 'like', 'adm%'), ('login', 'not like', '%admin%'),
                            ('warehouse_id', '=', warehouse and warehouse[0])], limit=1, order='login')
                user_id = users and users[0]
            if (stk_move.state == "done") and (str(stk_move.name)[0:10] != 'Extra Move'):
                try:
                    quant_ids = set([q.id for q in stk_move.quant_ids])  #if q.qty > 0 and q.inventory_value
                    quants = quant_obj.browse(cr, user_id, list(quant_ids), context=context)
                    if list(quant_ids): #autocreate & post entries from stock move
                        entry_ids = quant_obj._auto_create_post_move(cr, user_id, quants, stk_move, context=context)
                        if entry_ids:
                            self.write(cr, user_id, [stk_move.id], {'entries': True}, context=context)
                            context.update({'stk_move_ids': [stk_move.id]})
                            self.pool['account.move'].post(cr, user_id, entry_ids, context=context)  #Autopost entries
                    if autocommit:
                        cr.commit()
                except OperationalError:
                    if autocommit:
                        cr.rollback()
                        continue
                    else:
                        raise
        return True

    ### Run Scheduler - Stock to STJ (Stock Journal) ###
    def run_autocreate_posting_journal(self, cr, uid, use_new_cursor=False, date_start=False, date_stop=False, warehouse_id=False, context=None):
        if context is None:
            context = {}
        try:
            if use_new_cursor:
                cr = openerp.registry(cr.dbname).cursor()

            outlet_ids = [1, 2, 3, 4, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144]
            cr.execute("SELECT DISTINCT id FROM stock_warehouse WHERE active=true")
            res_outlet = cr.fetchall()
            if res_outlet:
                outlet_ids = [int(x[0]) for x in res_outlet]

            dom = [('state','=','done'),('entries','=',False)] #custom filter
            if date_start and date_stop:
                dom += [('date','>=',date_start),('date','<=',date_stop)]
            else:
                dom += [('date','>=',time.strftime('%Y-01-01'))]  #time.strftime(DEFAULT_SERVER_DATETIME_FORMAT) #'%Y-%m-%d'

            if warehouse_id:
                dom += [('warehouse_id', '=', warehouse_id)]
            else:
                dom += [('warehouse_id','in',tuple(outlet_ids))]

            prev_ids = []
            while True:
                ids = self.search(cr, SUPERUSER_ID, dom, context=context)
                if not ids or prev_ids == ids:
                    break
                else:
                    prev_ids = ids
                self.run_schedule_acc_entry(cr, SUPERUSER_ID, ids, autocommit=use_new_cursor, context=context)
                if use_new_cursor:
                    cr.commit()
        finally:
            if use_new_cursor:
                try:
                    cr.close()
                except Exception:
                    pass
        return {}
    



class stock_inventory(osv.osv):
    _inherit = "stock.inventory"

    def action_cancel_inventory(self, cr, uid, ids, context=None):
        move_obj = self.pool.get('stock.move')
        account_move_obj = self.pool.get('account.move')
        for inv in self.browse(cr, uid, ids, context=context):
            move_obj.action_cancel(cr, uid, [x.id for x in inv.move_ids], context=context)
            for move in inv.move_ids:
                account_move_ids = account_move_obj.search(cr, uid, [('name', '=', move.name)])
                if account_move_ids:
                    account_move_data_l = account_move_obj.read(cr, uid, account_move_ids, ['state'], context=context)
                    for account_move in account_move_data_l:
                        if account_move['state'] == 'posted':
                            raise osv.except_osv(_('User Error!'),
                                                 _('In order to cancel this inventory, you must first unpost related journal entries.'))
                        account_move_obj.unlink(cr, uid, [account_move['id']], context=context)
            self.write(cr, uid, [inv.id], {'state': 'cancel'}, context=context)
        return True

    def post_inventory(self, cr, uid, inv, context=None):
        move_obj = self.pool.get('stock.move')
        for move in inv.move_ids:
            if move.state != 'done':
                if move.date != inv.date:
                    move_obj.write(cr, uid, [move.id], {'date': inv.date}, context=context)
                if move.date_expected != inv.date:
                    move_obj.write(cr, uid, [move.id], {'date_expected': inv.date}, context=context)
                if inv.location_id:
                    warehouse = self.pool.get('stock.warehouse').search(cr,uid,[('lot_stock_id','=',inv.location_id.id)],limit=1)
                    move_obj.write(cr, uid, [move.id], {'warehouse_id': warehouse and warehouse[0] or False}, context=context)
        move_obj.action_done(cr, uid, [x.id for x in inv.move_ids if x.state != 'done'], context=context)


    def prepare_inventory(self, cr, uid, ids, context=None):
        inventory_line_obj = self.pool.get('stock.inventory.line')
        for inventory in self.browse(cr, uid, ids, context=context):
            # If there are inventory lines already (e.g. from import), respect those and set their theoretical qty
            line_ids = [line.id for line in inventory.line_ids]
            if not line_ids and inventory.filter != 'partial':
                #compute the inventory lines and create them
                vals = self._get_inventory_lines(cr, uid, inventory, context=context)
                for product_line in vals:
                    inventory_line_obj.create(cr, uid, product_line, context=context)
        return self.write(cr, uid, ids, {'state': 'confirm'}) ##remove_update_date

    def onchange_inventory_date(self, cr, uid, ids, inv_date, context=None):
        period_pool = self.pool.get('account.period')
        period_id = period_pool.find(cr, uid, context=context)
        if inv_date:
            pids = period_pool.find(cr, uid, inv_date, context=context)
            period_id = pids and pids[0]
        return {'value': {'period_id': period_id}}



class stock_quant(osv.osv):
    _inherit = "stock.quant"

    def _get_inventory_value(self, cr, uid, quant, context=None):
        get_inv_value = super(stock_quant, self)._get_inventory_value(cr, uid, quant, context=context)
        if quant.product_id.cost_method in ('standard'):
            move_crosscheck = []
            for move in quant.history_ids:
                if move.picking_id and move.picking_id.work_id and (move.picking_id.type_wo == "fg" or (move.location_id.usage in (
                        'internal', 'transit') and move.location_dest_id.usage == "customer")):
                    move_crosscheck.append(True)
                else:
                    move_crosscheck.append(False)
            if all(move_crosscheck):
                return quant.cost * quant.qty
            else:
                return get_inv_value
        elif quant.product_id.cost_method in ('real'):
            return quant.cost * quant.qty
        return get_inv_value

    def _auto_create_post_move(self, cr, uid, quants, move, context=None): #Custom
        if context is None:
            context = {}
        entry_ids = []
        location_obj = self.pool.get('stock.location')
        location_from = move.location_id
        location_to = quants[0].location_id
        company_from = location_obj._location_owner(cr, uid, location_from, context=context)
        company_to = location_obj._location_owner(cr, uid, location_to, context=context)

        for q in quants:
            if q.owner_id:
                return False
            if q.qty <= 0:
                return False

        if company_to and (move.location_id.usage not in ('internal', 'transit') and move.location_dest_id.usage == 'internal' or company_from != company_to):
            ctx = context.copy()
            ctx['force_company'] = company_to.id
            journal_id, acc_src, acc_dest, acc_valuation = self._get_accounting_data_for_valuation(cr, uid, move, context=ctx)
            if location_from and location_from.usage == 'customer': #goods returned from customer
                entry_ids = self._create_account_move_line(cr, uid, quants, move, acc_dest, acc_valuation, journal_id, context=ctx)
            else:
                entry_ids = self._create_account_move_line(cr, uid, quants, move, acc_src, acc_valuation, journal_id, context=ctx)

        if company_from and (move.location_id.usage == 'internal' and move.location_dest_id.usage not in ('internal', 'transit') or company_from != company_to):
            ctx = context.copy()
            ctx['force_company'] = company_from.id
            journal_id, acc_src, acc_dest, acc_valuation = self._get_accounting_data_for_valuation(cr, uid, move, context=ctx)
            if location_to and location_to.usage == 'supplier': #goods returned to supplier
                entry_ids = self._create_account_move_line(cr, uid, quants, move, acc_valuation, acc_src, journal_id, context=ctx)
            else:
                entry_ids = self._create_account_move_line(cr, uid, quants, move, acc_valuation, acc_dest, journal_id, context=ctx)
        return entry_ids

    def _get_accounting_data_for_valuation(self, cr, uid, move, context=None):
        journal_id, acc_src, acc_dest, acc_valuation = super(stock_quant, self)._get_accounting_data_for_valuation(cr, uid, move, context=context)
        if move.purchase_line_id and move.purchase_line_id.cost_categ_id:
            journal_id = move.purchase_line_id.cost_categ_id.journal_id.id or journal_id
        if (move.location_id.usage == 'inventory') and (move.location_dest_id.usage == 'internal') and move.inventory_id:
            jurnal_sto = self.pool.get('account.journal').search(cr, uid, [
                ('name', 'like', '%Stock Opname%'),('code','=','STO')], limit=1, context=context)
            journal_id = jurnal_sto and jurnal_sto[0] or journal_id
        if move.picking_id and move.picking_id.work_id:
            persediaan_dlm_proses = self.pool.get('account.account').search(cr, uid, [
                ('code', '=', '111340.00.01'), ('name', 'like', '%Persediaan Dalam Proses%'), ('active', '=', True)])
            persediaan_bahan_jadi = self.pool.get('account.account').search(cr, uid, [
                ('code', '=', '111350.00.01'), ('name', 'like', '%Persediaan Bahan Jadi%'), ('active', '=', True)])
            if move.location_id.usage in ('internal', 'transit') and move.location_dest_id.usage=='production' \
                    and move.picking_id.type_wo=='rm' and persediaan_dlm_proses: #move:stock to production (MAT'L)
                # acc_dest = persediaan_dlm_proses[0] or 4484  # debit
                acc_dest = persediaan_bahan_jadi[0] or 4486  # debit
            if move.location_dest_id.usage in ('internal','transit') and move.location_id.usage == 'production' \
                    and move.picking_id.type_wo == 'fg': #move: production to stock (FG)
                if persediaan_bahan_jadi:
                    acc_dest = persediaan_bahan_jadi[0] or 4486  #debit
                if persediaan_dlm_proses:
                    acc_valuation = persediaan_dlm_proses[0] or 4484  #credit
        return journal_id, acc_src, acc_dest, acc_valuation

    def _prepare_account_move_line(self, cr, uid, move, qty, cost, credit_account_id, debit_account_id, context=None):
        ctx = context.copy()
        inv_lines_obj = self.pool.get('stock.inventory.line')
        currency_obj = self.pool.get('res.currency')
        if move.picking_id and move.picking_id.work_id and (move.picking_id.type_wo == "fg" or (
                move.location_id.usage in ('internal', 'transit') and move.location_dest_id.usage == "customer")):
            ctx = dict(context, force_valuation_amount=move.price_unit)
        move_lines = super(stock_quant, self)._prepare_account_move_line(cr, uid, move, qty, cost, credit_account_id, debit_account_id, context=ctx)
        move_lines[0][2].update({'warehouse_id': move.warehouse_id and move.warehouse_id.id or False}) #update_WH_ID
        move_lines[1][2].update({'warehouse_id': move.warehouse_id and move.warehouse_id.id or False}) #update_WH_ID
        if move.purchase_line_id and move.purchase_line_id.cost_categ_id:
            move_lines[0][2].update({'account_id': move.purchase_line_id.cost_categ_id.debit_account_id.id,
                                     'debit': move.purchase_line_id.price_unit or cost})  #debit_account
            move_lines[1][2].update({'account_id': move.purchase_line_id.cost_categ_id.credit_account_id.id,
                                     'credit': move.purchase_line_id.price_unit or cost})  #credit_accounts
        if move.inventory_id:
            inv_line_ids = inv_lines_obj.search(cr, uid, [('inventory_id', '=', move.inventory_id.id),
                            ('product_id', '=', move.product_id.id)], context=context)
            if inv_line_ids:
                inv_line = inv_lines_obj.browse(cr, uid, inv_line_ids, context=context)[0]
                inv_line_price = currency_obj.round(cr, uid, move.company_id.currency_id, inv_line.inventory_value * qty)
                if move_lines[0][2]['debit'] != inv_line_price:
                    move_lines[0][2].update({'debit': inv_line_price})
                if move_lines[1][2]['credit'] != inv_line_price:
                    move_lines[1][2].update({'credit': inv_line_price})           
        return move_lines

    def _create_account_move_line(self, cr, uid, quants, move, credit_account_id, debit_account_id, journal_id, context=None):
        quant_cost_qty = {}
        for quant in quants:
            if quant_cost_qty.get(quant.cost):
                quant_cost_qty[quant.cost] += quant.qty
            else:
                quant_cost_qty[quant.cost] = quant.qty
        entry_ids = []
        move_obj = self.pool.get('account.move')
        period_obj = self.pool.get('account.period')
        cost_categ_obj = self.pool.get('cost.category')
        for cost, qty in quant_cost_qty.items():
            context.update({'user_id': move.create_uid and move.create_uid.id or False})
            move_lines = self._prepare_account_move_line(cr, uid, move, qty, cost, credit_account_id, debit_account_id, context=context)
            period_id = context.get('force_period', period_obj.find(cr, uid, move.date, context=context)[0])
            if move.date or move.date_expected:
                pids = period_obj.find(cr, uid, move.date or move.date_expected, context=context)
                period_id = pids and pids[0]
            acc_move_ref = move.picking_id and move.picking_id.name or move.product_id and move.product_id.name_template
            if move.picking_id.note:
                acc_move_ref += "; " + _(move.picking_id.note)
            move_id = move_obj.create(cr, uid, {
                'journal_id': journal_id,
                'line_id': move_lines,
                'period_id': period_id,
                'warehouse_id': move.warehouse_id and move.warehouse_id.id or False,
                'date': move.date or move.date_expected or fields.date.context_today(self, cr, uid, context=context),
                'ref': acc_move_ref,
            }, context=context)
            if move_id:
                journal_codes = ['STJ','STO']
                if move_id not in entry_ids:
                    entry_ids.append(move_id)
                jcc_ids = cost_categ_obj.search(cr, uid, [], context=context) #('journal_id','=',True)
                for jcc in cost_categ_obj.browse(cr, uid, jcc_ids, context=context):
                    if jcc.journal_id:
                        journal_codes.append(jcc.journal_id.code)
                jurnal = self.pool.get('account.journal').browse(cr, uid, journal_id, context=context)
                if jurnal.type=='general' and jurnal.code in journal_codes:
                    new_move = move_obj.browse(cr, uid, move_id, context=context)
                    self.pool.get('stock.move').write(cr, uid, [move.id], 
                                                      {'entries': True, 'related_journal': new_move.name}, context=context)
        return entry_ids



class stock_picking_type(osv.osv):
    _inherit = "stock.picking.type"

    def name_search(self, cr, user, name, args=None, operator='like', context=None, limit=100):
        warehouse_obj = self.pool.get('stock.warehouse')
        if not args:
            args = []
        wh_ids = warehouse_obj.search(cr, user, ['|', '|', ('name', 'like', '%' + _(name) + '%'),
                    ('code', 'like', '%' + _(name) + '%'), ('outlet', 'like', '%' + _(name) + '%')], context=context)
        if operator in expression.NEGATIVE_TERM_OPERATORS:
            domain = [('name', operator, name), ('warehouse_id', 'in', wh_ids)]
        else:
            domain = ['|', ('name', operator, '%' + _(name) + '%'), ('warehouse_id', 'in', wh_ids)]
        ids = self.search(cr, user, expression.AND([domain, args]), limit=limit, context=context)
        return self.name_get(cr, user, ids, context=context)

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]
        res = []
        if not ids:
            return res
        for record in self.browse(cr, uid, ids, context=context):
            name = record.name
            if record.warehouse_id:
                name = record.warehouse_id.outlet + ': ' + name
                if record.warehouse_id.code:
                    name = "[%s] %s" % (record.warehouse_id.code, name)
                if record.warehouse_id.zone_store:
                    name = name + "\n%s" % (str(record.warehouse_id.zone_store).replace("_", " ").title())
                if record.warehouse_id.kota_id:
                    kota_kab = self.pool.get('res.partner.kota').browse(cr, uid, record.warehouse_id.kota_id.id).name
                    name = name + ", %s" % (kota_kab)
            res.append((record.id, name))
        return res



class stock_inventory_line(osv.osv):
    _inherit = "stock.inventory.line"

    _columns = {
        'inventory_value': fields.float('Harga', digits_compute=dp.get_precision('Product Price'), help="Harga satuan produk saat inventory"),
        'product_qty': fields.float('Real Quantity', digits_compute=dp.get_precision('Stock Weight'), help="Quantity produk harap diinput dengan 2 digit dibelakang koma dengan pembulatan ke atas (contoh: 2.465 => 2.47), bukan 3 digit!"),
    }

    def onchange_createline(self, cr, uid, ids, location_id=False, product_id=False, uom_id=False, package_id=False, prod_lot_id=False, partner_id=False, price=False, qty=False, date_inventory=False, company_id=False, context=None):
        quant_obj = self.pool["stock.quant"]
        uom_obj = self.pool["product.uom"]
        warehouse_obj = self.pool["stock.warehouse"]
        user_obj = self.pool["res.users"]
        product_pricelist = self.pool["product.pricelist"]
        res = {'value': {}}
        # if location_id: #harus ada ib_pricelist_warehouse
        #     date_inventory_str = datetime.strptime(date_inventory, DEFAULT_SERVER_DATETIME_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT) or False
        #     warehouse_id = warehouse_obj.search(cr, uid, [('lot_stock_id','=',location_id)], limit=1, context=context)[0] or False
        #     if not warehouse_id:
        #         warehouse_id = user_obj.browse(cr, uid, uid, context=context).warehouse_id.id or False
        #     cr.execute("""SELECT pricelist_id FROM pricelist_warehouse_rel
        #                     WHERE warehouse_id=%s ORDER BY pricelist_id DESC LIMIT 1""", (warehouse_id,))
        #     pricelist_id = cr.fetchone() and cr.fetchone()[0] or False
        #     if pricelist_id:
        #         price = product_pricelist.price_get(cr, uid, [pricelist_id], product_id, qty or 1.0,
        #                     partner_id or False, {'uom': uom_id, 'date': date_inventory_str})[pricelist_id]
        # If no UoM already put the default UoM of the product
        if product_id:
            product = self.pool.get('product.product').browse(cr, uid, product_id, context=context)
            uom = self.pool['product.uom'].browse(cr, uid, uom_id, context=context)
            if product.uom_id.category_id.id != uom.category_id.id:
                res['value']['product_uom_id'] = product.uom_id.id
                res['value']['inventory_value'] = product.standard_price if price == 0.0 or False else price
                res['domain'] = {'product_uom_id': [('category_id','=',product.uom_id.category_id.id)]}
                uom_id = product.uom_id.id
        # Calculate theoretical quantity by searching the quants as in quants_get
        if product_id and location_id:
            product = self.pool.get('product.product').browse(cr, uid, product_id, context=context)
            if not company_id:
                company_id = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id
            dom = [('company_id', '=', company_id), ('location_id', '=', location_id), ('lot_id', '=', prod_lot_id),
                        ('product_id','=', product_id), ('owner_id', '=', partner_id), ('package_id', '=', package_id)]
            quants = quant_obj.search(cr, uid, dom, context=context)
            th_qty = sum([x.qty for x in quant_obj.browse(cr, uid, quants, context=context)])
            if product_id and uom_id and product.uom_id.id != uom_id:
                th_qty = uom_obj._compute_qty(cr, uid, product.uom_id.id, th_qty, uom_id)
            res['value']['theoretical_qty'] = th_qty
            res['value']['product_qty'] = th_qty
            res['value']['inventory_value'] = product.standard_price if price==0.0 or False else price
        return res


# class stock_picking(osv.osv):
#     _inherit = "stock.picking"
#
#     @api.cr_uid_ids_context
#     def action_merge_transfer_details(self, cr, uid, ids, context=None):
#         picking_ids = ids or []
#         if context and context.get('active_ids', False):
#             picking_ids = context['active_ids']
#         if not context:
#             context = {}
#         else:
#             context = context.copy()
#         context.update({
#             'active_model': self._name,
#             'active_ids': picking_ids,
#             'active_id': len(picking_ids) and picking_ids[0] or False
#         })
#
#         created_id = self.pool['stock.transfer_details'].create(cr, uid, {'picking_id': len(picking_ids) and picking_ids[0] or False}, context)
#         return self.pool['stock.transfer_details'].wizard_view(cr, uid, created_id, context)




# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
    