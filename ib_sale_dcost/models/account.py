##-*- coding: utf-8 -*-
## © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##################################################################################################################
import time
from openerp import workflow
#import openerp
from openerp import netsvc
from datetime import datetime
from openerp.osv import osv, orm, fields
from openerp.tools.translate import _
from openerp import models, api
from openerp.tools import float_compare
from openerp.exceptions import except_orm, Warning, RedirectWarning


BULAN = [(0,''), (1,'Januari'), (2,'Februari'),(3,'Maret'),(4,'April'), (5,'Mei'), (6,'Juni'),
        (7,'Juli'), (8,'Agustus'), (9,'September'),(10,'Oktober'), (11,'November'), (12,'Desember')]

class account_invoice_log_book(osv.osv):
    _inherit = "account.invoice"

    def _get_outlet(self, cr, uid, ids, name, arg, context=None):
        if context is None: context = {}
        res = {}
        for inv in self.browse(cr, uid, ids, context=context):
            res[inv.id] = _("")
            if inv.user_id:
                usr_warehouse_id = self.pool['res.users'].browse(cr,uid,inv.user_id.id,context=context).warehouse_id.id
                outlet_name = self.pool['stock.warehouse'].browse(cr, uid, usr_warehouse_id, context=context).outlet
                res[inv.id] = outlet_name
        return res

    def _get_bulan_periode(self, cr, uid, ids, name, arg, context=None):
        if context is None: context = {}
        res = {}
        for inv in self.browse(cr, uid, ids, context=context):
            res[inv.id] = _("")
            if inv.period_id:
                ac_period = self.pool['account.period'].browse(cr,uid,inv.period_id.id,context=context).name
                res[inv.id] = str.title(BULAN[int(ac_period[:2])][1])
        return res

    def _get_partner_category(self, cr, uid, ids, name, arg, context=None):
        if context is None: context = {}
        res = {}
        for inv in self.browse(cr, uid, ids, context=context):
            cr.execute("""SELECT category_id FROM res_partner_res_partner_category_rel  
                                WHERE partner_id = %s""", (inv.partner_id.id,))
            categ_ids = [x[0] for x in cr.fetchall()]
            list_categ = _("")
            if categ_ids:
                for categ in self.pool['res.partner.category'].browse(cr, uid, categ_ids, context=context):
                    list_categ += _(categ.name) + _(', ')
            res[inv.id] = list_categ[:-2]
        return res

    def _get_order_date(self, cr, uid, ids, name, arg, context=None):
        if context is None: context = {}
        res = {}
        for inv in self.browse(cr, uid, ids, context=context):
            cr.execute("""SELECT purchase_id FROM purchase_invoice_rel  
                                WHERE invoice_id = %s""", (inv.id,))
            purchase_ids = [y[0] for y in cr.fetchall()]
            date_order = _("")
            res[inv.id] = date_order
            if purchase_ids:
                for pur in self.pool['purchase.order'].browse(cr, uid, purchase_ids, context=context):
                    seq_dt = str(pur.date_order).split(' ')
                    exp = str(seq_dt[0]).split('-')
                    date_order += _(exp[2]) + _("-") + _(exp[1]) + _("-") + _(exp[0]) + _(', ')
                res[inv.id] = _("Pembelian Tanggal ") + date_order[:-2]
        return res

    def _get_date_invoice(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for inv in self.browse(cr, uid, ids, context=context):
            res[inv.id] = str(datetime.strptime(inv.date_invoice, '%Y-%m-%d').strftime("%m/%d/%Y"))
        return res

    def _get_date_due(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for inv in self.browse(cr, uid, ids, context=context):
            res[inv.id] = str(datetime.strptime(inv.date_due, '%Y-%m-%d').strftime("%m/%d/%Y"))
        return res

    _columns = {
        'outlet': fields.function(_get_outlet, type='char', string='Outlet'),
        'month': fields.function(_get_bulan_periode, type='char', string='Bulan Pembelian'),
        'partner_categ': fields.function(_get_partner_category, type='text', string='Kategori'),
        'description_purchase': fields.function(_get_order_date, type='text', string='Keterangan'),
        'date_invoice_dummy': fields.function(_get_date_invoice, type='char', string='Invoice Date'),
        'date_due_dummy': fields.function(_get_date_due, type='char', string='Due Date'),
        'tanggal_faktur': fields.date(string='Tanggal Tukar Faktur', readonly=True,
                states={'draft':[('readonly', False)],'open':[('readonly', False)]},copy=False)
    }


account_invoice_log_book()



class account_cash_statement_lines(osv.osv):
    _inherit = "account.bank.statement.line"

    _columns = {
        'analytic_account_id': fields.many2one('account.analytic.account', 'Analytic Account'),
    }

    def onchange_account_id(self, cr, uid, ids, account_id, context=None):
        user_obj = self.pool.get('res.users')
        analytic_cbg_obj = self.pool.get('analytic.cabang')
        if not account_id:
            return {}
        res = {}
        wh_id = user_obj.browse(cr, uid, uid).warehouse_id.id or False
        for abs_line in self.browse(cr, uid, ids, context=context):
            if abs_line.statement_id.user_id:
                wh_id = user_obj.browse(cr, uid, abs_line.statement_id.user_id.id).warehouse_id.id
        if account_id and wh_id:
            analytics = analytic_cbg_obj.search(cr, uid, [('account_id','=',account_id),
                            ('name','=',wh_id)], limit=1, context=context)
            for ac in analytic_cbg_obj.browse(cr, uid, analytics):
                res['analytic_account_id'] = ac.analytic_id and ac.analytic_id.id
        return {'value': res}




class account_cash_statement(osv.osv):
    _inherit = 'account.bank.statement'

    def _prepare_move_line_vals(self, cr, uid, st_line, move_id, debit, credit, currency_id=False,
                amount_currency=False, account_id=False, partner_id=False, context=None):
        acc_id = account_id or st_line.account_id.id
        cur_id = currency_id or st_line.statement_id.currency.id
        par_id = partner_id or (((st_line.partner_id) and st_line.partner_id.id) or False)
        return {
            'name': st_line.name,
            'date': st_line.date,
            'ref': st_line.ref,
            'move_id': move_id,
            'partner_id': par_id,
            'account_id': acc_id,
            'credit': credit,
            'debit': debit,
            'statement_id': st_line.statement_id.id,
            'journal_id': st_line.statement_id.journal_id.id,
            'period_id': st_line.statement_id.period_id.id,
            'currency_id': amount_currency and cur_id,
            'amount_currency': amount_currency,
            'analytic_account_id': st_line.analytic_account_id and st_line.analytic_account_id.id,
        }



class account_move(osv.osv):
    _inherit = "account.move"

    _columns = {
        'period_id': fields.many2one('account.period', 'Period', required=True,
                                     states={'posted': [('readonly', True)]}, track_visibility='onchange'),
        'journal_id': fields.many2one('account.journal', 'Journal', required=True,
                                      states={'posted': [('readonly', True)]}, track_visibility='onchange'),
        'state': fields.selection(
            [('draft', 'Unposted'), ('posted', 'Posted')], 'Status',
            required=True, readonly=True, copy=False,
            help='All manually created new journal entries are usually in the status \'Unposted\', '
                 'but you can set the option to skip that status on the related journal. '
                 'In that case, they will behave as journal entries automatically created by the '
                 'system on document validation (invoices, bank statements...) and will be created '
                 'in \'Posted\' status.', track_visibility='onchange'),
        'date': fields.date('Date', required=True, states={'posted': [('readonly', True)]},
                            select=True, track_visibility='onchange'),
    }



class account_asset_asset(orm.Model):
    _inherit = 'account.asset.asset'

    _columns = {
        'warehouse_id': fields.many2one(
            'stock.warehouse', 'Outlet',
            change_default=True, readonly=True,
            domain=[('active', '=', True)],
            states={'draft': [('readonly', False)]}),
    }


class account_move_line(osv.osv):
    _inherit = 'account.move.line'

    def create(self, cr, uid, vals, context=None, check=True):
        if not context:
            context = {}
        else:
            context = context.copy()
        user_obj = self.pool.get('res.users')
        account_obj = self.pool.get('account.account')
        journal_pool = self.pool.get('account.journal')

        result = super(account_move_line, self).create(cr, uid, vals, context=context)
        journal_ids = journal_pool.search(cr, uid, [('type','=','bank'),('code','not in',('BNK1', 'BNK2','CARD','DSK','PREP'))])
        jl_receivable_ids = journal_pool.search(cr, uid, [('type', 'in', ('bank','cash')),
                ('code', 'in', ('BNK1', 'BNK2', 'CARD', 'DSK', 'PREP', 'STRCS', 'STRCD', 'STRDV'))])
        jurnal_setoran = journal_pool.search(cr, uid, [('type', 'in', ('bank', 'cash')),('code','in',('STRCS', 'STRCD', 'STRDV'))])

        yid = {}
        ap = account_obj.browse(cr, uid, vals['account_id'])
        for ax in ap.analytic_line:
            yid[ax.name.id] = ax.analytic_id.id

        warehouse_id = False
        if vals.get('warehouse_id', False): #'warehouse_id' in vals and
            warehouse_id = vals['warehouse_id']
        elif 'user_id' in vals and vals['user_id']:
            uid = vals['user_id']
        elif 'move_id' in vals and vals['move_id']:
            cr.execute('SELECT i.user_id FROM account_invoice i ' \
                       'WHERE i.move_id=%s', (vals['move_id'],)) #vals['partner_id']
            for user in cr.fetchall():
                uid = user[0]

        users = user_obj.browse(cr, uid, uid)
        if not warehouse_id:
            warehouse_id = users.warehouse_id and users.warehouse_id.id

        if 'journal_id' in vals and (vals['journal_id'] in journal_ids) and \
                (ap.code[0:6]=='111120' or ap.code=='111211.00.08'):
            self.write(cr, uid, [result], {'analytic_account_id': yid[1] or False}, context=context)
        elif (vals['journal_id'] in jurnal_setoran) and (ap.code in ('111120.01.08','111120.01.07','111211.00.08')):
            self.write(cr, uid, [result], {'analytic_account_id': yid[1] or False}, context=context)
        elif 'journal_id' in vals and (vals['journal_id'] in journal_ids) and \
                (ap.code[0:6] == '121210' or ap.code in ('121210.00.02', '121210.00.01')):
            self.write(cr, uid, [result], {'analytic_account_id': yid[warehouse_id]}, context=context)
        elif yid.has_key(warehouse_id):
            self.write(cr, uid, [result], {'analytic_account_id': yid[warehouse_id]}, context=context)

        for aml in self.browse(cr, uid, result, context=context): #replace field name in move_lines = '/'
            if aml.account_id.code[0:6] in ('121210','111140') and aml.name=='/':
                self.write(cr, uid, [result], {'name': aml.move_id.name}, context=context)

        return result

    def onchange_account_id(self, cr, uid, ids, account_id=False, partner_id=False, context=None):
        account_obj = self.pool.get('account.account')
        partner_obj = self.pool.get('res.partner')
        fiscal_pos_obj = self.pool.get('account.fiscal.position')
        val = {}
        warehouse_id = self.pool.get('res.users').browse(cr, uid, uid).warehouse_id.id
        if account_id:
            res = account_obj.browse(cr, uid, account_id, context=context)
            tax_ids = res.tax_ids
            if tax_ids and partner_id:
                part = partner_obj.browse(cr, uid, partner_id, context=context)
                tax_id = fiscal_pos_obj.map_tax(cr, uid, part and part.property_account_position or False, tax_ids, context=context)[0]
            else:
                tax_id = tax_ids and tax_ids[0].id or False
            val['account_tax_id'] = tax_id
            if warehouse_id:
                mapping_analytic = {}
                for al in res.analytic_line:
                    mapping_analytic[al.name.id] = al.analytic_id.id
                if mapping_analytic.has_key(warehouse_id):
                    val['analytic_account_id'] = mapping_analytic[warehouse_id]
        return {'value': val}

    # def reconcile_partial(self, cr, uid, ids, type='auto', context=None, writeoff_acc_id=False, writeoff_period_id=False, writeoff_journal_id=False):
    #     move_rec_obj = self.pool.get('account.move.reconcile')
    #     merges = []
    #     unmerge = []
    #     total = 0.0
    #     merges_rec = []
    #     company_list = []
    #     if context is None:
    #         context = {}
    #     for line in self.browse(cr, uid, ids, context=context):
    #         if company_list and not line.company_id.id in company_list:
    #             raise osv.except_osv(_('Warning!'), _('To reconcile the entries company should be the same for all entries.'))
    #         company_list.append(line.company_id.id)
    #
    #     for line in self.browse(cr, uid, ids, context=context):
    #         if line.account_id.currency_id:
    #             currency_id = line.account_id.currency_id
    #         else:
    #             currency_id = line.company_id.currency_id
    #         if line.reconcile_id:
    #             raise osv.except_osv(_('Warning'), _("Journal Item '%s' (id: %s), Move '%s' is already reconciled!") % (line.name, line.id, line.move_id.name))
    #         if line.reconcile_partial_id:
    #             for line2 in line.reconcile_partial_id.line_partial_ids:
    #                 if line2.state != 'valid':
    #                     raise osv.except_osv(_('Warning'), _("Journal Item '%s' (id: %s) cannot be used in a reconciliation as it is not balanced!") % (line2.name, line2.id))
    #                 if not line2.reconcile_id:
    #                     if line2.id not in merges:
    #                         merges.append(line2.id)
    #                     if line2.account_id.currency_id:
    #                         total += line2.amount_currency
    #                     else:
    #                         total += (line2.debit or 0.0) - (line2.credit or 0.0)
    #             merges_rec.append(line.reconcile_partial_id.id)
    #         else:
    #             unmerge.append(line.id)
    #             if line.account_id.currency_id:
    #                 total += line.amount_currency
    #             else:
    #                 total += (line.debit or 0.0) - (line.credit or 0.0)
    #     ctx = context.copy() #baim
    #     if self.pool.get('res.currency').is_zero(cr, uid, currency_id, total):
    #         res = self.reconcile(cr, uid, merges+unmerge, context=ctx, writeoff_acc_id=writeoff_acc_id, writeoff_period_id=writeoff_period_id, writeoff_journal_id=writeoff_journal_id)
    #         return res
    #     # marking the lines as reconciled does not change their validity, so there is no need
    #     # to revalidate their moves completely.
    #     #reconcile_id = ctx.get('reconcile_id', False)
    #     reconcile_context = dict(context, novalidate=True)
    #     #if not reconcile_id:
    #     r_id = move_rec_obj.create(cr, uid, {
    #             'type': type,
    #             'line_partial_ids': map(lambda x: (4,x,False), merges+unmerge)
    #     }, context=reconcile_context)
    #     #else:
    #         #r_id = reconcile_id
    #     move_rec_obj.reconcile_partial_check(cr, uid, [r_id] + merges_rec, context=reconcile_context)
    #     return r_id
    #
    # def reconcile(self, cr, uid, ids, type='auto', writeoff_acc_id=False, writeoff_period_id=False, writeoff_journal_id=False, context=None):
    #     account_obj = self.pool.get('account.account')
    #     move_obj = self.pool.get('account.move')
    #     move_rec_obj = self.pool.get('account.move.reconcile')
    #     partner_obj = self.pool.get('res.partner')
    #     currency_obj = self.pool.get('res.currency')
    #     lines = self.browse(cr, uid, ids, context=context)
    #     unrec_lines = filter(lambda x: not x['reconcile_id'], lines)
    #     credit = debit = 0.0
    #     currency = 0.0
    #     account_id = False
    #     partner_id = False
    #     if context is None:
    #         context = {}
    #     company_list = []
    #     for line in lines:
    #         if company_list and not line.company_id.id in company_list:
    #             raise osv.except_osv(_('Warning!'), _('To reconcile the entries company should be the same for all entries.'))
    #         company_list.append(line.company_id.id)
    #     for line in unrec_lines:
    #         if line.state <> 'valid':
    #             raise osv.except_osv(_('Error!'),
    #                     _('Entry "%s" is not valid !') % line.name)
    #         credit += line['credit']
    #         debit += line['debit']
    #         currency += line['amount_currency'] or 0.0
    #         account_id = line['account_id']['id']
    #         partner_id = (line['partner_id'] and line['partner_id']['id']) or False
    #     writeoff = debit - credit
    #
    #     if context.has_key('date_p') and context['date_p']:
    #         date=context['date_p']
    #     else:
    #         date = time.strftime('%Y-%m-%d')
    #
    #     cr.execute('SELECT account_id, reconcile_id '\
    #                'FROM account_move_line '\
    #                'WHERE id IN %s '\
    #                'GROUP BY account_id,reconcile_id',
    #                (tuple(ids), ))
    #     r = cr.fetchall()
    #     #TODO: move this check to a constraint in the account_move_reconcile object
    #     if len(r) != 1:
    #         raise osv.except_osv(_('Error'), _('Entries are not of the same account or already reconciled ! '))
    #     if not unrec_lines:
    #         raise osv.except_osv(_('Error!'), _('Entry is already reconciled.'))
    #     account = account_obj.browse(cr, uid, account_id, context=context)
    #     if not account.reconcile:
    #         raise osv.except_osv(_('Error'), _('The account is not defined to be reconciled !'))
    #     if r[0][1] != None:
    #         raise osv.except_osv(_('Error!'), _('Some entries are already reconciled.'))
    #
    #     if (not currency_obj.is_zero(cr, uid, account.company_id.currency_id, writeoff)) or \
    #        (account.currency_id and (not currency_obj.is_zero(cr, uid, account.currency_id, currency))):
    #         # DO NOT FORWARD PORT
    #         if not writeoff_acc_id:
    #             if writeoff > 0:
    #                 writeoff_acc_id = account.company_id.expense_currency_exchange_account_id.id
    #             else:
    #                 writeoff_acc_id = account.company_id.income_currency_exchange_account_id.id
    #         if not writeoff_acc_id:
    #             raise osv.except_osv(_('Warning!'), _('You have to provide an account for the write off/exchange difference entry.'))
    #         if writeoff > 0:
    #             debit = writeoff
    #             credit = 0.0
    #             self_credit = writeoff
    #             self_debit = 0.0
    #         else:
    #             debit = 0.0
    #             credit = -writeoff
    #             self_credit = 0.0
    #             self_debit = -writeoff
    #         # If comment exist in context, take it
    #         if 'comment' in context and context['comment']:
    #             libelle = context['comment']
    #         else:
    #             libelle = _('Write-Off')
    #
    #         cur_obj = self.pool.get('res.currency')
    #         cur_id = False
    #         amount_currency_writeoff = 0.0
    #         if context.get('company_currency_id',False) != context.get('currency_id',False):
    #             cur_id = context.get('currency_id',False)
    #             for line in unrec_lines:
    #                 if line.currency_id and line.currency_id.id == context.get('currency_id',False):
    #                     amount_currency_writeoff += line.amount_currency
    #                 else:
    #                     tmp_amount = cur_obj.compute(cr, uid, line.account_id.company_id.currency_id.id, context.get('currency_id',False), abs(line.debit-line.credit), context={'date': line.date})
    #                     amount_currency_writeoff += (line.debit > 0) and tmp_amount or -tmp_amount
    #
    #         writeoff_lines = [
    #             (0, 0, {
    #                 'name': libelle,
    #                 'debit': self_debit,
    #                 'credit': self_credit,
    #                 'account_id': account_id,
    #                 'date': date,
    #                 'partner_id': partner_id,
    #                 'currency_id': cur_id or (account.currency_id.id or False),
    #                 'amount_currency': amount_currency_writeoff and -1 * amount_currency_writeoff or (account.currency_id.id and -1 * currency or 0.0)
    #             }),
    #             (0, 0, {
    #                 'name': libelle,
    #                 'debit': debit,
    #                 'credit': credit,
    #                 'account_id': writeoff_acc_id,
    #                 'analytic_account_id': context.get('analytic_id', False),
    #                 'date': date,
    #                 'partner_id': partner_id,
    #                 'currency_id': cur_id or (account.currency_id.id or False),
    #                 'amount_currency': amount_currency_writeoff and amount_currency_writeoff or (account.currency_id.id and currency or 0.0)
    #             })
    #         ]
    #         if context.get('bs_move_id'):
    #             writeoff_move_id = context['bs_move_id']
    #             for l in writeoff_lines:
    #                 self.create(cr, uid, dict(l[2], move_id=writeoff_move_id), dict(context, novalidate=True))
    #             if not move_obj.validate(cr, uid, writeoff_move_id, context=context):
    #                 raise osv.except_osv(_('Error!'), _('You cannot validate a non-balanced entry.'))
    #         else:
    #             writeoff_move_id = move_obj.create(cr, uid, {
    #                 'period_id': writeoff_period_id,
    #                 'journal_id': writeoff_journal_id,
    #                 'date':date,
    #                 'state': 'draft',
    #                 'line_id': writeoff_lines
    #             })
    #
    #         writeoff_line_ids = self.search(cr, uid, [('move_id', '=', writeoff_move_id), ('account_id', '=', account_id)])
    #         if account_id == writeoff_acc_id:
    #             writeoff_line_ids = [writeoff_line_ids[1]]
    #         ids += writeoff_line_ids
    #
    #     #reconcile_id = context.get('reconcile_id', False)
    #     reconcile_context = dict(context, novalidate=True)
    #     #if not reconcile_id:
    #     r_id = move_rec_obj.create(cr, uid, {'type': type}, context=reconcile_context)
    #     #else:
    #         #r_id = reconcile_id
    #     self.write(cr, uid, ids, {'reconcile_id': r_id, 'reconcile_partial_id': False}, context=reconcile_context)
    #
    #     for id in ids:
    #         workflow.trg_trigger(uid, 'account.move.line', id, cr)
    #
    #     if lines and lines[0]:
    #         partner_id = lines[0].partner_id and lines[0].partner_id.id or False
    #         if partner_id and not partner_obj.has_something_to_reconcile(cr, uid, partner_id, context=context):
    #             partner_obj.mark_as_reconciled(cr, uid, [partner_id], context=context)
    #     return r_id


class account_voucher(osv.osv):
    _inherit = 'account.voucher'

    def _get_payment_memo(self, cr, uid, context=None):
        if context is None: context = {}
        if not context.get('invoice_id', False):
            return _('')
        invoice = self.pool.get('account.invoice').browse(cr, uid, context['invoice_id'], context=context)
        return invoice.number or invoice.internal_number

    def _get_due_date_invoice(self, cr, uid, context=None):
        if context is None: context = {}
        if not context.get('invoice_id', False):
            return fields.date.context_today
        invoice = self.pool.get('account.invoice').browse(cr, uid, context['invoice_id'], context=context)
        return invoice.date_due or fields.date.context_today

    def _get_user_id(self, cr, uid, context=None):
        if context is None: context = {}
        if context.get('active_model', '') == 'account.invoice':
            invoices = self.pool.get('account.invoice').read(cr, uid, context['active_ids'], ['user_id'])
            for inv in invoices:
                if inv['user_id']:
                    return inv['user_id'][0] or False
        return False

    def _get_outlet_id(self, cr, uid, context=None):
        if context is None: context = {}
        if context.get('active_model', '') == 'account.invoice':
            invoices = self.pool.get('account.invoice').read(cr, uid, context['active_ids'], ['warehouse_id'])
            for inv in invoices:
                if inv['warehouse_id']:
                    return inv['warehouse_id'][0] or False
        return False

    _columns = {
        'user_id': fields.many2one('res.users', 'User'),
        'warehouse_id': fields.many2one('stock.warehouse', 'Outlet'),
    }

    _defaults = {
        'name': _get_payment_memo,
        'date': _get_due_date_invoice,
        'user_id': _get_user_id,
        'warehouse_id': _get_outlet_id,
    }

    def action_move_line_create(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        user = uid
        move_pool = self.pool.get('account.move')
        currency_pool = self.pool.get('res.currency')
        account_obj = self.pool.get('account.account')
        # invoice_obj = self.pool.get('account.invoice')
        analytic_cbg_obj = self.pool.get('analytic.cabang')
        move_line_pool = self.pool.get('account.move.line')
        warehouse_obj = self.pool.get('stock.warehouse')
        user_obj = self.pool.get('res.users')
        for voucher in self.browse(cr, uid, ids, context=context):
            local_context = dict(context, force_company=voucher.journal_id.company_id.id)
            if voucher.move_id:
                continue
            if voucher.user_id:
                user = voucher.user_id and voucher.user_id.id
                # local_context.update({'user_id': user})
            company_currency = self._get_company_currency(cr, uid, voucher.id, context)
            current_currency = self._get_current_currency(cr, uid, voucher.id, context)
            # we select the context to use accordingly if it's a multicurrency case or not
            context = self._sel_context(cr, uid, voucher.id, context)
            # But for the operations made by _convert_amount, we always need to give the date in the context
            ctx = context.copy()
            ctx.update({'date': voucher.date})
            if voucher.warehouse_id:
                # local_context.update({'warehouse_id': voucher.warehouse_id.id})
                warehouse_id = voucher.warehouse_id.id
            elif voucher.user_id:
                warehouse_id = user_obj.browse(cr, uid, voucher.user_id.id).warehouse_id.id
            else:
                warehouse_id = user_obj.browse(cr, uid, uid).warehouse_id.id or False
                if not warehouse_id:
                    warehouse_id = warehouse_obj.search(cr, uid, ['|', ('code', 'like', '%'+_(voucher.partner_id.code)+'%'),
                            ('partner_id', '=', voucher.partner_id.id)], limit=1)[0] or False
            # Create the account move record.
            move_vals = self.account_move_get(cr, user, voucher.id, context=context)
            if warehouse_id:
                move_vals.update({'warehouse_id': warehouse_id})
            move_id = move_pool.create(cr, user, move_vals, context=context)
            # Get the name of the account_move just created
            name = move_pool.browse(cr, user, move_id, context=context).name
            # Create the first line of the voucher
            move_line_vals = self.first_move_line_get(cr, user, voucher.id, move_id, company_currency, current_currency, local_context)
            if warehouse_id:
                move_line_vals.update({'warehouse_id': warehouse_id})
            move_line_id = move_line_pool.create(cr, user, move_line_vals, local_context)
            move_line_brw = move_line_pool.browse(cr, user, move_line_id, context=context)
            line_total = move_line_brw.debit - move_line_brw.credit
            rec_list_ids = []
            if voucher.type == 'sale':
                line_total = line_total - self._convert_amount(cr, uid, voucher.tax_amount, voucher.id, context=ctx)
            elif voucher.type == 'purchase':
                line_total = line_total + self._convert_amount(cr, uid, voucher.tax_amount, voucher.id, context=ctx)
            # Create one move line per voucher line where amount is not 0.0
            line_total, rec_list_ids = self.voucher_move_line_create(cr, uid, voucher.id, line_total, move_id, company_currency, current_currency, warehouse_id, context)

            # Create the writeoff line if needed
            ml_writeoff = self.writeoff_move_line_get(cr, user, voucher.id, line_total, move_id, name, company_currency, current_currency, local_context)
            if ml_writeoff:
                if warehouse_id:
                    ml_writeoff.update({'warehouse_id': warehouse_id})
                move_line_pool.create(cr, user, ml_writeoff, local_context)
            # We post the voucher.
            self.write(cr, user, [voucher.id], {
                'move_id': move_id,
                'state': 'posted',
                'number': name,
            })
            if voucher.journal_id.entry_posted:
                move_pool.post(cr, user, [move_id], context={})
            # We automatically reconcile the account move lines.
            reconcile = False
            for rec_ids in rec_list_ids:
                if len(rec_ids) >= 2:
                    reconcile = move_line_pool.reconcile_partial(cr, user, rec_ids, context=ctx, writeoff_acc_id=voucher.writeoff_acc_id.id, writeoff_period_id=voucher.period_id.id, writeoff_journal_id=voucher.journal_id.id)
            if move_line_id not in rec_list_ids:
                rec_list_ids.append(move_line_id)

            if voucher.type == 'payment' and cr.dbname in ('PB1', 'PEBO', 'PB', 'OUTLET', 'LCD', 'LOE CUAN DULUAN'):
                for aml_ids in rec_list_ids:
                    for aml in  move_line_pool.browse(cr, uid, aml_ids, context=context):
                        if aml.journal_id.type == 'bank' and \
                                (not currency_pool.is_zero(cr, uid, voucher.currency_id, aml.credit)) and \
                                    (aml.account_id.code[0:6]=='111120' or aml.account_id.parent_id.code in
                                        ('111120.01','111120.02','111120.03','111120.04','111120.05')):
                            hutang_cbg_pst_acc_id = account_obj.search(cr, uid, ['|',('code', 'like', '%121500.00.01%'),
                                                    ('name', 'like', '%Hutang Cabang ke Pusat%')], limit=1)[0]
                            analytic_cbg = analytic_cbg_obj.search(cr, uid, [('name', '=', warehouse_id),
                                                ('account_id', '=', hutang_cbg_pst_acc_id)], limit=1)
                            hutang_cbg_pst_analytic = analytic_cbg_obj.browse(cr, uid, analytic_cbg and analytic_cbg[0], context=context).analytic_id.id
                            if hutang_cbg_pst_acc_id and aml.move_id:
                                move_line_pool.create(cr, user, {
                                        'journal_id': aml.journal_id.id or voucher.journal_id.id,
                                        'period_id': aml.period_id.id or voucher.period_id.id,
                                        'name': 'Hutang Cabang ke Pusat [autocreate]',
                                        'account_id': hutang_cbg_pst_acc_id,
                                        'move_id': aml.move_id and aml.move_id.id,
                                        'partner_id': aml.partner_id.id or voucher.partner_id.id,
                                        'currency_id': aml.currency_id.id or False,
                                        'analytic_account_id': hutang_cbg_pst_analytic or False,
                                        'quantity': 1,
                                        'credit': aml.credit,
                                        'debit': 0.0,
                                        'date': aml.date or voucher.date,
                                        'warehouse_id': warehouse_id,
                                }, local_context)
                        elif aml.journal_id.type=='bank' and (not currency_pool.is_zero(cr, uid, voucher.currency_id, aml.debit)) and (aml.account_id.code[0:9] in ('121400.01','121400.02','121400.03','121400.04','121400.05') or aml.account_id.code=='121210.00.02'):
                            #aml.account_id.code=='121210.00.02' or aml.account_id.name=='Hutang Usaha ke Supplier Sudah Ditagih'
                            piutang_account_id = account_obj.search(cr, uid, ['|',('code','like','%111211.00.08%'),
                                                    ('name','like','%Piutang Antar Kantor%')], limit=1)[0]
                            analytic_cbg = analytic_cbg_obj.search(cr, uid, [('name','=',1),
                                                ('account_id','=',piutang_account_id)], limit=1)
                            piutang_analytic_id = analytic_cbg_obj.browse(cr,uid,analytic_cbg and analytic_cbg[0],context=context).analytic_id.id
                            if piutang_account_id and aml.move_id:
                                move_line_pool.create(cr, user, {
                                        'journal_id': aml.journal_id.id or voucher.journal_id.id,
                                        'period_id': aml.period_id.id or voucher.period_id.id,
                                        'name': 'Piutang Pusat ke Cabang [autocreate]',
                                        'account_id': piutang_account_id,
                                        'move_id': aml.move_id and aml.move_id.id,
                                        'partner_id': aml.partner_id.id or voucher.partner_id.id,
                                        'currency_id': aml.currency_id.id or False,
                                        'analytic_account_id': piutang_analytic_id or False,
                                        'quantity': 1,
                                        'credit': 0.0,
                                        'debit': aml.debit,
                                        'date': aml.date or voucher.date,
                                        'warehouse_id': warehouse_id,
                                }, local_context)
        return True

    def voucher_move_line_create(self, cr, uid, voucher_id, line_total, move_id, company_currency, current_currency, warehouse_id, context=None):
        if context is None:
            context = {}
        move_line_obj = self.pool.get('account.move.line')
        currency_obj = self.pool.get('res.currency')
        tax_obj = self.pool.get('account.tax')
        tot_line = line_total
        rec_lst_ids = []

        date = self.read(cr, uid, [voucher_id], ['date'], context=context)[0]['date']
        ctx = context.copy()
        ctx.update({'date': date})
        voucher = self.pool.get('account.voucher').browse(cr, uid, voucher_id, context=ctx)
        voucher_currency = voucher.journal_id.currency or voucher.company_id.currency_id
        ctx.update({
            'voucher_special_currency_rate': voucher_currency.rate * voucher.payment_rate ,
            'voucher_special_currency': voucher.payment_rate_currency_id and voucher.payment_rate_currency_id.id or False,})
        prec = self.pool.get('decimal.precision').precision_get(cr, uid, 'Account')
        for line in voucher.line_ids:
            if not line.amount and not (line.move_line_id and not float_compare(line.move_line_id.debit, line.move_line_id.credit, precision_digits=prec) and not float_compare(line.move_line_id.debit, 0.0, precision_digits=prec)):
                continue

            amount = self._convert_amount(cr, uid, line.untax_amount or line.amount, voucher.id, context=ctx)
            if line.amount == line.amount_unreconciled:
                if not line.move_line_id:
                    raise osv.except_osv(_('Wrong voucher line'),_("The invoice you are willing to pay is not valid anymore."))
                sign = line.type =='dr' and -1 or 1
                currency_rate_difference = sign * (line.move_line_id.amount_residual - amount)
            else:
                currency_rate_difference = 0.0
            move_line = {
                'journal_id': voucher.journal_id.id,
                'period_id': voucher.period_id.id,
                'name': line.name or '/',
                'account_id': line.account_id.id,
                'move_id': move_id,
                'partner_id': voucher.partner_id.id,
                'currency_id': line.move_line_id and (company_currency <> line.move_line_id.currency_id.id and line.move_line_id.currency_id.id) or False,
                'analytic_account_id': line.account_analytic_id and line.account_analytic_id.id or False,
                'quantity': 1,
                'credit': 0.0,
                'debit': 0.0,
                'date': voucher.date,
            }
            if amount < 0:
                amount = -amount
                if line.type == 'dr':
                    line.type = 'cr'
                else:
                    line.type = 'dr'

            if (line.type=='dr'):
                tot_line += amount
                move_line['debit'] = amount
            else:
                tot_line -= amount
                move_line['credit'] = amount

            if voucher.tax_id and voucher.type in ('sale', 'purchase'):
                move_line.update({
                    'account_tax_id': voucher.tax_id.id,
                })

            # compute the amount in foreign currency
            foreign_currency_diff = 0.0
            amount_currency = False
            if line.move_line_id:
                # We want to set it on the account move line as soon as the original line had a foreign currency
                if line.move_line_id.currency_id and line.move_line_id.currency_id.id != company_currency:
                    # we compute the amount in that foreign currency.
                    if line.move_line_id.currency_id.id == current_currency:
                        # if the voucher and the voucher line share the same currency, there is no computation to do
                        sign = (move_line['debit'] - move_line['credit']) < 0 and -1 or 1
                        amount_currency = sign * (line.amount)
                    else:
                        amount_currency = currency_obj.compute(cr, uid, company_currency, line.move_line_id.currency_id.id, move_line['debit']-move_line['credit'], context=ctx)
                if line.amount == line.amount_unreconciled:
                    foreign_currency_diff = line.move_line_id.amount_residual_currency - abs(amount_currency)

            move_line['amount_currency'] = amount_currency
            if warehouse_id:
                move_line['warehouse_id'] = warehouse_id or False
            voucher_line = move_line_obj.create(cr, uid, move_line)
            rec_ids = [voucher_line, line.move_line_id.id]

            if not currency_obj.is_zero(cr, uid, voucher.company_id.currency_id, currency_rate_difference):
                # Change difference entry in company currency
                exch_lines = self._get_exchange_lines(cr, uid, line, move_id, currency_rate_difference, company_currency, current_currency, context=context)
                if warehouse_id:
                    exch_lines[0]['warehouse_id'] = warehouse_id or False
                    exch_lines[1]['warehouse_id'] = warehouse_id or False
                new_id = move_line_obj.create(cr, uid, exch_lines[0],context)
                move_line_obj.create(cr, uid, exch_lines[1], context)
                rec_ids.append(new_id)

            if line.move_line_id and line.move_line_id.currency_id and not currency_obj.is_zero(cr, uid, line.move_line_id.currency_id, foreign_currency_diff):
                # Change difference entry in voucher currency
                move_line_foreign_currency = {
                    'journal_id': line.voucher_id.journal_id.id,
                    'period_id': line.voucher_id.period_id.id,
                    'name': _('change')+': '+(line.name or '/'),
                    'account_id': line.account_id.id,
                    'move_id': move_id,
                    'partner_id': line.voucher_id.partner_id.id,
                    'currency_id': line.move_line_id.currency_id.id,
                    'amount_currency': (-1 if line.type == 'cr' else 1) * foreign_currency_diff,
                    'quantity': 1,
                    'credit': 0.0,
                    'debit': 0.0,
                    'date': line.voucher_id.date,
                    'warehouse_id': warehouse_id or False,
                }
                new_id = move_line_obj.create(cr, uid, move_line_foreign_currency, context=context)
                rec_ids.append(new_id)
            if line.move_line_id.id:
                rec_lst_ids.append(rec_ids)
        return (tot_line, rec_lst_ids)



class InheritAccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice=False,
            payment_term=False, partner_bank_id=False, company_id=False, warehouse_id=False):
        account_id = False
        payment_term_id = False
        fiscal_position = False
        bank_id = False

        p = self.env['res.partner'].browse(partner_id or False)
        if partner_id:
            rec_account = p.property_account_receivable
            pay_account = p.property_account_payable
            if company_id:
                if p.property_account_receivable.company_id and \
                        p.property_account_receivable.company_id.id != company_id and \
                        p.property_account_payable.company_id and \
                        p.property_account_payable.company_id.id != company_id:
                    prop = self.env['ir.property']
                    rec_dom = [('name', '=', 'property_account_receivable'), ('company_id', '=', company_id)]
                    pay_dom = [('name', '=', 'property_account_payable'), ('company_id', '=', company_id)]
                    res_dom = [('res_id', '=', 'res.partner,%s' % partner_id)]
                    rec_prop = prop.search(rec_dom + res_dom) or prop.search(rec_dom)
                    pay_prop = prop.search(pay_dom + res_dom) or prop.search(pay_dom)
                    rec_account = rec_prop.get_by_record(rec_prop)
                    pay_account = pay_prop.get_by_record(pay_prop)
                    if not rec_account and not pay_account:
                        action = self.env.ref('account.action_account_config')
                        msg = _('Cannot find a chart of accounts for this company, You should configure it. \nPlease go to Account Configuration.')
                        raise RedirectWarning(msg, action.id, _('Go to the configuration panel'))

            if type in ('out_invoice', 'out_refund'):
                account_id = rec_account.id
                payment_term_id = p.property_payment_term.id
            else:
                account_id = pay_account.id
                payment_term_id = p.property_supplier_payment_term.id
            fiscal_position = p.property_account_position.id

        result = {'value': {
            'account_id': account_id,
            'payment_term': payment_term_id,
            'fiscal_position': fiscal_position,
        }}

        if type in ('in_invoice', 'out_refund'):
            bank_ids = p.commercial_partner_id.bank_ids
            bank_id = bank_ids[0].id if bank_ids else False
            result['value']['partner_bank_id'] = bank_id
            result['domain'] = {'partner_bank_id':  [('id', 'in', bank_ids.ids)]}

        if p.code:
            outlet_id = self.env['stock.warehouse'].search(['|', ('code', 'like', '%' + _(p.code) + '%'),
                            ('partner_id', '=', partner_id)], limit=1).id
            if outlet_id and (warehouse_id != outlet_id):
                result['value']['warehouse_id'] = outlet_id

        if payment_term != payment_term_id:
            if payment_term_id:
                to_update = self.onchange_payment_term_date_invoice(payment_term_id, date_invoice)
                result['value'].update(to_update.get('value', {}))
            else:
                result['value']['date_due'] = False

        if partner_bank_id != bank_id:
            to_update = self.onchange_partner_bank(bank_id)
            result['value'].update(to_update.get('value', {}))

        return result

    @api.multi
    def action_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        account_invoice_tax = self.env['account.invoice.tax']
        account_move = self.env['account.move']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise except_orm(_('Error!'), _('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line:
                raise except_orm(_('No Invoice Lines!'), _('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            company_currency = inv.company_id.currency_id
            if not inv.date_invoice:
                # FORWARD-PORT UP TO SAAS-6
                if inv.currency_id != company_currency and inv.tax_line:
                    raise except_orm(
                        _('Warning!'),
                        _('No invoice date!'
                            '\nThe invoice currency is not the same than the company currency.'
                            ' An invoice date is required to determine the exchange rate to apply. Do not forget to update the taxes!'
                        )
                    )
                inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
            date_invoice = inv.date_invoice

            # create the analytical lines, one move line per invoice line
            iml = inv._get_analytic_lines()
            # check if taxes are all computed
            compute_taxes = account_invoice_tax.compute(inv.with_context(lang=inv.partner_id.lang))
            inv.check_tax_lines(compute_taxes)

            # I disabled the check_total feature
            if self.env.user.has_group('account.group_supplier_inv_check_total'):
                if inv.type in ('in_invoice', 'in_refund') and abs(inv.check_total - inv.amount_total) >= (inv.currency_id.rounding / 2.0):
                    raise except_orm(_('Bad Total!'), _('Please verify the price of the invoice!\nThe encoded total does not match the computed total.'))

            if inv.payment_term:
                total_fixed = total_percent = 0
                for line in inv.payment_term.line_ids:
                    if line.value == 'fixed':
                        total_fixed += line.value_amount
                    if line.value == 'procent':
                        total_percent += line.value_amount
                total_fixed = (total_fixed * 100) / (inv.amount_total or 1.0)
                if (total_fixed + total_percent) > 100:
                    raise except_orm(_('Error!'), _("Cannot create the invoice.\nThe related payment term is probably misconfigured as it gives a computed amount greater than the total invoiced amount. In order to avoid rounding issues, the latest line of your payment term must be of type 'balance'."))

            # Force recomputation of tax_amount, since the rate potentially changed between creation
            # and validation of the invoice
            inv._recompute_tax_amount()
            # one move line per tax line
            iml += account_invoice_tax.move_line_get(inv.id)

            if inv.type in ('in_invoice', 'in_refund'):
                ref = inv.reference
            else:
                ref = inv.number

            diff_currency = inv.currency_id != company_currency
            # create one move line for the total and possibly adjust the other lines amount
            total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, ref, iml)

            name = inv.supplier_invoice_number or inv.name or '/'
            totlines = []
            if inv.payment_term:
                totlines = inv.with_context(ctx).payment_term.compute(total, date_invoice)[0]
            if totlines:
                res_amount_currency = total_currency
                ctx['date'] = date_invoice
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency

                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': inv.account_id.id,
                        'date_maturity': t[0],
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'ref': ref,
                    })
            else:
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': total,
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'ref': ref
                })

            date = date_invoice

            part = self.env['res.partner']._find_accounting_partner(inv.partner_id)

            line = [(0, 0, self.line_get_convert(l, part.id, date, inv.id)) for l in iml]
            line = inv.group_lines(iml, line)

            journal = inv.journal_id.with_context(ctx)
            if journal.centralisation:
                raise except_orm(_('User Error!'),
                        _('You cannot create an invoice on a centralized journal. Uncheck the centralized counterpart box in the related journal from the configuration menu.'))

            line = inv.finalize_invoice_move_lines(line)

            move_vals = {
                'ref': inv.reference or inv.name,
                'line_id': line,
                'journal_id': journal.id,
                'date': inv.date_invoice,
                'narration': inv.comment,
                'company_id': inv.company_id.id,
            }
            ctx['company_id'] = inv.company_id.id
            period = inv.period_id
            if not period:
                period = period.with_context(ctx).find(date_invoice)[:1]
            if period:
                move_vals['period_id'] = period.id
                for i in line:
                    i[2]['period_id'] = period.id

            ctx['invoice'] = inv
            ctx_nolang = ctx.copy()
            ctx_nolang.pop('lang', None)
            move = account_move.with_context(ctx_nolang).create(move_vals)

            # make the invoice point to that move
            vals = {
                'move_id': move.id,
                'period_id': period.id,
                'move_name': move.name,
            }
            inv.with_context(ctx).write(vals)
            # Pass invoice in context in method post: used if you want to get the same
            # account move reference when creating the same invoice after a cancelled one:
            move.post()
        self._log_event()
        return True

    @api.model
    def line_get_convert(self, line, part, date, inv_id):
        analytic_cbg_obj = self.env['analytic.cabang']
        analytic_id = line.get('account_analytic_id', False)
        invoice = self.env['account.invoice'].browse(inv_id)
        if invoice.warehouse_id:
            warehouse_id = invoice.warehouse_id.id
        elif invoice.user_id:
            warehouse_id = self.env['res.users'].browse(invoice.user_id.id).warehouse_id.id
        else:
            warehouse_id = self.env['res.users'].browse(self._uid).warehouse_id.id or False
        # acc_except_code = ('121210.00.02','121400.02.04','111500.02.03','111500.02.02',)
        # acc_except_name = ('Hutang Usaha ke Supplier Sudah Ditagih','Hutang Internet','Asuransi CGL',)
        if (not analytic_id) and line['account_id'] and warehouse_id:
            account = self.env['account.account'].browse(line['account_id'])
            # warehouse_id = invoice.warehouse_id and invoice.warehouse_id.id or False
            analytic_cbg_ids = analytic_cbg_obj.search([('name', '=', warehouse_id), ('account_id', '=', account.id)], limit=1)
            analytic_cbg = analytic_cbg_obj.browse(analytic_cbg_ids and analytic_cbg_ids.id)
            analytic_id = analytic_cbg.analytic_id and analytic_cbg.analytic_id.id or False
        return {
            'date_maturity': line.get('date_maturity', False),
            'partner_id': part,
            'name': line['name'][:64],
            'date': date,
            'debit': line['price']>0 and line['price'],
            'credit': line['price']<0 and -line['price'],
            'account_id': line['account_id'],
            'analytic_lines': line.get('analytic_lines', []),
            'amount_currency': line['price']>0 and abs(line.get('amount_currency', False)) or -abs(line.get('amount_currency', False)),
            'currency_id': line.get('currency_id', False),
            'tax_code_id': line.get('tax_code_id', False),
            'tax_amount': line.get('tax_amount', False),
            'ref': line.get('ref', False),
            'quantity': line.get('quantity',1.00),
            'product_id': line.get('product_id', False),
            'product_uom_id': line.get('uos_id', False),
            'analytic_account_id': analytic_id,
            'warehouse_id': warehouse_id or False,
        }



class AccountInvoiceCancelled(osv.osv_memory):
    _inherit = "account.invoice.cancel"
    _description = "Cancel the Selected Invoices"



class AccountInvoiceActionSetToDraft(osv.osv_memory):
    _name = "account.invoice.set.draft"

    def set_to_draft_invoices(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        proxy = self.pool['account.invoice']
        active_ids = context.get('active_ids', []) or []

        for record in proxy.browse(cr, uid, active_ids, context=context):
            if record.state not in ('cancel','draft'):
                raise osv.except_osv(_('Warning!'), _("Invoice state cannot be changed to draft."))
            proxy.write(cr, uid, ids, {'state': 'draft'})
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_delete(uid, 'account.invoice', record.id, cr)
            wf_service.trg_create(uid, 'account.invoice', record.id, cr)
        return {'type': 'ir.actions.act_window_close'}


class account_account(osv.osv):
    _inherit = "account.account"
    _description = "Account"

    def create(self, cr, uid, vals, context=None):
        if self.pool['res.partner'].user_access_validation(cr, uid, [], context=context):
            raise osv.except_osv(_('Perhatian !!!'),
                                 _('Akses anda terbatas. Anda tidak bisa membuat data Account baru!'))
        return super(account_account, self).create(cr, uid, vals, context)

    def write(self, cr, uid, ids, vals, context=None):
        if self.pool['res.partner'].user_access_validation(cr, uid, ids, context=context):
            raise osv.except_osv(_('Perhatian !!!'),
                                 _('Akses anda terbatas. Anda tidak bisa mengubah/mengedit data Account ini!'))
        return super(account_account, self).write(cr, uid, ids, vals, context=context)

    def unlink(self, cr, uid, ids, context=None):
        self._check_moves(cr, uid, ids, "unlink", context=context)
        if self.pool['res.partner'].user_access_validation(cr, uid, ids, context=context):
            raise osv.except_osv(_('Perhatian !!!'),
                                 _('Akses anda terbatas. Anda tidak bisa menghapus data Account ini!'))
        return super(account_account, self).unlink(cr, uid, ids, context=context)



class account_analytic_account(osv.osv):
    _inherit = "account.analytic.account"
    _description = "Account"

    def create(self, cr, uid, vals, context=None):
        if self.pool['res.partner'].user_access_validation(cr, uid, [], context=context):
            raise osv.except_osv(_('Perhatian !!!'),
                                 _('Akses anda terbatas. Anda tidak bisa membuat data Analytic Account baru!'))
        return super(account_analytic_account, self).create(cr, uid, vals, context)

    def write(self, cr, uid, ids, vals, context=None):
        if self.pool['res.partner'].user_access_validation(cr, uid, ids, context=context):
            raise osv.except_osv(_('Perhatian !!!'),
                                 _('Akses anda terbatas. Anda tidak bisa mengubah/mengedit data Analytic Account ini!'))
        return super(account_analytic_account, self).write(cr, uid, ids, vals, context=context)

    def unlink(self, cr, uid, ids, context=None):
        if self.pool['res.partner'].user_access_validation(cr, uid, ids, context=context):
            raise osv.except_osv(_('Perhatian !!!'),
                                 _('Akses anda terbatas. Anda tidak bisa menghapus data Analytic Account ini!'))
        return super(account_analytic_account, self).unlink(cr, uid, ids, context=context)


