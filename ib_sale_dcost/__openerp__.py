# -*- coding: utf-8 -*-
####################################################################################################
#    Copyright (c) 2016 - Create by Ibrohim Binladin (ibradiiin@gmail.com | +62-838 7190 9782)
####################################################################################################
{
    "name": "Custom Module - Sales, Acct, Purchase",
    "version": "0.1",
    "depends": ["base", "sale", "account",
                "siu_work_order", "aa_analytic_otomatis",
                "siu_import_pos", "ib_reports", "stock_account",
                "account_voucher", "ib_sequence_number",
                "ib_duplicate_purchase_sale", "account_asset_management_bg", 
                "ib_pricelist_warehouse"],
    "author": "Ibrohim Binladin - 083871909782 [ ibradiiin@gmail.com ]",
    "category":"Custom Modules",
    "description": """ custom modules to make sales orders and work orders based on POS data """,
    "init_xml": [],
    'data': [
        "security/rules.xml",
        "security/ir.model.access.csv",
        "data/decimal_precision.xml",
        "views/sales_data.xml",
        "wizard/menu_ranking_info.xml",
        "views/view.xml",
        "wizard/schedulers_all_view.xml",
        "views/wizard_view.xml",
        "views/res_partner.xml",
        # "wizard/pos_make_wo.xml",
        # "wizard/pos_line_to_order.xml",
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
    'auto_install': False,
    "application": True,
}
