# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
from openerp.osv import fields, osv
from openerp.tools.translate import _

class res_partner_bank(osv.osv):
    _inherit = "res.partner.bank"
    _columns = {
        'active': fields.boolean('Active'),
    }
    _defaults = {
        'active': lambda *a: 1,
    }


class purchase_order(osv.osv):
    _inherit = "purchase.order"

    def _prepare_invoice(self, cr, uid, order, line_ids, context=None):
        journal_ids = self.pool['account.journal'].search(
            cr, uid, [('type', '=', 'purchase'),
                      ('company_id', '=', order.company_id.id)],
            limit=1)
        if not journal_ids:
            raise osv.except_osv(
                _('Error!'),
                _('Define purchase journal for this company: "%s" (id:%d).') % \
                (order.company_id.name, order.company_id.id))
        bank_acc_ids = self.pool['res.partner.bank'].search(cr, uid,
                        [('active','=',True),('partner_id','=',order.partner_id.id)],
                        order='create_date', limit=1)
        return {
            'name': order.partner_ref or order.name,
            'reference': order.partner_ref or order.name,
            'account_id': order.partner_id.property_account_payable.id,
            'type': 'in_invoice',
            'partner_id': order.partner_id.id,
            'currency_id': order.currency_id.id,
            'journal_id': len(journal_ids) and journal_ids[0] or False,
            'invoice_line': [(6, 0, line_ids)],
            'origin': order.name,
            'fiscal_position': order.fiscal_position.id or False,
            'payment_term': order.payment_term_id.id or False,
            'company_id': order.company_id.id,
            'partner_bank_id': len(bank_acc_ids) and bank_acc_ids[0] or False,
            'round_off': order.round_off
        }

    def action_invoice_cancel(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'except_invoice'}, context=context)
        return True


