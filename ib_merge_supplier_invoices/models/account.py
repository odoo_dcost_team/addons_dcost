# -*- coding: utf-8 -*-
##############################################################################

#from openerp import models, fields, api, _
#from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
from openerp import netsvc

#class account_invoice(models.Model):
class account_invoice(osv.osv):
    _inherit = "account.invoice"

    # purchase_ids = fields.Many2many('purchase.order', 'purchase_invoice_rel',
    #         'invoice_id', 'purchase_id', string='Purchases', copy=False)
    _columns = {
        'purchase_ids': fields.many2many('purchase.order', 'purchase_invoice_rel',
                    'invoice_id', 'purchase_id', 'Purchases'),
    }

    def action_cancel(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        account_move_obj = self.pool.get('account.move')
        purchase_obj = self.pool.get('purchase.order')
        move_obj = self.pool.get('stock.move')
        invoices = self.read(cr, uid, ids, ['id', 'type', 'move_id', 'payment_ids'])
        move_ids, purchase_ids = [], [] # ones that we will need to remove
        for i in invoices:
            if i['move_id']:
                move_ids.append(i['move_id'][0])
            if i['payment_ids']:
                account_move_line_obj = self.pool.get('account.move.line')
                pay_ids = account_move_line_obj.browse(cr, uid, i['payment_ids'])
                for move_line in pay_ids:
                    if move_line.reconcile_partial_id and move_line.reconcile_partial_id.line_partial_ids:
                        raise osv.except_osv(_('Error!'), _('You cannot cancel an invoice which is partially paid. You need to unreconcile related payment entries first.'))
            if i['type'] in ('in_invoice', 'in_refund'):
                cr.execute("SELECT purchase_id FROM purchase_invoice_rel WHERE invoice_id=%s", (i['id'],))
                for pid in cr.fetchall():
                    purchase = purchase_obj.browse(cr, uid, pid[0])
                    if purchase.invoice_method in ('manual','picking'):
                        for pol in purchase.order_line:
                            self.pool['purchase.order.line'].write(cr, uid, [pol.id], {'invoiced': False})
                            cr.execute("DELETE FROM purchase_order_line_invoice_rel WHERE order_line_id=%s", (pol.id,))
                            if purchase.invoice_method == 'picking':
                                pol_move_ids = move_obj.search(cr, uid, [('purchase_line_id','=',pol.id)], context=context)
                                if pol_move_ids:
                                    move_obj.write(cr, uid, pol_move_ids, {'invoice_state': '2binvoiced'}, context=context)
                        purchase_obj.write(cr, uid, [purchase.id], {'state': 'except_invoice'}, context=context)
                        purchase_ids.append(purchase.id)
                if purchase_ids:
                    cr.execute("DELETE FROM purchase_invoice_rel WHERE invoice_id=%s AND purchase_id IN %s", (i['id'],tuple(purchase_ids),))

        # First, set the invoices as cancelled and detach the move ids
        self.write(cr, uid, ids, {'state':'cancel', 'move_id':False})
        if move_ids:
            account_move_obj.button_cancel(cr, uid, move_ids, context=context)
            account_move_obj.unlink(cr, uid, move_ids, context=context)
        self._log_event(cr, uid, ids, -1.0, 'Cancel Invoice')
        return True

    def action_cancel_draft(self, cr, uid, ids, *args):
        purchase_obj = self.pool.get('purchase.order')
        self.write(cr, uid, ids, {'state':'draft'})
        wf_service = netsvc.LocalService("workflow")
        for invoice in self.browse(cr, uid, ids):
            if invoice.type in ('in_invoice', 'in_refund'):
                cr.execute("SELECT purchase_id FROM purchase_invoice_rel WHERE invoice_id=%s", (invoice.id,))
                for pid in cr.fetchall():
                    purchase = purchase_obj.browse(cr, uid, pid[0])
                    if purchase.state == 'except_invoice':
                        purchase_obj.write(cr, uid, [pid[0]], {'state': 'approved'})
        for inv_id in ids:
            wf_service.trg_delete(uid, 'account.invoice', inv_id, cr)
            wf_service.trg_create(uid, 'account.invoice', inv_id, cr)
        return True



class account_invoice_line(osv.osv):
    _inherit = "account.invoice.line"

    def unlink(self, cr, uid, ids, context=None):
        move_obj = self.pool.get('stock.move')
        pol_obj = self.pool.get('purchase.order.line')
        purchase_ids = []
        for line in self.browse(cr, uid, ids, context=context):
            if line.invoice_id.state in ['open', 'paid']:
                raise osv.except_osv(_('Invalid Action!'), _('Cannot delete a invoice line with invoice status \'%s\'.') %(line.invoice_id.state,))
            if line.invoice_id.type in ('in_invoice', 'in_refund'):
                cr.execute("SELECT order_line_id FROM purchase_order_line_invoice_rel WHERE invoice_id=%s", (line.id,))
                for pol in cr.fetchall():
                    pol_data = pol_obj.browse(cr, uid, pol[0])
                    if pol_data.order_id and pol_data.order_id.invoice_method in ('manual', 'picking'):
                        pol_obj.write(cr, uid, [pol_data.id], {'invoiced': False})
                        cr.execute("DELETE FROM purchase_order_line_invoice_rel WHERE order_line_id=%s", (pol_data.id,))
                        pol_move_ids = move_obj.search(cr, uid, [('purchase_line_id','=',pol_data.id)], context=context)
                        if pol_move_ids:
                            move_obj.write(cr, uid, pol_move_ids, {'invoice_state': '2binvoiced'}, context=context)
                        purchase_ids.append(pol_data.order_id.id)
            if purchase_ids and line.invoice_id.id:
                cr.execute("DELETE FROM purchase_invoice_rel WHERE invoice_id=%s AND purchase_id IN %s", (line.invoice_id.id, tuple(purchase_ids),))
        return super(account_invoice_line, self).unlink(cr, uid, ids, context=context)



class account_journal(osv.osv):
    _inherit = "account.journal"

    _columns = {
        'categ_journal': fields.selection([('hpp', 'HPP 8893'),('non_hpp', 'Non-HPP 9337')], 'Category Journal'),
    }

    def name_get(self, cr, user, ids, context=None):
        if not ids:
            return []
        if isinstance(ids, (int, long)):
            ids = [ids]
        result = self.browse(cr, user, ids, context=context)
        res = []
        for rs in result:
            if rs.currency:
                currency = rs.currency
            else:
                currency = rs.company_id.currency_id
            categ = ''
            if rs.categ_journal=='hpp':
                categ = 'HPP'
            elif rs.categ_journal=='non_hpp':
                categ = 'Non-HPP'
            name = "%s (%s) %s" % (rs.name, currency.name, categ)
            res += [(rs.id, name)]
        return res

