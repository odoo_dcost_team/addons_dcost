# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

class stock_invoice_onshipping(osv.osv_memory):

    def _get_overwrite_supplier(self, cr, uid, context=None):
        active_ids = context and context.get('active_ids', [])
        if len(active_ids) > 1:
            return True
        else:
            return False
        # partner_ids = []
        # for picking in self.pool.get('stock.picking').browse(cr, uid, actv_ids, context=context):
        #     if picking.partner_id and (picking.partner_id.id not in partner_ids):
        #         partner_ids.append(picking.partner_id.id)
        # if len(partner_ids)!=1:
        #     return True
        # else:
        #     return False

    def _get_supplier(self, cr, uid, context=None):
        actv_ids = context and context.get('active_ids', [])
        partner_ids = []
        for picking in self.pool.get('stock.picking').browse(cr, uid, actv_ids, context=context):
            if picking.partner_id and (picking.partner_id.id not in partner_ids):
                partner_ids.append(picking.partner_id.id)
        if len(partner_ids)==1:
            return partner_ids and partner_ids[0] or False
        else:
            return False

    def _check_moving_lines(self, cr, uid, context):
        if context.get('active_model', '') == 'stock.picking':
            ids = context['active_ids']
            if len(ids) > 1:
                picking_obj = self.pool.get('stock.picking')
                pickings = picking_obj.read(cr, uid, ids, ['state', 'company_id'])
                for sp in pickings:
                    if sp['state'] != 'done':
                        raise orm.except_orm(_('Warning'), _('At least one of the selected picking is %s! \nPicking status allowed is Transferred [Done].') % sp['state'])
                    if (sp['company_id'] != pickings[0]['company_id']):
                        raise orm.except_orm(_('Warning'), _('Not all Quotation Lines are at the same company!'))
        return {}

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        if context is None:
            context = {}
        res = super(stock_invoice_onshipping, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=False)
        self._check_moving_lines(cr, uid, context)
        return res

    _inherit = "stock.invoice.onshipping"
    _columns = {
        'overwrite_partner': fields.boolean("Overwrite Supplier"),
        'supplier_id': fields.many2one('res.partner', 'Supplier'),
    }
    _defaults = {
        'overwrite_partner': _get_overwrite_supplier,
        'supplier_id': _get_supplier,
    }

    def create_invoice(self, cr, uid, ids, context=None):
        context = dict(context or {})
        picking_pool = self.pool.get('stock.picking')
        data = self.browse(cr, uid, ids[0], context=context)
        journal2type = {'sale':'out_invoice', 'purchase':'in_invoice', 'sale_refund':'out_refund', 'purchase_refund':'in_refund'}
        context['date_inv'] = data.invoice_date
        acc_journal = self.pool.get("account.journal")
        inv_type = journal2type.get(data.journal_type) or 'out_invoice'
        context['inv_type'] = inv_type

        active_ids = context.get('active_ids', [])
        res = picking_pool.action_invoice_create(cr, uid, active_ids,
              journal_id = data.journal_id.id,
              partner_id = data.supplier_id.id or False,
              group = data.group,
              type = inv_type,
              overwrite = data.overwrite_partner,
              context=context)
        return res


