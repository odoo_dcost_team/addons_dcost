# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

class purchase_advance_merge_inv(osv.osv_memory):
    _name = "purchase.order.make.merge.invoice"
    _description = "Purchases Make Merge Invoice"

    def _get_inv_control_method(self, cr, uid, context=None):
        actv_ids = context and context.get('active_ids', [])
        inv_method_list = {'manual': 0, 'order': 0, 'picking': 0}
        for po in self.pool.get('purchase.order').browse(cr, uid, actv_ids, context=context):
            inv_method_list[po.invoice_method] += 1

        val = str('')
        max_method = max(inv_method_list.values())
        for method in inv_method_list.keys():
            if inv_method_list[method] == max_method:
                val = str(method)
                break

        return val

    def _check_order_list(self, cr, uid, context):
        if context.get('active_model', '') == 'purchase.order':
            ids = context['active_ids']
            purchase_obj = self.pool.get('purchase.order')
            if ids and len(ids) > 0:
                for po in purchase_obj.browse(cr, uid, ids, context=context):
                    inv_line_ids = []
                    for line in po.order_line:
                        if line.state != 'cancel':
                            inv_line_ids.append(line.invoiced)
                    if all(inv_line_ids):
                        raise orm.except_orm(_('Warning'),
                            _("All lines of purchase have been invoiced. There must be at least one purchase line with status 'To Be Invoiced' or Uninvoiced."))
            if len(ids) > 1:
                purchases = purchase_obj.read(cr, uid, ids, ['state','company_id','picking_type_id','invoice_method'])
                for p in purchases:
                    if p['state'] not in('approved','except_invoice','except_picking'):
                        raise orm.except_orm(_('Warning'), _('At least one of the selected purchase order is %s! \nPurchase Order status allowed is Confirmed or Exception.') % p['state'])
                    if (p['company_id'] != purchases[0]['company_id']):
                        raise orm.except_orm(_('Warning'), _('Not all Purchase Order are at the same company!'))
                    if (p['picking_type_id'] != purchases[0]['picking_type_id']):
                        raise orm.except_orm(_('Warning'), _('Not all Purchase Order are for the same warehouse (outlet)!'))
                    if (p['invoice_method'] != purchases[0]['invoice_method']):
                        raise orm.except_orm(_('Warning'), _('Not all Purchase Order are for the same method (Invoicing Control)!'))
        return {}

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        if context is None:
            context = {}
        res = super(purchase_advance_merge_inv, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=False)
        self._check_order_list(cr, uid, context)
        return res

    _columns = {
        'inv_control': fields.char('Invoicing Control'),
    }
    _defaults = {
        'inv_control': _get_inv_control_method,
    }

    def act_view_moving_line(self, cr, uid, ids, context=None):
        purchase_obj = self.pool.get('purchase.order')
        purchase_ids = context.get('active_ids', [])
        data_wzd = self.browse(cr, uid, ids[0], context=context)

        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        result = {}
        if data_wzd.inv_control=='manual':
            result = mod_obj.get_object_reference(cr, uid, 'purchase', 'purchase_line_form_action2')
        elif data_wzd.inv_control=='picking':
            result = mod_obj.get_object_reference(cr, uid, 'ib_merge_supplier_invoices', 'action_view_picking_in_tobe_invoice')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]

        #compute the number of pickings to display
        todo_list = []
        for po in purchase_obj.browse(cr, uid, purchase_ids, context=context):
            if data_wzd.inv_control == 'manual':
                for oline in po.order_line:
                    if oline.state in ('confirmed','done') and oline.invoiced==False:
                        todo_list.append(oline.id)
            elif data_wzd.inv_control == 'picking':
                for picking in po.picking_ids:
                    if picking.invoice_state=='2binvoiced' and picking.state=='done':
                        todo_list.append(picking.id)

        if len(todo_list)>1:
            result['domain'] = "[('id','in',["+','.join(map(str, todo_list))+"])]"
        else:
            if data_wzd.inv_control == 'manual':
                res = mod_obj.get_object_reference(cr, uid, 'purchase', 'purchase_order_line_form2')
            elif data_wzd.inv_control == 'picking':
                res = mod_obj.get_object_reference(cr, uid, 'stock', 'view_picking_form')
            result['views'] = [(res and res[1] or False, 'form')]
            result['res_id'] = todo_list and todo_list[0] or False
        return result

