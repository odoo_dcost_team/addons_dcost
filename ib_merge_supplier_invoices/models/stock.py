# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
from openerp.osv import fields, osv
#from openerp.tools.translate import _

class stock_picking(osv.osv):
    _inherit = 'stock.picking'

    def action_invoice_create(self, cr, uid, ids, journal_id, partner_id, group=False, type='out_invoice', overwrite=False, context=None):
        context = context or {}
        todo = {}
        if not partner_id:
            partner_id = False
        for picking in self.browse(cr, uid, ids, context=context):
            partner = self._get_partner_to_invoice(cr, uid, picking, dict(context, type=type))
            #grouping is based on the invoiced partner
            if overwrite:
                key = partner_id
            elif group:
                key = partner
            else:
                key = picking.id

            for move in picking.move_lines:
                if move.invoice_state == '2binvoiced':
                    if (move.state != 'cancel') and not move.scrapped:
                        todo.setdefault(key, [])
                        todo[key].append(move)
        invoices = []
        for moves in todo.values():
            invoices += self._invoice_create_line(cr, uid, moves, journal_id, partner_id, type, context=context)
        return invoices

    def _get_invoice_vals(self, cr, uid, key, inv_type, journal_id, move, context=None):
        if context is None:
            context = {}
        partner, currency_id, company_id, user_id = key
        if inv_type in ('out_invoice', 'out_refund'):
            account_id = partner.property_account_receivable.id
            payment_term = partner.property_payment_term.id or False
        else:
            account_id = partner.property_account_payable.id
            payment_term = partner.property_supplier_payment_term.id or False
        outlet_ids = []
        partner_bank = False
        if move.picking_type_id and move.picking_type_id.warehouse_id:
            outlet_ids.append(move.picking_type_id.warehouse_id.id)
        elif move.purchase_line_id and move.purchase_line_id.order_id.picking_type_id and  move.purchase_line_id.order_id.picking_type_id.warehouse_id:
            outlet_ids.append(move.purchase_line_id.order_id.picking_type_id.warehouse_id.id)
        if outlet_ids:
            query = """
                    SELECT rpb.id FROM res_partner_bank rpb, res_partner_bank_warehouse_rel bwr
                        WHERE rpb.active=True and rpb.partner_id=%s and bwr.bank_id=rpb.id and bwr.warehouse_id in %s   
                        ORDER BY create_date DESC LIMIT 1
                    """
            cr.execute(query, (partner.id,tuple(outlet_ids)))
            banks = cr.fetchall()
            if banks:
                for bank_id in banks:
                    partner_bank = bank_id

        if (not partner_bank):
            bank_acc_ids = self.pool['res.partner.bank'].search(cr, uid,
                    [('active', '=', True),('partner_id', '=', partner.id)],
                    order='create_date', limit=1)
            partner_bank = len(bank_acc_ids) and bank_acc_ids[0]
        values = {
            'origin': move.picking_id.name,
            'date_invoice': context.get('date_inv', False),
            'user_id': user_id,
            'partner_id': partner.id,
            'account_id': account_id,
            'payment_term': payment_term,
            'type': inv_type,
            'fiscal_position': partner.property_account_position.id,
            'company_id': company_id,
            'currency_id': currency_id,
            'journal_id': journal_id,
            'partner_bank_id': partner_bank,
        }
        if not context.get('merging'):
            values.update({'round_off': move.purchase_line_id and move.purchase_line_id.order_id.round_off})
        return values


    def _invoice_create_line(self, cr, uid, moves, journal_id, partner_id, inv_type='out_invoice', context=None):
        invoice_obj = self.pool.get('account.invoice')
        move_obj = self.pool.get('stock.move')
        invoices = {}
        is_extra_move, extra_move_tax = move_obj._get_moves_taxes(cr, uid, moves, inv_type, context=context)
        product_price_unit = {}
        list_po = [m.purchase_line_id.order_id.name for m in moves if m.purchase_line_id]
        for move in moves:
            company = move.company_id
            origin = move.picking_id.name
            partner, user_id, currency_id = move_obj._get_master_data(cr, uid, move, company, context=context)

            if partner_id:
                partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
                key = (partner, currency_id, company.id, user_id)
            else:
                key = (partner, currency_id, company.id, user_id)
            if len(list_po)>1:
                context.update({'merging': True})
            else:
                context.update({'merging': False})
            invoice_vals = self._get_invoice_vals(cr, uid, key, inv_type, journal_id, move, context=context)

            if key not in invoices:
                # Get account and payment terms
                invoice_id = self._create_invoice_from_picking(cr, uid, move.picking_id, invoice_vals, context=context)
                invoices[key] = invoice_id
            else:
                invoice = invoice_obj.browse(cr, uid, invoices[key], context=context)
                merge_vals = {}
                if not invoice.origin or invoice_vals['origin'] not in invoice.origin.split(', '):
                    invoice_origin = filter(None, [invoice.origin, invoice_vals['origin']])
                    merge_vals['origin'] = ', '.join(invoice_origin)
                if invoice_vals.get('name', False) and (not invoice.name or invoice_vals['name'] not in invoice.name.split(', ')):
                    invoice_name = filter(None, [invoice.name, invoice_vals['name']])
                    merge_vals['name'] = ', '.join(invoice_name)
                if merge_vals:
                    invoice.write(merge_vals)
            invoice_line_vals = move_obj._get_invoice_line_vals(cr, uid, move, partner, inv_type, context=dict(context, fp_id=invoice_vals.get('fiscal_position', False)))
            invoice_line_vals['invoice_id'] = invoices[key]
            invoice_line_vals['origin'] = origin
            if not is_extra_move[move.id]:
                product_price_unit[invoice_line_vals['product_id'], invoice_line_vals['uos_id']] = invoice_line_vals['price_unit']
            if is_extra_move[move.id] and (invoice_line_vals['product_id'], invoice_line_vals['uos_id']) in product_price_unit:
                invoice_line_vals['price_unit'] = product_price_unit[invoice_line_vals['product_id'], invoice_line_vals['uos_id']]
            if is_extra_move[move.id]:
                desc = (inv_type in ('out_invoice', 'out_refund') and move.product_id.product_tmpl_id.description_sale) or \
                    (inv_type in ('in_invoice','in_refund') and move.product_id.product_tmpl_id.description_purchase)
                invoice_line_vals['name'] += ' ' + desc if desc else ''
                if extra_move_tax[move.picking_id, move.product_id]:
                    invoice_line_vals['invoice_line_tax_id'] = extra_move_tax[move.picking_id, move.product_id]
                #the default product taxes
                elif (0, move.product_id) in extra_move_tax:
                    invoice_line_vals['invoice_line_tax_id'] = extra_move_tax[0, move.product_id]

            move_obj._create_invoice_line_from_vals(cr, uid, move, invoice_line_vals, context=context)
            move_obj.write(cr, uid, move.id, {'invoice_state': 'invoiced'}, context=context)

        invoice_obj.button_compute(cr, uid, invoices.values(), context=context, set_total=(inv_type in ('in_invoice', 'in_refund')))
        return invoices.values()



class stock_move(osv.osv):
    _inherit = "stock.move"

    def _get_master_data(self, cr, uid, move, company, context=None):
        partner, user_id, currency = super(stock_move, self)._get_master_data(cr, uid, move, company, context=context)
        if move.purchase_line_id:
            user_id = move.purchase_line_id and move.purchase_line_id.order_id and move.purchase_line_id.order_id.user_id.id
        return partner, user_id, currency

