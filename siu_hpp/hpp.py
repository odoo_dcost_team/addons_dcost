import csv
import time
import base64
import tempfile
import cStringIO
from lxml import etree
from dateutil import parser
from openerp import models, api
from openerp.osv import fields, osv, orm
import openerp.addons.decimal_precision as dp
from datetime import date, datetime, timedelta
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from reportlab.lib.units import pica

allowed_users = ['admin', 'admpurchase', 'BomAdmin']

class stock_picking(osv.osv):
    _inherit = 'stock.picking' 

    def action_setdraft(self, cr, uid, ids, context=None):
        for pick in self.browse(cr, uid, ids, context=context):
            ids2 = [move.id for move in pick.move_lines]
            self.write(cr, uid, [pick.id], {'state': 'draft'})
            self.pool.get('stock.move').write(cr, uid, ids2, {'state': 'draft'})
        return True


class account_bank_statement(osv.osv):
    _inherit = "account.bank.statement"
    

    def balance_check(self, cr, uid, st_id, journal_type='bank', context=None):
        st = self.browse(cr, uid, st_id, context=context)
        if not ((abs((st.balance_end or 0.0) - st.balance_end_real) < 0.0001) or (abs((st.balance_end or 0.0) - st.balance_end_real) < 0.0001)):
            pass #raise osv.except_osv(_('Error!'), _('The statement balance is incorrect !\nThe expected balance (%.2f) is different than the computed one. (%.2f)') % (st.balance_end_real, st.balance_end))
        return True


class account_cash_statement(osv.osv):
    _inherit = 'account.bank.statement'

    def user_id_change(self, cr, uid, ids, user):
        if user:
            return {'value': {'user_id': uid}}


class stock_move(osv.osv):
    _inherit = "stock.move"
    
    def _check_uom(self, cr, uid, ids, context=None):
        for move in self.browse(cr, uid, ids, context=context):
            if move.product_id.uom_id.category_id.id != move.product_uom.category_id.id:
                raise osv.except_osv(('Perhatian'), ('Salah UoM pada product %s !' % move.product_id.partner_ref))
                return False
        return True

    _constraints = [
        (_check_uom,
            'You try to move a product using a UoM that is not compatible with the UoM of the product moved. Please use an UoM in the same UoM category.',
            ['product_uom']),
    ]


class stock_inventory(osv.osv):
    _inherit = "stock.inventory"
    _columns = {
                'create_uid': fields.many2one('res.users', 'Responsible'),
    }
        
    def _default_stock_location(self, cr, uid, context=None):
        return self.pool.get('res.users').browse(cr, uid, uid).warehouse_id.lot_stock_id.id or False

    _defaults = {
        'location_id': _default_stock_location,
    }


    def unlink(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids, context={})[0]
        if val.state != 'draft':
            raise osv.except_osv(('Perhatian !'), ('Inventory Adjustment tidak bisa dihapus pada state \'%s\'!') % (val.state,))
        return super(stock_inventory, self).unlink(cr, uid, ids, context=context)

    def create(self, cr, uid, vals, context=None):
        #stock = self.pool.get('res.users').browse(cr, uid, uid).warehouse_id.lot_stock_id.id
        #if vals['location_id'] != stock:    
            #raise osv.except_osv(('Perhatian'), ('Kolom "Inventoried Location" harus disesuaikan cabang !'))

        return super(stock_inventory, self).create(cr, uid, vals, context=context)

    def post_inventory(self, cr, uid, inv, context=None):
        move_obj = self.pool.get('stock.move')
        quant_obj = self.pool.get('stock.quant')
        move_obj.action_done(cr, uid, [x.id for x in inv.move_ids], context=context)
        harga = {}
        for l in inv.line_ids:
            harga[l.product_id.id] = l.inventory_value
        for x in inv.move_ids:
            move_obj.write(cr, uid, [x.id], {'price_unit': harga[x.product_id.id]})
            for i in x.quant_ids:
                quant_obj.write(cr, uid, [i.id], {'cost': harga[x.product_id.id]})


    def _get_inventory_lines(self, cr, uid, inventory, context=None):
        location_obj = self.pool.get('stock.location')
        product_obj = self.pool.get('product.product')
        location_ids = location_obj.search(cr, uid, [('id', 'child_of', [inventory.location_id.id])], context=context)
        domain = ' location_id in %s'
        args = (tuple(location_ids),)
        if inventory.partner_id:
            domain += ' and owner_id = %s'
            args += (inventory.partner_id.id,)
        if inventory.lot_id:
            domain += ' and lot_id = %s'
            args += (inventory.lot_id.id,)
        if inventory.product_id:
            domain += ' and product_id = %s'
            args += (inventory.product_id.id,)
        if inventory.package_id:
            domain += ' and package_id = %s'
            args += (inventory.package_id.id,)

        cr.execute('''
           SELECT product_id, max(cost) as inventory_value, sum(qty) as product_qty, location_id, lot_id as prod_lot_id, package_id, owner_id as partner_id
           FROM stock_quant WHERE''' + domain + '''
           GROUP BY product_id, location_id, lot_id, package_id, partner_id
        ''', args)
        vals = []
        for product_line in cr.dictfetchall():
            #replace the None the dictionary by False, because falsy values are tested later on
            for key, value in product_line.items():
                if not value:
                    product_line[key] = False
            product_line['inventory_id'] = inventory.id
            product_line['theoretical_qty'] = product_line['product_qty']
            if product_line['product_id']:
                product = product_obj.browse(cr, uid, product_line['product_id'], context=context)
                product_line['product_uom_id'] = product.uom_id.id
                #product_line['inventory_value'] = product_line['cost']
            vals.append(product_line)
        return vals
     

class purchase_order(osv.osv):
    _inherit = "purchase.order"
    
    def onchange_picking_type_id(self, cr, uid, ids, picking_type_id, context=None):
        value = {}
        picking_type_id = self.pool.get('res.users').browse(cr, uid, uid).warehouse_id.in_type_id.id or picking_type_id
        if picking_type_id:
            picktype = self.pool.get("stock.picking.type").browse(cr, uid, picking_type_id, context=context)
            if picktype.default_location_dest_id:
                value.update({'location_id': picktype.default_location_dest_id.id})
            value.update({'picking_type_id': picking_type_id, 'related_location_id': picktype.default_location_dest_id and picktype.default_location_dest_id.id or False})
        return {'value': value}

class stock_transfer_details(models.TransientModel):
    _inherit = 'stock.transfer_details'

    @api.one
    def do_detailed_transfer(self):
        processed_ids = []
        # Create new and update existing pack operations
        
        for lstits in [self.item_ids, self.packop_ids]:
            for prod in lstits:
                
                if prod.quantity <= 0:
                    raise osv.except_osv(('Perhatian'), ('Quantity harus diatas 0 !'))

#                 for x in self.picking_id.move_lines:
#                     if prod.product_id.id == x.product_id.id : 
#                         if prod.quantity > x.product_uom_qty:
#                             raise osv.except_osv(('Perhatian'), ('Quantity lebih besar dari order !'))

                pack_datas = {
                    'product_id': prod.product_id.id,
                    'product_uom_id': prod.product_uom_id.id,
                    'product_qty': prod.quantity,
                    'package_id': prod.package_id.id,
                    'lot_id': prod.lot_id.id,
                    'location_id': prod.sourceloc_id.id,
                    'location_dest_id': prod.destinationloc_id.id,
                    'result_package_id': prod.result_package_id.id,
                    'date': prod.date if prod.date else datetime.now(),
                    'owner_id': prod.owner_id.id,
                }
                if prod.packop_id:
                    prod.packop_id.write(pack_datas)
                    processed_ids.append(prod.packop_id.id)
                else:
                    pack_datas['picking_id'] = self.picking_id.id
                    packop_id = self.env['stock.pack.operation'].create(pack_datas)
                    processed_ids.append(packop_id.id)
                
        # Delete the others
        packops = self.env['stock.pack.operation'].search(['&', ('picking_id', '=', self.picking_id.id), '!', ('id', 'in', processed_ids)])
        for packop in packops:
            packop.unlink()

        # Execute the transfer of the picking
        self.picking_id.do_transfer()

        return True
   

# class product_uom(osv.osv):
#     _inherit = 'product.uom'
#
#     def create(self, cr, uid, data, context=None):
#         if uid not in (1, 207, 347):
#             raise osv.except_osv(('Perhatian !'), ('Selain Administrator atau BomAdmin tidak bisa membuat uom  !'))
#
#         if 'factor_inv' in data:
#             if data['factor_inv'] != 1:
#                 data['factor'] = self._compute_factor_inv(data['factor_inv'])
#             del(data['factor_inv'])
#         return super(product_uom, self).create(cr, uid, data, context)
#
#
# class product_product(osv.osv):
#     _inherit = "product.product"
#
#     def create(self, cr, uid, vals, context=None):
#         if context is None:
#             context = {}
#
#         if uid not in (1, 207, 347):
#             raise osv.except_osv(('Perhatian !'), ('Selain Administrator atau BomAdmin tidak bisa membuat product  !'))
#
#         ctx = dict(context or {}, create_product_product=True)
#         return super(product_product, self).create(cr, uid, vals, context=ctx)
#
#
#     def onchange_uom(self, cursor, user, ids, uom_id, uom_po_id):
#         if uom_id and uom_po_id:
#
#             if user not in (1, 207, 347):
#                 raise osv.except_osv(('Perhatian !'), ('Selain Administrator atau BomAdmin tidak bisa merubah uom  !'))
#
#             uom_obj=self.pool.get('product.uom')
#             uom=uom_obj.browse(cursor,user,[uom_id])[0]
#             uom_po=uom_obj.browse(cursor,user,[uom_po_id])[0]
#             if uom.category_id.id != uom_po.category_id.id:
#                 return {'value': {'uom_po_id': uom_id}}
#         return False
#
#
# class product_template(osv.osv):
#     _inherit = "product.template"
#
#
#     def onchange_uom(self, cursor, user, ids, uom_id, uom_po_id):
#         if uom_id:
#             if user not in (1, 207, 347):
#                 raise osv.except_osv(('Perhatian !'), ('Selain Administrator atau BomAdmin tidak bisa merubah uom  !'))
#             return {'value': {'uom_po_id': uom_id}}
#         return {}
#
#     def create(self, cr, uid, vals, context=None):
#         if uid not in (1, 207, 347):
#             raise osv.except_osv(('Perhatian !'), ('Selain Administrator atau BomAdmin tidak bisa membuat product  !'))
#
#         product_template_id = super(product_template, self).create(cr, uid, vals, context=context)
#         if not context or "create_product_product" not in context:
#             self.create_variant_ids(cr, uid, [product_template_id], context=context)
#         self._set_standard_price(cr, uid, product_template_id, vals.get('standard_price', 0.0), context=context)
#
#         related_vals = {}
#         if vals.get('ean13'):
#             related_vals['ean13'] = vals['ean13']
#         if vals.get('default_code'):
#             related_vals['default_code'] = vals['default_code']
#         if related_vals:
#             self.write(cr, uid, product_template_id, related_vals, context=context)
#
#         return product_template_id


class mutasi_stock(osv.osv_memory):
    _name = "mutasi.stock"
    _columns = {
        'warehouse_id' : fields.many2one('stock.warehouse', 'Warehouse'),  #,required=True
        'type': fields.selection((('pss', 'Piutang Senlog Senkit'),
                                          ('so','Stock Order'),
                                          ('do','Delivery Order'),
                                          ('is','Incoming Shipment'),
                                          ('smi', 'Stock Move (Shipment-Inv)'),
                                          ('im', 'Internal Move'),
                                          ('fg', 'Finish Goods'),
                                          ('rm', 'Raw Materials')), 'Type', required=True),
        'date_start': fields.date('Start date', required=True),
        'date_stop': fields.date('End date', required=True),
        'name': fields.char('File Name', 254),
        'data_file': fields.binary('File'),
        'warehouse_ids': fields.many2many('stock.warehouse', 'mutasi_stock_report_warehouses_rel',
                        'mutasi_id', 'warehouse_id', 'Warehouses'),
        'authorized_users': fields.boolean('Authorized Users'),
    }

    def _get_warehouse(self, cr, uid, context=None):
        usr_obj = self.pool.get('res.users')
        company_id = usr_obj._get_company(cr, uid, context=context)
        warehouse_ids = self.pool.get('stock.warehouse').search(cr, uid, [('company_id', '=', company_id)], context=context)
        user = usr_obj.browse(cr, uid, uid, context=context)
        if user.login not in allowed_users:
            if user.warehouse_id:
                warehouse_id = user.warehouse_id.id
            elif warehouse_ids:
                warehouse_id = warehouse_ids[0]
            else:
                warehouse_id = False
        else:
            warehouse_id = False
        return warehouse_id

    def _get_all_warehouse(self, cr, uid, context=None):
        usr_obj = self.pool.get('res.users')
        user = usr_obj.browse(cr, uid, uid, context=context)
        company_id = usr_obj._get_company(cr, uid, context=context)
        if user.login in allowed_users:
            return self.pool.get('stock.warehouse').search(cr, uid, [('company_id', '=', company_id)])
        else:
            return []

    def _get_authorized_users(self, cr, uid, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        if user.login in allowed_users:
            return True
        else:
            return False
    
    _defaults = {
        'type' : 'do',
        'warehouse_id': _get_warehouse,     ##'warehouse_id' : 1,
        'date_start': time.strftime('%Y-%m-%d'),
        'date_stop': time.strftime('%Y-%m-%d'),
        #'warehouse_ids': _get_all_warehouse,
        'authorized_users': _get_authorized_users,
    }
    
    
    def eksport_excel(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        judul = ''; data = ''
            
        obj_move = self.pool.get('stock.move')
        obj_picking = self.pool.get('stock.picking')
        obj_warehouse = self.pool.get('stock.warehouse')
        obj_picking_type = self.pool.get('stock.picking.type')
        obj_mrp_production = self.pool.get('mrp.production')

        if val.authorized_users and (not val.warehouse_ids or val.warehouse_ids==[]):
            raise orm.except_orm(_('Warning'), _('Warehouses can not be empty!'))
        elif (not val.authorized_users) and (not val.warehouse_id or val.warehouse_id==False):
            raise orm.except_orm(_('Warning'), _('Warehouse field can not be empty!'))
         
        if val.type == 'do':
            judul = 'DO_%s_%s.xls' % (val.warehouse_id.name, time.strftime('%d-%B-%Y', time.strptime(val.date_start,'%Y-%m-%d')))
            data = 'sep=;\nDelivery Orders;Origin;Date;Customer;Product;Category;UoM;Quantity;Price;Total'
            if val.authorized_users and val.warehouse_ids:
                pte_ids = obj_picking_type.search(cr, uid, [('code','=','outgoing'),('warehouse_id','in',val.warehouse_ids),('return_picking_type_id','!=',False)])
                pic = obj_picking.search(cr, uid, [('picking_type_id','in',pte_ids),('state','=','done'),('date','>=',val.date_start),('date','<=',val.date_stop)])
            else:
                pte = obj_picking_type.search(cr, uid, [('code', '=', 'outgoing'), ('warehouse_id', '=', val.warehouse_id.id), ('return_picking_type_id', '!=', False)])
                pic = obj_picking.search(cr, uid, [('picking_type_id', '=', pte[0]), ('state', '=', 'done'), ('date', '>=', val.date_start), ('date', '<=',  val.date_stop)])
            for x in obj_picking.browse(cr, uid, pic):
                for i in x.move_lines:
                    d = [str((x.name).encode('utf-8')),
                         str(x.origin),
                         str(time.strftime('%d-%B-%Y', time.strptime(x.date,'%Y-%m-%d %H:%M:%S'))),
                         str(x.partner_id.name), 
                         str(i.product_id.partner_ref),
                         str(i.product_id.categ_id.name),
                         str((i.product_uom.name).encode('utf-8')),
                         str(i.product_uom_qty), 
                         str(i.price_unit * (1.0/i.product_uom.factor)), #edited-by-baim
                         str(i.product_uom_qty * i.price_unit * (1.0/i.product_uom.factor))]  #edited-by-baim  
                    data += '\n' + ';'.join(d)
         
        if val.type == 'is':
            judul = 'IS_%s_%s.xls' % (val.warehouse_id.name, time.strftime('%d-%B-%Y', time.strptime(val.date_start,'%Y-%m-%d')))
            data = 'sep=;\nIncoming Shipment;Origin;Date;Supplier;Product;Category;UoM Purchase;Quantity;Price;Total'
            if val.authorized_users and val.warehouse_ids:
                pte_ids = obj_picking_type.search(cr, uid, [('code','=','incoming'),('warehouse_id','in',val.warehouse_ids)])
                pic = obj_picking.search(cr, uid, [('picking_type_id','in',pte_ids),('state','=','done'),('date','>=',val.date_start),('date','<=',val.date_stop)])
            else:
                pte = obj_picking_type.search(cr, uid, [('code', '=', 'incoming'), ('warehouse_id', '=', val.warehouse_id.id)])
                pic = obj_picking.search(cr, uid, [('picking_type_id', '=', pte[0]), ('state', '=', 'done'), ('date', '>=', val.date_start), ('date', '<=',  val.date_stop)])
            for x in obj_picking.browse(cr, uid, pic):
                for i in x.move_lines:
                    d = [str((x.name).encode('utf-8')),
                         str(x.origin),
                         str(time.strftime('%d-%B-%Y', time.strptime(x.date,'%Y-%m-%d %H:%M:%S'))),
                         str(x.partner_id.name), 
                         str(i.product_id.partner_ref),
                         str(i.product_id.categ_id.name),
                         str((i.product_uom.name).encode('utf-8')),
                         str(i.product_uom_qty), 
                         str(i.price_unit * (1.0/i.product_uom.factor)), #edited-by-baim
                         str(i.product_uom_qty * i.price_unit * (1.0/i.product_uom.factor))]  #edited-by-baim  
                    data += '\n' + ';'.join(d)

        if val.type == 'smi': #added-by-baim
            judul = 'SMI_%s_%s.xls' % (val.warehouse_id.name, time.strftime('%d-%B-%Y', time.strptime(val.date_start,'%Y-%m-%d')))
            data = 'sep=;\nShipment Number;Origin;Date;Supplier;Product;Category;Quantity Inventory;UoM Inventory;Unit Price Inv;Total'
            if val.authorized_users and val.warehouse_ids:
                pte_ids = obj_picking_type.search(cr, uid, [('code','=','incoming'),('warehouse_id','in',val.warehouse_ids)])
                pic = obj_picking.search(cr, uid, [('picking_type_id','in',pte_ids), ('state', '=', 'done'),('date', '>=', val.date_start), ('date', '<=', val.date_stop)])
            else:
                pte = obj_picking_type.search(cr, uid, [('code', '=', 'incoming'), ('warehouse_id', '=', val.warehouse_id.id)])
                pic = obj_picking.search(cr, uid, [('picking_type_id', '=', pte[0]), ('state', '=', 'done'), ('date', '>=', val.date_start), ('date', '<=',  val.date_stop)])
            for x in obj_picking.browse(cr, uid, pic):
                for i in x.move_lines:
                    d = [str((x.name).encode('utf-8')),
                         str(x.origin),
                         str(time.strftime('%d-%B-%Y', time.strptime(x.date,'%Y-%m-%d %H:%M:%S'))),
                         str(x.partner_id.name),
                         str(i.product_id.partner_ref),
                         str(i.product_id.categ_id.name),
                         str(i.product_qty),
                         str((i.product_id.uom_id.name).encode('utf-8')),
                         '%.2f' % (i.price_unit), #edited-by-baim
                         '%.2f' % (i.product_qty * i.price_unit)]  #edited-by-baim
                    data += '\n' + ';'.join(d)
         
        if val.type == 'im':
            judul = 'IM_%s_%s.xls' % (val.warehouse_id.name, time.strftime('%d-%B-%Y', time.strptime(val.date_start,'%Y-%m-%d')))
            data = 'sep=;\nInternal Moves;Origin;Date;Source;Destination;Partner;Product;Category;UoM;Quantity;Price;Total'
            if val.authorized_users and val.warehouse_ids:
                pte_ids = obj_picking_type.search(cr, uid, [('code','=','internal'),('warehouse_id','in',val.warehouse_ids)])
                pic = obj_picking.search(cr, uid, [('picking_type_id','in',pte_ids), ('type_wo', '=', False),
                        ('work_id','=',False), ('state','=','done'),('date', '>=', val.date_start), ('date', '<=', val.date_stop)])
            else:
                pte = obj_picking_type.search(cr, uid, [('code', '=', 'internal'), ('warehouse_id', '=', val.warehouse_id.id)])
                pic = obj_picking.search(cr, uid, [('picking_type_id', '=', pte[0]), ('type_wo', '=', False), ('work_id', '=', False), ('state', '=', 'done'), ('date', '>=', val.date_start), ('date', '<=',  val.date_stop)])
            for x in obj_picking.browse(cr, uid, pic):
                for i in x.move_lines:
                    d = [str((x.name).encode('utf-8')),
                         str(x.origin),
                         str(time.strftime('%d-%B-%Y', time.strptime(x.date,'%Y-%m-%d %H:%M:%S'))),
                         str(i.location_id.location_id.name)+'/'+str(i.location_id.name), 
                         str(i.location_dest_id.location_id.name)+'/'+str(i.location_dest_id.name),
                         str(x.partner_id.name or val.warehouse_id.name), 
                         str(i.product_id.partner_ref),
                         str(i.product_id.categ_id.name),
                         str((i.product_uom.name).encode('utf-8')),
                         str(i.product_uom_qty), 
                         str(i.price_unit), 
                         str(i.product_uom_qty*i.price_unit)]  
                    data += '\n' + ';'.join(d)
         
        if val.type == 'so':
            judul = 'SO_%s_%s.xls' % (val.warehouse_id.name, time.strftime('%d-%B-%Y', time.strptime(val.date_start,'%Y-%m-%d')))
            data = 'sep=;\nInternal Moves;Origin;MRP;Date;Source;Destination;Partner;Product;Category;UoM;Quantity;Harga Senlog;Total;Harga Mutasi'
            if val.authorized_users and val.warehouse_ids:
                partner_ids = []
                for wh in obj_warehouse.browse(cr, uid, val.warehouse_ids):
                    if wh.partner_id.id not in partner_ids:
                        partner_ids.append(wh.partner_id.id)
                pic = obj_picking.search(cr, uid, [('location_dest_id', '=', 10),
                                                   ('partner_id','in', partner_ids),
                                                   ('state', '=', 'done'), ('date', '>=', val.date_start),
                                                   ('date', '<=', val.date_stop)])
            else:
                pic = obj_picking.search(cr, uid, [('location_dest_id', '=', 10),
                                                   ('partner_id', '=', val.warehouse_id.partner_id.id),
                                                   ('state', '=', 'done'), ('date', '>=', val.date_start),
                                                   ('date', '<=',  val.date_stop)])
            harga = {}; pricelist = {}
            for u in obj_picking.browse(cr, uid, pic):
                if u.requirement_id.pricelist_id:
                    for v in u.requirement_id.pricelist_id.version_id:
                        pricelist[v.id] = []
                        
            for p in self.pool.get('product.pricelist.version').browse(cr, uid, pricelist.keys()):
                for f in p.items_id:
                    harga[f.product_id.id] = f.price_surcharge
            
            for x in obj_picking.browse(cr, uid, pic):
                for i in x.move_lines:
                    d = [str((x.name).encode('utf-8')),
                         str(x.origin),
                         str(x.requirement_id.name or '-'),
                         str(time.strftime('%d-%B-%Y', time.strptime(x.date,'%Y-%m-%d %H:%M:%S'))),
                         str(i.location_id.location_id.name)+'/'+str(i.location_id.name), 
                         str(i.location_dest_id.location_id.name)+'/'+str(i.location_dest_id.name),
                         str(x.partner_id.name or val.warehouse_id.name), 
                         str(i.product_id.partner_ref),
                         str(i.product_id.categ_id.name),
                         str((i.product_uom.name).encode('utf-8')),
                         str(i.product_uom_qty), 
                         str(harga.get(i.product_id.id, 0.0)), 
                         str(i.product_uom_qty*harga.get(i.product_id.id, 0.0)),
                         str(i.price_unit)]  
                    data += '\n' + ';'.join(d)


        if val.type == 'pss':
            obj_piutang = self.pool.get('piutang.senlog')
            judul = 'PSS_%s_%s.xls' % (val.warehouse_id.name, time.strftime('%d-%B-%Y', time.strptime(val.date_start,'%Y-%m-%d')))
            data = 'sep=;\nName;Origin;Date;Customer;Product;UoM;Quantity;Price Unit;Total;Account;Analytic;State'
            if val.authorized_users and val.warehouse_ids:
                partner_ids = []
                for wh in obj_warehouse.browse(cr, uid, val.warehouse_ids):
                    if wh.partner_id.id not in partner_ids:
                        partner_ids.append(wh.partner_id.id)
                piu = obj_piutang.search(cr, uid, [('partner_id','in',partner_ids),
                                                   ('date', '>=', val.date_start), ('date', '<=', val.date_stop)])
            else:
                piu = obj_piutang.search(cr, uid, [('partner_id', '=', val.warehouse_id.partner_id.id),
                                                   ('date', '>=', val.date_start), ('date', '<=',  val.date_stop)])
            
            for x in obj_piutang.browse(cr, uid, piu):
                for i in x.piutang_line:
                    d = [str((x.name).encode('utf-8')),
                         str(x.origin),
                         str(time.strftime('%d-%B-%Y', time.strptime(x.date,'%Y-%m-%d'))),
                         str(x.partner_id.name), 
                         str(i.name),
                         str((i.product_uom.name).encode('utf-8')),
                         str(i.product_qty), 
                         str(i.price_unit * (1.0/i.product_uom.factor)), #edited-by-baim
                         str(i.product_qty * i.price_unit * (1.0/i.product_uom.factor)),  #edited-by-baim
                         str(i.account_id.code) + ' ' + str(i.account_id.name),
                         str(i.account_analytic_id.code) + ' ' + str(i.account_analytic_id.name),
                         str(x.state)]  
                    data += '\n' + ';'.join(d)
                       

        if val.type == 'fg':
            judul = 'FG_%s_%s.xls' % (val.warehouse_id.name, time.strftime('%d-%B-%Y', time.strptime(val.date_start,'%Y-%m-%d')))
            data = 'sep=;\nDocuments;Origin;Date;Product;Category;UoM;Quantity;Price;Total'
            if val.authorized_users and val.warehouse_ids:
                location_ids = []
                for wh in obj_warehouse.browse(cr, uid, val.warehouse_ids):
                    if wh.lot_stock_id.id not in location_ids:
                        location_ids.append(wh.lot_stock_id.id)
                pic = obj_mrp_production.search(cr, uid,
                                                [('state', '=', 'done'), ('date_planned', '>=', val.date_start),
                                                 ('date_planned', '<=', val.date_stop),
                                                 ('location_src_id', 'in', location_ids),
                                                 ('location_dest_id', 'in', location_ids)])
            else:
                pic = obj_mrp_production.search(cr, uid, [('state', '=', 'done'), ('date_planned', '>=', val.date_start), ('date_planned', '<=',  val.date_stop), ('location_src_id', '=', val.warehouse_id.lot_stock_id.id), ('location_dest_id', '=', val.warehouse_id.lot_stock_id.id)])
            for x in obj_mrp_production.browse(cr, uid, pic):
                for i in x.move_created_ids2:
                    d = [str((x.name).encode('utf-8')),
                         str(x.origin),
                         str(time.strftime('%d-%B-%Y', time.strptime(x.date_planned,'%Y-%m-%d %H:%M:%S'))),
                         str(i.product_id.partner_ref),
                         str(i.product_id.categ_id.name),
                         str((i.product_uom.name).encode('utf-8')),
                         str(i.product_uom_qty),
                         str(i.price_unit * (1.0/i.product_uom.factor)), #edited-by-baim
                         str(i.product_uom_qty * i.price_unit * (1.0/i.product_uom.factor))]  #edited-by-baim
                    data += '\n' + ';'.join(d)

            if val.authorized_users and val.warehouse_ids:
                pte_ids = obj_picking_type.search(cr, uid, [('code','=','internal'),('warehouse_id','in',val.warehouse_ids)])
                pic = obj_picking.search(cr, uid, [('picking_type_id','in',pte_ids), ('state', '=', 'done'),
                                                   ('type_wo', '=', 'fg'), ('date', '>=', val.date_start),
                                                   ('date', '<=', val.date_stop)])
            else:
                pte = obj_picking_type.search(cr, uid, [('code', '=', 'internal'), ('warehouse_id', '=', val.warehouse_id.id)])
                pic = obj_picking.search(cr, uid, [('picking_type_id', '=', pte[0]), ('state', '=', 'done'), ('type_wo', '=', 'fg'), ('date', '>=', val.date_start), ('date', '<=',  val.date_stop)])
            for x in obj_picking.browse(cr, uid, pic):
                for i in x.move_lines:
                    d = [str((x.name).encode('utf-8')),
                         str(x.origin),
                         str(time.strftime('%d-%B-%Y', time.strptime(x.date,'%Y-%m-%d %H:%M:%S'))),
                         str(i.product_id.partner_ref),
                         str(i.product_id.categ_id.name),
                         str((i.product_uom.name).encode('utf-8')),
                         str(i.product_uom_qty),
                         str(i.price_unit * (1.0/i.product_uom.factor)), #edited-by-baim
                         str(i.product_uom_qty * i.price_unit * (1.0/i.product_uom.factor))]  #edited-by-baim  
                    data += '\n' + ';'.join(d)
         
         
        if val.type == 'rm':
            judul = 'RM_%s_%s.xls' % (val.warehouse_id.name, time.strftime('%d-%B-%Y', time.strptime(val.date_start,'%Y-%m-%d')))
            data = 'sep=;\nDocuments;Origin;Date;Product;Category;UoM;Quantity;Price;Total'
            if val.authorized_users and val.warehouse_ids:
                location_ids = []
                for wh in obj_warehouse.browse(cr, uid, val.warehouse_ids):
                    if wh.lot_stock_id.id not in location_ids:
                        location_ids.append(wh.lot_stock_id.id)
                pic = obj_mrp_production.search(cr, uid,
                                                [('state', '=', 'done'), ('date_planned', '>=', val.date_start),
                                                 ('date_planned', '<=', val.date_stop),
                                                 ('location_src_id','in',location_ids),
                                                 ('location_dest_id','in',location_ids)])
            else:
                pic = obj_mrp_production.search(cr, uid, [('state', '=', 'done'), ('date_planned', '>=', val.date_start), ('date_planned', '<=',  val.date_stop), ('location_src_id', '=', val.warehouse_id.lot_stock_id.id), ('location_dest_id', '=', val.warehouse_id.lot_stock_id.id)])
            for x in obj_mrp_production.browse(cr, uid, pic):
                for i in x.move_lines2:
                    d = [str((x.name).encode('utf-8')),
                         str(x.origin),
                         str(time.strftime('%d-%B-%Y', time.strptime(x.date_planned,'%Y-%m-%d %H:%M:%S'))),
                         str(i.product_id.partner_ref),
                         str(i.product_id.categ_id.name),
                         str((i.product_uom.name).encode('utf-8')),
                         str(i.product_uom_qty),
                         str(i.price_unit * (1.0/i.product_uom.factor)), #edited-by-baim
                         str(i.product_uom_qty * i.price_unit * (1.0/i.product_uom.factor))]  #edited-by-baim
                    data += '\n' + ';'.join(d)

            if val.authorized_users and val.warehouse_ids:
                pte_ids = obj_picking_type.search(cr, uid, [('code','=','internal'), ('warehouse_id','in',val.warehouse_ids)])
                pic = obj_picking.search(cr, uid, [('picking_type_id','in',pte_ids), ('state', '=', 'done'),
                                                   ('type_wo', '=', 'rm'), ('date', '>=', val.date_start),
                                                   ('date', '<=', val.date_stop)])
            else:
                pte = obj_picking_type.search(cr, uid, [('code', '=', 'internal'), ('warehouse_id', '=', val.warehouse_id.id)])
                pic = obj_picking.search(cr, uid, [('picking_type_id', '=', pte[0]), ('state', '=', 'done'), ('type_wo', '=', 'rm'), ('date', '>=', val.date_start), ('date', '<=',  val.date_stop)])
            for x in obj_picking.browse(cr, uid, pic):
                for i in x.move_lines:
                    d = [str((x.name).encode('utf-8')),
                         str(x.origin),
                         str(time.strftime('%d-%B-%Y', time.strptime(x.date,'%Y-%m-%d %H:%M:%S'))),
                         str(i.product_id.partner_ref),
                         str(i.product_id.categ_id.name),
                         str((i.product_uom.name).encode('utf-8')),
                         str(i.product_uom_qty),
                         str(i.price_unit * (1.0/i.product_uom.factor)), #edited-by-baim
                         str(i.product_uom_qty * i.price_unit * (1.0/i.product_uom.factor))]  #edited-by-baim
                    data += '\n' + ';'.join(d)
         
          
        #out = base64.b64encode(data.encode('ascii',errors='ignore'))
        out = base64.b64encode(data)
        self.write(cr, uid, ids, {'data_file':out, 'name':judul}, context=context)

        view_rec = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'siu_hpp', 'view_wizard_mutasi_stock')
        view_id = view_rec[1] or False
               
        return {
            'view_type': 'form',
            'view_id' : [view_id],
            'view_mode': 'form',
            'res_id': val.id,
            'res_model': 'mutasi.stock',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }


class mrp_production(osv.osv):
    _inherit = 'mrp.production'

    def action_production_end(self, cr, uid, ids, context=None):
        for production in self.browse(cr, uid, ids):
            self._costs_generate(cr, uid, production)   
        
        write_res = self.write(cr, uid, ids, {'state': 'done', 'date_finished': time.strftime('%Y-%m-%d %H:%M:%S')})
        # Check related procurements
        proc_obj = self.pool.get("procurement.order")
        procs = proc_obj.search(cr, uid, [('production_id', 'in', ids)], context=context)
        proc_obj.check(cr, uid, procs, context=context)
        
        for production in self.browse(cr, uid, ids):
            harga = 0.0
            for x in production.move_lines2:
                harga += x.price_unit * x.product_uom_qty
            
            for i in production.move_created_ids2:    
                self.pool.get('stock.move').write(cr, uid, [i.id], {'price_unit': harga})
                for o in i.quant_ids:
                    self.pool.get('stock.quant').write(cr, uid, [o.id], {'cost': harga})
        
        return write_res



class account_analytic_account(osv.osv):
    _inherit = 'account.analytic.account'


    def name_get(self, cr, uid, ids, context=None):
        res = []
        if not ids:
            return res
        if isinstance(ids, (int, long)):
            ids = [ids]
        for id in ids:
            elmt = self.browse(cr, uid, id, context=context)
            nama = '[%s] %s' % (elmt.code,elmt.name)
            res.append((id, nama))
        return res


    def name_search(self, cr, uid, name, args=None, operator='ilike', context=None, limit=100):
        if not args:
            args=[]
        if context is None:
            context={}
        if name:
            account_ids = self.search(cr, uid, [('code', '=', name)] + args, limit=limit, context=context)
            if not account_ids:
                account_ids = set(self.search(cr, uid, args + [('code', operator, name)], limit=limit, context=context))
                if not limit or len(account_ids) < limit:
                    limit2 = (limit - len(account_ids)) if limit else False
                    account_ids.update(self.search(cr, uid, args + [('name', operator, name)], limit=limit2, context=context))
                account_ids = list(account_ids)
        else:
            account_ids = self.search(cr, uid, args, limit=limit, context=context)
        return self.name_get(cr, uid, account_ids, context=context)



class stock_inventory_line(osv.osv):
    _inherit = "stock.inventory.line"
    
    def _get_default_product_uom(self, cr, uid, context=None):
        uom_obj = self.pool.get('product.uom')
        satuan = uom_obj.search(cr, uid, [('name','like','%Unit%')], context=context)
        if not satuan:
            satuan = uom_obj.search(cr, uid, [('name','like','%unit%')], context=context)
            if not satuan:
                satuan = uom_obj.search(cr, uid, [('name','like','%Pcs%')], context=context)
                if not satuan:
                    raise osv.except_osv(_('Perhatian !'), _("Pastikan terdapat salah satu Satuan(UoM) berupa Unit atau Pcs di daftar Satuan(UoM)."))
        return satuan and satuan[0] or False
    
    _columns = {
        'product_id': fields.many2one('product.product', 'Product', required=True, select=True, domain=[('type','=','product'), ('sale_ok','=',False)]),
    }
    _defaults = {
        'product_uom_id': _get_default_product_uom,
    }
    

class purchase_order_line(osv.osv):
    _inherit = "purchase.order.line"
    
    def onchange_product_id(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', context=None):
        
        if context is None:
            context = {}

        res = {'value': {'price_unit': price_unit or 0.0, 'name': name or '', 'product_uom' : uom_id or False}}
        if not product_id:
            return res

        product_product = self.pool.get('product.product')
        product_uom = self.pool.get('product.uom')
        res_partner = self.pool.get('res.partner')
        product_pricelist = self.pool.get('product.pricelist')
        account_fiscal_position = self.pool.get('account.fiscal.position')
        account_tax = self.pool.get('account.tax')

        context_partner = context.copy()
        if partner_id:
            lang = res_partner.browse(cr, uid, partner_id).lang
            context_partner.update( {'lang': lang, 'partner_id': partner_id} )
        product = product_product.browse(cr, uid, product_id, context=context_partner)
        dummy, name = product_product.name_get(cr, uid, product_id, context=context_partner)[0]
        if product.description_purchase:
            name += '\n' + product.description_purchase
        res['value'].update({'name': name})

        res['domain'] = {'product_uom': [('category_id','=',product.uom_id.category_id.id)]}

        product_uom_po_id = product.uom_po_id.id
        if not uom_id:
            uom_id = product_uom_po_id
        else:
            uom_id = product_uom_po_id

        if product.uom_id.category_id.id != product_uom.browse(cr, uid, uom_id, context=context).category_id.id:
            if context.get('purchase_uom_check') and self._check_product_uom_group(cr, uid, context=context):
                res['warning'] = {'title': _('Warning!'), 'message': _('Selected Unit of Measure does not belong to the same category as the product Unit of Measure.')}
            uom_id = product_uom_po_id

        res['value'].update({'product_uom': uom_id})

        # - determine product_qty and date_planned based on seller info
        if not date_order:
            date_order = fields.datetime.now()


        supplierinfo = False
        for supplier in product.seller_ids:
            if partner_id and (supplier.name.id == partner_id):
                supplierinfo = supplier
                if supplierinfo.product_uom.id != uom_id:
                    res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier only sells this product by %s') % supplierinfo.product_uom.name }
                min_qty = product_uom._compute_qty(cr, uid, supplierinfo.product_uom.id, supplierinfo.min_qty, to_uom_id=uom_id)
                if (qty or 0.0) < min_qty: # If the supplier quantity is greater than entered from user, set minimal.
                    if qty:
                        res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier has a minimal quantity set to %s %s, you should not purchase less.') % (supplierinfo.min_qty, supplierinfo.product_uom.name)}
                    qty = min_qty
        dt = self._get_date_planned(cr, uid, supplierinfo, date_order, context=context).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        qty = qty or 1.0
        res['value'].update({'date_planned': date_planned or dt})
        if qty:
            res['value'].update({'product_qty': qty})

        price = price_unit
        if state not in ('sent','bid'):
            # - determine price_unit and taxes_id
            if pricelist_id:
                date_order_str = datetime.strptime(date_order, DEFAULT_SERVER_DATETIME_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT)
                price = product_pricelist.price_get(cr, uid, [pricelist_id],
                        product.id, qty or 1.0, partner_id or False, {'uom': uom_id, 'date': date_order_str})[pricelist_id]
            else:
                price = product.standard_price

        taxes = account_tax.browse(cr, uid, map(lambda x: x.id, product.supplier_taxes_id))
        fpos = fiscal_position_id and account_fiscal_position.browse(cr, uid, fiscal_position_id, context=context) or False
        taxes_ids = account_fiscal_position.map_tax(cr, uid, fpos, taxes)
        res['value'].update({'price_unit': price})

        return res



class stock_return_picking(osv.osv_memory):
    _inherit = 'stock.return.picking'

    def _create_returns(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        record_id = context and context.get('active_id', False) or False
        move_obj = self.pool.get('stock.move')
        pick_obj = self.pool.get('stock.picking')
        uom_obj = self.pool.get('product.uom')
        data_obj = self.pool.get('stock.return.picking.line')
        pick = pick_obj.browse(cr, uid, record_id, context=context)
        data = self.read(cr, uid, ids[0], context=context)
        returned_lines = 0

        # Cancel assignment of existing chained assigned moves
        moves_to_unreserve = []
        for move in pick.move_lines:
            to_check_moves = [move.move_dest_id]
            if move.move_dest_id:
                while to_check_moves:
                    current_move = to_check_moves.pop()
                    if current_move.state not in ('done', 'cancel') and current_move.reserved_quant_ids:
                        moves_to_unreserve.append(current_move.id)
                    split_move_ids = move_obj.search(cr, uid, [('split_from', '=', current_move.id)], context=context)
                    if split_move_ids:
                        to_check_moves += move_obj.browse(cr, uid, split_move_ids, context=context)

        if moves_to_unreserve:
            move_obj.do_unreserve(cr, uid, moves_to_unreserve, context=context)
            #break the link between moves in order to be able to fix them later if needed
            move_obj.write(cr, uid, moves_to_unreserve, {'move_orig_ids': False}, context=context)

        #Create new picking for returned products
        pick_type_id = pick.picking_type_id.return_picking_type_id and pick.picking_type_id.return_picking_type_id.id or pick.picking_type_id.id
        new_picking = pick_obj.copy(cr, uid, pick.id, {
            'move_lines': [],
            'picking_type_id': pick_type_id,
            'state': 'draft',
            'origin': pick.name,
        }, context=context)

        for data_get in data_obj.browse(cr, uid, data['product_return_moves'], context=context):
            move = data_get.move_id
            if not move:
                raise osv.except_osv(_('Warning !'), _("You have manually created product lines, please delete them to proceed"))
            new_qty = data_get.quantity
            if new_qty:
                returned_lines += 1
                move_obj.copy(cr, uid, move.id, {
                    'product_id': data_get.product_id.id,
                    'product_uom_qty': new_qty,
                    'product_uos_qty': uom_obj._compute_qty(cr, uid, move.product_uom.id, new_qty, move.product_uos.id),
                    'picking_id': new_picking,
                    'state': 'draft',
                    'location_id': move.location_dest_id.id,
                    'location_dest_id': move.location_id.id,
                    'origin_returned_move_id': move.id,
                    'procure_method': 'make_to_stock',
                    'restrict_lot_id': data_get.lot_id.id,
                })

        if not returned_lines:
            raise osv.except_osv(_('Warning!'), _("Please specify at least one non-zero quantity."))

        pick_obj.action_confirm(cr, uid, [new_picking], context=context)
        pick_obj.action_assign(cr, uid, [new_picking], context)
        return new_picking, pick_type_id



#         <record id="account_invoice_cabang_rule" model="ir.rule">
#             <field name="name">Invoice Cabang</field>
#             <field ref="account.model_account_invoice" name="model_id"/>
#             <field name="domain_force">['|',('user_id','=',[x.name.id for x in user.kelompok_ids]),('user_id','=',False)]</field>
#         </record>



#         if val.type == 'pos':
#             judul = 'PoS_%s_%s.xls' % (val.warehouse_id.name, time.strftime('%d-%B-%Y', time.strptime(val.date_start,'%Y-%m-%d')))
#             data = 'sep=;\nPoint of Sales;Origin;Date;Customer;Product;UoM;Quantity;Price;Total'
#             pte = obj_picking_type.search(cr, uid, [('code', '=', 'outgoing'), ('warehouse_id', '=', val.warehouse_id.id), ('return_picking_type_id', '=', False)])
#             pic = obj_picking.search(cr, uid, [('picking_type_id', '=', pte[0]), ('state', '=', 'done'), ('date', '>=', val.date_start), ('date', '<=',  val.date_stop)])
#             for x in obj_picking.browse(cr, uid, pic):
#                 for i in x.move_lines:
#                     d = [str(x.name),
#                          str(x.origin),
#                          str(time.strftime('%d-%B-%Y', time.strptime(x.date,'%Y-%m-%d %H:%M:%S'))),
#                          str(x.partner_id.name), 
#                          str(i.product_id.partner_ref),
#                          str(i.product_uom.name),
#                          str(i.product_uom_qty), 
#                          str(i.price_unit), 
#                          str(i.product_uom_qty*i.price_unit)]  
#                     data += '\n' + ';'.join(d)
         





# <field name="groups" eval="[(4, ref('account.group_account_invoice'))]"/>

# class stock_location(osv.osv):
#     _inherit = 'stock.location' 
#     _columns = {
#         'stock_in_line': fields.one2many('stock.move', 'location_dest_id', 'Stock Masuk', readonly=True, domain=[('state', '=', 'done')]),
#         'stock_out_line':fields.one2many('stock.move', 'location_id', 'Stock Keluar', readonly=True, domain=[('state', '=', 'done')]),
#     }
# 
# class stock_move(osv.osv):
#     _inherit = 'stock.move' 
#     _columns = {
#         'mutasi_out_line':fields.one2many('mutasi.out.line', 'stock_move_id', 'Stock Keluar', readonly=True),
#     }
# 
# 
# class mutasi_out_line(osv.osv):
#     _name = 'mutasi.out.line' 
#     _columns = {
#         'stock_move_id': fields.many2one('stock.move', 'Stock Move', required=True, ondelete='cascade', domain=[('state', '=', 'done')]),
#         'out_id': fields.many2one('stock.move', 'Stock Out'),
#     }







#         <record id="view_stock_location_hpp" model="ir.ui.view">
#             <field name="name">stock.location.hpp</field>
#             <field name="model">stock.location</field>
#             <field name="inherit_id" ref="stock.view_location_form"/>
#             <field name="arch" type="xml">
#                 <field name="comment" position="replace">
#                     <notebook colspan="5">
#                         <page string="Mutasi In">
#                             <field colspan="4" name="stock_in_line" nolabel="1" widget="one2many_list"/>
#                         </page>
#                         <page string="Mutasi Out">
#                             <field colspan="4" name="stock_out_line" nolabel="1" widget="one2many_list"/>
#                         </page>
#                         <page string="Notes">
#                             <field colspan="4" name="comment" nolabel="1"/>
#                         </page>
#                     </notebook>
#                 </field>    
#             </field>
#         </record>




#                 <group name="quants_grp" position="before">
#                     <group name="stock_out" string="Stock Out" colspan="4">
#                         <field colspan="4" name="mutasi_out_line" nolabel="1" widget="one2many_list">
#                             <tree string="" editable="top">
#                                 <field name="out_id"/>
#                             </tree>
#                         </field>
#                     </group>
#                 </group>



#     def _get_picking_in(self, cr, uid, context=None):
#         return self.pool.get('res.users').browse(cr, uid, uid).warehouse_id.in_type_id.id or False
# 
#     _defaults = {
#         'picking_type_id': _get_picking_in,
#         'related_location_id' : lambda self, cr, uid, context: self.pool.get('res.users').browse(cr, uid, uid).warehouse_id.lot_stock_id.id or False
#     }
#     
#     
#     def create(self, cr, uid, vals, context=None):
#         stock = self.pool.get('res.users').browse(cr, uid, uid).warehouse_id.in_type_id.id
#         if vals['picking_type_id'] != stock:    
#             raise osv.except_osv(('Perhatian'), ('Kolom "Deliver to" harus disesuaikan cabang !'))
# 
#         if vals.get('name','/')=='/':
#             vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'purchase.order') or '/'
#         context = dict(context or {}, mail_create_nolog=True)
#         order =  super(purchase_order, self).create(cr, uid, vals, context=context)
#         self.message_post(cr, uid, [order], body=_("RFQ created"), context=context)
#         return order