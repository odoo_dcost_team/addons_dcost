# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com

import time
import xmlrpclib
from datetime import datetime
from openerp import pooler, SUPERUSER_ID
#from openerp import models, api, fields as Fields
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from openerp.osv import osv, orm, fields
from openerp.tools.translate import _
from openerp.exceptions import except_orm
from openerp.tools.float_utils import float_compare

db_groceries = ['LOGISTIK', 'SENLOGPB5', 'PB5', 'SENLOG', 'SENKIT']
db_outlets = ['PB1', 'PB', 'OUTLET', 'PEBO']
allow_usr = ['admin', 'BomAdmin']
other_users = ['admsenlog', 'admsenkit', 'admpurchase', 'ordersenlog', 'ordersenkit', 'purchaseslg', 'purchasesnk']


class RPCProxyOne(object):
    def __init__(self, server, ressource, user_id):
        self.server = server
        self.user_id = user_id
        local_url = 'http://%s:%d/xmlrpc/common' % (server.server_url,
                                                    server.server_port)
        rpc = xmlrpclib.ServerProxy(local_url)
        self.uid = rpc.login(server.server_db, server.login, server.password)
        local_url = 'http://%s:%d/xmlrpc/object' % (server.server_url,
                                                    server.server_port)
        self.rpc = xmlrpclib.ServerProxy(local_url)
        self.ressource = ressource

    def __getattr__(self, name):
        RPCProxy(self.server, self.user_id)
        server_db = self.server.server_db
        server_pass = self.server.password
        resource = self.ressource
        return lambda cr, uid, *args, **kwargs: self.rpc.execute(server_db,
                                                                 self.user_id or self.uid,
                                                                 server_pass,
                                                                 resource,
                                                                 name, *args)

class RPCProxy(object):
    def __init__(self, server, user):
        self.server = server
        self.user = user

    def get(self, ressource):
        return RPCProxyOne(self.server, ressource, self.user)


class ProductProduct(osv.osv):
    _inherit = "product.product"

    # def unlink(self, cr, uid, ids, context=None):
    #     unlink_ids = []
    #     unlink_product_tmpl_ids = []
    #     users_obj = self.pool.get("res.users")
    #     user = users_obj.browse(cr, uid, uid)
    #     users = users_obj.search(cr, uid, [('login', 'in', tuple(allow_usr + other_users))])
    #     for product in self.browse(cr, uid, ids, context=context):
    #         if not product.exists():
    #             continue
    #         if (uid not in tuple(users)) or user.login not in tuple(allow_usr + other_users):
    #             raise except_orm(_('Perhatian !!!'),
    #                              _("Anda tidak diperbolehkan untuk menghapus produk ini."))
    #         tmpl_id = product.product_tmpl_id.id
    #         other_product_ids = self.search(cr, uid, [('product_tmpl_id', '=', tmpl_id), ('id', '!=', product.id)], context=context)
    #         if not other_product_ids:
    #             unlink_product_tmpl_ids.append(tmpl_id)
    #         unlink_ids.append(product.id)
    #     res = super(ProductProduct, self).unlink(cr, uid, unlink_ids, context=context)
    #     self.pool.get('product.template').unlink(cr, uid, unlink_product_tmpl_ids, context=context)
    #     return res
    #
    # def create(self, cr, uid, vals, context=None):
    #     if context is None:
    #         context = {}
    #
    #     user_obj = self.pool.get("res.users")
    #     user = user_obj.browse(cr, uid, uid, context=context)
    #     users = user_obj.search(cr, uid, [('login', 'in', tuple(allow_usr + other_users))], context=context)
    #     if (uid not in users) or user.login not in tuple(allow_usr + other_users):
    #         raise except_orm(_('Perhatian !!!'),
    #             _("Anda tidak diperbolehkan membuat produk baru, kecuali user-user dibawah ini:\n[%s].") %
    #             ("DB PB1: 'Administrator / BomAdmin / admpurchase'; \nDB LOGISTIK: 'Administrator / BomAdmin / ordersenlog / ordersenkit / admsenlog / admsenkit / purchaseslg / purchasesnk'",))
    #
    #     ctx = dict(context or {}, create_product_product=True)
    #     return super(ProductProduct, self).create(cr, uid, vals, context=ctx)
    #
    # def onchange_uom(self, cursor, user, ids, uom_id, uom_po_id):
    #     if uom_id and uom_po_id:
    #         user_obj = self.pool.get("res.users")
    #         usr = user_obj.browse(cursor, user, user)
    #         users = user_obj.search(cursor, user, [('login', 'in', tuple(allow_usr + other_users))])
    #         if (user not in users) or usr.login not in tuple(allow_usr + other_users):
    #             raise except_orm(_('Perhatian !!!'),
    #                 _("Anda tidak diperbolehkan untuk merubah uom/satuan, kecuali user-user dibawah ini:\n[%s].") %
    #                 ("DB PB1: 'Administrator / BomAdmin / admpurchase'; \nDB LOGISTIK: 'Administrator / BomAdmin / ordersenlog / ordersenkit / admsenlog / admsenkit / purchaseslg / purchasesnk'",))
    #
    #         uom_obj=self.pool.get('product.uom')
    #         uom=uom_obj.browse(cursor,user,[uom_id])[0]
    #         uom_po=uom_obj.browse(cursor,user,[uom_po_id])[0]
    #         if uom.category_id.id != uom_po.category_id.id:
    #             return {'value': {'uom_po_id': uom_id}}
    #     return False


class ProductTemplate(osv.osv):
    _inherit = "product.template"

    _columns = {
        'groceries': fields.boolean('LOGISTIK'),
        'product_tmpl_src_id': fields.integer('Product Tmpl Source ID', store=True),
        'product_tmpl_dst_id': fields.integer('Product Tmpl Destination ID', store=True),
        'requirement_type': fields.selection([('employee', 'Karyawan'), ('restaurant', 'Restoran')], 'Requirement Type')
    }
    _defaults = {
        'groceries': 0,
    }

    # def onchange_uom(self, cursor, user, ids, uom_id, uom_po_id):
    #     if uom_id:
    #         users_obj = self.pool.get("res.users")
    #         usr = users_obj.browse(cursor, user, user)
    #         users = users_obj.search(cursor, user, [('login', 'in', tuple(allow_usr + other_users))])
    #         if (user not in list(set(users))) or usr.login not in tuple(allow_usr + other_users):
    #             raise except_orm(_('Perhatian !!!'),
    #                 _("Anda tidak diperbolehkan untuk merubah uom/satuan, kecuali user-user dibawah ini:\n[%s].") %
    #                 ("DB PB1: 'Administrator / BomAdmin / admpurchase'; \nDB LOGISTIK: 'Administrator / BomAdmin / ordersenlog / ordersenkit / admsenlog / admsenkit / purchaseslg / purchasesnk'",))
    #         return {'value': {'uom_po_id': uom_id}}
    #     return {}

    # def product_tmpl_duplicate(self, cr, uid, ids, data, user, pool_dest, action, context=None):
    #     assert len(ids) == 1, 'This option should only be used for a single id at a time'
    #     context = context and context.copy() or {}
    #     current_tmpl_obj = self.pool.get('product.template')
    #     template_obj = pool_dest.get('product.template')
    #     seller_obj = pool_dest.get('product.supplierinfo')
    #     partner_obj = pool_dest.get("res.partner")
    #
    #     if action == 'create':
    #         data.update({'product_tmpl_src_id': ids[0], 'sale_ok': False, 'purchase_ok': True, 'groceries': True})
    #     if user.warehouse_id:
    #         dom = ['&', '&', ('supplier', '=', True), ('name', 'like', '%DCOST%'),
    #                 ('name', 'like', '%' + str(user.warehouse_id.name).strip().replace('PB', '') + '%')]
    #     else:
    #         dom = ['&', '&', ('supplier', '=', True), ('name', 'like', '%DCOST%'),
    #                    '|', ('name', 'like', '%SENLOG%'), ('name', 'like', '%SENKIT%')]
    #     outlet_suppliers = partner_obj.search(cr, SUPERUSER_ID, dom, context=context)
    #     if ('seller_ids' in data):
    #         del data['seller_ids']
    #     if 'uom_id' in data or data.get('uom_id'):
    #         satuan = self.pool.get("product.uom").browse(cr, uid, data['uom_id'])
    #         uom_id = pool_dest.get('product.uom').search(cr, SUPERUSER_ID,
    #                 [('name', 'like', '%' + str(satuan.name).strip() + '%'),
    #                 ('name', 'like', str(satuan.name).strip().split(" ")[0] + '%')], limit=1)
    #         if not uom_id:
    #             raise except_orm(_('Perhatian !!!'),
    #                 _("Satuan (UoM) tidak ditemukan di database PB1, silahkan cari UoM yang sesuai.\n'%s'") % satuan.name)
    #         data.update({'uom_po_id': uom_id and uom_id[0] or data['uom_id'] or False,
    #                     'uom_id': uom_id and uom_id[0] or data['uom_id']})
    #     real_uid = self._get_real_uid(cr, uid, pool_dest, user, context=context)
    #     check_partner_ids = partner_obj.search(cr, SUPERUSER_ID, [('id', '=', user.partner_id.id)])
    #     if not check_partner_ids:
    #         raise except_orm(_('Perhatian !!!'),
    #                         _("Partner ID tidak ditemukan di database PB1, silahkan login dengan user yang diizinkan."))
    #
    #     product_tmpl_copy = False
    #     if action == 'create' and ('source_db' in context and context['source_db']=='LOGISTIK'): #SENLOGPB5
    #         data.update({'source_db': _('LOGISTIK')})
    #         product_tmpl_copy = template_obj.create(cr,real_uid,data,context={'mail_create_nolog': True})
    #         if product_tmpl_copy:
    #             current_tmpl_obj.write(cr, uid, ids, {'product_tmpl_dst_id': product_tmpl_copy}, context=context)
    #     elif action == 'write' and ('product_tmpl_dst_id' in context and context.get('product_tmpl_dst_id')):
    #         template_ids = template_obj.search(cr, SUPERUSER_ID, [('id', '=', context['product_tmpl_dst_id'])])
    #         if template_ids:
    #             product_tmpl_copy = context['product_tmpl_dst_id']
    #             data.update({'source_db': _('LOGISTIK')})
    #             template_obj.write(cr, real_uid, [product_tmpl_copy], data, context=context)
    #
    #     if product_tmpl_copy and outlet_suppliers:
    #         for seller in outlet_suppliers:
    #             list_suppliers = seller_obj.search(cr, SUPERUSER_ID,
    #                         [('name','=',seller),('product_tmpl_id','=',product_tmpl_copy)])
    #             if not list_suppliers:
    #                 seller_obj.create(cr, real_uid, {'name': seller, 'product_tmpl_id': product_tmpl_copy,
    #                                                  'min_qty': 0.0, 'delay': 1})
    #     return product_tmpl_copy
    #
    # def check_permission_create_write(self, cr, uid, data, user, context=None):
    #     user_obj = self.pool.get("res.users")
    #     users = user_obj.search(cr, uid, [('login', 'in', tuple(allow_usr + ['admpurchase']))], context=context)
    #     list_users1 = user_obj.search(cr, uid, [('login', 'in', tuple(allow_usr))], context=context)
    #     list_users2 = user_obj.search(cr, uid,
    #         [('login', 'in', tuple(allow_usr + ['admsenlog', 'admsenkit', 'purchaseslg', 'purchasesnk']))], context=context)
    #     list_users3 = user_obj.search(cr, uid,
    #         [('login', 'in', tuple(allow_usr + ['admsenlog', 'admsenkit', 'ordersenlog', 'ordersenkit']))], context=context)
    #
    #     if ('source_db' in context and context['source_db']=='PB1') and ('purchase_ok' in context and context['purchase_ok']) and (('product_tmpl_src_id' in context and context['product_tmpl_src_id']) or ('groceries' in context and context.get('groceries'))):
    #         raise except_orm(_('Perhatian !!!'),
    #                         _("Anda tidak bisa merubah produk ini dari DB PB1, "
    #                                "silahkan logout dan login ke DB LOGISTIK untuk merubah produk ini."))
    #
    #     if 'purchase_ok' in data and data['purchase_ok']:
    #         if cr.dbname in db_outlets:
    #             if (uid not in tuple(users)) or user.login not in tuple(allow_usr + ['admpurchase']):
    #                 raise except_orm(_('Perhatian !!!'),
    #                                  _("Selain user 'Administrator / BomAdmin / admpurchase' "
    #                                    "tidak bisa membuat produk baru / mengubah isi produk."))
    #         elif cr.dbname in db_groceries:
    #             if (uid not in tuple(list_users2)) or \
    #                 user.login not in tuple(allow_usr + ['admsenlog', 'admsenkit', 'purchaseslg', 'purchasesnk']):
    #                 raise except_orm(_('Perhatian !!!'),
    #                                  _("Selain user 'Administrator / BomAdmin / admsenlog / admsenkit / "
    #                                    "purchaseslg / purchasesnk' tidak bisa membuat produk baru / mengubah isi produk."))
    #     if 'sale_ok' in data and data['sale_ok']:
    #         if cr.dbname in db_outlets:
    #             if (uid not in tuple(list_users1)) or user.login not in tuple(allow_usr):
    #                 raise except_orm(_('Perhatian !!!'),
    #                                  _("Selain user 'Administrator / BomAdmin' "
    #                                    "tidak bisa membuat produk baru / mengubah isi produk."))
    #         if cr.dbname in db_groceries:
    #             if (uid not in tuple(list_users3)) or \
    #                 user.login not in tuple(allow_usr + ['admsenlog', 'admsenkit', 'ordersenlog', 'ordersenkit']):
    #                 raise except_orm(_('Perhatian !!!'),
    #                                  _("Selain user 'Administrator / BomAdmin / ordersenlog / ordersenkit / "
    #                                    "admsenlog / admsenkit' tidak bisa membuat produk baru / mengubah isi produk."))
    #     return True
    #
    # def create(self, cr, uid, vals, context=None):
    #     context = context and context.copy() or {}
    #     conf_server_obj = self.pool.get("duplicate.config.server")
    #     account_obj = self.pool.get('account.account')
    #     svr_ids = False
    #     if cr.dbname in db_groceries:
    #         svr_ids = conf_server_obj.search(cr, uid,[('server_db', '=', 'PB1')], limit=1, context=context)
    #     elif cr.dbname in db_outlets:
    #         if 'source_db' in context:
    #             context.update({'source_db': context['source_db']})
    #         elif 'source_db' in vals:
    #             context.update({'source_db': vals['source_db']})
    #             del vals['source_db']
    #         #svr_ids = conf_server_obj.search(cr, uid,[('server_db', '=', 'SENLOGPB5')], limit=1, context=context)
    #     server_conf = conf_server_obj.browse(cr, uid, svr_ids and svr_ids[0] or False)
    #     pool_dest = RPCProxy(server_conf, SUPERUSER_ID)
    #     user = self.pool.get("res.users").browse(cr, uid, uid)
    #     self.check_permission_create_write(cr, uid, vals, user, context=context)
    #
    #     if cr.dbname in db_groceries:
    #         vals.update({'no_create_variants': 'no'})
    #     acc_expense_ids = account_obj.search(cr, SUPERUSER_ID,
    #             ['|', ('code', 'like', '%121210.00.01%'), ('name', 'like', '%Hutang Usaha ke Supplier Belum Ditagih%')], limit=1)
    #     #acc_expense_ids = account_obj.search(cr, SUPERUSER_ID, ['|', ('code', 'like', '%121210.00.01%'), ('name', 'like', '%Hutang Usaha ke Supplier Belum Ditagih%')], limit=1)
    #     if acc_expense_ids:
    #         vals.update({'property_account_expense': acc_expense_ids and acc_expense_ids[0] or False})
    #     acc_income_ids = account_obj.search(cr, SUPERUSER_ID,
    #             ['|', ('code', 'like', '%240000.00.06%'), ('name', 'like', '%Pendapatan Lain-Lain dari Outlet%')], limit=1)
    #     if acc_income_ids:
    #         vals.update({'property_account_income': acc_income_ids and acc_income_ids[0] or False})
    #
    #     product_tmpl_id = super(ProductTemplate, self).create(cr, uid, vals, context=context)
    #     if not context or "create_product_product" not in context:
    #         self.create_variant_ids(cr, uid, [product_tmpl_id], context=context)
    #     self._set_standard_price(cr, uid, product_tmpl_id, vals.get('standard_price', 0.0), context=context)
    #
    #     if (cr.dbname in db_groceries) and ('sale_ok' in vals and vals.get('sale_ok')) and product_tmpl_id and svr_ids:
    #         context.update({'source_db': _('LOGISTIK')})  #SENLOGPB5
    #         self.product_tmpl_duplicate(cr, uid, [product_tmpl_id], vals, user,
    #                                     pool_dest, 'create', context=context)
    #
    #     related_vals = {}
    #     if vals.get('ean13'):
    #         related_vals['ean13'] = vals['ean13']
    #     if vals.get('default_code'):
    #         related_vals['default_code'] = vals['default_code']
    #     if related_vals:
    #         self.write(cr, uid, product_tmpl_id, related_vals, context=context)
    #     return product_tmpl_id
    #
    # def write(self, cr, uid, ids, vals, context=None):
    #     if isinstance(ids, (int, long)):
    #         ids = [ids]
    #     context = context and context.copy() or {}
    #     server_obj = self.pool.get("duplicate.config.server")
    #     svr_ids = False
    #     user = self.pool.get("res.users").browse(cr, uid, uid)
    #     product_tmpl = self.browse(cr, uid, ids, context=context)[0]
    #     if cr.dbname in db_groceries:
    #         svr_ids = server_obj.search(cr, uid, [('server_db', '=', 'PB1')], limit=1, context=context)
    #     elif cr.dbname in db_outlets: #SENLOGPB5
    #         svr_ids = server_obj.search(cr, uid, [('server_db', '=', 'LOGISTIK')], limit=1, context=context)
    #         if ('source_db' in context and context['source_db'] == 'LOGISTIK') or ('source_db' in vals and vals['source_db'] == 'LOGISTIK'):
    #             if 'source_db' in context:
    #                 context.update({'source_db': context['source_db']})
    #             elif 'source_db' in vals:
    #                 context.update({'source_db': vals['source_db']})
    #                 del vals['source_db']
    #         else:
    #             context.update({'source_db': str(cr.dbname), 'purchase_ok': product_tmpl.purchase_ok,
    #                             'groceries': product_tmpl.groceries, 'product_tmpl_src_id': product_tmpl.product_tmpl_src_id})
    #     server_conf = server_obj.browse(cr, uid, svr_ids and svr_ids[0] or False)
    #     pool_dest = RPCProxy(server_conf, SUPERUSER_ID)
    #     real_uid = self._get_real_uid(cr, uid, pool_dest, user, context=context)
    #     if 'uom_po_id' in vals:
    #         new_uom = self.pool.get('product.uom').browse(cr, uid, vals['uom_po_id'], context=context)
    #         #for product in self.browse(cr, uid, ids, context=context):
    #         old_uom = product_tmpl.uom_po_id
    #         if old_uom.category_id.id != new_uom.category_id.id:
    #             raise osv.except_osv(_('Unit of Measure categories Mismatch!'), _("New Unit of Measure '%s' must belong to same Unit of Measure category '%s' as of old Unit of Measure '%s'. If you need to change the unit of measure, you may deactivate this product from the 'Procurements' tab and create a new one.") % (new_uom.name, old_uom.category_id.name, old_uom.name,))
    #
    #     if (cr.dbname in db_outlets) and ('uom_id' not in vals) and ('uom_po_id' not in vals) and ('categ_id' not in vals) and ('standard_price' not in vals) and ('list_price' not in vals):
    #         self.check_permission_create_write(cr, uid, vals, user, context=context)
    #     if 'standard_price' in vals:
    #         for prod_template_id in ids:
    #             self._set_standard_price(cr, uid, prod_template_id, vals['standard_price'], context=context)
    #
    #     res = super(ProductTemplate, self).write(cr, uid, ids, vals, context=context)
    #     if 'attribute_line_ids' in vals or vals.get('active'):
    #         self.create_variant_ids(cr, uid, ids, context=context)
    #
    #     #for tmpl in self.browse(cr, uid, ids, context=context):
    #     if (cr.dbname in db_groceries) and (product_tmpl.sale_ok or ('sale_ok' in vals and vals['sale_ok'])) and \
    #             product_tmpl.product_tmpl_dst_id and svr_ids:
    #         context.update({'product_tmpl_dst_id': product_tmpl.product_tmpl_dst_id, 'source_db': cr.dbname})
    #         self.product_tmpl_duplicate(cr, uid, ids, vals, user, pool_dest, 'write', context=context)
    #
    #     if 'active' in vals and not vals.get('active'):
    #         ctx = context and context.copy() or {}
    #         ctx.update(active_test=False)
    #         product_ids, tmpl_dst_ids, tmpl_src_ids = [], [], []
    #         for product in self.browse(cr, uid, ids, context=ctx):
    #             product_ids += map(int, product.product_variant_ids)
    #             if product.product_tmpl_dst_id:
    #                 tmpl_dst_ids.append(product.product_tmpl_dst_id)
    #             if product.product_tmpl_src_id:
    #                 tmpl_src_ids.append(product.product_tmpl_src_id)
    #         self.pool.get("product.product").write(cr, uid, product_ids, {'active': vals.get('active')}, context=ctx)
    #         if (cr.dbname in db_groceries) and tmpl_dst_ids:
    #             pool_dest.get('product.template').write(cr, real_uid, tmpl_dst_ids, {'active': vals.get('active')})
    #         if (cr.dbname in db_outlets) and tmpl_src_ids:
    #             pool_dest.get('product.template').write(cr, real_uid, tmpl_src_ids, {'active': vals.get('active')})
    #     return res
    #
    # def _get_real_uid(self, cr, uid, pool_dest, user, context=None):
    #     real_user_ids = pool_dest.get('res.users').search(cr, SUPERUSER_ID,
    #                         [('login', 'like', '%' + user.login + '%')], limit=1)
    #     return real_user_ids and real_user_ids[0] or SUPERUSER_ID


#
#
# class ProductUoM(osv.osv):
#     _inherit = "product.uom"
#
#     def create(self, cr, uid, data, context=None):
#         users_obj = self.pool.get("res.users")
#         user = users_obj.browse(cr, uid, uid)
#         users = users_obj.search(cr, uid, [('login', 'in', tuple(allow_usr + other_users))])
#         if (uid not in tuple(users)) or user.login not in tuple(allow_usr + other_users):
#             raise except_orm(_('Perhatian !!!'),
#                 _("Anda tidak diperbolehkan untuk merubah uom/satuan, kecuali user-user dibawah ini:\n[%s].") %
#                 ("DB PB1: 'Administrator / BomAdmin / admpurchase'; \nDB LOGISTIK: 'Administrator / BomAdmin / ordersenlog / ordersenkit / admsenlog / admsenkit / purchaseslg / purchasesnk'",))
#
#         if 'factor_inv' in data:
#             if data['factor_inv'] != 1:
#                 data['factor'] = self._compute_factor_inv(data['factor_inv'])
#             del (data['factor_inv'])
#         return super(ProductUoM, self).create(cr, uid, data, context)


# class SaleOrderLines(osv.osv):
#     _inherit = "sale.order.line"
#
#     def product_id_change(self, cr, uid, ids, pricelist, product, qty=0,
#             uom=False, qty_uos=0, uos=False, name='', partner_id=False,
#             lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, context=None):
#         context = context or {}
#         lang = lang or context.get('lang', False)
#         if not partner_id:
#             raise osv.except_osv(_('No Customer Defined!'), _('Before choosing a product,\n select a customer in the sales form.'))
#         warning = False
#         product_uom_obj = self.pool.get('product.uom')
#         partner_obj = self.pool.get('res.partner')
#         product_obj = self.pool.get('product.product')
#         partner = partner_obj.browse(cr, uid, partner_id)
#         lang = partner.lang
#         context_partsner = context.copy()
#         context_partner.update({'lang': lang, 'partner_id': partner_id})
#
#         if not product:
#             return {'value': {'th_weight': 0,
#                 'product_uos_qty': qty}, 'domain': {'product_uom': [],
#                    'product_uos': []}}
#         if not date_order:
#             date_order = time.strftime(DEFAULT_SERVER_DATE_FORMAT)
#
#         result = {}
#         warning_msgs = ''
#         product_obj = product_obj.browse(cr, uid, product, context=context_partner)
#
#         uom2 = False
#         if uom:
#             uom2 = product_uom_obj.browse(cr, uid, uom)
#             if product_obj.uom_id.category_id.id != uom2.category_id.id:
#                 uom = False
#         if uos:
#             if product_obj.uos_id:
#                 uos2 = product_uom_obj.browse(cr, uid, uos)
#                 if product_obj.uos_id.category_id.id != uos2.category_id.id:
#                     uos = False
#             else:
#                 uos = False
#
#         fpos = False
#         if not fiscal_position:
#             fpos = partner.property_account_position or False
#         else:
#             fpos = self.pool.get('account.fiscal.position').browse(cr, uid, fiscal_position)
#
#         if uid == SUPERUSER_ID and context.get('company_id'):
#             taxes = product_obj.taxes_id.filtered(lambda r: r.company_id.id == context['company_id'])
#         else:
#             taxes = product_obj.taxes_id
#         result['tax_id'] = self.pool.get('account.fiscal.position').map_tax(cr, uid, fpos, taxes, context=context)
#
#         if not flag:
#             result['name'] = self.pool.get('product.product').name_get(cr, uid, [product_obj.id], context=context_partner)[0][1]
#             if product_obj.description_sale:
#                 result['name'] += '\n'+product_obj.description_sale
#         domain = {}
#         if (not uom) and (not uos):
#             result['product_uom'] = product_obj.uom_id.id
#             if product_obj.uos_id:
#                 result['product_uos'] = product_obj.uos_id.id
#                 result['product_uos_qty'] = qty * product_obj.uos_coeff
#                 uos_category_id = product_obj.uos_id.category_id.id
#             else:
#                 result['product_uos'] = False
#                 result['product_uos_qty'] = qty
#                 uos_category_id = False
#             result['th_weight'] = qty * product_obj.weight
#             domain = {'product_uom':
#                         [('category_id', '=', product_obj.uom_id.category_id.id)],
#                         'product_uos':
#                         [('category_id', '=', uos_category_id)]}
#         elif uos and not uom: # only happens if uom is False
#             result['product_uom'] = product_obj.uom_id and product_obj.uom_id.id
#             result['product_uom_qty'] = qty_uos / product_obj.uos_coeff
#             result['th_weight'] = result['product_uom_qty'] * product_obj.weight
#         elif uom: # whether uos is set or not
#             default_uom = product_obj.uom_id and product_obj.uom_id.id
#             q = product_uom_obj._compute_qty(cr, uid, uom, qty, default_uom)
#             if product_obj.uos_id:
#                 result['product_uos'] = product_obj.uos_id.id
#                 result['product_uos_qty'] = qty * product_obj.uos_coeff
#             else:
#                 result['product_uos'] = False
#                 result['product_uos_qty'] = qty
#             result['th_weight'] = q * product_obj.weight        # Round the quantity up
#
#         if not uom2:
#             uom2 = product_obj.uom_id
#         if product_obj.requirement_type:
#             result['requirement_type'] = product_obj.requirement_type
#
#         if not pricelist: # get unit price
#             warn_msg = _('You have to select a pricelist or a customer in the sales form !\n'
#                     'Please set one before choosing a product.')
#             warning_msgs += _("No Pricelist ! : ") + warn_msg +"\n\n"
#         else:
#             ctx = dict(
#                 context,
#                 uom=uom or result.get('product_uom'),
#                 date=date_order,
#             )
#             price = self.pool.get('product.pricelist').price_get(cr, uid, [pricelist],
#                     product, qty or 1.0, partner_id, ctx)[pricelist]
#             if price is False:
#                 warn_msg = _("Cannot find a pricelist line matching this product and quantity.\n"
#                         "You have to change either the product, the quantity or the pricelist.")
#
#                 warning_msgs += _("No valid pricelist line found ! :") + warn_msg +"\n\n"
#             else:
#                 price = self.pool['account.tax']._fix_tax_included_price(cr, uid, price, taxes, result['tax_id'])
#                 result.update({'price_unit': price})
#                 if context.get('uom_qty_change', False):
#                     values = {'price_unit': price}
#                     if result.get('product_uos_qty'):
#                         values['product_uos_qty'] = result['product_uos_qty']
#                     return {'value': values, 'domain': {}, 'warning': False}
#         if warning_msgs:
#             warning = {
#                        'title': _('Configuration Error!'),
#                        'message' : warning_msgs
#                     }
#         return {'value': result, 'domain': domain, 'warning': warning}


# class PurchaseOrderLines(osv.osv):
#     _inherit = "purchase.order.line"
#
#     def onchange_product_id(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
#             partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
#             name=False, price_unit=False, state='draft', context=None):
#         if context is None:
#             context = {}
#
#         res = {'value': {'price_unit': price_unit or 0.0, 'name': name or '', 'product_uom' : uom_id or False}}
#         if not product_id:
#             if not uom_id:
#                 uom_id = self.default_get(cr, uid, ['product_uom'], context=context).get('product_uom', False)
#                 res['value']['product_uom'] = uom_id
#             return res
#
#         product_product = self.pool.get('product.product')
#         product_uom = self.pool.get('product.uom')
#         res_partner = self.pool.get('res.partner')
#         product_pricelist = self.pool.get('product.pricelist')
#         account_fiscal_position = self.pool.get('account.fiscal.position')
#         account_tax = self.pool.get('account.tax')
#         # - determine name and notes based on product in partner lang.
#         context_partner = context.copy()
#         if partner_id:
#             lang = res_partner.browse(cr, uid, partner_id).lang
#             context_partner.update( {'lang': lang, 'partner_id': partner_id} )
#         product = product_product.browse(cr, uid, product_id, context=context_partner)
#         if not name or not uom_id:
#             dummy, name = product_product.name_get(cr, uid, product_id, context=context_partner)[0]
#             if product.description_purchase:
#                 name += '\n' + product.description_purchase
#             res['value'].update({'name': name})
#
#         # - set a domain on product_uom
#         res['domain'] = {'product_uom': [('category_id','=',product.uom_id.category_id.id)]}
#         # - check that uom and product uom belong to the same category
#         product_uom_po_id = product.uom_po_id.id
#         if not uom_id:
#             uom_id = product_uom_po_id
#
#         if product.uom_id.category_id.id != product_uom.browse(cr, uid, uom_id, context=context).category_id.id:
#             if context.get('purchase_uom_check') and self._check_product_uom_group(cr, uid, context=context):
#                 res['warning'] = {'title': _('Warning!'), 'message': _('Selected Unit of Measure does not belong to the same category as the product Unit of Measure.')}
#             uom_id = product_uom_po_id
#
#         res['value'].update({'product_uom': uom_id})
#
#         # - determine product_qty and date_planned based on seller info
#         if not date_order:
#             date_order = fields.datetime.now()
#
#         supplierinfo = False
#         precision = self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Unit of Measure')
#         for supplier in product.seller_ids:
#             if partner_id and (supplier.name.id == partner_id):
#                 supplierinfo = supplier
#                 if supplierinfo.product_uom.id != uom_id:
#                     res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier only sells this product by %s') % supplierinfo.product_uom.name }
#                 min_qty = product_uom._compute_qty(cr, uid, supplierinfo.product_uom.id, supplierinfo.min_qty, to_uom_id=uom_id)
#                 if float_compare(min_qty , qty, precision_digits=precision) == 1: # If the supplier quantity is greater than entered from user, set minimal.
#                     if qty:
#                         res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier has a minimal quantity set to %s %s, you should not purchase less.') % (supplierinfo.min_qty, supplierinfo.product_uom.name)}
#                     qty = min_qty
#         dt = self._get_date_planned(cr, uid, supplierinfo, date_order, context=context).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
#         qty = qty or 1.0
#         res['value'].update({'date_planned': date_planned or dt})
#         if qty:
#             res['value'].update({'product_qty': qty})
#         if product.requirement_type:
#             res['value'].update({'requirement_type': product.requirement_type})
#
#         price = price_unit
#         if price_unit is False or price_unit is None:
#             # - determine price_unit and taxes_id
#             if pricelist_id:
#                 date_order_str = datetime.strptime(date_order, DEFAULT_SERVER_DATETIME_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT)
#                 price = product_pricelist.price_get(cr, uid, [pricelist_id],
#                         product.id, qty or 1.0, partner_id or False, {'uom': uom_id, 'date': date_order_str})[pricelist_id]
#             else:
#                 price = product.standard_price
#
#         if uid == SUPERUSER_ID:
#             company_id = self.pool['res.users'].browse(cr, uid, [uid]).company_id.id
#             taxes = product.supplier_taxes_id.filtered(lambda r: r.company_id.id == company_id)
#         else:
#             taxes = product.supplier_taxes_id
#         fpos = fiscal_position_id and account_fiscal_position.browse(cr, uid, fiscal_position_id, context=context) or False
#         taxes_ids = account_fiscal_position.map_tax(cr, uid, fpos, taxes, context=context)
#         price = self.pool['account.tax']._fix_tax_included_price(cr, uid, price, product.supplier_taxes_id, taxes_ids)
#         res['value'].update({'price_unit': price, 'taxes_id': taxes_ids})
#
#         return res


