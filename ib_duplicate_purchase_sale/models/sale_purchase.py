# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################

from openerp import models, fields, api, _
from openerp.exceptions import Warning


class SaleOrder(models.Model):
    _inherit = "sale.order"

    purchase_order = fields.Boolean('Purchase Order', default=False,
            readonly=False, states={'done': [('readonly', True)],'cancel': [('readonly', True)]},
            help="already duplicated to purchase order (OUTLET)", copy=False)
    related_docs = fields.Char(string='Related Document', readonly=True,
            states={'done': [('readonly', True)], 'cancel': [('readonly', True)]}, copy=False)
    order_lines_count = fields.Integer('Products', compute='_compute_order_lines_count')
    purchase_id = fields.Integer('Purchase ID')

    @api.one
    @api.depends('order_line.order_id')
    def _compute_order_lines_count(self):
        self.order_lines_count = len(self.order_line)

    @api.multi
    def unlink(self):
        for order in self:
            if order.state not in ('draft', 'cancel'):
                raise Warning(_('Invalid Action!!!\n In order to delete a confirmed sales order, you must cancel it before!'))
            if order.purchase_order and order.related_docs:
                raise Warning(_('Invalid Action!!!\n This order is already integrated with Outlet or Branch.'))
        return super(SaleOrder, self).unlink()


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    requirement_type = fields.Selection([('employee', 'Karyawan'), ('restaurant', 'Restoran')],
                                        string='Requirement Type')
    purchase_line_id = fields.Integer('Purchase Order ID')



class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    sales_order = fields.Boolean('Sales Order', default=False,
            readonly=False, states={'done': [('readonly', True)],'cancel': [('readonly', True)]},
            help="already duplicated to sales order (Groceries)", copy=False)
    related_docs = fields.Char(string='Related Document', readonly=True,
            states={'done': [('readonly', True)], 'cancel': [('readonly', True)]}, copy=False)
    order_lines_count = fields.Integer('Products', compute='_compute_order_lines_count')
    user_id = fields.Many2one('res.users', string='User', readonly=True,
            states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
            default=lambda self: self.env.user)
    sale_id = fields.Integer('Sales ID')

    @api.one
    @api.depends('order_line.order_id')
    def _compute_order_lines_count(self):
        self.order_lines_count = len(self.order_line)

    @api.multi
    def unlink(self):
        for order in self:
            if order.state not in ('draft', 'cancel'):
                raise Warning(_('Invalid Action!!!\n In order to delete a purchase order, you must cancel it first.'))
            if order.sales_order and order.related_docs:
                raise Warning(_('Invalid Action!!!\n This order is already integrated with logistics (SENLOG/SENKIT).'))
        return super(PurchaseOrder, self).unlink()



class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    requirement_type = fields.Selection([('employee', 'Karyawan'), ('restaurant', 'Restoran')],
            string='Requirement Type')
    sale_line_id = fields.Integer('Sale Order Lines ID')

