# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################

import time
from openerp import models, fields, api

class DuplicateConfigServer(models.Model):
    '''Config server for duplication purchase order to sales order'''
    _name = "duplicate.config.server"
    _description = "Synchronized server"

    name = fields.Char('Server name', required=True)
    server_url = fields.Char('Server URL', required=True)
    server_port = fields.Integer('Server Port', required=True, default=8069)
    server_db = fields.Char('Server Database', required=True)
    login = fields.Char('User Name', required=True)
    password = fields.Char('Password', required=True)
    # obj_ids = fields.One2many('duplicate.config.obj', 'server_id',
    #                           string='Models', ondelete='cascade')



class DuplicateHistory(models.Model):
    _name = "duplicate.history"
    _description = "history of duplication"

    current_time = lambda *args: time.strftime('%Y-%m-%d %H:%M:%S')
    name = fields.Datetime('Date', required=True,
                           default=current_time)
    user_id = fields.Many2one('res.users', "Duplicated by",
                              default=lambda self: self.env.user)
    src_model_id = fields.Many2one('ir.model', string='Source Object')
    dest_model_id = fields.Many2one('ir.model', string='Destination Object')
    source_id = fields.Integer('Source ID', readonly=True)
    destination_id = fields.Integer('Destination ID', readonly=True)



class ResDuplicate(models.Model):
    _name = 'res.duplicate'
    _order = 'date desc'
    _description = 'Result duplication'

    name = fields.Char('Subject', required=True)
    date = fields.Datetime('Date')
    act_from = fields.Many2one('res.users', 'From')
    act_to = fields.Many2one('res.users', 'To')
    body = fields.Text('Request')

# class DuplicateConfigObj(wizard.Model):
#     _name = "duplicate.config.obj"
#     _description = "Register Class"
#     _order = 'sequence'
#
#     name = fields.Char('Name', select=1, required=True)
#     domain = fields.Char('Domain', select=1, required=True, default='[]')
#     server_id = fields.Many2one('duplicate.config.server', 'Server',
#                                 ondelete='cascade', select=1, required=True)
#     model_id = fields.Many2one('ir.model', string='Source Object', required=True)
#     sequence = fields.Integer('Sequence')
#     active = fields.Boolean('Active', default=True)
#     duplicate_date = fields.Datetime('Latest Duplication', readonly=True)
#     line_id = fields.One2many('duplicate.config.obj.line', 'obj_id',
#                               'IDs Affected', ondelete='cascade')
#     avoid_ids = fields.One2many('duplicate.config.obj.avoid', 'obj_id',
#                                 'Fields Not Sync.')
#     dest_model_id = fields.Many2one('ir.model', string='Destination Object', required=True)
#
#     # Return a list of changes: [ (date, id) ]
#
#     @api.model
#     def get_ids(self, obj, dt, domain=[]):
#         return self._get_ids(obj, dt, domain)
#
#     @api.model
#     def _get_ids(self, obj, dt, domain=[]):
#         POOL = self.env[obj]
#         result = []
#         if dt:
#             domain2 = domain + [('write_date', '>=', dt)]
#             domain3 = domain + [('create_date', '>=', dt)]
#         else:
#             domain2 = domain3 = domain
#         obj_rec = POOL.search(domain2)
#         obj_rec += POOL.search(domain3)
#         for r in obj_rec.read(['create_date', 'write_date']):
#             result.append((r['write_date'] or r['create_date'], r['id']))
#         return result


# class DuplicateConfigObjAvoid(wizard.Model):
#     _name = "duplicate.config.obj.avoid"
#     _description = "Fields to not duplicate"
#
#     name = fields.Char('Field Name', select=1, required=True)
#     obj_id = fields.Many2one('duplicate.config.obj', 'Object',
#                              required=True, ondelete='cascade')


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
