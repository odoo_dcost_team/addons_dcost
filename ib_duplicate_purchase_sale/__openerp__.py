# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
{
    "name": "Duplicate PO to SO [Multi-DB]",
    "version": "0.1",
    "category": "Custom Module",
    "depends": [
        "base",
        "siu_import_pos",
        "purchase",
    ], #"base_synchro",
    "author":"Ibrohim Binladin | +6283871909782 | ibradiiin@gmail.com",
    "website": "http://ibrohimbinladin.wordpress.com",
    "description": """
Duplicate Purchase Order to Sales Order [Multi-DB] 
""",
    "data": [
        "security/ir.model.access.csv",
        "wizard/wzd_view.xml",
        "views/view.xml",
    ],
    "installable": True,
    "auto_install": False,
}
