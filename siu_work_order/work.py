import time
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

class work_order(osv.osv):
    _name = "work.order"
    _columns = {
        'name': fields.char('Work Order', required=True, size=64, readonly=True, states={'draft': [('readonly', False)]}),
        'date' : fields.date('Order Date', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'bom_id': fields.many2one('mrp.bom', 'BoM', readonly=True, states={'draft': [('readonly', False)]}),
        
        'product_id': fields.many2one('product.product', 'Product', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'product_qty': fields.float('Quantity', digits_compute=dp.get_precision('Product UoM'), required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'product_uom': fields.many2one('product.uom', 'UoM', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        
        'state': fields.selection([('draft', 'Draft'), ('approve', 'Approved'), ('done', 'Done')], 'State', readonly=True),
        'material_lines': fields.one2many('raw.material.line', 'work_id', 'Material Consumption', readonly=True, states={'draft': [('readonly', False)]}),
        'note': fields.text('Notes'),
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', required=True, readonly=True, states={'draft':[('readonly',False)]}),
        'location_src_id': fields.many2one('stock.location', 'Source Location', readonly=True, states={'draft': [('readonly', False)]}),
        'location_dest_id': fields.many2one('stock.location', 'Destination Location', readonly=True, states={'draft': [('readonly', False)]}),
    }

    def _get_warehouse_id(self, cr, uid, ids, context=None): #modified-by-Baim
        warehouse_id = False
        user = self.pool['res.users'].browse(cr, uid, uid, context=context)
        if user.warehouse_id:
            warehouse_id = user.warehouse_id.id
        return warehouse_id
    
    _defaults = {
        'name': '/',
        'note': '-',
        'product_qty': 1,
        'state': 'draft',
        'date': time.strftime('%Y-%m-%d'),
        'warehouse_id': _get_warehouse_id, #modified-by-Baim
    }


    def unlink(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids, context={})[0]
        if val.state != 'draft':
            raise osv.except_osv(('Perhatian !'), ('WO tidak bisa dihapus pada state \'%s\'!') % (val.state,))
        return super(work_order, self).unlink(cr, uid, ids, context=context)

    def create(self, cr, uid, vals, context=None): 
        vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'work.order')
        return super(work_order, self).create(cr, uid, vals, context=context)

    def warehouse_change(self, cr, uid, ids):
        res = {'warehouse_id': self.pool.get('res.users').browse(cr, uid, uid).warehouse_id.id or False}
        return  {'value': res}
        
    def product_change(self, cr, uid, ids, product):
        bom_obj = self.pool.get('mrp.bom')
        res = {}
        if product:
            brg = self.pool.get('product.product').browse(cr, uid, product)
            bom_id = bom_obj._bom_find(cr, uid, product_tmpl_id=brg.product_tmpl_id.id, product_id=brg.id, properties=[], context=None)
            res = {'product_uom': brg.uom_id and brg.uom_id.id, 'bom_id': bom_id}
            return  {'value': res}
        return True
        
    def bom_change(self, cr, uid, ids, product, qty, bmid):
        bom_obj = self.pool.get('mrp.bom')
        uom_obj = self.pool.get('product.uom')
        
        material = []
        if product:
            brg = self.pool.get('product.product').browse(cr, uid, product)
            bom_id = bom_obj._bom_find(cr, uid, brg.uom_id.id, product_id=product, properties=[], context=None)
            if bom_id:
                bom_point = bom_obj.browse(cr, uid, bom_id)
                factor = uom_obj._compute_qty(cr, uid, brg.uom_id.id, qty, bom_point.product_uom.id)
                material = bom_obj._bom_explode(cr, uid, bom_point, brg, factor/bom_point.product_qty, properties=[], routing_id=False, context={})[0]
            else:
                bom_id = bmid
                bom_point = bom_obj.browse(cr, uid, bmid)
                material = bom_obj._bom_explode(cr, uid, bom_point, bom_point.product_tmpl_id, 1.0/1.0, properties=[], routing_id=False, context={})[0]
            if qty and qty>0: #edited-baim
                for line in material:
                    line['product_qty'] = (line['product_qty'] * qty)
            res = {'product_uom': brg.uom_id.id, 'bom_id': bom_id, 'material_lines': material}
            return  {'value': res}
        return True
        
    def work_cancel(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'draft'})
        return True                                  
         
    def work_confirm(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        if not val.material_lines:
            raise osv.except_osv(('Perhatian !'), ('Tabel Raw Material harus di isi !'))
        if val.product_qty <= 0 :
            raise osv.except_osv(('Perhatian !'), ('Quantity product harus diatas 0!'))

        self.write(cr, uid, ids, {'state': 'approve'})
        return True

    def work_validate(self, cr, uid, ids, context=None):
        obj_move = self.pool.get('stock.move')
        obj_picking = self.pool.get('stock.picking')
        val = self.browse(cr, uid, ids, context={})[0]
        
        material_id = obj_picking.create(cr,uid, {
                                    'origin': val.name,
                                    'work_id': val.id,
                                    'type_wo': 'rm',
                                    'picking_type_id': val.warehouse_id.int_type_id.id,
                                    'move_type': 'one',
                                    'date': val.date,
                                    'note': val.note or "",
                                    'company_id': val.warehouse_id.company_id.id,
                                })
            
        goods_id = obj_picking.create(cr,uid, {
                                    'origin': val.name,
                                    'work_id': val.id,
                                    'type_wo': 'fg',
                                    'picking_type_id': val.warehouse_id.int_type_id.id,
                                    'move_type': 'one',
                                    'date': val.date,
                                    'note': val.note or "",
                                    'company_id': val.warehouse_id.company_id.id,
                                })
        
        picking_list = [material_id, goods_id]

        idg = obj_move.create(cr,uid, {
                                    'name': val.product_id.partner_ref,
                                    'date': val.date,
                                    'product_id': val.product_id.id,
                                    'product_uom': val.product_uom.id,
                                    'product_uom_qty': val.product_qty,
                                    'location_id': val.location_src_id and val.location_src_id.id or 7,
                                    'location_dest_id': val.location_dest_id and val.location_dest_id.id or val.warehouse_id.lot_stock_id.id,
                                    'picking_id': goods_id,
                                    'origin': val.name or val.note,
                                    'warehouse_id': val.warehouse_id.id,
                                    'company_id': val.warehouse_id.company_id.id})

        sum = []
        for x in val.material_lines:
            sum.append(obj_move.create(cr,uid, {
                                    'name': x.product_id.partner_ref,
                                    'date': val.date,
                                    'product_id': x.product_id.id,
                                    'product_uom': x.product_uom.id,
                                    'product_uom_qty': x.product_qty,
                                    'location_id': x.location_src_id and x.location_src_id.id or val.warehouse_id.lot_stock_id.id,
                                    'location_dest_id': x.location_dest_id and x.location_dest_id.id or 7,
                                    'picking_id': material_id,
                                    'origin': val.name or val.note,
                                    'warehouse_id': val.warehouse_id.id,
                                    'company_id': val.warehouse_id.company_id.id}))
                
        obj_picking.action_confirm(cr, uid, picking_list, context=context)
        obj_picking.force_assign(cr, uid, picking_list, context=context)
        obj_picking.action_done(cr, uid, picking_list, context=context)
        
        cost = 0.0
        for s in obj_move.browse(cr, uid, sum):
            harga = s.price_unit
            if not harga:
                harga = 0
            cost += harga*s.product_uom_qty

        if val.product_qty > 0.0:
            obj_move.write(cr, uid, [idg], {'price_unit': float(cost)/float(val.product_qty)})
            for o in obj_move.browse(cr, uid, idg).quant_ids:
                self.pool.get('stock.quant').write(cr, uid, [o.id], {'cost': cost/val.product_qty})
                
        self.write(cr, uid, ids, {'state': 'done'})
        return True
      

class raw_material_line(osv.osv):
    _name = "raw.material.line"
    _columns = {
        'name': fields.char('Description'),
        'work_id': fields.many2one('work.order', 'Work Order', required=True, ondelete='cascade'),
        'product_id': fields.many2one('product.product', 'Product', required=True),
        'product_qty': fields.float('Quantity', digits_compute=dp.get_precision('Product Unit of Measure'), required=True),
        'product_uom': fields.many2one('product.uom', 'UoM', required=True),
        'location_src_id': fields.many2one('stock.location', 'Source Location'),
        'location_dest_id': fields.many2one('stock.location', 'Destination Location'),
    }     

    _defaults = {
        'product_qty': 1,
    }
     
    def product_change(self, cr, uid, ids, product):
        if product:
            obj_product = self.pool.get('product.product').browse(cr, uid, product)
            res = {'name': obj_product.partner_ref, 'product_uom': obj_product.uom_id.id}
            return  {'value': res}
        return True



class validate_manufacture(osv.osv_memory):
    _name = "validate.manufacture"
    _columns = {
                'name': fields.char('File Name', 16),
                'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', required=True),
    }    
         
    def onchange_warehouse(self, cr, uid, ids, wid):
        gdg = self.pool.get('res.users').browse(cr, uid, uid).warehouse_id.id
        return {'value': {'warehouse_id': gdg}}

    def validate(self, cr, uid, ids, context=None):                
        val = self.browse(cr, uid, ids)[0]
        
        obj_product = self.pool.get('product.product')
        obj_sto = self.pool.get('stock.order')
        obj_point = self.pool.get('stock.warehouse.orderpoint') ##Reordering rules
        
        loc = val.warehouse_id.lot_stock_id.id            
        sid = obj_point.search(cr, uid, [('location_id', '=', loc)])
        if sid:
            sad = obj_point.browse(cr, uid, sid)
            bpid = [x.product_id.id for x in sad]
            senlog = obj_product.search(cr, uid, [('id', 'in', bpid), ('tipe', '=', 'log'), ('purchase_ok', '=', True)])
            senkit = obj_product.search(cr, uid, [('id', 'in', bpid), ('tipe', '=', 'kit'), ('purchase_ok', '=', True)])
         
            kit_line = []; log_line = []
            for l in senlog:
                pro = obj_product.browse(cr, uid, l)
                beli = self.hitung_order(cr, uid, ids, l, loc)
                if beli:
                    log_line.append((0, 0, {
                                             'name': pro.partner_ref,
                                             'product_id': l,
                                             'product_qty': beli,
                                             'product_uom': pro.uom_id.id
                    }))
             
            for k in senkit:
                pro = obj_product.browse(cr, uid, k)
                order = self.hitung_order(cr, uid, ids, k, loc)
                if order:
                    kit_line.append((0, 0, {
                                             'name': pro.partner_ref,
                                             'product_id': k,
                                             'product_qty': order,
                                             'product_uom': pro.uom_id.id
                    }))
             
            if log_line:
                stlogid = obj_sto.create(cr, uid, {
                                         'warehouse_id': val.warehouse_id.id,
                                         'location_id': loc,
                                         'type': 'log',
                                         'user_id': uid,
                                         'stock_line': log_line 
                })
                obj_sto.stock_confirm(cr, uid, [stlogid])
                
    
            if kit_line:           
                stkitid = obj_sto.create(cr, uid, {
                                         'warehouse_id': val.warehouse_id.id,
                                         'location_id': loc,
                                         'type': 'kit',
                                         'user_id': uid,
                                         'stock_line': kit_line
                })
                obj_sto.stock_confirm(cr, uid, [stkitid])
                    

        return {}

    def hitung_order(self, cr, uid, ids, k, lok):
        obj_point = self.pool.get('stock.warehouse.orderpoint')
        obj_quant = self.pool.get('stock.quant')
        obj_move = self.pool.get('stock.move')
        
        bfid = obj_point.search(cr, uid, [('product_id', '=', k), ('location_id', '=', lok)])
        min = obj_point.browse(cr, uid, bfid)[0].product_min_qty
        max = obj_point.browse(cr, uid, bfid)[0].product_max_qty
        multi = obj_point.browse(cr, uid, bfid)[0].qty_multiple
        
        ada = 0; order = 0
        cari = obj_quant.search(cr, uid, [('product_id', '=', k), ('location_id', '=', lok)])
        if cari:
            ada = sum([x.qty for x in obj_quant.browse(cr, uid, cari)])
            
        mov = obj_move.search(cr, uid, [('product_id', '=', k), ('state', 'in', ('waiting', 'confirmed', 'assigned')), ('location_dest_id', '=', lok)])
        if mov:
            ada += sum([x.product_uom_qty for x in obj_move.browse(cr, uid, mov)])
            
        if ada < 0 :
            order = abs(ada) + max
        elif ada < min :
            order = max - ada
        
        if multi > 1 :
            order = int(order/multi)*multi
            
        return order
            


#         obj_move.action_confirm(cr, uid, move_list, context=context)
#         obj_move.force_assign(cr, uid, move_list, context=context)
#         obj_move.action_done(cr, uid, move_list, context=context)

