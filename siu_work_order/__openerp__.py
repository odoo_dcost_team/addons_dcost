{
    "name":"Work Order",
    "version":"1.0",
    "author":"PT. SUMBER INTEGRASI UTAMA",
    "category":"Manufacturing",
    "description": """
        Provide Fleksibel Manufacture Order.
    """,
    "depends":["base", "mrp"],
    "init_xml":[],
    "demo_xml":[],
    "data":["work_view.xml"],  #update_xml
    "installable":True,
    "active":False,
}

