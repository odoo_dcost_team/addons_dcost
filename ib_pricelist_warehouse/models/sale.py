## -*- coding: utf-8 -*-
## © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
import time
##from datetime import datetime
from openerp import SUPERUSER_ID
from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT

class SalesOrder(osv.osv):
    _inherit = 'sale.order'

    def _get_default_pricelist(self, cr, uid, context=None):
        context = context or {}
        partner_id = context.get('partner_id', False) and context['partner_id'] or False
        customer = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
        res = customer.property_product_pricelist and customer.property_product_pricelist.id or False
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        if user.warehouse_id:
            cr.execute("""SELECT pricelist_id FROM pricelist_warehouse_rel  
                    WHERE warehouse_id=%s ORDER BY pricelist_id DESC LIMIT 1""", (user.warehouse_id.id,))
            result = cr.fetchone()
            if result:
                res = result[0]
        return res

    _defaults = {
        'pricelist_id': _get_default_pricelist,
    }

    def onchange_pricelist_id(self, cr, uid, ids, pricelist_id, order_lines, context=None):
        context = context or {}
        if not pricelist_id:
            return {}
        value = {'currency_id': self.pool.get('product.pricelist').browse(cr, uid, pricelist_id, context=context).currency_id.id}
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        if user.warehouse_id:
            cr.execute("""SELECT pricelist_id FROM pricelist_warehouse_rel  
                        WHERE warehouse_id=%s ORDER BY pricelist_id DESC LIMIT 1""", (user.warehouse_id.id,))
            result = cr.fetchone()
            if result and (pricelist_id <> result[0]):
                pricelist_id = result[0]

        if not order_lines or order_lines == [(6, 0, [])]:
            if pricelist_id:
                value.update({'pricelist_id': pricelist_id})
            return {'value': value}
        warning = {
            'title': _('Pricelist Warning!'),
            'message' : _('If you change the pricelist of this order (and eventually the currency), prices of existing order lines will not be updated.')
        }
        return {'warning': warning, 'value': value}

    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        if not part:
            return {'value': {'partner_invoice_id': False, 'partner_shipping_id': False,  'payment_term': False, 'fiscal_position': False}}

        part = self.pool.get('res.partner').browse(cr, uid, part, context=context)
        addr = self.pool.get('res.partner').address_get(cr, uid, [part.id], ['delivery', 'invoice', 'contact'])
        pricelist = part.property_product_pricelist and part.property_product_pricelist.id or False
        invoice_part = self.pool.get('res.partner').browse(cr, uid, addr['invoice'], context=context)
        payment_term = invoice_part.property_payment_term and invoice_part.property_payment_term.id or False
        dedicated_salesman = part.user_id and part.user_id.id or uid
        val = {
            'partner_invoice_id': addr['invoice'],
            'partner_shipping_id': addr['delivery'],
            'payment_term': payment_term,
            'user_id': dedicated_salesman,
        }

        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        if user.warehouse_id:
            cr.execute("""SELECT pricelist_id FROM pricelist_warehouse_rel  
                            WHERE warehouse_id=%s ORDER BY pricelist_id DESC LIMIT 1""", (user.warehouse_id.id,))
            result = cr.fetchone()
            if result:
                pricelist = result[0]

        delivery_onchange = self.onchange_delivery_id(cr, uid, ids, False, part.id, addr['delivery'], False,  context=context)
        val.update(delivery_onchange['value'])
        if pricelist:
            val['pricelist_id'] = pricelist
        if not self._get_default_section_id(cr, uid, context=context) and part.section_id:
            val['section_id'] = part.section_id.id
        sale_note = self.get_salenote(cr, uid, ids, part.id, context=context)
        if sale_note: val.update({'note': sale_note})
        return {'value': val}



class SaleOrderLines(osv.osv):
    _inherit = 'sale.order.line'

    def unit_price_onchange(self, cr, uid, ids, pricelist, product, qty=0,
                uom=False, qty_uos=0, uos=False, partner_id=False,
                lang=False, date_order=False, fiscal_position=False, price_unit=False, context=None):
        product_uom_obj = self.pool.get('product.uom')
        partner_obj = self.pool.get('res.partner')
        product_obj = self.pool.get('product.product')
        fscl_position = self.pool.get('account.fiscal.position')
        context = context or {}
        lang = lang or context.get('lang', False)
        if not partner_id:
            raise osv.except_osv(_('No Customer Defined!'), _('Before choosing a product,\n select a customer in the sales form.'))
        partner = partner_obj.browse(cr, uid, partner_id)
        lang = partner.lang
        context_partner = context.copy()
        context_partner.update({'lang': lang, 'partner_id': partner_id})
        if not product:
            if not price_unit:
                return {}
            return {'value': {'price_unit': price_unit or 0.0}}
        if not date_order:
            date_order = time.strftime(DEFAULT_SERVER_DATE_FORMAT)

        vals = {'price_unit': price_unit or 0.0}
        warning, result = {}, {}
        product_obj = product_obj.browse(cr, uid, product, context=context_partner)
        uom2 = False
        if uom:
            uom2 = product_uom_obj.browse(cr, uid, uom)
            if product_obj.uom_id.category_id.id != uom2.category_id.id:
                uom = False
        if uos:
            if product_obj.uos_id:
                uos2 = product_uom_obj.browse(cr, uid, uos)
                if product_obj.uos_id.category_id.id != uos2.category_id.id:
                    uos = False
            else:
                uos = False

        fpos = False
        if not fiscal_position:
            fpos = partner.property_account_position or False
        else:
            fpos = fscl_position.browse(cr, uid, fiscal_position)

        if uid == SUPERUSER_ID and context.get('company_id'):
            taxes = product_obj.taxes_id.filtered(lambda r: r.company_id.id == context['company_id'])
        else:
            taxes = product_obj.taxes_id
        result['tax_id'] = fscl_position.map_tax(cr, uid, fpos, taxes, context=context)
        if not uom2:
            uom2 = product_obj.uom_id

        if not pricelist:
            warn_msg = _('You have to select a pricelist or a customer in the sales form !\n'
                    'Please set one before choosing a product.')
            warning = {'title': _('Configuration Error!'),
                       'message': _("No Pricelist ! : ") + warn_msg +"\n\n"}
        else:
            ctx = dict(
                context,
                uom=uom or result.get('product_uom'),
                date=date_order,
            )
            price = self.pool.get('product.pricelist').price_get(cr, uid, [pricelist], product_obj.id, qty or 1.0, partner_id or False, ctx)[pricelist]
            if price is False:
                warn_msg = _("Cannot find a pricelist line matching this product and quantity.\n"
                        "You have to change either the product, the quantity or the pricelist.")
                warning = {'title': _('Configuration Error!'),
                           'message': _("No valid pricelist line found ! :") + warn_msg +"\n\n"}
            else:
                price = self.pool['account.tax']._fix_tax_included_price(cr, uid, price, taxes, result['tax_id'])

        if price_unit > price:
            vals.update({'price_unit': price or 0.0})
            warning = {'title': _('Peringatan !!!'),
                       'message': _("Harga per unit tidak boleh melebihi harga jual \nSale Price: %s") % (price,)}

        return {'value': vals, 'warning': warning}

