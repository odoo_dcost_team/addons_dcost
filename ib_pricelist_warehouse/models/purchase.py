## -*- coding: utf-8 -*-
## © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
from datetime import datetime
from openerp import SUPERUSER_ID
from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp.tools.float_utils import float_compare
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

JABODETABEK = ['KOTA JAKARTA UTARA','KOTA JAKARTA TIMUR','KOTA JAKARTA SELATAN','KOTA JAKARTA BARAT','KOTA JAKARTA PUSAT','KABUPATEN BOGOR','KOTA BOGOR','KOTA DEPOK','KABUPATEN TANGERANG','KOTA TANGERANG','KOTA TANGERANG SELATAN','KABUPATEN BEKASI','KOTA BEKASI']

class PurchaseOrder(osv.osv):
    _inherit = 'purchase.order'

    def _get_pricelist_purchase(self, cr, uid, context=None):
        context = context or {}
        partner_id = context.get('partner_id', False) and context['partner_id']
        supplier = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
        res = supplier.property_product_pricelist_purchase.id or False
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        if user.warehouse_id:
            cr.execute("""SELECT pricelist_id FROM pricelist_warehouse_rel  
                    WHERE warehouse_id=%s ORDER BY pricelist_id DESC LIMIT 1""", (user.warehouse_id.id,))
            result = cr.fetchone()
            if result:
                res = result[0]
        return res

    _defaults = {
        'pricelist_id': _get_pricelist_purchase,
    }

    def onchange_pricelist(self, cr, uid, ids, pricelist_id, context=None):
        if not pricelist_id:
            return {}
        values = {'currency_id': self.pool.get('product.pricelist').browse(cr, uid, pricelist_id, context=context).currency_id.id}
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        if user.warehouse_id:
            cr.execute("""SELECT pricelist_id FROM pricelist_warehouse_rel  
                                WHERE warehouse_id=%s ORDER BY pricelist_id DESC LIMIT 1""", (user.warehouse_id.id,))
            result = cr.fetchone()
            if result and (pricelist_id <> result[0]):
                pricelist_id = result[0]
        if pricelist_id:
            values.update({'pricelist_id': pricelist_id})
        return {'value': values}

    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        partner = self.pool.get('res.partner')
        if not partner_id:
            return {'value': {
                'fiscal_position': False,
                'payment_term_id': False,
            }}
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = context.get('company_id') or self.pool.get('res.users')._get_company(cr, uid, context=context)
        if not company_id:
            raise osv.except_osv(_('Error!'), _('There is no default company for the current user!'))
        fp = self.pool['account.fiscal.position'].get_fiscal_position(cr, uid, company_id, partner_id, context=context)
        supplier_address = partner.address_get(cr, uid, [partner_id], ['default'], context=context)
        supplier = partner.browse(cr, uid, partner_id, context=context)
        pricelist_id = supplier.property_product_pricelist_purchase.id
        if user.warehouse_id:
            cr.execute("""SELECT pricelist_id FROM pricelist_warehouse_rel  
                WHERE warehouse_id=%s ORDER BY pricelist_id DESC LIMIT 1""", (user.warehouse_id.id,))
            pricelist_ids = cr.fetchall()
            if cr.rowcount > 0:
                for pid in pricelist_ids:
                    pricelist_id = pid[0]
        return {'value': {
            'pricelist_id': pricelist_id,
            'fiscal_position': fp or supplier.property_account_position and supplier.property_account_position.id,
            'payment_term_id': supplier.property_supplier_payment_term.id or False,
        }}


class PurchaseOrderLine(osv.osv):
    _inherit = 'purchase.order.line'

    def onchange_unit_price(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=False, fiscal_position_id=False, price_unit=False, picking_type_id=False, context=None):
        account_fiscal_position = self.pool.get('account.fiscal.position')
        product_obj = self.pool.get('product.product')
        picking_type_obj = self.pool.get('stock.picking.type')
        warning = {}
        values = {'price_unit': price_unit or 0.0}
        if not product_id:
            if not uom_id:
                uom_id = self.default_get(cr, uid, ['product_uom'], context=context).get('product_uom', False)
                values.update({'product_uom': uom_id})
            return values

        context_partner = context.copy()
        if partner_id:
            lang = self.pool.get('res.partner').browse(cr, uid, partner_id).lang
            context_partner.update({'lang': lang, 'partner_id': partner_id})
        product = product_obj.browse(cr, uid, product_id, context=context_partner)
        picking_type = picking_type_obj.browse(cr, uid, picking_type_id, context=context)
        if not uom_id:
            uom_id = product.uom_po_id and product.uom_po_id.id
        if not date_order:
            date_order = fields.datetime.now()

        if pricelist_id:
            date_order_str = datetime.strptime(date_order, DEFAULT_SERVER_DATETIME_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT)
            if picking_type.warehouse_id.kota_id.jabodetabek and (product.categ_id.apply_contract_price or product.apply_contract_price):
                price_base = self.pool.get('product.pricelist').price_get(cr, uid, [pricelist_id],
                                product.id, qty or 1.0, partner_id or False,
                                {'uom': uom_id, 'date': date_order_str})[pricelist_id]
            else:
                price_base = price_unit
        else:
            price_base = product.standard_price
        if uid == SUPERUSER_ID:
            company_id = self.pool['res.users'].browse(cr, uid, [uid]).company_id.id
            taxes = product.supplier_taxes_id.filtered(lambda r: r.company_id.id == company_id)
        else:
            taxes = product.supplier_taxes_id
        fpos = fiscal_position_id and account_fiscal_position.browse(cr, uid, fiscal_position_id,
                                                                     context=context) or False
        taxes_ids = account_fiscal_position.map_tax(cr, uid, fpos, taxes, context=context)
        price_base = self.pool['account.tax']._fix_tax_included_price(cr, uid, price_base, product.supplier_taxes_id,
                                                                      taxes_ids)
        if (price_unit > price_base) and picking_type.warehouse_id.kota_id.jabodetabek and (product.categ_id.apply_contract_price or product.apply_contract_price):
            values.update({'price_unit': price_base or 0.0})
            warning = {'title': _('Peringatan !!!'),
                       'message': _("Harga per unit tidak boleh melebihi harga dasar/harga kontrak.\nHarga Beli (Std Price): %s, Harga yang diinput: %s") % (price_base,price_unit)}

        return {'value': values, 'warning': warning}

    def onchange_product_id(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', picking_type_id=False, context=None):
        if context is None:
            context = {}

        res = {'value': {'price_unit': price_unit or 0.0, 'name': name or '', 'product_uom' : uom_id or False}}
        if not product_id:
            if not uom_id:
                uom_id = self.default_get(cr, uid, ['product_uom'], context=context).get('product_uom', False)
                res['value']['product_uom'] = uom_id
            return res

        product_product = self.pool.get('product.product')
        product_uom = self.pool.get('product.uom')
        res_partner = self.pool.get('res.partner')
        account_tax = self.pool.get('account.tax')
        account_obj = self.pool.get('account.account')
        product_pricelist = self.pool.get('product.pricelist')
        picking_type_obj = self.pool.get('stock.picking.type')
        account_fiscal_position = self.pool.get('account.fiscal.position')
        context_partner = context.copy()
        if partner_id:
            lang = res_partner.browse(cr, uid, partner_id).lang
            context_partner.update( {'lang': lang, 'partner_id': partner_id} )
        product = product_product.browse(cr, uid, product_id, context=context_partner)
        if not name or not uom_id:
            dummy, name = product_product.name_get(cr, uid, product_id, context=context_partner)[0]
            if product.description_purchase:
                name += '\n' + product.description_purchase
            res['value'].update({'name': name})

        res['domain'] = {'product_uom': [('category_id','=',product.uom_id.category_id.id)]}
        product_uom_po_id = product.uom_po_id.id
        if not uom_id:
            uom_id = product_uom_po_id

        if product.uom_id.category_id.id != product_uom.browse(cr, uid, uom_id, context=context).category_id.id:
            if context.get('purchase_uom_check') and self._check_product_uom_group(cr, uid, context=context):
                res['warning'] = {'title': _('Warning!'), 'message': _('Selected Unit of Measure does not belong to the same category as the product Unit of Measure.')}
            uom_id = product_uom_po_id

        res['value'].update({'product_uom': uom_id})

        if not date_order:
            date_order = fields.datetime.now()

        supplierinfo = False
        precision = self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Unit of Measure')
        for supplier in product.seller_ids:
            if partner_id and (supplier.name.id == partner_id):
                supplierinfo = supplier
                if supplierinfo.product_uom.id != uom_id:
                    res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier only sells this product by %s') % supplierinfo.product_uom.name }
                min_qty = product_uom._compute_qty(cr, uid, supplierinfo.product_uom.id, supplierinfo.min_qty, to_uom_id=uom_id)
                if float_compare(min_qty , qty, precision_digits=precision) == 1:
                    if qty:
                        res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier has a minimal quantity set to %s %s, you should not purchase less.') % (supplierinfo.min_qty, supplierinfo.product_uom.name)}
                    qty = min_qty
        dt = self._get_date_planned(cr, uid, supplierinfo, date_order, context=context).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        qty = qty or 1.0
        res['value'].update({'date_planned': date_planned or dt})
        if qty:
            res['value'].update({'product_qty': qty})
        if product.cost_categ_id:
            res['value'].update({'cost_categ_id': product.cost_categ_id.id})
        if product.requirement_type:
            res['value'].update({'requirement_type': product.requirement_type})
        picking_type = picking_type_obj.browse(cr, uid, picking_type_id, context=context)
        price = price_unit
        if price_unit is False or price_unit is None:
            if pricelist_id:
                date_order_str = datetime.strptime(date_order, DEFAULT_SERVER_DATETIME_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT)
                if picking_type.warehouse_id.kota_id.jabodetabek and (product.categ_id.apply_contract_price or product.apply_contract_price):
                    price = product_pricelist.price_get(cr, uid, [pricelist_id], product.id, qty or 1.0,
                            partner_id or False, {'uom': uom_id, 'date': date_order_str})[pricelist_id]
                else:
                    price = product.standard_price
            else:
                price = product.standard_price

        if product.property_account_expense:
            account_id = product.property_account_expense.id
        else:
            account_id = False
            if product.categ_id:
                account_id = product.categ_id.property_account_expense_categ.id
        if account_id and picking_type.warehouse_id:
            aid = {}  #onchange_analytic_id
            account = account_obj.browse(cr, uid, account_id)
            for al in account.analytic_line:
                aid[al.name.id] = al.analytic_id.id
            if aid.has_key(picking_type.warehouse_id.id):
                res['value'].update({'account_analytic_id': aid[picking_type.warehouse_id.id]})

        if uid == SUPERUSER_ID:
            company_id = self.pool['res.users'].browse(cr, uid, [uid]).company_id.id
            taxes = product.supplier_taxes_id.filtered(lambda r: r.company_id.id == company_id)
        else:
            taxes = product.supplier_taxes_id

        fpos = fiscal_position_id and account_fiscal_position.browse(cr, uid, fiscal_position_id, context=context) or False
        taxes_ids = account_fiscal_position.map_tax(cr, uid, fpos, taxes, context=context)
        price = account_tax._fix_tax_included_price(cr, uid, price, product.supplier_taxes_id, taxes_ids)
        res['value'].update({'price_unit': price, 'taxes_id': taxes_ids})

        return res

