## -*- coding: utf-8 -*-
## © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
from openerp.osv import fields, osv
from openerp.tools.translate import _

class ProductPricelist(osv.osv):
    _inherit = 'product.pricelist'
    
    _columns = {
        'warehouse_ids': fields.many2many('stock.warehouse', 'pricelist_warehouse_rel', 'pricelist_id', 'warehouse_id', 'Warehouses', copy=False),
    }



class ProductTemplate(osv.osv):
    _inherit = 'product.template'
    _columns = {
        'apply_contract_price': fields.boolean('Apply Contract Price', copy=False),
    }
    _defaults = {
        'apply_contract_price': lambda *a: 0,
    }



class ProductCategory(osv.osv):
    _inherit = 'product.category'
    _columns = {
        'apply_contract_price': fields.boolean('Apply Contract Price', copy=False),
    }
    _defaults = {
        'apply_contract_price': lambda *a: 0,
    }


