# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
#import time
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import openerp.addons.jasper_reports as jasper_reports

BULAN = [(0,''), (1,'Januari'), (2,'Februari'),(3,'Maret'),(4,'April'), (5,'Mei'), (6,'Juni'),
        (7,'Juli'), (8,'Agustus'), (9,'September'),(10,'Oktober'), (11,'November'), (12,'Desember')]


class stock_move(osv.osv):
    _inherit = "stock.move"

    def _amount_move_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        context = context or {}
        for move in self.browse(cr, uid, ids, context=context):
            total = move.price_unit * move.product_uom_qty
            # if move.picking_type_id.code<>'outgoing':
            if move.product_uom.id != move.product_id.uom_id.id:
                total = ((1.0 / move.product_uom.factor) * move.price_unit) * move.product_uom_qty
            cur = move.company_id.currency_id
            res[move.id] = self.pool['res.currency'].round(cr, uid, cur, total)
        return res

    _columns = {
        'price_subtotal': fields.function(_amount_move_line, string='Subtotal',
                                          digits_compute=dp.get_precision('Account')),
    }

    def action_cancel(self, cr, uid, ids, context=None):
        procurement_obj = self.pool.get('procurement.order')
        context = context or {}
        procs_to_check = set()
        for move in self.browse(cr, uid, ids, context=context):
            if move.state == 'done' and (move.location_id.usage<>"inventory" and move.location_dest_id.usage<>"inventory"): #edited-baim for Dcost
                raise osv.except_osv(_('Operation Forbidden!'),
                        _('You cannot cancel a stock move that has been set to \'Done\'.'))
            if move.reserved_quant_ids:
                self.pool.get("stock.quant").quants_unreserve(cr, uid, move, context=context)
            if context.get('cancel_procurement'):
                if move.propagate:
                    procurement_ids = procurement_obj.search(cr, uid, [('move_dest_id', '=', move.id)], context=context)
                    procurement_obj.cancel(cr, uid, procurement_ids, context=context)
            else:
                if move.move_dest_id:
                    if move.propagate:
                        self.action_cancel(cr, uid, [move.move_dest_id.id], context=context)
                    elif move.move_dest_id.state == 'waiting':
                        #If waiting, the chain will be broken and we are not sure if we can still wait for it (=> could take from stock instead)
                        self.write(cr, uid, [move.move_dest_id.id], {'state': 'confirmed'}, context=context)
                if move.procurement_id:
                    # Does the same as procurement check, only eliminating a refresh
                    procs_to_check.add(move.procurement_id.id)
                    
        res = self.write(cr, uid, ids, {'state': 'cancel', 'move_dest_id': False}, context=context)
        if procs_to_check:
            procurement_obj.check(cr, uid, list(procs_to_check), context=context)
        return res




class stock_inventory(osv.osv):
    _inherit = "stock.inventory"
    
    def action_cancel_inventory(self, cr, uid, ids, context=None):
        """ Cancels both stock move and inventory
        @return: True
        """
        move_obj = self.pool.get('stock.move')
        account_move_obj = self.pool.get('account.move')
        for inv in self.browse(cr, uid, ids, context=context):
            move_obj.action_cancel(cr, uid, [x.id for x in inv.move_ids], context=context)
            for move in inv.move_ids:
                account_move_ids = account_move_obj.search(cr, uid, [('name', '=', move.name)])
                if account_move_ids:
                    account_move_data_l = account_move_obj.read(cr, uid, account_move_ids, ['state'], context=context)
                    for account_move in account_move_data_l:
                        if account_move['state'] == 'posted':
                            raise osv.except_osv(_('User Error!'),
                                                 _('In order to cancel this inventory, you must first unpost related journal entries.'))
                        account_move_obj.unlink(cr, uid, [account_move['id']], context=context)
            self.write(cr, uid, [inv.id], {'state': 'cancel'}, context=context)
        return True

    def print_stock_inv_adjust(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        report_name = context.get('reports')
        context = context or {}
        datas = {
            'ids': ids,
            'model': 'stock.inventory',
            'form': self.read(cr, uid, ids[0], context=context),
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': report_name,
            'datas': datas,
            'nodestroy': True
        }




class stock_picking(osv.osv):
    _inherit = "stock.picking"

    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        context = context or {}
        for picking in self.browse(cr, uid, ids, context=context):
            val = 0.0
            for line in picking.move_lines:
                val += line.price_subtotal
            cur = picking.company_id.currency_id
            res[picking.id] = self.pool['res.currency'].round(cr, uid, cur, val)
        return res

    def _get_pickings(self, cr, uid, ids, context=None):
        res = set()
        for move in self.browse(cr, uid, ids, context=context):
            if move.picking_id:
                res.add(move.picking_id.id)
        return list(res)

    _columns = {
        'amount_total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Total',
                                        store={
                                            'stock.picking': (lambda self, cr, uid, ids, ctx: ids, ['move_lines'], 20),
                                            'stock.move': (_get_pickings,
                                                           ['picking_id', 'price_unit', 'product_uom_qty',
                                                            'price_subtotal'], 20),
                                        }, help="The total amount."),
    }

    def print_picking(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        report_name = context.get('reports')
        context = context or {}
        for id in ids:
            picking = self.pool.get('stock.picking').browse(cr, uid, id, context=context)
            pickdate = str(picking.date).split(" ")
            date = str(pickdate[0]).split("-")
            date_indo = str(date[2]) + " " + str(BULAN[int(date[1])][1]).title() + " " + str(date[0])
        if report_name=='bukti.penerimaan.pdf':
            title = 'BUKTI PENERIMAAN BARANG'
        else:
            title = 'SURAT JALAN'
        datas = {
            'ids': ids,
            'model': 'stock.picking',
            'form': self.read(cr, uid, ids[0], context=context),
            'pick_date': date_indo,
            'title': title,
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': report_name,
            'datas': datas,
            'nodestroy': True
        }





def inventory_movement_report( cr, uid, ids, data, context ):
    return {
        'parameters': {
            'title': data['form']['title'],
            'start_date': data['form']['start_date'],
            'end_date': data['form']['end_date'],
            'SUBREPORT_DIR': data['form']['subdir_report'],
            'location_id': data['form']['location_id'],
            'warehouse_id': data['form']['warehouse_id'],
            'company_id': data['form']['company_id'],
        },
    }
jasper_reports.report_jasper(
    'report.inv.movement.pdf',
    'stock.move',
    parser=inventory_movement_report
    )
jasper_reports.report_jasper(
    'report.inv.movement.xls',
    'stock.move',
    parser=inventory_movement_report
    )
jasper_reports.report_jasper(
    'report.inv.movement.wh.pdf',
    'stock.move',
    parser=inventory_movement_report
    )
jasper_reports.report_jasper(
    'report.inv.movement.wh.xls',
    'stock.move',
    parser=inventory_movement_report
    )
jasper_reports.report_jasper(
    'report.stock.opname.pdf',
    'stock.move',
    parser=inventory_movement_report
    )
jasper_reports.report_jasper(
    'report.stock.opname.xls',
    'stock.move',
    parser=inventory_movement_report
    )
jasper_reports.report_jasper(
    'report.stock.opname.wh.pdf',
    'stock.move',
    parser=inventory_movement_report
    )
jasper_reports.report_jasper(
    'report.stock.opname.wh.xls',
    'stock.move',
    parser=inventory_movement_report
    )

