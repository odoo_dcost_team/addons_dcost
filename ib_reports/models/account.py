# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
import amount_to_text_en
import amount_to_text_id
from openerp.osv import fields, osv
from openerp.tools.translate import _

class account_invoice(osv.osv):
    _inherit = "account.invoice"

    def _get_categ_transaction(self, cr, uid, context=None):
        categ = [('hpp', _('HPP')),('non_hpp', _('Non-HPP')),('kaskecil', _('Kas Kecil')),('kasbesar', _('Kas Besar'))]
        if cr.dbname in ('PB1', 'PEBO', 'PB', 'OUTLET_PB'):
            categ = [('hpp', _('HPP 8893')),
                     ('bank1', _('Bank BCA 7777')),
                     ('bank2', _('Bank 7711')),
                     ('non_hpp', _('Non-HPP 9337')),
                     ('kaskecil1', _('Kas Kecil HPP 8893')),
                     ('kaskecil2', _('Kas Kecil Non-HPP 9337'))]
        elif cr.dbname in ('LCD', 'LOE CUAN DULUAN', 'OUTLET_LCD'):
            categ = [('hpp', _('HPP 7555')),
                     ('pb1', _('PB1 5555')),
                     ('bank1', _('Bank 8899')),
                     ('bank2', _('Bank 9798')),
                     ('bank3', _('Bank 5757')),
                     ('kemang', _('Kemang 5511')),
                     ('non_hpp', _('Non-HPP 7237')),
                     ('kaskecil1', _('Kas Kecil HPP 7555')),
                     ('kaskecil2', _('Kas Kecil Non-HPP 7237'))]
        elif cr.dbname in ('LOGISTIK', 'SENLOGPB5', 'PB5', 'SENLOG', 'SENKIT'):
            categ = [('bank1', _('Bank 9844')),('kaskecil', _('Kas Kecil 9844')),('kasbesar', _('Kas Besar 9844'))]  #PEBO
            # categ = [('bank1', _('Bank 7888')), ('kaskecil', _('Kas Kecil 7888')), ('kasbesar', _('Kas Besar 7888'))]  #LCD
        return categ

    _columns = {
        'category_transaction': fields.selection(_get_categ_transaction, 'Category',
                readonly=True, states={'draft': [('readonly', False)]}),
        'text_amount': fields.char('Spelled Out', size=256),
        # 'category_transaction': fields.selection(
        #     [('hpp', 'HPP 8893'),('non_hpp', 'Non-HPP 9337'),
        #      ('kaskecil1','Kas Kecil HPP 8893'),('kaskecil2','Kas Kecil Non-HPP 9337')], 'Category'),
    }

    def print_invoice_docs(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        report_name = context.get('reports')
        context = context or {}
        self.pool['account.invoice'].write(cr, uid, ids, {'sent': True}, context=context)
        text_amount = ''
        for id in ids:
            acc_inv = self.pool.get('account.invoice').browse(cr, uid, id, context=context)
            inv_id = int(acc_inv.id) or ids[0]          ##round(acc_inv.amount_total, 0)
            text_amount = amount_to_text_id.amount_to_text(acc_inv.amount_total, 'id', acc_inv.currency_id.name)
            if acc_inv.currency_id.name == "USD":
                text_amount = amount_to_text_en.amount_to_text(acc_inv.amount_total, 'en', acc_inv.currency_id.name)

        title = "Supplier Invoice  No."
        if report_name == 'tanda.terima.faktur.pdf':
            title = "REKAP PEMBELIAN SUPPLIER"
        elif report_name in ('bank.voucher.pdf','bank.voucher1.pdf'):
            title = "BUKTI PENGELUARAN BANK"

        if cr.dbname in ('PB1', 'PEBO', 'PB', 'OUTLET', 'SENLOGPB5', 'PB5', 'SENLOG', 'SENKIT'):
            subdir_report = "/odoo/custom/addons/ib_reports/report/"
        elif cr.dbname in ('LCD','LOGISTIK'):
            subdir_report = "/opt/odoo8/custom/addons/ib_reports/report/"
        else:
            subdir_report = "/opt/odoo8/custom/addons/ib_reports/report/"

        datas = {
            'ids': ids,
            'model': 'account.invoice',
            'form': self.read(cr, uid, ids[0], context=context),
            'title': title,
            'invoice_id': inv_id,
            'text_amount': text_amount,
            'subdir_report': subdir_report,
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': report_name,
            'datas': datas,
            'nodestroy': True
        }

    def print_voucher_receipt(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        context = context or {}
        report_name = context.get('reports')
        datas = {
            'ids': ids,
            'model': 'account.invoice',
            'form': self.read(cr, uid, ids[0], context=context),
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': report_name,
            'datas': datas,
            'nodestroy': True
        }

    def write(self, cr, uid, ids, vals, context=None):
        for inv in self.browse(cr, uid, ids, context=context):
            if inv.type in ('in_invoice', 'in_refund'):
                #total = inv.amount_total
                total = sum([line.price_subtotal for line in inv.invoice_line])
                if vals.get('amount_total', False) and vals['amount_total'] > 0.0:
                    total = vals['amount_total']
                text_amount = amount_to_text_id.amount_to_text(total, 'id', inv.currency_id.name)
                if inv.currency_id.name == "USD":
                    text_amount = amount_to_text_en.amount_to_text(total, 'en', inv.currency_id.name)
                vals.update({'text_amount': text_amount})
        return super(account_invoice, self).write(cr, uid, ids, vals, context=context)



class account_cash_statement(osv.osv):
    _inherit = 'account.bank.statement'

    def print_cash_statement(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        context = context or {}
        text_amount = ''
        for id in ids:
            abs = self.pool.get('account.bank.statement').browse(cr, uid, id, context=context)
            abs_id = int(abs.id) or ids[0]
            amount_total = sum([l.amount for l in abs.line_ids])
            text_amount = amount_to_text_id.amount_to_text(amount_total, 'id', abs.company_id.currency_id.name)
            if abs.company_id.currency_id.name == "USD":
                text_amount = amount_to_text_en.amount_to_text(amount_total, 'en', abs.company_id.currency_id.name)

        if cr.dbname in ('PB1', 'PEBO', 'PB', 'OUTLET', 'SENLOGPB5', 'PB5', 'SENLOG', 'SENKIT'):
            subdir_report = "/odoo/custom/addons/ib_reports/report/"
        elif cr.dbname in ('LCD','LOGISTIK'):
            subdir_report = "/opt/odoo8/custom/addons/ib_reports/report/"
        else:
            subdir_report = "/opt/odoo8/custom/addons/ib_reports/report/"

        datas = {
            'ids': ids,
            'model': 'account.bank.statement',
            'form': self.read(cr, uid, ids[0], context=context),
            'title': "BUKTI PENGELUARAN BANK",
            'statement_id': abs_id,
            'text_amount': text_amount,
            'subdir_report': subdir_report,
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'cash.statement.pdf',
            'datas': datas,
            'nodestroy': True
        }


class account_common_partner_report(osv.osv_memory):
    _inherit = 'account.common.partner.report'
    _columns = {
        'result_selection': fields.selection([('customer','Receivable Accounts [Piutang]'),
                                              ('supplier','Payable Accounts [Hutang]'),
                                              ('customer_supplier','Receivable and Payable Accounts')],
                                              "Partner's", required=True),
    }


class account_aged_trial_balance(osv.osv_memory):
    _inherit = 'account.aged.trial.balance'
    _columns = {
        'period_length':fields.integer('Period Length (days)', required=True),
    }
    _defaults = {
        'period_length': 7,
    }
