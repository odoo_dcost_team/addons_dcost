<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="balance_sheet" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="808" leftMargin="17" rightMargin="17" topMargin="11" bottomMargin="11" uuid="ed4849d9-344c-4a00-8a7f-9ff7b5f46de8">
	<property name="ireport.zoom" value="1.2100000000000053"/>
	<property name="ireport.x" value="67"/>
	<property name="ireport.y" value="0"/>
	<parameter name="title" class="java.lang.String"/>
	<parameter name="fiscalyear_id" class="java.lang.Integer"/>
	<parameter name="company_id" class="java.lang.Integer"/>
	<parameter name="coa_id" class="java.lang.Integer"/>
	<parameter name="print_datetime" class="java.lang.String"/>
	<parameter name="period_date_start" class="java.lang.String"/>
	<parameter name="period_date_stop" class="java.lang.String"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String">
		<defaultValueExpression><![CDATA["/home/baim/Documents/Customs/8.0/ib_reports/report/"]]></defaultValueExpression>
	</parameter>
	<parameter name="period_id" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="label_year_todate" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT d.id as parent_id, d.code as parent_code, d.name as parent_name,
  c.id as child1_id, c.code as child1_code, c.name as child1_name,
  b.id as child2_id, b.code as child2_code, b.name as child2_name,
  a.id as child3_id, a.code as child3_code, a.name as child3_name, a.type, q.symbol as currency,
(select date_stop from account_period where id=$P{period_id}) as period_date_stop,
    (SELECT COALESCE(SUM(l.debit),0) - COALESCE(SUM(l.credit), 0) FROM account_move_line as l
    	left join account_move as m on l.move_id=m.id WHERE l.account_id=a.id and
	l.company_id=$P{company_id} and m.state<>'draft' and l.period_id=$P{period_id}
	GROUP BY l.account_id) as BALANCE_PERIOD

FROM account_account as a left join account_account as b on a.parent_id=b.id
left join account_account as c on b.parent_id=c.id left join account_account as d on c.parent_id=d.id
left join res_company as p on d.company_id=p.id left join res_currency as q on p.currency_id=q.id

WHERE d.parent_id in (select id from account_account where (code='1' or name='BALANCE SHEET')
	and parent_id=$P{coa_id}) AND d.company_id=$P{company_id} AND a.active is True and b.active is True

ORDER BY d.code, c.code, b.code, a.code asc]]>
	</queryString>
	<field name="parent_id" class="java.lang.Integer">
		<fieldDescription><![CDATA[Parent]]></fieldDescription>
	</field>
	<field name="parent_code" class="java.lang.String"/>
	<field name="parent_name" class="java.lang.String"/>
	<field name="child1_id" class="java.lang.Integer"/>
	<field name="child1_code" class="java.lang.String"/>
	<field name="child1_name" class="java.lang.String"/>
	<field name="child2_id" class="java.lang.Integer"/>
	<field name="child2_code" class="java.lang.String"/>
	<field name="child2_name" class="java.lang.String"/>
	<field name="child3_id" class="java.lang.Integer"/>
	<field name="child3_code" class="java.lang.String"/>
	<field name="child3_name" class="java.lang.String"/>
	<field name="type" class="java.lang.String">
		<fieldDescription><![CDATA[Internal Type]]></fieldDescription>
	</field>
	<field name="currency" class="java.lang.String">
		<fieldDescription><![CDATA[Currency]]></fieldDescription>
	</field>
	<field name="period_date_stop" class="java.sql.Date"/>
	<field name="balance_period" class="java.math.BigDecimal"/>
	<variable name="balance_period_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Parent" calculation="Sum">
		<variableExpression><![CDATA[$F{balance_period}]]></variableExpression>
	</variable>
	<variable name="balance_period_2" class="java.math.BigDecimal" resetType="Group" resetGroup="Child1" calculation="Sum">
		<variableExpression><![CDATA[$F{balance_period}]]></variableExpression>
	</variable>
	<variable name="balance_period_3" class="java.math.BigDecimal" resetType="Group" resetGroup="Child2" calculation="Sum">
		<variableExpression><![CDATA[$F{balance_period}]]></variableExpression>
	</variable>
	<variable name="balance_period_4" class="java.math.BigDecimal" resetType="Group" resetGroup="Child1" calculation="Sum">
		<variableExpression><![CDATA[$F{balance_period}]]></variableExpression>
	</variable>
	<variable name="balance_period_5" class="java.math.BigDecimal" resetType="Group" resetGroup="Parent" calculation="Sum">
		<variableExpression><![CDATA[$F{balance_period}]]></variableExpression>
	</variable>
	<group name="Parent">
		<groupExpression><![CDATA[$F{parent_id}]]></groupExpression>
		<groupHeader>
			<band height="18">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="03a3b0ad-d7fa-40bd-835c-350e56c92c96" positionType="Float" stretchType="RelativeToTallestObject" x="85" y="0" width="549" height="18"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="10" isBold="true"/>
						<paragraph leftIndent="5"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{parent_name}.toUpperCase()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="29396b9a-d4cd-461b-830a-cf1a7fb954ec" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="85" height="18"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="10" isBold="true"/>
						<paragraph leftIndent="5"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{parent_code}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="72a7a444-0270-475a-9f61-6054bf852e75" positionType="Float" stretchType="RelativeToTallestObject" x="634" y="0" width="24" height="18"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="Parent" pattern="#,##0.00;(#,##0.00)" isBlankWhenNull="true">
					<reportElement uuid="fdfbbf43-8994-4443-9987-b9f1a5045190" positionType="Float" stretchType="RelativeToTallestObject" x="658" y="0" width="150" height="18"/>
					<box>
						<topPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<paragraph rightIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{balance_period_1}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="18">
				<textField>
					<reportElement uuid="54a7c86e-1b6c-4126-abd2-5b57b8d93573" x="0" y="0" width="634" height="18"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["JUMLAH " + $F{parent_name}.toUpperCase()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00;(#,##0.00)" isBlankWhenNull="true">
					<reportElement uuid="847de0c4-5880-4c15-91be-c057d6b23953" positionType="Float" stretchType="RelativeToTallestObject" x="658" y="0" width="150" height="18"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
						<paragraph rightIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{balance_period_5}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="5a15a39f-800c-4241-aa5f-8fca2c9d3400" positionType="Float" stretchType="RelativeToTallestObject" x="634" y="0" width="24" height="18"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="Child1">
		<groupExpression><![CDATA[$F{child1_id}]]></groupExpression>
		<groupHeader>
			<band height="18">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="1c4ed3f9-da90-4e74-9b05-585c20b455d0" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="85" height="18"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement verticalAlignment="Middle">
						<paragraph leftIndent="5"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{child1_code}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="806f1eff-3f3c-4035-9818-433a261c15f0" positionType="Float" stretchType="RelativeToTallestObject" x="85" y="0" width="549" height="18"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement verticalAlignment="Middle">
						<paragraph leftIndent="5"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{child1_name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="cf26fefa-69b9-472c-a6c1-91725fee9bb7" positionType="Float" stretchType="RelativeToTallestObject" x="634" y="0" width="24" height="18"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement verticalAlignment="Middle">
						<paragraph leftIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="Child1" pattern="#,##0.00;(#,##0.00)" isBlankWhenNull="true">
					<reportElement uuid="12a72499-f3c3-4f40-a225-000b732e2452" positionType="Float" stretchType="RelativeToTallestObject" x="658" y="0" width="150" height="18"/>
					<box>
						<topPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<paragraph rightIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{balance_period_2}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="18">
				<textField>
					<reportElement uuid="7a481555-c48f-41a0-aab1-8021f8983c6f" x="0" y="0" width="634" height="18"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Jumlah " + $F{child1_name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00;(#,##0.00)" isBlankWhenNull="true">
					<reportElement uuid="392ba433-ac38-4a3e-bae8-078d26f3ffac" positionType="Float" stretchType="RelativeToTallestObject" x="658" y="0" width="150" height="18"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
						<paragraph rightIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{balance_period_4}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="5da171b2-da31-44d1-9ca0-8315ff6043b5" positionType="Float" stretchType="RelativeToTallestObject" x="634" y="0" width="24" height="18"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="Child2">
		<groupExpression><![CDATA[$F{child2_id}]]></groupExpression>
		<groupHeader>
			<band height="18">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="0a94c23d-9b17-4de3-b429-c9f6218c41c2" x="0" y="0" width="85" height="18" isRemoveLineWhenBlank="true"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement verticalAlignment="Middle">
						<paragraph leftIndent="5"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{child2_code}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="a4777cc9-1cfc-4c87-9c18-dcdad16c2860" x="85" y="0" width="549" height="18" isRemoveLineWhenBlank="true"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement verticalAlignment="Middle">
						<paragraph leftIndent="5"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{child2_name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="508b6b34-8652-4940-ad05-b19e8f1e7561" positionType="Float" stretchType="RelativeToTallestObject" x="634" y="0" width="24" height="18"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement verticalAlignment="Middle">
						<paragraph leftIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="Child2" pattern="#,##0.00;(#,##0.00)" isBlankWhenNull="true">
					<reportElement uuid="65d52af0-a479-4020-9687-418737bb1011" positionType="Float" stretchType="RelativeToTallestObject" x="658" y="0" width="150" height="18"/>
					<box>
						<topPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<paragraph rightIndent="2"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{balance_period_3}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="70" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="520bfd66-45d9-465f-94b6-fd375354fde9" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="693" height="55"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="14" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{title}]]></textFieldExpression>
			</textField>
			<image>
				<reportElement uuid="82ca6270-6d53-4638-9522-d3fd925c79d3" x="693" y="0" width="115" height="55"/>
				<imageExpression><![CDATA["/odoo/custom/addons/ib_reports/report/logo_dcost.png"]]></imageExpression>
			</image>
		</band>
	</title>
	<columnHeader>
		<band height="35">
			<textField>
				<reportElement uuid="21f6a268-01f4-42a5-b85a-ad171dfa32c4" x="634" y="0" width="174" height="35"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" markup="none">
					<font size="11" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["PERIOD\n" + new SimpleDateFormat("dd/MM/yyyy").format($F{period_date_stop})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="acc50ac2-fff0-4f86-92ea-a32294bbba1d" x="0" y="0" width="85" height="35"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11" isBold="true"/>
				</textElement>
				<text><![CDATA[CODE]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="ca6f9d2b-bbbe-4e20-886f-bf8afbe1ce34" x="85" y="0" width="549" height="35"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11" isBold="true"/>
				</textElement>
				<text><![CDATA[ACCOUNT NAME]]></text>
			</staticText>
		</band>
	</columnHeader>
	<pageFooter>
		<band height="14" splitType="Stretch">
			<textField isStretchWithOverflow="true" pattern="dd MMMMM yyyy" isBlankWhenNull="true">
				<reportElement uuid="a95ae054-e279-4a71-8662-bb49c4cb5334" x="0" y="0" width="268" height="14"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA["Printed on : " + ($P{print_datetime}.isEmpty() ? new java.util.Date() : $P{print_datetime})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="15c9e308-02e2-4e5e-8968-1fb7511ad179" positionType="Float" stretchType="RelativeToTallestObject" x="688" y="0" width="80" height="14"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement uuid="45f0b9a5-4587-4fb4-b73d-7f9364a5f3c2" positionType="Float" stretchType="RelativeToTallestObject" x="768" y="0" width="40" height="14"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="71" splitType="Stretch">
			<subreport>
				<reportElement uuid="13acd3e0-8518-4a60-9ec0-e04f19b559b6" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="808" height="35"/>
				<subreportParameter name="period_id">
					<subreportParameterExpression><![CDATA[$P{period_id}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="company_id">
					<subreportParameterExpression><![CDATA[$P{company_id}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="fiscalyear_id">
					<subreportParameterExpression><![CDATA[$P{fiscalyear_id}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="period_date_start">
					<subreportParameterExpression><![CDATA[$P{period_date_start}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="coa_id">
					<subreportParameterExpression><![CDATA[$P{coa_id}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="period_date_stop">
					<subreportParameterExpression><![CDATA[$P{period_date_stop}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "balance_sheet_subreport1.jasper"]]></subreportExpression>
			</subreport>
			<staticText>
				<reportElement uuid="66890b40-a2ba-4657-83cb-dea38e84f2f9" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="47" width="413" height="24"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[*) Disajikan dalam Rupiah (IDR), kecuali dinyatakan lain
*) Filter : All Outlet (Warehouses)]]></text>
			</staticText>
		</band>
	</summary>
</jasperReport>
