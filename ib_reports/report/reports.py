# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
#from openerp.osv import fields, osv
from datetime import datetime
import openerp.addons.jasper_reports as jasper_reports
#from openerp import netsvc
#import openerp.addons.decimal_precision as dp

x = datetime.now()
# if x.hour==17:
#     hour = 24
# elif x.hour==18:
#     hour = 1
# elif x.hour==19:
#     hour = 2
# elif x.hour==20:
#     hour = 3
# elif x.hour==21:
#     hour = 4
# elif x.hour==22:
#     hour = 5
# elif x.hour==23:
#     hour = 6
# else:
#     hour = x.hour + 7

def picking_reports(cr, uid, ids, data, context):
    return {
        'parameters': {
            'title': data['title'],
            'picking_date': data['pick_date'],
            'print_datetime': str(datetime(x.year, x.month, x.day, x.hour, x.minute, x.second).strftime("%d-%m-%Y %H:%M:%S")),
        },
    }
jasper_reports.report_jasper(
    'report.surat.jalan.pdf',
    'stock.picking',
    parser=picking_reports
)
jasper_reports.report_jasper(
    'report.bukti.penerimaan.pdf',
    'stock.picking',
    parser=picking_reports
)
def draft_picking_reports(cr, uid, ids, data, context):
    return {
        'parameters': {
            'title': 'SURAT JALAN',
            'print_datetime': str(datetime(x.year, x.month, x.day, x.hour, x.minute, x.second).strftime("%d-%m-%Y %H:%M:%S")),
        },
    }
jasper_reports.report_jasper(
    'report.draft.surat.jalan.pdf',
    'sale.order',
    parser=draft_picking_reports
)
jasper_reports.report_jasper(
    'report.draft.bukti.penerimaan.pdf',
    'purchase.order',
    parser=draft_picking_reports
)

def sales_reports( cr, uid, ids, data, context ):
    return {
        'parameters': { 
            'title': data['title'],
            'quotation_date': data['quo_date'],
            'print_datetime': str(datetime(x.year, x.month, x.day, x.hour, x.minute, x.second).strftime("%d-%m-%Y %H:%M:%S")),
        },
    }
jasper_reports.report_jasper(
    'report.sale.quotation.pdf',
    'sale.order',
    parser=sales_reports
    )

def purchase_reports( cr, uid, ids, data, context ):
    return {
        'parameters': { 
            'title': "Purchase Order  No.",
			'print_datetime': str(datetime(x.year, x.month, x.day, x.hour, x.minute, x.second).strftime("%d-%m-%Y %H:%M:%S")),
        },
    }
jasper_reports.report_jasper(
    'report.purchase.order.pdf',
    'purchase.order',
    parser=purchase_reports
    )

def account_invoice_reports( cr, uid, ids, data, context ):
    return {
        'parameters': {
            # 'ids': str(ids),
            'title': data['title'],
            'invoice_id': data['invoice_id'],
            'text_amount': data['text_amount'],
            'SUBREPORT_DIR': data['subdir_report'],
            ##'SUBREPORT_DIR': data['form']['subdir_report'],
			##'print_datetime': str(datetime(x.year, x.month, x.day, x.hour, x.minute, x.second).strftime("%d-%m-%Y %H:%M:%S")),
        },
    }
jasper_reports.report_jasper(
    'report.supplier.invoice.pdf',
    'account.invoice',
    parser=account_invoice_reports
    )
jasper_reports.report_jasper(
    'report.tanda.terima.faktur.pdf',
    'account.invoice',
    parser=account_invoice_reports
    )
jasper_reports.report_jasper(
    'report.bank.voucher.pdf',
    'account.invoice',
    parser=account_invoice_reports
    )
jasper_reports.report_jasper(
    'report.bank.voucher1.pdf',
    'account.invoice',
    parser=account_invoice_reports
    )


# def reports(cr, uid, ids, data, context):
#     return {
#         'parameters': {
#             'ids': str(ids),
#             'title': "BUKTI PENGELUARAN BANK",
#             'text_amount': "Test Sejuta Rupiah",
#             'SUBREPORT_DIR': "/opt/odoo8/custom/addons/ib_reports/report/",
#         }
#     }


def supplier_invoice_report( cr, uid, ids, data, context ):
    return {
        'parameters': {
            'ids': ids,
        }
    }
jasper_reports.report_jasper(
    'report.invoice.receipt2.pdf',
    'account.invoice',
    parser=supplier_invoice_report
    )
jasper_reports.report_jasper(
    'report.bank.voucher2.pdf',
    'account.invoice',
    parser=supplier_invoice_report
    )



def summary_reports( cr, uid, ids, data, context ):
    return {
        'parameters': {
            'title': data['form']['title'],
            'start_date': data['form']['start_date'],
            'end_date': data['form']['end_date'],
            'SUBREPORT_DIR': data['form']['subdir_report'],
            'state_purchase': data['form']['status_purchase'],
            'state_sale': data['form']['status_sale'],
			'print_datetime': str(datetime(x.year, x.month, x.day, x.hour, x.minute, x.second).strftime("%d-%m-%Y %H:%M:%S")),
        },
    }

jasper_reports.report_jasper(
    'report.summary.po.pdf',
    'purchase.order',
    parser=summary_reports
    )
jasper_reports.report_jasper(
    'report.summary.po.xls',
    'purchase.order',
    parser=summary_reports
    )
jasper_reports.report_jasper(
    'report.summary.po.states.xls',
    'purchase.order',
    parser=summary_reports
    )
jasper_reports.report_jasper(
    'report.summary.po.states.pdf',
    'purchase.order',
    parser=summary_reports
    )
jasper_reports.report_jasper(
    'report.rekap.omzet.xls',
    'stock.picking',
    parser=summary_reports
    )
jasper_reports.report_jasper(
    'report.rekap.omzet.pdf',
    'stock.picking',
    parser=summary_reports
    )
jasper_reports.report_jasper(
    'report.summary.sale.pdf',
    'sale.order',
    parser=summary_reports
    )
jasper_reports.report_jasper(
    'report.summary.sale.xls',
    'sale.order',
    parser=summary_reports
    )
jasper_reports.report_jasper(
    'report.summary.sale.states.xls',
    'sale.order',
    parser=summary_reports
    )
jasper_reports.report_jasper(
    'report.summary.sale.states.pdf',
    'sale.order',
    parser=summary_reports
    )
    

def summary_stock_warehouse( cr, uid, ids, data, context ):
    return {
        'parameters': {
            'title': data['form']['title'],
            'current_date': data['form']['curr_date'],
            #'prev_date': data['form']['prev_date'],
            'SUBREPORT_DIR': data['form']['subdir_report'],
			'print_datetime': str('Update ') + str(datetime(x.year, x.month, x.day, x.hour, x.minute, x.second).strftime("%d-%m-%Y %H:%M:%S")),
            'location_id': data['form']['loc_id'],
        },
    }
jasper_reports.report_jasper(
    'report.summary.stock.card.xls',
    'product.product',
    parser=summary_stock_warehouse
    )
jasper_reports.report_jasper(
    'report.summary.stock.card.pdf',
    'product.product',
    parser=summary_stock_warehouse
    )

def inventory_reports(cr, uid, ids, data, context):
    return {
        'parameters': {
            'title': 'Stock Inventory',
            'print_datetime': str(datetime(x.year, x.month, x.day, x.hour, x.minute, x.second).strftime("%d-%m-%Y %H:%M:%S")),
        },
    }
jasper_reports.report_jasper(
    'report.stock.inventory.pdf',
    'stock.inventory',
    parser=inventory_reports
)

def acc_bank_statement_report(cr, uid, ids, data, context):
    return {
        'parameters': {
            'title': data['title'],
            'statement_id': data['statement_id'],
            'text_amount': data['text_amount'],
            'print_datetime': str(datetime(x.year, x.month, x.day, x.hour, x.minute, x.second).strftime("%d-%m-%Y %H:%M:%S")),
        },
    }
jasper_reports.report_jasper(
    'report.cash.statement.pdf',
    'account.bank.statement',
    parser=acc_bank_statement_report
)

def accounting_reports( cr, uid, ids, data, context ):
    return {
        'parameters': {
            'title': data['form']['title'],
            'SUBREPORT_DIR': data['form']['subdir_report'],
            'fiscalyear_id': data['form']['fiscalyear_id'],  #integer
            'company_id': data['form']['company_id'],  #integer
            'coa_id': data['form']['coa_id'],  #integer
            'period_id': data['form']['period_id'], #integer
            'warehouse_id': data['form']['warehouse_id'],  #integer
            'period_date_start': data['form']['period_date_start'],
            'period_date_stop': data['form']['period_date_stop'],
            'label_year_todate': data['form']['label_year_todate'],
            'journal_id': data['form']['journal_id'],
			'print_datetime': str(datetime(x.year, x.month, x.day, x.hour, x.minute, x.second).strftime("%d-%m-%Y %H:%M:%S")),
            'year': data['form']['year'],
            'account_id': data['form']['account_id'],
        },
    }

jasper_reports.report_jasper(#Trial Balance
    'report.tb.manywh.oneperiod.xls',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.tb.manywh.oneperiod.pdf',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.tb.manywh.multiperiod.xls',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.tb.manywh.multiperiod.pdf',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper( #Balance Sheet
    'report.bs.singlewh.xls',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.bs.singlewh.pdf',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.bs.manywh.xls',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.bs.manywh.pdf',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.bs.singlewh.oneperiod.pdf',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.bs.singlewh.oneperiod.xls',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper( #Profit and Loss
    'report.pl.singlewh.xls',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.pl.singlewh.pdf',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.pl.manywh.xls',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.pl.manywh.pdf',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.pl.singlewh.oneperiod.xls',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.pl.singlewh.oneperiod.pdf',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper( #General Ledger  singlejr.
    'report.gl.singlewh.xls',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.gl.singlewh.pdf',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.gl.singlewh.singlejr.pdf',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.gl.singlewh.singlejr.xls',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.gl.singlewh.oneaccount.pdf',
    'account.account',
    parser=accounting_reports
)
jasper_reports.report_jasper(
    'report.gl.singlewh.oneaccount.xls',
    'account.account',
    parser=accounting_reports
)

def payable_paid_report( cr, uid, ids, data, context ):
    return {
        'parameters': {
            'title': data['form']['title'],
            'SUBREPORT_DIR': data['form']['subdir_report'],
            'partner_id': data['form']['partner_id'],  #integer
            'date_start': data['form']['date_start'],
            'date_stop': data['form']['date_stop'],
			'print_datetime': str(datetime(x.year, x.month, x.day, x.hour, x.minute, x.second).strftime("%d-%m-%Y %H:%M:%S")),
        },
    }

jasper_reports.report_jasper(
    'report.kh.byduedate.supp.pdf',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.kh.byduedate.supp.xls',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.kh.byduedate.pdf',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.kh.byduedate.xls',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.kh.byinvdate.supp.pdf',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.kh.byinvdate.supp.xls',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.kh.byinvdate.pdf',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.kh.byinvdate.xls',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.pr.bypaydate.supp.pdf',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.pr.bypaydate.supp.xls',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.pr.bypaydate.pdf',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.pr.bypaydate.xls',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.pr.byinvdate.supp.pdf',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.pr.byinvdate.supp.xls',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.pr.byinvdate.pdf',
    'account.invoice',
    parser=payable_paid_report
)
jasper_reports.report_jasper(
    'report.pr.byinvdate.xls',
    'account.invoice',
    parser=payable_paid_report
)

