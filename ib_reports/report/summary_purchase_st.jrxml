<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="summary_purchase" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="824" leftMargin="9" rightMargin="9" topMargin="14" bottomMargin="14" uuid="dfde6ff1-2d9b-4151-b6b8-c7ebe25ca9cf">
	<property name="ireport.zoom" value="1.363636363636366"/>
	<property name="ireport.x" value="128"/>
	<property name="ireport.y" value="0"/>
	<parameter name="start_date" class="java.lang.String"/>
	<parameter name="title" class="java.lang.String"/>
	<parameter name="end_date" class="java.lang.String"/>
	<parameter name="print_datetime" class="java.lang.String"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["/home/baim/Documents/Customs/8.0/ib_reports/report/"]]></defaultValueExpression>
	</parameter>
	<parameter name="state_purchase" class="java.lang.String"/>
	<queryString>
		<![CDATA[select x.id as order_id, x.name as po_number, x.date_order, x.origin, y.id as supplier_id, y.name as supplier, a.name as supplier_title, z.name as currency, z.symbol, x.amount_untaxed, x.amount_tax, x.amount_total, b.name as term_payment, c.name as incoterm, e.name as warehouse, x.state

from purchase_order as x left join res_partner as y on x.partner_id=y.id left join res_currency as z on x.currency_id=z.id left join res_partner_title as a on y.title=a.id left join account_payment_term as b on x.payment_term_id=b.id left join stock_incoterms as c on x.incoterm_id=c.id left join stock_picking_type as d on x.picking_type_id=d.id left join stock_warehouse as e on d.warehouse_id=e.id

where x.date_order BETWEEN to_date($P{start_date}, 'YYYY-MM-DD') AND to_date($P{end_date}, 'YYYY-MM-DD')
AND x.state=$P{state_purchase}

order by supplier asc, x.date_order desc, po_number asc]]>
	</queryString>
	<field name="order_id" class="java.lang.Integer">
		<fieldDescription><![CDATA[SO existing]]></fieldDescription>
	</field>
	<field name="po_number" class="java.lang.String"/>
	<field name="date_order" class="java.sql.Timestamp">
		<fieldDescription><![CDATA[Order Date]]></fieldDescription>
	</field>
	<field name="origin" class="java.lang.String">
		<fieldDescription><![CDATA[Source Document]]></fieldDescription>
	</field>
	<field name="supplier_id" class="java.lang.Integer">
		<fieldDescription><![CDATA[Supplier]]></fieldDescription>
	</field>
	<field name="supplier" class="java.lang.String">
		<fieldDescription><![CDATA[Supplier]]></fieldDescription>
	</field>
	<field name="supplier_title" class="java.lang.String"/>
	<field name="currency" class="java.lang.String">
		<fieldDescription><![CDATA[Currency]]></fieldDescription>
	</field>
	<field name="symbol" class="java.lang.String">
		<fieldDescription><![CDATA[Symbol]]></fieldDescription>
	</field>
	<field name="amount_untaxed" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[Subtotal]]></fieldDescription>
	</field>
	<field name="amount_tax" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[Tax]]></fieldDescription>
	</field>
	<field name="amount_total" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[Total]]></fieldDescription>
	</field>
	<field name="term_payment" class="java.lang.String"/>
	<field name="incoterm" class="java.lang.String">
		<fieldDescription><![CDATA[Incoterm]]></fieldDescription>
	</field>
	<field name="warehouse" class="java.lang.String"/>
	<field name="state" class="java.lang.String"/>
	<variable name="amount_untaxed_1" class="java.math.BigDecimal" resetType="Group" resetGroup="supplier" calculation="Sum">
		<variableExpression><![CDATA[$F{amount_untaxed}]]></variableExpression>
	</variable>
	<variable name="amount_tax_1" class="java.math.BigDecimal" resetType="Group" resetGroup="supplier" calculation="Sum">
		<variableExpression><![CDATA[$F{amount_tax}]]></variableExpression>
	</variable>
	<variable name="amount_total_1" class="java.math.BigDecimal" resetType="Group" resetGroup="supplier" calculation="Sum">
		<variableExpression><![CDATA[$F{amount_total}]]></variableExpression>
	</variable>
	<group name="supplier">
		<groupExpression><![CDATA[$F{supplier_id}]]></groupExpression>
		<groupHeader>
			<band height="18">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="033e57a0-78b2-470d-a850-0df485dbb5e4" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="824" height="18"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
						<paragraph leftIndent="3"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{supplier}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="6571df37-1075-4fbc-abbe-5f60bf200b0a" positionType="Float" stretchType="RelativeToTallestObject" x="352" y="0" width="93" height="20"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<paragraph rightIndent="3"/>
					</textElement>
					<textFieldExpression><![CDATA[new DecimalFormat("#,##0.00;(#,##0.00)").format($V{amount_untaxed_1})]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="b5350033-6f00-4e9f-9b4d-5fd31f501d02" positionType="Float" stretchType="RelativeToTallestObject" x="445" y="0" width="90" height="20"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<paragraph rightIndent="3"/>
					</textElement>
					<textFieldExpression><![CDATA[new DecimalFormat("#,##0.00;(#,##0.00)").format($V{amount_tax_1})]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement uuid="c79c50d2-03e5-4d28-b50a-bbc42a0b8d84" positionType="Float" stretchType="RelativeToTallestObject" x="535" y="0" width="96" height="20"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<paragraph rightIndent="3"/>
					</textElement>
					<textFieldExpression><![CDATA[new DecimalFormat("#,##0.00;(#,##0.00)").format($V{amount_total_1})]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="3d3e0f60-32d1-41d7-91b0-55a3b66e95bc" x="0" y="0" width="352" height="20"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle" markup="none">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Total " + $F{supplier}]]></textFieldExpression>
				</textField>
				<frame>
					<reportElement uuid="74456a1b-d4bc-4190-aa87-c3b250ef1626" x="679" y="0" width="55" height="20"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
				</frame>
				<frame>
					<reportElement uuid="343867b5-0886-4d77-addb-c0a360e9efe5" x="734" y="0" width="40" height="20"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
				</frame>
				<frame>
					<reportElement uuid="0408a143-21cc-49b8-922d-2b1294bfdfa0" x="774" y="0" width="50" height="20"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
				</frame>
				<frame>
					<reportElement uuid="3e857008-f6af-450d-8604-db8c3c3a9386" x="631" y="0" width="48" height="20"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
				</frame>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="38" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="dd7c8066-4851-47c5-880e-dde9262b111b" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="824" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="14" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{title}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="40" splitType="Stretch">
			<staticText>
				<reportElement uuid="09950d5d-6547-4477-937a-a0053bf2ebe1" x="26" y="0" width="95" height="40"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[PO Number]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="7ae5b8d1-b285-4b66-810d-f81e8d37d252" x="121" y="0" width="53" height="40"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Order Date]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="5a8bc653-c618-4d77-b63c-c414b416a9b2" x="174" y="0" width="48" height="40"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Source Doc.]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="98017ec0-c8c2-419c-a015-2a39314fffb7" x="352" y="0" width="93" height="40"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Subtotal
(Untaxed)]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="2b9b125f-d6ab-45fc-ba3f-7df9eb6c5eaf" x="445" y="0" width="90" height="40"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Tax]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="7bc5b792-212f-48c7-9fbf-2f2fb622a0a5" x="535" y="0" width="96" height="40"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total
(+Tax)]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="a3149d34-2eb3-432f-a091-f178f6d2efc8" x="679" y="0" width="55" height="40"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Term
of Payment]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f686f5aa-fb1a-4b24-ab0c-eecd49db9698" x="734" y="0" width="40" height="40"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Inco
term]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="c28fa08d-796b-4301-9099-9a960538d380" x="774" y="0" width="50" height="40"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[WH]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="093d674a-4f7c-4471-826d-36f6aaae865b" x="222" y="0" width="130" height="40"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Product Description]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="d2ce0101-ce3e-4539-a40a-0944e87a7c5f" x="0" y="0" width="26" height="40"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[No]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="352450cb-f4cd-43d0-89ac-ac74d4b7887e" x="631" y="0" width="48" height="40"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Status]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="25" splitType="Stretch">
			<frame>
				<reportElement uuid="3d753df9-692b-4739-ade8-d4972639a692" positionType="Float" stretchType="RelativeToTallestObject" x="121" y="0" width="53" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="5029bca9-6cb4-4913-8853-11ed29921ade" positionType="Float" stretchType="RelativeToTallestObject" x="26" y="0" width="95" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="d5b0455c-482e-4dc4-ab70-179d099cd92b" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="26" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="dfede192-f7f7-4839-bdbd-66d1cc6feb37" positionType="Float" stretchType="RelativeToTallestObject" x="352" y="0" width="93" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="63b45688-299b-47a8-868a-4d89ff52838c" positionType="Float" stretchType="RelativeToTallestObject" x="445" y="0" width="90" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="66393aae-0d79-473f-b952-c4fb30f17ccd" positionType="Float" stretchType="RelativeToTallestObject" x="535" y="0" width="96" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="f6f4efb4-7406-4f99-9aa6-fae5409ab6c9" positionType="Float" stretchType="RelativeToTallestObject" x="631" y="0" width="48" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="eb3b5f30-8f80-4fbc-908c-f9c773ec434e" positionType="Float" stretchType="RelativeToTallestObject" x="679" y="0" width="55" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="bf649546-c742-4577-b409-6d8c5d1b465a" positionType="Float" stretchType="RelativeToTallestObject" x="774" y="0" width="50" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="221cd3c5-5f5b-4a6e-ab01-7cf0d55f638a" positionType="Float" stretchType="RelativeToTallestObject" x="174" y="0" width="48" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="1a10ada3-e723-4c22-88dd-c56dc9bf7b5b" positionType="Float" stretchType="RelativeToTallestObject" x="734" y="0" width="40" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement uuid="e6d6577b-3658-434e-8425-0564b004be09" positionType="Float" stretchType="RelativeToTallestObject" x="121" y="2" width="53" height="23"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{date_order}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="479c2bd6-636e-4c36-96aa-3e041f79ce3d" positionType="Float" stretchType="RelativeToTallestObject" x="174" y="2" width="48" height="23"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[($F{origin}.isEmpty() ? null : $F{origin})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="21dabebf-b46f-47d8-860e-663f3c0c400f" positionType="Float" stretchType="RelativeToTallestObject" x="352" y="2" width="27" height="23"/>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="674d49ff-453e-42f1-bfd7-ddb5e4413908" positionType="Float" stretchType="RelativeToTallestObject" x="379" y="2" width="66" height="23"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[new DecimalFormat("#,##0.00;(#,##0.00)").format($F{amount_untaxed})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="833c0699-59bf-4e71-9c6f-b72f5ffc9c78" positionType="Float" stretchType="RelativeToTallestObject" x="472" y="2" width="63" height="23"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[new DecimalFormat("#,##0.00;(#,##0.00)").format($F{amount_tax})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="ee74f4f3-229c-4e95-9795-3a9cfd98dcd0" positionType="Float" stretchType="RelativeToTallestObject" x="562" y="2" width="69" height="23"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[new DecimalFormat("#,##0.00;(#,##0.00)").format($F{amount_total})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="67aa66f3-67b5-4165-8084-8d2d68a9022a" positionType="Float" stretchType="RelativeToTallestObject" x="679" y="2" width="55" height="23"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[($F{term_payment}.isEmpty() ? null : $F{term_payment})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="b47a417c-afc4-4fad-988c-c78891ace4eb" positionType="Float" stretchType="RelativeToTallestObject" x="734" y="2" width="40" height="23"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[($F{incoterm}.isEmpty() ? null : $F{incoterm})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="841fac25-e96b-4164-9d59-92f8c56f5d23" positionType="Float" stretchType="RelativeToTallestObject" x="774" y="2" width="50" height="23"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[($F{warehouse}.isEmpty() ? null : $F{warehouse})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="31372d22-c86f-44fc-b926-45f8e74a6e81" positionType="Float" stretchType="RelativeToTallestObject" x="26" y="2" width="95" height="23"/>
				<textElement verticalAlignment="Middle">
					<font isBold="false"/>
					<paragraph leftIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{po_number}.isEmpty() ? null : $F{po_number})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="04f3d390-501c-455b-8bc3-5417b36007af" positionType="Float" stretchType="RelativeToTallestObject" x="445" y="2" width="27" height="23"/>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="d00d4dcc-726f-4365-9137-d7b030839e87" positionType="Float" stretchType="RelativeToTallestObject" x="535" y="2" width="27" height="23"/>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="d3ecb92d-8324-4d57-bce5-bbf6b5adeecd" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="2" width="26" height="23"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{REPORT_COUNT}]]></textFieldExpression>
			</textField>
			<frame>
				<reportElement uuid="e3e8bde6-fb75-40d8-ae25-8e3a9173f0a3" positionType="Float" stretchType="RelativeToTallestObject" x="222" y="0" width="130" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
			</frame>
			<subreport>
				<reportElement uuid="d4ab8c33-d29c-4665-a35e-547a264189b4" positionType="Float" stretchType="RelativeToTallestObject" x="222" y="0" width="130" height="25" isPrintWhenDetailOverflows="true"/>
				<subreportParameter name="order_id">
					<subreportParameterExpression><![CDATA[$F{order_id}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "summary_purchase_subreport1.jasper"]]></subreportExpression>
			</subreport>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="24c978b2-e403-4b1b-9261-cd2ab51c1adc" positionType="Float" stretchType="RelativeToTallestObject" x="631" y="2" width="48" height="23"/>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{state}.isEmpty() ? null :
    ($F{state}.equalsIgnoreCase("draft") ? "Draft PO" :
        ($F{state}.equalsIgnoreCase("sent") ? "RFQ" :
            ($F{state}.equalsIgnoreCase("confirmed") ? "Waiting Approval" :
                ($F{state}.equalsIgnoreCase("approved") ? "Purchase Confirmed" :
                    ($F{state}.equalsIgnoreCase("except_picking") ? "Shipping Exception" :
                        ($F{state}.equalsIgnoreCase("except_invoice") ? "Invoice Exception" :
                            ($F{state}.equalsIgnoreCase("done") ? "Done" :
                                ($F{state}.equalsIgnoreCase("cancel") ? "Cancelled" : null)))))))))]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="14" splitType="Stretch">
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy h.mm a" isBlankWhenNull="true">
				<reportElement uuid="4a5645e3-950d-4ed8-97af-f7c9d5319c44" x="0" y="0" width="233" height="14"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="d9b15f83-84e7-4af6-80de-a0a6c1e75dcb" x="704" y="0" width="80" height="14"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement uuid="296091f5-22f1-4484-aa3b-cc6ddcff5eec" x="784" y="0" width="40" height="14"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
