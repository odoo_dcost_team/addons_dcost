# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
###########################################################################################################
from collections import Counter
from openerp import SUPERUSER_ID
from datetime import datetime
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from openerp.tools.float_utils import float_compare
from openerp.osv.orm import browse_record_list, browse_record, browse_null
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP

class SaleOrder(osv.osv):
    _inherit = "sale.order"

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        if (vals.get('name', '/') == '/') or not vals['name']:
            wh_code = 'XXX'
            if 'warehouse_id' in vals or vals.get('warehouse_id'):
                wh_code = self.pool.get('stock.warehouse').browse(cr, uid, vals['warehouse_id'], context=context).code
            order_number = self.pool.get('ir.sequence').next_by_code(cr, uid, 'sale.order.dcost', context=context)
            nomor = list(str(order_number))  #SO/12345 -> SO/sunter/12345
            nomor.insert(3, wh_code + "/")
            vals.update({'name': ''.join(nomor)})
        result = super(SaleOrder, self).create(cr, uid, vals, context=context)
        lines=[]
        if vals.get('order_line', []):
            for ol in vals['order_line']:
                if ol[0] in [0, 1] and ol[2].get('product_id'):
                    lines.append(ol[2]['product_id'])

        cek_lines = Counter(lines)
        for key, val in cek_lines.items():
            if (val > 1) and (key in lines):
                product = self.pool.get('product.product').browse(cr, uid, key, context=context)
                raise osv.except_osv(_('Peringatan!!!'),
                    _('Product tidak boleh ada yang sama (Double Product ID).\nSilahkan Cek Kembali...\n(%s)') % (product.name_template,))
        return result

    def _check_so_lines(self, cr, uid, ids, context=None):
        for obj in self.browse(cr, uid, ids, context=context):
            cr.execute('select name, count(product_id) total '
                       'from sale_order_line where order_id=%s group by product_id, name', (obj.id,))
            for line in cr.dictfetchall():
                if line['total'] > 1:
                    raise osv.except_osv(_('Peringatan!'),
                        _('Product tidak boleh ada yang sama (Double Product ID).\nSilahkan Cek Kembali...\n(%s)') % (line['name'],))
        return True

    def write(self, cr, uid, ids, vals, context=None):
        result = super(SaleOrder, self).write(cr, uid, ids, vals, context=context)
        self._check_so_lines(cr, uid, ids, context=context)
        return result


class CostCategory(osv.osv):
    _name = 'cost.category'
    _description = "Purchase Cost Category"
    _columns = {
        'name': fields.char('Cost Name', required=True),
        'journal_id': fields.many2one('account.journal', 'Default Journal', required=True),
        'debit_account_id': fields.many2one('account.account', 'Default Debit Account', required=True),
        'credit_account_id': fields.many2one('account.account', 'Default Credit Account', required=True),
    }


class ProductTemplate(osv.osv):
    _inherit = "product.template"
    _columns = {
        'cost_categ_id': fields.many2one('cost.category', 'Default Cost Category'),
    }


class PurchaseOrder(osv.osv):
    _inherit = 'purchase.order'

    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('purchase.order.line').browse(cr, uid, ids, context=context):
            result[line.order_id.id] = True
        return result.keys()

    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        cur_obj=self.pool.get('res.currency')
        line_obj = self.pool['purchase.order.line']
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_untaxed': 0.0,
                'amount_tax': 0.0,
                'amount_discount': 0.0,
                'amount_total': 0.0,
            }
            val = val1 = val2 = 0.0
            cur = order.pricelist_id.currency_id
            for line in order.order_line:
                val1 += line.price_subtotal
                line_price = line_obj._calc_line_base_price(cr, uid, line, context=context)
                line_qty = line_obj._calc_line_quantity(cr, uid, line, context=context)
                for c in self.pool['account.tax'].compute_all(cr, uid, line.taxes_id, line_price, line_qty,
                        line.product_id, order.partner_id)['taxes']:
                    val += c.get('amount', 0.0)
            if order.discount_method == 'fixed':
                val2 = order.discount
                val = val - ((val / val1) * order.discount)
            elif order.discount_method == 'percent':
                val2 = val1 * ((order.discount or 0.0) / 100.0)
                val = val - (val * ((order.discount or 0.0) / 100.0))

            res[order.id]['amount_tax']=cur_obj.round(cr, uid, cur, val)
            res[order.id]['amount_untaxed']=cur_obj.round(cr, uid, cur, val1)
            #res[order.id]['amount_total']=res[order.id]['amount_untaxed'] + res[order.id]['amount_tax'] + order.round_off
            res[order.id]['amount_discount'] = cur_obj.round(cr, uid, cur, val2)
            res[order.id]['amount_total'] = (res[order.id]['amount_untaxed'] - res[order.id]['amount_discount']) + \
                                            res[order.id]['amount_tax'] + order.round_off
        return res

    _columns = {
        'round_off': fields.float('Round Off', states={'confirmed': [('readonly', True)],
                    'approved': [('readonly', True)], 'done': [('readonly', True)]}, help="Round Off Amount"),
        'amount_untaxed': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Untaxed Amount',
                store={
                    'purchase.order': (lambda self, cr, uid, ids, c={}: ids, ['round_off', 'order_line', 'discount_method', 'discount'], 10),
                    'purchase.order.line': (_get_order, ['price_unit', 'taxes_id', 'discount', 'product_qty'], 10),
                }, multi="sums", help="The amount without tax", track_visibility='always'),
        'amount_tax': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Taxes',
                store={
                    'purchase.order': (lambda self, cr, uid, ids, c={}: ids, ['round_off', 'order_line', 'discount_method', 'discount'], 10),
                    'purchase.order.line': (_get_order, ['price_unit', 'taxes_id', 'discount', 'product_qty'], 10),
                }, multi="sums", help="The tax amount"),
        'amount_total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Total',
                store={
                    'purchase.order': (lambda self, cr, uid, ids, c={}: ids, ['round_off', 'order_line', 'discount_method', 'discount'], 10),
                    'purchase.order.line': (_get_order, ['price_unit', 'taxes_id', 'discount', 'product_qty'], 10),
                }, multi="sums", help="The total amount"),
        'discount_method': fields.selection([('percent', 'Percentage'), ('fixed', 'Fixed Amount')], 'Discount Method',
                    states={'confirmed': [('readonly', True)],'approved': [('readonly', True)], 'done': [('readonly', True)]}),
        'discount': fields.float('Discount', digits_compute=dp.get_precision('Account'),
                    states={'confirmed': [('readonly', True)],'approved': [('readonly', True)], 'done': [('readonly', True)]}),
        'amount_discount': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Discount',
                store={
                    'purchase.order': (lambda self, cr, uid, ids, c={}: ids, ['round_off', 'order_line', 'discount_method', 'discount'], 10),
                    'purchase.order.line': (_get_order, ['price_unit', 'taxes_id', 'discount', 'product_qty'], 10),
                }, multi='sums', help="The discount amount.", track_visibility='always'),
    }
    _defaults = {
        'discount': 0.0,
    }

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        result = super(PurchaseOrder, self).create(cr, uid, vals, context=context)
        lines=[]
        if vals.get('order_line', []):
            for ol in vals['order_line']:
                if ol[0] in [0, 1] and ol[2].get('product_id'):
                    lines.append(ol[2]['product_id'])
        #print lines
        cek_lines = Counter(lines)
        for key, val in cek_lines.items():
            if (val > 1) and (key in lines):
                product = self.pool.get('product.product').browse(cr, uid, key, context=context)
                raise osv.except_osv(_('Peringatan!!!'),
                    _('Product tidak boleh ada yang sama (Double Product ID).\nSilahkan Cek Kembali...\n(%s)') % (product.name_template,))
        return result

    def _check_po_lines(self, cr, uid, ids, context=None):
        for obj in self.browse(cr, uid, ids, context=context):
            cr.execute('select name, count(product_id) total '
                       'from purchase_order_line where order_id=%s group by product_id, name', (obj.id,))
            for line in cr.dictfetchall():
                if line['total'] > 1:
                    raise osv.except_osv(_('Peringatan!'),
                        _('Product tidak boleh ada yang sama (Double Product ID).\nSilahkan Cek Kembali...\n(%s)') % (line['name'],))
        return True

    def write(self, cr, uid, ids, vals, context=None):
        res = super(PurchaseOrder, self).write(cr, uid, ids, vals, context=context)
        self._check_po_lines(cr, uid, ids, context=context)
        return res

    def _prepare_invoice(self, cr, uid, order, line_ids, context=None):
        journal_ids = self.pool['account.journal'].search(cr, uid, [('type', '=', 'purchase'),
                        ('company_id', '=', order.company_id.id)], limit=1)
        if not journal_ids:
            raise osv.except_osv(_('Error!'),
                _('Define purchase journal for this company: "%s" (id:%d).') % \
                    (order.company_id.name, order.company_id.id))
        outlet_ids = []
        partner_bank = False
        if order.picking_type_id and order.picking_type_id.warehouse_id:
            outlet_ids += [order.picking_type_id.warehouse_id.id]
        query = """
                SELECT rpb.id FROM res_partner_bank rpb, res_partner_bank_warehouse_rel bwr
                    WHERE rpb.active=True and rpb.partner_id=%s and bwr.bank_id=rpb.id and bwr.warehouse_id in %s   
                    ORDER BY create_date DESC LIMIT 1
                """
        cr.execute(query, (order.partner_id.id,tuple(outlet_ids)))
        banks = cr.fetchall()
        if banks:
            for bank_id in banks:
                partner_bank = bank_id

        if (not partner_bank):
            bank_acc_ids = self.pool['res.partner.bank'].search(cr, uid,
                    [('active', '=', True),('partner_id', '=', order.partner_id.id)],
                    order='create_date', limit=1)
            partner_bank = len(bank_acc_ids) and bank_acc_ids[0]
        return {
            'name': order.partner_ref or order.name,
            'reference': order.partner_ref or order.name,
            'account_id': order.partner_id.property_account_payable.id,
            'type': 'in_invoice',
            'partner_id': order.partner_id.id,
            'currency_id': order.currency_id.id,
            'journal_id': len(journal_ids) and journal_ids[0] or False,
            'invoice_line': [(6, 0, line_ids)],
            'origin': order.name,
            'fiscal_position': order.fiscal_position.id or False,
            'payment_term': order.payment_term_id.id or False,
            'company_id': order.company_id.id,
            'partner_bank_id': partner_bank,
            'round_off':order.round_off
        }

    def onchange_discount_method(self, cr, uid, ids, method, context=None):
        warning = {}
        val = {'discount_method': False, 'discount': 0.0,}
        for order in self.browse(cr, uid, ids, context=context):
            if method in ('percent','fixed'):
                val.update({'discount_method' : method, 'discount': 0.0})
                for line in order.order_line:
                    if line.discount <> 0.0 or line.discount_method in ('percent','fixed'):
                        val.update({'discount_method' : '' or False, 'discount': 0.0})
                        warning = {'title': _('Error!!!'),
                            'message' : _("Salah satu produk/item sudah ditetapkan diskon, untuk menggunakan 'metode diskon total (diluar produk)' maka kosongkan semua diskon persentase/fixed didalam produk.")}
        return {'value': val, 'warning': warning}

    def do_merge(self, cr, uid, ids, context=None):
        #TOFIX: merged order line should be unlink
        def make_key(br, fields):
            list_key = []
            for field in fields:
                field_val = getattr(br, field)
                if field in ('product_id', 'account_analytic_id'):
                    if not field_val:
                        field_val = False
                if isinstance(field_val, browse_record):
                    field_val = field_val.id
                elif isinstance(field_val, browse_null):
                    field_val = False
                elif isinstance(field_val, browse_record_list):
                    field_val = ((6, 0, tuple([v.id for v in field_val])),)
                list_key.append((field, field_val))
            list_key.sort()
            return tuple(list_key)

        context = dict(context or {})
        oline_obj = self.pool.get('purchase.order.line')
        new_product_ids = []

        # Compute what the new orders should contain
        new_orders = {}

        order_lines_to_move = {}
        for porder in [order for order in self.browse(cr, uid, ids, context=context) if order.state == 'draft']:
            order_key = make_key(porder, ('partner_id', 'location_id', 'pricelist_id', 'currency_id'))
            new_order = new_orders.setdefault(order_key, ({}, []))
            new_order[1].append(porder.id)
            order_infos = new_order[0]
            order_lines_to_move.setdefault(order_key, [])

            if not order_infos:
                order_infos.update({
                    'origin': porder.origin,
                    'date_order': porder.date_order,
                    'partner_id': porder.partner_id.id,
                    'dest_address_id': porder.dest_address_id.id,
                    'picking_type_id': porder.picking_type_id.id,
                    'location_id': porder.location_id.id,
                    'pricelist_id': porder.pricelist_id.id,
                    'currency_id': porder.currency_id.id,
                    'state': 'draft',
                    'order_line': {},
                    'notes': '%s' % (porder.notes or '',),
                    'fiscal_position': porder.fiscal_position and porder.fiscal_position.id or False,
                })
            else:
                if porder.date_order < order_infos['date_order']:
                    order_infos['date_order'] = porder.date_order
                if porder.notes:
                    order_infos['notes'] = (order_infos['notes'] or '') + ('\n%s' % (porder.notes,))
                if porder.origin:
                    order_infos['origin'] = (order_infos['origin'] or '') + ' ' + porder.origin

            for order_line in porder.order_line: ##Edited by baim
                if order_line.state != 'cancel':
                    if order_line.product_id.id in new_product_ids:
                        oline_ids = oline_obj.search(cr, uid, [('id','in',order_lines_to_move[order_key]),('product_id','=',order_line.product_id.id)], context=context)
                        if oline_ids:
                            for oline in oline_obj.browse(cr, uid, oline_ids, context=context):
                                oline_obj.write(cr, uid, [oline.id], {'product_qty': oline.product_qty + order_line.product_qty}, context=context)
                            oline_obj.write(cr, uid, [order_line.id], {'name': str(order_line.name)+"\n[MERGED]",'state': 'cancel','product_qty':0.0}, context=context)
                    else:
                        order_lines_to_move[order_key] += [order_line.id]
                        new_product_ids.append(order_line.product_id.id)
            # order_lines_to_move[order_key] += [order_line.id for order_line in porder.order_line
            #                                    if order_line.state != 'cancel']

        allorders = []
        orders_info = {}
        for order_key, (order_data, old_ids) in new_orders.iteritems():
            # skip merges with only one order
            if len(old_ids) < 2:
                allorders += (old_ids or [])
                continue

            # cleanup order line data
            for key, value in order_data['order_line'].iteritems():
                del value['uom_factor']
                value.update(dict(key))
            order_data['order_line'] = [(6, 0, order_lines_to_move[order_key])]

            # create the new order
            context.update({'mail_create_nolog': True})
            neworder_id = self.create(cr, uid, order_data)
            self.message_post(cr, uid, [neworder_id], body=_("RFQ created"), context=context)
            orders_info.update({neworder_id: old_ids})
            allorders.append(neworder_id)

            # make triggers pointing to the old orders point to the new order
            for old_id in old_ids:
                self.redirect_workflow(cr, uid, [(old_id, neworder_id)])
                self.signal_workflow(cr, uid, [old_id], 'purchase_cancel')

        return orders_info


class PurchaseOrderLines(osv.osv):
    _inherit = 'purchase.order.line'

    def _calc_line_base_price(self, cr, uid, line, context=None):
        line_price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
        if line.discount_method == "fixed":
            line_price = (line.product_qty * line.price_unit) - line.discount
        return line_price

    def _calc_line_quantity(self, cr, uid, line, context=None):
        line_qty = line.product_qty
        if line.discount_method == "fixed":
            line_qty = 1.0
        return line_qty

    _columns = {
        'discount_method': fields.selection([('percent', 'Percentage'), ('fixed', 'Fixed Amount')], 'Discount Method',
                readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}),
        'discount': fields.float('Discount', digits_compute=dp.get_precision('Discount'),
                readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}),
        'cost_categ_id': fields.many2one('cost.category', 'Cost Category'),
    }

    def onchange_discount(self, cr, uid, ids, method, parent_disc, context=None):
        warning = {}
        val = {'discount_method': method, 'discount': 0.0,}
        if parent_disc in ('percent','fixed'):
            val.update({'discount_method': False, 'discount': 0.0})
            warning = {'title': _('Error!!!'),
                    'message' : _("Sudah ada perhitungan diskon diluar per produk, silahkan periksa kembali dokumen ini.")}
        return {'value': val, 'warning': warning}

    def _prepare_inv_line(self, cr, uid, account_id, order_line, context=None):
        result = super(PurchaseOrderLines, self)._prepare_inv_line(cr, uid, account_id, order_line, context=context)
        result['discount_method'] = order_line.discount_method
        result['discount'] = order_line.discount or 0.0
        return result

    def _prepare_order_line_move(self, cr, uid, order, order_line, picking_id, group_id, context=None):
        res = super(PurchaseOrderLines, self)._prepare_order_line_move(cr, uid, order, order_line, picking_id, group_id, context=context)
        for vals in res:
            if order_line.discount_method == 'percent':
                vals['price_unit'] = (vals.get('price_unit', 0.0) * (1 - (order_line.discount / 100)))
            elif order_line.discount_method == 'fixed':
                vals['price_unit'] = vals.get('price_unit', 0.0) - order_line.discount
        return res

    def onchange_product_id(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', picking_type_id=False, context=None):
        if context is None:
            context = {}

        res = {'value': {'price_unit': price_unit or 0.0, 'name': name or '', 'product_uom' : uom_id or False}}
        if not product_id:
            if not uom_id:
                uom_id = self.default_get(cr, uid, ['product_uom'], context=context).get('product_uom', False)
                res['value']['product_uom'] = uom_id
            return res

        product_product = self.pool.get('product.product')
        product_uom = self.pool.get('product.uom')
        res_partner = self.pool.get('res.partner')
        account_obj = self.pool.get('account.account')
        picking_type_obj = self.pool.get('stock.picking.type')
        product_pricelist = self.pool.get('product.pricelist')
        account_fiscal_position = self.pool.get('account.fiscal.position')
        # - determine name and notes based on product in partner lang.
        context_partner = context.copy()
        if partner_id:
            lang = res_partner.browse(cr, uid, partner_id).lang
            context_partner.update( {'lang': lang, 'partner_id': partner_id} )
        product = product_product.browse(cr, uid, product_id, context=context_partner)
        picking_type = picking_type_obj.browse(cr, uid, picking_type_id, context=context)
        #call name_get() with partner in the context to eventually match name and description in the seller_ids field
        if not name or not uom_id:
            dummy, name = product_product.name_get(cr, uid, product_id, context=context_partner)[0]
            if product.description_purchase:
                name += '\n' + product.description_purchase
            res['value'].update({'name': name})

        # - set a domain on product_uom
        res['domain'] = {'product_uom': [('category_id','=',product.uom_id.category_id.id)]}
        # - check that uom and product uom belong to the same category
        product_uom_po_id = product.uom_po_id.id
        if not uom_id:
            uom_id = product_uom_po_id

        if product.uom_id.category_id.id != product_uom.browse(cr, uid, uom_id, context=context).category_id.id:
            if context.get('purchase_uom_check') and self._check_product_uom_group(cr, uid, context=context):
                res['warning'] = {'title': _('Warning!'), 'message': _('Selected Unit of Measure does not belong to the same category as the product Unit of Measure.')}
            uom_id = product_uom_po_id

        res['value'].update({'product_uom': uom_id})

        # - determine product_qty and date_planned based on seller info
        if not date_order:
            date_order = fields.datetime.now()

        supplierinfo = False
        precision = self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Unit of Measure')
        for supplier in product.seller_ids:
            if partner_id and (supplier.name.id == partner_id):
                supplierinfo = supplier
                if supplierinfo.product_uom.id != uom_id:
                    res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier only sells this product by %s') % supplierinfo.product_uom.name }
                min_qty = product_uom._compute_qty(cr, uid, supplierinfo.product_uom.id, supplierinfo.min_qty, to_uom_id=uom_id)
                if float_compare(min_qty , qty, precision_digits=precision) == 1: # If the supplier quantity is greater than entered from user, set minimal.
                    if qty:
                        res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier has a minimal quantity set to %s %s, you should not purchase less.') % (supplierinfo.min_qty, supplierinfo.product_uom.name)}
                    qty = min_qty
        dt = self._get_date_planned(cr, uid, supplierinfo, date_order, context=context).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        qty = qty or 1.0
        res['value'].update({'date_planned': date_planned or dt})
        if qty:
            res['value'].update({'product_qty': qty})
        if product.cost_categ_id:  #onchange_cost_category_id
            res['value'].update({'cost_categ_id': product.cost_categ_id.id})
        if product.requirement_type:
            res['value'].update({'requirement_type': product.requirement_type})

        if product.property_account_expense:  #onchange_analytic_id
            account_id = product.property_account_expense.id
        else:
            account_id = False
            if product.categ_id:
                account_id = product.categ_id.property_account_expense_categ.id
        if account_id and picking_type.warehouse_id:
            aid = {}  #onchange_analytic_id
            account = account_obj.browse(cr, uid, account_id)
            for al in account.analytic_line:
                aid[al.name.id] = al.analytic_id.id
            if aid.has_key(picking_type.warehouse_id.id):
                res['value'].update({'account_analytic_id': aid[picking_type.warehouse_id.id]})

        price = price_unit
        if price_unit is False or price_unit is None:
            # - determine price_unit and taxes_id
            if pricelist_id:
                date_order_str = datetime.strptime(date_order, DEFAULT_SERVER_DATETIME_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT)
                price = product_pricelist.price_get(cr, uid, [pricelist_id],
                        product.id, qty or 1.0, partner_id or False, {'uom': uom_id, 'date': date_order_str})[pricelist_id]
            else:
                price = product.standard_price

        if uid == SUPERUSER_ID:
            company_id = self.pool['res.users'].browse(cr, uid, [uid]).company_id.id
            taxes = product.supplier_taxes_id.filtered(lambda r: r.company_id.id == company_id)
        else:
            taxes = product.supplier_taxes_id
        fpos = fiscal_position_id and account_fiscal_position.browse(cr, uid, fiscal_position_id, context=context) or False
        taxes_ids = account_fiscal_position.map_tax(cr, uid, fpos, taxes, context=context)
        price = self.pool['account.tax']._fix_tax_included_price(cr, uid, price, product.supplier_taxes_id, taxes_ids)
        res['value'].update({'price_unit': price, 'taxes_id': taxes_ids})

        return res



class res_partner_bank(osv.osv):
    _inherit = 'res.partner.bank'

    _columns = {
        'warehouse_ids': fields.many2many('stock.warehouse', 'res_partner_bank_warehouse_rel', 'bank_id', 'warehouse_id', 'Warehouses', copy=False),
    }



