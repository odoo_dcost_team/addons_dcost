## -*- coding: utf-8 -*-
## © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
import time
from openerp.osv import osv, fields
from openerp.tools.translate import _    
    
class purchase_order(osv.osv):
    _inherit = 'purchase.order'
    
    def _get_picking_in(self, cr, uid, context=None):
        obj_data = self.pool.get('ir.model.data')
        type_obj = self.pool.get('stock.picking.type')
        user_obj = self.pool.get('res.users')
        users = user_obj.browse(cr, uid, uid, context=context)
        company_id = user_obj.browse(cr, uid, uid, context=context).company_id.id
        types = type_obj.search(cr, uid, [('code', '=', 'incoming'), ('warehouse_id.company_id', '=', company_id)], context=context)
        if users.warehouse_id:
            types = type_obj.search(cr, uid, [('code', '=', 'incoming'), ('warehouse_id', '=', users.warehouse_id.id)], context=context)
        if not types:
            types = type_obj.search(cr, uid, [('code', '=', 'incoming'), ('warehouse_id', '=', False)], context=context)
            if not types:
                raise osv.except_osv(_('Error!'), _("Make sure you have at least an incoming picking type defined"))
        return types[0]
    
    _defaults = {
        'picking_type_id': _get_picking_in,
    }
    
    def create(self, cr, uid, vals, context=None):
        if vals.get('name','/')=='/':
            user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
            warehouse_code = user.warehouse_id and user.warehouse_id.code
            if vals.get('picking_type_id', False):
                pick_type = self.pool.get('stock.picking.type').browse(cr, uid, vals['picking_type_id'], context=context)
                warehouse_code = pick_type.warehouse_id and pick_type.warehouse_id.code
            po_seq = list(str(self.pool.get('ir.sequence').get(cr, uid, 'purchase.order.dcost')))
            po_seq.insert(3, warehouse_code +"/")
            vals['name'] = ''.join(po_seq) or '/'
        context = dict(context or {}, mail_create_nolog=True)
        order =  super(purchase_order, self).create(cr, uid, vals, context=context)
        self.message_post(cr, uid, [order], body=_("RFQ created"), context=context)
        return order
    
    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        warehouse_code = user.warehouse_id and user.warehouse_id.code
        purchase = self.browse(cr, uid, id, context=context)
        # if purchase.picking_type_id:
        #     ptype = self.pool.get('stock.picking.type').browse(cr, uid, purchase.picking_type_id.id, context=context)
        #     warehouse_code = ptype.warehouse_id and ptype.warehouse_id.code
            
        po_seq = list(str(self.pool.get('ir.sequence').get(cr, uid, 'purchase.order.dcost')))
        po_seq.insert(3, warehouse_code +"/")
        number = ''.join(po_seq) or '/'
        default.update({
            'date_order': time.strftime('%Y-%m-%d %H:%M:%S'),
            'state': 'draft',
            'invoice_ids': [],
            'date_approve': False,
            'name': number,
        })
        return super(purchase_order, self).copy(cr, uid, id, default, context=context)
    
    def write(self, cr, uid, ids, vals, context=None):
        if vals.get('picking_type_id'):
            for po in self.browse(cr, uid, ids, context):
                po_number = str(po.name).split("/")
                if len(po_number)==3:
                    wh_code = str(po_number[1])
                    if ('picking_type_id' in vals):
                        pick_type = self.pool.get('stock.picking.type').browse(cr, uid, vals['picking_type_id'], context=context)
                        wh_code = pick_type.warehouse_id.code
                    number = str(po_number[0]) +"/"+ wh_code +"/"+ str(po_number[2])
                    vals.update({'name': number})
        return super(purchase_order, self).write(cr, uid, ids, vals, context=context)
    
    
    
# class account_move(osv.osv):
#     _inherit = "account.move"
#
#     def post(self, cr, uid, ids, context=None):
#         if context is None:
#             context = {}
#         invoice = context.get('invoice', False)
#         valid_moves = self.validate(cr, uid, ids, context)
#
#         if not valid_moves:
#             raise osv.except_osv(_('Error!'), _('You cannot validate a non-balanced entry.\nMake sure you have configured payment terms properly.\nThe latest payment term line should be of the "Balance" type.'))
#         obj_sequence = self.pool.get('ir.sequence')
#         for move in self.browse(cr, uid, valid_moves, context=context):
#             user = self.pool['res.users'].browse(cr, uid, uid, context=context)  #edited
#             wh_code = user.warehouse_id and user.warehouse_id.code or 'XXX'
#             if move.name =='/':
#                 new_name = False
#                 journal = move.journal_id
#
#                 if invoice and invoice.internal_number:
#                     new_name = invoice.internal_number
#                 else:
#                     if journal.sequence_id: #EXJ/2017/12345 => EXJ/bekasi/2017/12345
#                         c = {'fiscalyear_id': move.period_id.fiscalyear_id.id}
#                         if invoice and invoice.type=='in_invoice': #edited
#                             inv_sequences = obj_sequence.get(cr, uid, 'supplier.invoice.dcost', context=context)
#                             supp_inv = list(str(inv_sequences))
#                             supp_inv.insert(4, wh_code +"/")
#                             new_name = ''.join(supp_inv)
#                         else:
#                             new_name = obj_sequence.next_by_id(cr, uid, journal.sequence_id.id, c)
#                     else:
#                         raise osv.except_osv(_('Error!'), _('Please define a sequence on the journal.'))
#
#                 if new_name:
#                     self.write(cr, uid, [move.id], {'name':new_name})
#
#         cr.execute('UPDATE account_move '\
#                    'SET state=%s '\
#                    'WHERE id IN %s',
#                    ('posted', tuple(valid_moves),))
#         self.invalidate_cache(cr, uid, ['state', ], valid_moves, context=context)
#         return True
    
    
class stock_picking(osv.osv):
    _inherit = 'stock.picking'
     
    def create(self, cr, user, vals, context=None):
        context = context or {}  #WH/IN/12345
        seq_obj = self.pool.get('ir.sequence')
        if ('name' not in vals) or (vals.get('name') in ('/', False)):
            ptype_id = vals.get('picking_type_id', context.get('default_picking_type_id', False))
            ptype = self.pool.get('stock.picking.type').browse(cr, user, ptype_id, context=context)
            pick_number = seq_obj.get_id(cr, user, ptype.sequence_id and ptype.sequence_id.id, 'id', context=context)
            if ptype.code=='incoming' or ptype.default_location_src_id.usage=='supplier':
                wh_code = ptype.warehouse_id and ptype.warehouse_id.code
                picking_in = seq_obj.get(cr, user, 'stock.picking.incoming', context=context)
                pick_seq = list(str(picking_in))  #BUGS
                pick_seq.insert(3, wh_code +"/")
                pick_number = ''.join(pick_seq)
            vals['name'] = pick_number
        return super(stock_picking, self).create(cr, user, vals, context)
     
    def write(self, cr, uid, ids, vals, context=None):
        if vals.get('picking_type_id') and not vals.get('pack_operation_ids'):
            # pack operations are directly dependant of move lines, it needs to be recomputed
            pack_operation_obj = self.pool.get('stock.pack.operation')
            existing_package_ids = pack_operation_obj.search(cr, uid, [('picking_id', 'in', ids)], context=context)
            if existing_package_ids:
                pack_operation_obj.unlink(cr, uid, existing_package_ids, context)
        if vals.get('picking_type_id'):
            for pick in self.browse(cr, uid, ids, context):
                if pick.picking_type_id.code=='incoming' or pick.picking_type_id.default_location_src_id.usage=='supplier':
                    pick_number = str(pick.name).split("/")
                    if len(pick_number)==4:  #WH/bekasi/IN/12345
                        wh_code = str(pick_number[2])
                        if ('picking_type_id' in vals): 
                            pick_type = self.pool.get('stock.picking.type').browse(cr, uid, vals['picking_type_id'], context=context)
                            wh_code = pick_type.warehouse_id.code
                        number = str(pick_number[0]) +"/"+ str(pick_number[1]) +"/"+ wh_code +"/"+ str(pick_number[3])
                        vals.update({'name': number})
        res = super(stock_picking, self).write(cr, uid, ids, vals, context=context)
        #if we changed the move lines or the pack operations, we need to recompute the remaining quantities of both
        if 'move_lines' in vals or 'pack_operation_ids' in vals:
            self.do_recompute_remaining_quantities(cr, uid, ids, context=context)
        return res
    
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
     