import csv
import time
import base64
import tempfile
from datetime import date, datetime, timedelta
import cStringIO
from dateutil import parser
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp


class stock_picking(osv.osv):
    _inherit = 'stock.picking' 
    _columns = {
        'work_id': fields.many2one('work.order', 'Work Order', ondelete='cascade'),
        'stock_id': fields.many2one('stock.order', 'Stock Order', ondelete='cascade'),
        'type_wo': fields.selection([('fg', 'Finish Goods'), ('rm', 'Raw Material')], 'Type WO', readonly=True),
    }

    _order = 'name desc'

class stock_order(osv.osv):
    _name = "stock.order"
    
    def _get_picking_ids(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for so_id in ids:
            res[so_id] = []
        query = """
        SELECT picking_id, so.id FROM stock_picking p, stock_move m, stock_order_line sol, stock_order so
            WHERE so.id in %s and so.id = sol.stock_id and sol.id = m.stock_line_id and m.picking_id = p.id
            GROUP BY picking_id, so.id             
        """
        cr.execute(query, (tuple(ids), ))
        picks = cr.fetchall()
        for pick_id, so_id in picks:
            res[so_id].append(pick_id)
        return res
    
    _columns = {
        'name': fields.char('Reference', size=64, required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'location_id': fields.many2one('stock.location', 'Outlet', domain = [('usage', '=', 'internal')], required=True),
        'type': fields.selection([('log', 'Senlog'), ('kit', 'Senkit')], 'Type', required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        #'purchase_id': fields.many2one('purchase.order', 'Purchase Order', readonly=True),
        'description': fields.char('Description', size=256, readonly=True, states={'draft': [('readonly', False)]}),
        'mps': fields.boolean('MPS'),
        'date': fields.date('Date', readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('cancel', 'Cancel'),
            ('approve', 'Approve'),
            ('done', 'Done'),
            ], 'State', readonly=True, select=True),
        'user_id': fields.many2one('res.users', 'Responsible', states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, select=True, track_visibility='onchange'),
        'stock_line': fields.one2many('stock.order.line', 'stock_id', 'Stock Lines', readonly=True, required=True, states={'draft': [('readonly', False)]}),   
        'picking_ids': fields.function(_get_picking_ids, method=True, type='one2many', relation='stock.picking', string='Picking List'),
    }
     
    _defaults = {
        'name': '/',
        'state':'draft',
        'type': 'log',
        'user_id': lambda obj, cr, uid, context: uid,
        'date': lambda *a: time.strftime('%Y-%m-%d'),     
    }

    _order = 'name desc'

    def unlink(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids, context={})[0]
        if val.state != 'draft':
            raise osv.except_osv(('Perhatian !'), ('Stock Order tidak bisa dihapus pada state \'%s\'!') % (val.state,))
        return super(stock_order, self).unlink(cr, uid, ids, context=context)

    def user_id_change(self, cr, uid, ids, idp):
        res = {}                       
        res['user_id'] = uid        
        return {'value': res}

    def location_change(self, cr, uid, ids, idp):
        res = {}
        loc = self.pool.get('res.users').browse(cr, uid, uid).warehouse_id.lot_stock_id.id or False
        res['location_id'] = loc
        return {'value': res}

    def create(self, cr, uid, vals, context=None): 
        vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'stock.order')
        return super(stock_order, self).create(cr, uid, vals, context=context)

    def stock_draft(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'draft'})
        return True                               
    
    def stock_cancel(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'cancel'})
        return True                                  
         
    def stock_confirm(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids, context={})[0]
        if not val.stock_line:
            raise osv.except_osv(('Perhatian !'), ('Tabel Stock Lines harus di isi !'))
        self.write(cr, uid, ids, {'state': 'approve'})
        return True
    
    def stock_validate(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        
        obj_picking = self.pool.get('stock.picking')
        obj_move = self.pool.get('stock.move')
        
        goods_id = obj_picking.create(cr,uid, {
                                                'origin': val.name,
                                                'stock_id': val.id,
                                                'type_wo': 'fg',
                                                'picking_type_id': val.user_id.warehouse_id.int_type_id.id,
                                                'move_type': 'one',
                                                'date': val.date,
                                                'company_id': 1})
        for x in val.stock_line:
            obj_move.create(cr,uid, {
                                    'name': x.product_id.partner_ref,
                                    'date': val.date,
                                    'stock_line_id': x.id,
                                    'product_id': x.product_id.id,
                                    'product_uom': x.product_uom.id,
                                    'product_uom_qty': x.product_qty,
                                    'location_id': 10,
                                    'location_dest_id': val.location_id.id,
                                    'picking_id': goods_id,
                                    'origin': val.name,
                                    'company_id': 1})
        
        obj_picking.action_confirm(cr, uid, [goods_id], context=context)
        obj_picking.force_assign(cr, uid, [goods_id], context=context) 
        
        self.write(cr, uid, ids, {'state': 'done'})
        return True 

    def view_picking(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        mod_obj = self.pool.get('ir.model.data')
        dummy, action_id = tuple(mod_obj.get_object_reference(cr, uid, 'stock', 'action_picking_tree'))
        action = self.pool.get('ir.actions.act_window').read(cr, uid, action_id, context=context)

        pick_ids = []
        for so in self.browse(cr, uid, ids, context=context):
            pick_ids += [picking.id for picking in so.picking_ids]

        action['context'] = {}
        if len(pick_ids) > 1:
            action['domain'] = "[('id','in',[" + ','.join(map(str, pick_ids)) + "])]"
        else:
            res = mod_obj.get_object_reference(cr, uid, 'stock', 'view_picking_form')
            action['views'] = [(res and res[1] or False, 'form')]
            action['res_id'] = pick_ids and pick_ids[0] or False
        return action


    def view_invoice(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids, context=context)
        mod_obj = self.pool.get('ir.model.data')
        invoice_obj = self.pool.get('account.invoice')
        
        inv_ids = invoice_obj.search(cr, uid, [('stock_id', '=', val.id)])
        
        res = mod_obj.get_object_reference(cr, uid, 'account', 'invoice_supplier_form')
        res_id = res and res[1] or False

        return {
            'name': ('Hutang Senlog Senkit'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'account.invoice',
            'context': "{'type':'in_invoice', 'journal_type': 'purchase', 'tipe':'logkit'}",
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': inv_ids and inv_ids[0] or False,
        }

class stock_order_line(osv.osv):
    _name = 'stock.order.line' 
    _columns = {
        'stock_id': fields.many2one('stock.order', 'Stock Order', required=True, ondelete='cascade'),
        'name': fields.char('Description', size=256, required=True),
        'product_id': fields.many2one('product.product', 'Product', required=True),
        'product_qty': fields.float('Quantity', digits_compute=dp.get_precision('Product UoM')),
        'product_uom': fields.many2one('product.uom', 'UoM', required=True),
    }
        
    _defaults = {
        'product_uom': 1,
        'product_qty': 1,
    }

    def product_id_change(self, cr, uid, ids, idp):
        res = {}                       
        product_obj = self.pool.get('product.product')
        if idp:
            product = product_obj.browse(cr, uid, idp)
            res['name'] = product_obj.name_get(cr, uid, [idp])[0][1]
            res['product_uom'] = product.uom_id.id           
                    
        return {'value': res}


#         val = self.browse(cr, uid, ids, context={})[0]
#         obj_move = self.pool.get('stock.move')
#         obj_picking = self.pool.get('stock.picking')
#         
#         type = self.pool.get('stock.picking.type').search(cr, uid, [('code', '=', 'internal'), ('default_location_src_id', '=', val.location_id.id)])
#         
#         pid = obj_picking.create(cr,uid, {
#                                     'origin': val.name,
#                                     'stock_id': val.id,
#                                     'picking_type_id': type[0],
#                                     'type': 'internal',
#                                     'move_type': 'one',
#                                     'date': val.date,
#                                     'company_id': 1,
#         })
#         
#         source = self.pool.get('stock.warehouse').browse(cr, uid, 1).lot_stock_id.id
#         for x in val.stock_line:    
#             obj_move.create(cr,uid, {
#                                     'name': x.product_id.partner_ref,
#                                     'date': val.date,
#                                     'product_id': x.product_id.id,
#                                     'product_uom': x.product_uom.id,
#                                     'product_uom_qty': x.product_qty,
#                                     'location_id': source,
#                                     'location_dest_id': val.location_id.id,
#                                     #'move_dest_id': source,
#                                     'picking_id': pid,
#                                     'origin': val.name,
#                                     'company_id': 1
#             })
#                 
#         obj_picking.action_confirm(cr, uid, pid, context=context)
#         obj_picking.force_assign(cr, uid, pid, context=context)