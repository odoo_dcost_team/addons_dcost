{
    "name":"Stock Order",
    "version":"0.1",
    "author":"PT. SUMBER INTEGRASI UTAMA",
    "category":"Custom Modules",
    "description": """
        To handle stock order form outlet.
    """,
    "depends":["base", "stock", "purchase", "siu_work_order"],
    "init_xml":[],
    "demo_xml":[],
    "update_xml":["stock_view.xml"],
    "active":False,
    "installable":True
}
