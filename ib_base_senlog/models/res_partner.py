# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
from openerp.osv import osv, fields


class ResPartner(osv.Model):
    _inherit = "res.partner"

    _columns = {
        'code': fields.char('Partner Code', size=64),
    }
    _sql_constraints = [
        ('customer_code_uniq', 'unique (code,company_id)', 'The code of the partner must be unique per company !')
    ]

    # def create(self, cr, uid, vals, context=None):
    #     account_obj = self.pool.get('account.account')
    #     if context is None:
    #         context = {}
    #
    #     if uid not in (1, 206, 207, 347, 353, 375, 376, 371, 394, 408):
    #         raise osv.except_osv(('Perhatian !'), (
    #             'Selain Administrator/BomAdmin/adminpusat/Mgr Akunting tidak bisa membuat Customer/Supplier!'))
    #     ar_account_id = ap_account_id = False
    #     if 'supplier' in vals and vals['supplier']:
    #         # ar_account_id = account_obj.search(cr, uid, ['&', '|', ('active', '=', True), ('code', 'like', '%111600.02.01%'),
    #         #                              ('name', 'like', '%UM- Supplier%')], limit=1)[0]
    #         ap_account_id = account_obj.search(cr, uid, ['&', '|', ('active', '=', True), ('code', 'like', '%121210.00.02%'),
    #                                      ('name', 'like', '%Hutang Usaha ke Supplier Sudah Ditagih%')], limit=1)[0]
    #     elif 'customer' in vals and vals['customer']:
    #         ar_account_id = account_obj.search(cr, uid, ['&', '|', ('active', '=', True), ('code', 'like', '%111140.00.01%'),
    #                                      ('name', 'like', '%Piutang Usaha Penjualan Harian%')], limit=1)[0]
    #         # ap_account_id = account_obj.search(cr, uid, ['&', '|', ('active', '=', True), ('code', 'like', '%121210.00.01%'),
    #         #                              ('name', 'like', '%Hutang Usaha ke Supplier Belum Ditagih%')], limit=1)[0]
    #     if ar_account_id:
    #         vals['property_account_receivable'] = ar_account_id
    #     if ap_account_id:
    #         vals['property_account_payable'] = ap_account_id
    #
    #     return super(ResPartner, self).create(cr, uid, vals, context=context)

    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = []
        for record in self.browse(cr, uid, ids, context=context):
            name = record.name
            if record.code:
                name = "[%s] %s" % (record.code, name)
            if record.parent_id and not record.is_company:
                name = "%s, %s" % (record.parent_name, name)
            if context.get('show_address_only'):
                name = self._display_address(cr, uid, record, without_company=True, context=context)
            if context.get('show_address'):
                name = name + "\n" + self._display_address(cr, uid, record, without_company=True, context=context)
            name = name.replace('\n\n','\n')
            name = name.replace('\n\n','\n')
            if context.get('show_email') and record.email:
                name = "%s <%s>" % (name, record.email)
            res.append((record.id, name))
        return res




