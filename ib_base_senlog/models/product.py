# -*- coding: utf-8 -*-
import xmlrpclib
from openerp.osv import osv, orm, fields
from openerp import pooler, SUPERUSER_ID


db_groceries = ['SENLOGPB5', 'PB5', 'SENLOG', 'SENKIT']

class RPCProxyOne(object):
    def __init__(self, server, ressource, user_id):
        self.server = server
        self.user_id = user_id
        local_url = 'http://%s:%d/xmlrpc/common' % (server.server_url,
                                                    server.server_port)
        rpc = xmlrpclib.ServerProxy(local_url)
        self.uid = rpc.login(server.server_db, server.login, server.password)
        local_url = 'http://%s:%d/xmlrpc/object' % (server.server_url,
                                                    server.server_port)
        self.rpc = xmlrpclib.ServerProxy(local_url)
        self.ressource = ressource

    def __getattr__(self, name):
        RPCProxy(self.server, self.user_id)
        server_db = self.server.server_db
        server_pass = self.server.password
        resource = self.ressource
        return lambda cr, uid, *args, **kwargs: self.rpc.execute(server_db,
                                                                 self.user_id or self.uid,
                                                                 server_pass,
                                                                 resource,
                                                                 name, *args)


class RPCProxy(object):
    def __init__(self, server, user):
        self.server = server
        self.user = user

    def get(self, ressource):
        return RPCProxyOne(self.server, ressource, self.user)



class ProductTemplate(osv.osv):
    _inherit = "product.template"

    def unlink(self, cr, uid, ids, context=None):
        svr_ids = False
        conf_server_obj = self.pool.get("duplicate.config.server")
        if cr.dbname in db_groceries:
            svr_ids = conf_server_obj.search(cr, uid, [('server_db', '=', 'PB1')], limit=1, context=context)
        server_conf = conf_server_obj.browse(cr, uid, svr_ids and svr_ids[0] or False)
        pool_dest = RPCProxy(server_conf, SUPERUSER_ID)
        for tmpl in self.browse(cr, uid, ids, context=context):
            if cr.dbname in db_groceries:  # delete product tmpl in PB1
                check_tmpl_dst_ids = pool_dest.get('product.template').search(cr, SUPERUSER_ID,
                                    [('id', '=', tmpl.product_tmpl_dst_id)], context=context)
                if check_tmpl_dst_ids:
                    pool_dest.get('product.template').unlink(cr, SUPERUSER_ID, check_tmpl_dst_ids)
        return super(ProductTemplate, self).unlink(cr, uid, ids, context=context)

