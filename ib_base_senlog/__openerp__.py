# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
{
    "name": "Custom Module - Base Dcost",
    "version": "0.1",
    "depends": [
        "sale",
        "sale_stock",
        "product",
        "account",
        "website_quote",
        "ib_duplicate_purchase_sale",
        "aa_analytic_otomatis",
        "ib_reports",
    ],
    "author": "Ibrohim Binladin - 083871909782 [ ibradiiin@gmail.com ]",
    "category":"Custom Modules",
    "description": """ Custom Module""",
    "init_xml": [],
    'data': [
        "security/base_security.xml",
        "security/ir.model.access.csv",
        "data/schedule.xml",
        "views/view.xml",
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}