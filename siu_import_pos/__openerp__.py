{
    "name":"Import POS",
    "version":"0.1",
    "author":"PT. SUMBER INTEGRASI UTAMA",
    "website":"http://openerp.com",
    "category":"Custom Modules/Point of Sales",
    "description": """
        Import POS dengan API.
    """,
    "depends":["base", "point_of_sale", "account"],
    "init_xml":[],
    "demo_xml":[],
    "update_xml":[
                  "pos_view.xml",
                  #"cron_view.xml",
                  ],
    "active":False,
    "installable":True
}
