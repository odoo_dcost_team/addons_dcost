{
    "name":"Eksport Import Object Data",
    "version":"0.1",
    "author":"PT. SUMBER INTEGRASI UTAMA",
    "website":"http://myabcerp.com",
    "category":"Custom Modules",
    "description": """
        The base module to generate excel report.
    """,
    "depends":["base", "analytic"],
    "init_xml":[],
    "demo_xml":[],
    "update_xml":["eksport_import_view.xml"],
    "active":False,
    "installable":True
}
