# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import logging

_logger = logging.getLogger(__name__)


class wizard_setoran_outlet(osv.osv_memory):
    _name = "wizard.setoran.outlet"
    _description = "Daily Cash Deposit"

    def _check_invoice_list(self, cr, uid, context):
        if context['active_model'] == 'account.invoice':
            ids = context['active_ids']
            user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
            if ids or (len(ids) > 1):
                invoices = self.pool.get('account.invoice').read(cr, uid, ids,
                                                                 ['state','company_id','deposit_outlet','deposit_residual'])
                for o in invoices:
                    if o['state'] != 'paid':
                        raise orm.except_orm(_('Warning!'), _(
                            'At least one of the selected invoice is %s! \nAllowed invoice status is paid.') % o['state'])
                    if o['company_id'][0] <> user.company_id.id:
                        raise orm.except_orm(_('Warning!'), _('Not all Invoices are in the same company as the user!'))
                    if o['deposit_residual'] < 0.0:
                        raise orm.except_orm(_('Warning!'), _(
                            'Setoran Outlet sudah Completed tapi Balance setoran minus, silahkan cek kembali...!'))
        return {}

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        if context is None:
            context = {}
        res = super(wizard_setoran_outlet, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=False)
        self._check_invoice_list(cr, uid, context)
        return res

    def _get_currency(self, cr, uid, context=None):
        if context is None: context = {}
        if context.get('currency_id', False):
            return context.get('currency_id')
        return self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.currency_id.id

    def _compute_remaining_amount(self, cr, uid, ids, name, args, context=None):
        if context is None:
            context = {}
        res = {}
        for wzd in self.browse(cr, uid, ids, context=context):
            res[wzd.id] = 0.0
            invoice_id = False
            if context and context.get('invoice_id', False):
                invoice_id = context['invoice_id']
            elif context and context.get('active_id', False):
                invoice_id = context['active_id']
            invoice = self.pool.get('account.invoice').browse(cr, uid, invoice_id, context=context)
            if invoice.deposit_residual:
                res[wzd.id] = invoice.deposit_residual
        return res

    _columns = {
        'reference': fields.char('Deposit Ref#', help="Transaction reference number.", copy=False),
        'name': fields.char('Note'),
        'journal_id': fields.many2one('account.journal', 'Journal', required=True,
                domain=[('code','in',('STRCS','STRCD','STRDV')),('type', 'in', ('bank','cash'))]),
        'date': fields.date('Date', select=True, help="Tanggal Outlet setor uang penjualan ke Pusat.", copy=False),
        'period_id': fields.many2one('account.period', 'Period', required=True),
        'warehouse_id': fields.many2one('stock.warehouse', 'Outlet'),
        'amount': fields.float('Deposit Amount', digits_compute=dp.get_precision('Account'), required=True),
        'remaining_amount': fields.function(_compute_remaining_amount, string='Remaining Amount',
                type='float', readonly=True, help="Sisa penjualan yang belum disetorkan ke Pusat."),
        'user_id': fields.many2one('res.users', 'User'),
        'currency_id': fields.many2one('res.currency', 'Currency'),
        'source_journal_id': fields.many2one('account.journal', 'Journal'),
        'amount_provisi': fields.float('Provision Fee', digits_compute=dp.get_precision('Account')),
        'tag': fields.char('Tag'),
    }
    _defaults = {
        'currency_id': _get_currency,
        'user_id': lambda self, cr, uid, context: uid,
        'date': fields.date.context_today,
    }

    def onchange_date(self, cr, uid, ids, date, context=None):
        if context is None:
            context ={}
        res = {'value': {}}
        ctx = context.copy()
        company_id = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id
        ctx.update({'company_id': company_id, 'account_period_prefer_normal': True})
        pids = self.pool.get('account.period').find(cr, uid, date, context=ctx)
        if pids:
            res['value'].update({'period_id':pids[0]})
        return res

    def onchange_journal_id(self, cr, uid, ids, journal, context=None):
        if context is None:
            context ={}
        res = {'value': {}}
        main_journal = self.pool.get('account.journal').browse(cr, uid, journal, context=context)
        if main_journal.related_journal_id:
            res['value'].update({'source_journal_id': main_journal.related_journal_id.id})
        res['value'].update({'tag': main_journal.code})
        return res

    def setoran_outlet(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        user_obj = self.pool.get('res.users')
        move_obj = self.pool.get('account.move')
        cur_obj = self.pool.get('res.currency')
        setoran_obj = self.pool.get('setoran.outlet')
        invoice_obj = self.pool.get('account.invoice')
        account_obj = self.pool.get('account.account')
        analytic_cbg_obj = self.pool.get('analytic.cabang')

        ctx = context.copy()
        move_line_vals = entry_ids = []
        currency = user_obj.browse(cr, uid, uid).company_id.currency_id
        for wzd in self.browse(cr, uid, ids, context=context):
            if (not wzd.amount) or wzd.amount < 0:
                raise osv.except_osv(_('Invalid Invoice!'), _('Amount tidak boleh NOL atau minus.'))
            for inv in invoice_obj.browse(cr, uid, context.get('active_ids'), context=context):
                ctx.update({'lang': inv.partner_id.lang})
                ref_setoran = "%s" % (inv.number or inv.internal_number or '')
                if inv.state != 'paid':
                    raise osv.except_osv(_('Invalid Invoice!'), _(
                        'Lakukan pembayaran (register payment) terlebih dahulu, sebelum melakukan transaksi setoran outlet.') % (inv.name,))
                if not inv.payment_ids:
                    raise osv.except_osv(_('Invalid Invoice!'), _(
                        'Tidak ada data pembayaran (payment list) dari invoice %s.') % (inv.name,))
                if inv.deposit_outlet:
                    raise osv.except_osv(_('Invalid Invoice!'), _('Setoran dari Outlet sudah sesuai penjualan (komplit).'))

                total_card, total_cash, total_delivery = 0.0, 0.0, 0.0
                query = """ SELECT p.card_amount, p.cash_amount, p.delivery_amount 
                            FROM sales_pos_order p, sales_pos_order_invoice_rel l  
                            WHERE l.invoice_id=%s and p.id = l.pos_id """
                cr.execute(query, (inv.id,))
                result = cr.fetchall()
                if result:
                    for card, cash, delv in result:
                        total_card += card
                        total_cash += cash
                        total_delivery += delv
                if wzd.journal_id.code=='STRCD' or wzd.journal_id.name=='Setoran Card':
                    if (wzd.amount + wzd.amount_provisi) > total_card:
                        raise osv.except_osv(_('Incorrect !!!'),
                            _("The total amount of cash deposit [SETORAN CARD] is not equal POS Data.\n[Result: %s <> %s]") %
                                    ((wzd.amount + wzd.amount_provisi),total_card))
                else:
                    if wzd.journal_id.code=='STRCS' and wzd.amount > total_cash:
                        raise osv.except_osv(_('Incorrect !!!'),
                            _("The total amount of cash deposit [SETORAN CASH] is not equal POS Data.\n[Result: %s <> %s]") %
                                    (wzd.amount,total_cash))
                    elif wzd.journal_id.code=='STRDV' and wzd.amount > total_delivery:
                        raise osv.except_osv(_('Incorrect !!!'),
                            _("The total amount of cash deposit [SETORAN DELIVERY] is not equal POS Data.\n[Result: %s <> %s]") %
                                    (wzd.amount,total_delivery))
                warehouse_id = wzd.warehouse_id.id or False
                if not warehouse_id:
                    if inv.warehouse_id:
                        warehouse_id = inv.warehouse_id.id
                    elif inv.user_id:
                        warehouse_id = user_obj.browse(cr, uid, inv.user_id.id).warehouse_id.id
                    else:
                        warehouse_id = user_obj.browse(cr, uid, uid).warehouse_id.id
                for payment in inv.payment_ids:  # move_line with reconcile_id (payment list)
                    if payment.journal_id.id == wzd.source_journal_id.id:        #wzd.journal_id.id:
                        ctx.update(company_id=inv.company_id.id, account_period_prefer_normal=True)
                        ref_setoran += ", %s, %s" % (payment.move_id.name,payment.reconcile_id.name)
                        ref_mv_line = "%s, %s" % (payment.name or payment.move_id.name, payment.reconcile_id.name)
                        if wzd.reference:
                            ref_mv_line = "%s, %s, %s" % (wzd.reference, payment.name or payment.move_id.name, payment.reconcile_id.name)
                        move_vals = {
                            'ref': ref_mv_line,
                            'line_id': [],
                            'journal_id': wzd.journal_id.id or payment.journal_id.id,
                            'date': wzd.date or payment.date,
                            'narration': wzd.name or inv.comment,
                            'company_id': inv.company_id.id,
                            'warehouse_id': warehouse_id,
                        }
                        for pay_mv_line in move_obj.browse(cr, uid, [payment.move_id.id], context=context):
                            for mv_line in pay_mv_line.line_id:
                                line_vals = {
                                    'journal_id': wzd.journal_id.id or mv_line.journal_id.id or pay_mv_line.journal_id.id,
                                    'period_id': wzd.period_id.id or mv_line.period_id.id or pay_mv_line.period_id.id,
                                    'partner_id': mv_line.partner_id.id or pay_mv_line.partner_id.id,
                                    'quantity': 1,
                                    'credit': 0.0,
                                    'debit': 0.0,
                                    'date': wzd.date or fields.date.context_today(self, cr, uid, context=context),
                                    'warehouse_id': warehouse_id,
                                }
                                if mv_line.journal_id.type in ('cash', 'bank') and \
                                    (not cur_obj.is_zero(cr, uid, currency, mv_line.debit)) and \
                                    (mv_line.account_id.code in ('111111.00.01', '111140.00.02', '121600.00.01', '111111.00.03')
                                        or mv_line.account_id.parent_id.code in ('121600', '111140', '111111')):
                                    if (mv_line.journal_id.name == 'CASH' or mv_line.journal_id.code == 'BNK1'):
                                        bank_pst_cash_acc_id = account_obj.search(cr, uid,
                                            ['|', ('code', 'like', '%111120.01.08%'),
                                                ('name', 'like', '%BCA Virtual Kpusat 7520348888%')], limit=1)[0]
                                        bank_pst_analytic = analytic_cbg_obj.search(cr, uid, [('name', '=', 1),
                                             ('account_id', '=', bank_pst_cash_acc_id)], limit=1)
                                        bank_pst_analytic_id = analytic_cbg_obj.browse(cr, uid, bank_pst_analytic and
                                            bank_pst_analytic[0], context=context).analytic_id.id
                                        if bank_pst_cash_acc_id and bank_pst_analytic_id:
                                            line_vals.update({ #111120.01.01  - BCA Kpusat 093-360-1819 (LCD)
                                                'name': '%s' % ('Bank Kpusat 8888 [' + mv_line.name + ']'),
                                                'account_id': bank_pst_cash_acc_id or False,
                                                'analytic_account_id': bank_pst_analytic_id or False,
                                                'debit': wzd.amount or 0.0,
                                            })
                                    elif (mv_line.journal_id.name == 'CARD' or mv_line.journal_id.code == 'CARD'):
                                        bank_pst_card_acc_id = account_obj.search(cr, uid,
                                            ['|', ('code', 'like', '%111120.01.07%'),
                                                ('name', 'like','%BCA EDC Kpusat 7520257777%')], limit=1)[0]
                                        bank_pst_card_analytic = analytic_cbg_obj.search(cr, uid, [('name', '=', 1),
                                             ('account_id', '=', bank_pst_card_acc_id)], limit=1)
                                        bank_pst_card_analytic_id = analytic_cbg_obj.browse(cr, uid,
                                            bank_pst_card_analytic and bank_pst_card_analytic[0], context=context).analytic_id.id
                                        if bank_pst_card_acc_id and bank_pst_card_analytic_id:
                                            line_vals.update({ #111120.01.03  - BCA Kpusat 093-360-5555 (LCD)
                                                'name': '%s' % ('Bank Kpusat 7777 [' + mv_line.name + ']'),
                                                'account_id': bank_pst_card_acc_id or False,
                                                'analytic_account_id': bank_pst_card_analytic_id or False,
                                                'debit': wzd.amount or 0.0,
                                            })
                                    elif (mv_line.journal_id.name == 'PREPAID' or mv_line.journal_id.code == 'PREP'):
                                        htg_cbg_acc_id = account_obj.search(cr, uid, ['|', ('code', 'like', '%121500.00.01%'),
                                            ('name', 'like', '%Hutang Cabang ke Pusat%')], limit=1)[0]
                                        htg_cbg_analytic = analytic_cbg_obj.search(cr, uid, [('name', '=', 1),
                                            ('account_id', '=', htg_cbg_acc_id)], limit=1)
                                        htg_cbg_analytic_id = analytic_cbg_obj.browse(cr, uid,
                                            htg_cbg_analytic and htg_cbg_analytic[0], context=context).analytic_id.id
                                        if htg_cbg_acc_id and htg_cbg_analytic_id:
                                            line_vals.update({
                                                'name': '%s' % ('Hutang Cabang ke Pusat [' + mv_line.name + ']'),
                                                'account_id': htg_cbg_acc_id or False,
                                                'analytic_account_id': htg_cbg_analytic_id or False,
                                                'debit': wzd.amount or 0.0, #mv_line.debit,
                                            })
                                    elif (mv_line.journal_id.name == 'DELIVERY' or mv_line.journal_id.code == 'DELV'):
                                        bank_delivery_acc_id = account_obj.search(cr, uid,
                                            ['|', ('code', 'like', '%111120.01.10%'),
                                                ('name', 'like', '%BCA Delive 6840509000%')], limit=1)[0]
                                        bank_delv_analytic = analytic_cbg_obj.search(cr, uid, [('name', '=', 1),
                                                ('account_id', '=', bank_delivery_acc_id)], limit=1)
                                        bank_delv_analytic_id = analytic_cbg_obj.browse(cr, uid, bank_delv_analytic and
                                                bank_delv_analytic[0], context=context).analytic_id.id
                                        if bank_delivery_acc_id and bank_delv_analytic_id:
                                            line_vals.update({
                                                'name': '%s' % ('BCA Delive 9000 [' + mv_line.name + ']'),
                                                'account_id': bank_delivery_acc_id or False,
                                                'analytic_account_id': bank_delv_analytic_id or False,
                                                'debit': wzd.amount or 0.0,
                                            })
                                    move_line_vals.append((0, 0, line_vals))
                                elif mv_line.journal_id.type in ('cash', 'bank') and \
                                    (not cur_obj.is_zero(cr, uid, currency, mv_line.credit)) and \
                                        (mv_line.account_id.code[0:12] == '111140.00.01' or
                                            mv_line.account_id.name[0:7] == 'Piutang'):
                                    kbo_acc_id = account_obj.search(cr, uid, ['|', ('code', 'like', '%111111.00.01%'),
                                                ('name', 'like', '%Kas Besar Outlet%')], limit=1)[0]
                                    kbo_analytic_cbg = analytic_cbg_obj.search(cr, uid,
                                                [('name', '=', warehouse_id), ('account_id', '=', kbo_acc_id)], limit=1)
                                    kbo_analytic_id = analytic_cbg_obj.browse(cr, uid,
                                                kbo_analytic_cbg and kbo_analytic_cbg[0], context=context).analytic_id.id
                                    if (mv_line.journal_id.name == 'CASH' or mv_line.journal_id.code == 'BNK1'):
                                        if kbo_acc_id and kbo_analytic_id:
                                            line_vals.update({
                                                'name': '%s' % ('Kas Besar Outlet [' + mv_line.name + ']'),
                                                'account_id': kbo_acc_id,
                                                'analytic_account_id': kbo_analytic_id or False,
                                                'credit': wzd.amount or 0.0,
                                            })
                                    elif (mv_line.journal_id.name == 'CARD' or mv_line.journal_id.code == 'CARD'):
                                        tag_dr_cr_acc_id = account_obj.search(cr, uid, ['|', ('code', 'like', '%111140.00.02%'),
                                                ('name', 'like', '%Tagihan Kartu Kredit dan Debit%')], limit=1)[0]
                                        tag_dr_cr_analytic_cbg = analytic_cbg_obj.search(cr, uid,
                                                [('name', '=', warehouse_id),('account_id', '=', tag_dr_cr_acc_id)], limit=1)
                                        tag_dr_cr_analytic_id = analytic_cbg_obj.browse(cr, uid,
                                            tag_dr_cr_analytic_cbg and tag_dr_cr_analytic_cbg[0], context=context).analytic_id.id
                                        if tag_dr_cr_acc_id and tag_dr_cr_analytic_id:
                                            line_vals.update({
                                                'name': '%s' % ('Tagihan Kartu Kredit dan Debit [' + mv_line.name + ']'),
                                                'account_id': tag_dr_cr_acc_id,
                                                'analytic_account_id': tag_dr_cr_analytic_id or False,
                                                'credit': wzd.amount or 0.0,
                                            })
                                        if (wzd.period_id.id!=28 and wzd.period_id.code!='01/2018') and wzd.amount > 0.0:
                                            line_vals.update({'credit': (wzd.amount_provisi + wzd.amount)})
                                    elif (mv_line.journal_id.name == 'PREPAID' or mv_line.journal_id.code == 'PREP'):
                                        prepaid_acc_id = account_obj.search(cr, uid, ['|', ('code', 'like', '%111211.00.08%'),
                                                ('name', 'like', '%Piutang Antar Kantor%')], limit=1)[0]
                                        prepaid_analytic_cbg = analytic_cbg_obj.search(cr, uid,
                                                [('name', '=', warehouse_id),('account_id', '=', prepaid_acc_id)], limit=1)
                                        prepaid_analytic_id = analytic_cbg_obj.browse(cr, uid,
                                            prepaid_analytic_cbg and prepaid_analytic_cbg[0], context=context).analytic_id.id
                                        if prepaid_acc_id and prepaid_analytic_id:
                                            line_vals.update({
                                                'name': '%s' % ('Penjualan Prepaid [' + mv_line.name + ']'),
                                                'account_id': prepaid_acc_id,
                                                'analytic_account_id': prepaid_analytic_id or False,
                                                'credit': wzd.amount or 0.0, #mv_line.credit,
                                            })
                                    elif (mv_line.journal_id.name == 'DELIVERY' or mv_line.journal_id.code == 'DELV'):
                                        if kbo_acc_id and kbo_analytic_id:
                                            line_vals.update({
                                                'name': '%s' % ('Kas Besar Outlet [' + mv_line.name + ']'),
                                                'account_id': kbo_acc_id or False,
                                                'analytic_account_id': kbo_analytic_id or False,
                                                'credit': wzd.amount or 0.0,
                                            })
                                    move_line_vals.append((0, 0, line_vals))

                        if wzd.source_journal_id.name in ('CARD', 'CASH', 'DELIVERY') and \
                                        wzd.source_journal_id.code in ('BNK1', 'CARD', 'DELV'):
                            htg_cbg_pst_acc_id = account_obj.search(cr, uid, ['|', ('code', 'like', '%121500.00.01%'),
                                    ('name', 'like', '%Hutang Cabang ke Pusat%')], limit=1)[0]
                            htg_cbg_pst_analytic = analytic_cbg_obj.search(cr, uid, [('name', '=', warehouse_id),
                                    ('account_id', '=', htg_cbg_pst_acc_id)], limit=1)
                            htg_cbg_pst_analytic_id = analytic_cbg_obj.browse(cr, uid,
                                    htg_cbg_pst_analytic and htg_cbg_pst_analytic[0], context=context).analytic_id.id
                            if htg_cbg_pst_acc_id and htg_cbg_pst_analytic_id:
                                htg_cbg_vals = {
                                    'journal_id': wzd.journal_id.id,
                                    'period_id': wzd.period_id.id,
                                    'quantity': 1,
                                    'credit': 0.0,
                                    'name': '%s' % ('Hutang Cabang ke Pusat [auto]'),
                                    'account_id': htg_cbg_pst_acc_id or False,
                                    'analytic_account_id': htg_cbg_pst_analytic_id or False,
                                    'debit': wzd.amount or 0.0,
                                    'date': wzd.date or fields.date.context_today(self, cr, uid, context=context),
                                    'warehouse_id': warehouse_id,
                                }
                                move_line_vals.append((0, 0, htg_cbg_vals))
                            pak_account_id = account_obj.search(cr, uid, ['|', ('code', 'like', '%111211.00.08%'),
                                    ('name', 'like', '%Piutang Antar Kantor%')], limit=1)[0]
                            pak_analytic_cbg = analytic_cbg_obj.search(cr, uid, [('name', '=', 1),
                                    ('account_id', '=', pak_account_id)], limit=1)
                            pak_analytic_id = analytic_cbg_obj.browse(cr, uid,
                                    pak_analytic_cbg and pak_analytic_cbg[0], context=context).analytic_id.id
                            if pak_account_id and pak_analytic_id:
                                pak_account_vals = {
                                    'journal_id': wzd.journal_id.id,
                                    'period_id': wzd.period_id.id,
                                    'quantity': 1,
                                    'name': '%s' % ('Piutang Antar Kantor [auto]'),
                                    'account_id': pak_account_id or False,
                                    'analytic_account_id': pak_analytic_id or False,
                                    'credit': wzd.amount or 0.0,  # mv_line.debit,
                                    'debit': 0.0,
                                    'date': wzd.date or fields.date.context_today(self, cr, uid, context=context),
                                    'warehouse_id': warehouse_id,
                                }
                                move_line_vals.append((0, 0, pak_account_vals))
                            if wzd.source_journal_id.name=='CARD' and wzd.source_journal_id.code=='CARD' \
                                    and (wzd.amount_provisi > 0.0) and (wzd.period_id.id!=28 and wzd.period_id.code!='01/2018'):
                                beban_provisi_id = account_obj.search(cr, uid, ['|', ('code', 'like', '%282000.00.05%'),
                                        ('name', 'like', '%Beban Provisi%')], limit=1)[0]
                                beban_prov_analytic = analytic_cbg_obj.search(cr, uid, [('name', '=', warehouse_id),
                                        ('account_id', '=', beban_provisi_id)], limit=1)
                                beban_prov_analytic_id = analytic_cbg_obj.browse(cr, uid,
                                        beban_prov_analytic and beban_prov_analytic[0], context=context).analytic_id.id
                                if beban_provisi_id and beban_prov_analytic_id:
                                    beban_prov_vals = {
                                        'journal_id': wzd.journal_id.id,
                                        'period_id': wzd.period_id.id,
                                        'quantity': 1,
                                        'credit': 0.0,
                                        'name': '%s' % ('Beban Provisi [auto]'),
                                        'account_id': beban_provisi_id or False,
                                        'analytic_account_id': beban_prov_analytic_id or False,
                                        'debit': wzd.amount_provisi or 0.0, #(tag_cc_amount - wzd.amount)
                                        'date': wzd.date or fields.date.context_today(self, cr, uid, context=context),
                                        'warehouse_id': warehouse_id,
                                    }
                                    move_line_vals.append((0, 0, beban_prov_vals))

                        if move_line_vals:
                            move_vals['line_id'] = move_line_vals
                        move_id = move_obj.create(cr, uid, move_vals, context=ctx)
                        if move_id:
                            entry_ids.append(move_id)
                            setoran_id = setoran_obj.create(cr, uid, {
                                'reference': wzd.reference or _(''),
                                'note': ref_setoran,
                                'journal_id': wzd.journal_id.id or payment.journal_id.id,
                                'date': wzd.date or fields.date.context_today(self, cr, uid, context=context),
                                'period_id': wzd.period_id.id or payment.period_id.id,
                                'warehouse_id': warehouse_id or False,
                                'amount': wzd.amount,
                                'user_id': wzd.user_id.id or uid,
                                'currency_id': inv.currency_id.id or currency.id or 13,
                                'move_id': move_id or False,
                                'invoice_id': inv.id or False,
                                'src_move_line_id': payment.id or False,
                                'amount_provisi': wzd.amount_provisi or 0.0,
                            }, context=context)
                            cr.execute('INSERT INTO account_invoice_setoran_outlet_rel \
                                    (invoice_id,setoran_id) values (%s,%s)', (inv.id, setoran_id))
                            if setoran_id:
                                setoran_obj.action_validate(cr, uid, [setoran_id], context=context)

        if context.get('active_ids', False):
            for inv in invoice_obj.browse(cr, uid, context.get('active_ids'), context=context):
                invoice_obj.button_calculate_balance(cr, uid, ids, context)
                inv.refresh()

        if not entry_ids:
            raise osv.except_osv(_('Failed!'), _('Journal entry failed to create.'))
        return {'type': 'ir.actions.act_window_close'}


