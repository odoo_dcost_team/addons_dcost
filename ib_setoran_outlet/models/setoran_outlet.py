#-*- coding: utf-8 -*-
##########   IBRAHIM BILADIN | ibradiiin@gmail.com | 083871909782   ##########
import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)


class setoran_outlet_pusat(osv.osv):
    _name = 'setoran.outlet'
    _description = 'Setoran Outlet ke Pusat'
    _inherit = ['mail.thread']
    _order = "date desc, id desc"

    _columns = {
        'reference': fields.char('Deposit Ref#', help="Transaction reference number.", copy=False,
                                 readonly=True, states={'draft':[('readonly',False)]}),
        'name': fields.char('Number', copy=False, readonly=True, states={'draft':[('readonly',False)]}),
        'note': fields.text('Other Reference', readonly=True, states={'draft':[('readonly',False)]}),
        'journal_id': fields.many2one('account.journal', 'Deposit Method', required=True, track_visibility='onchange',
                readonly=True, states={'draft': [('readonly', False)]}),
        'date': fields.date('Date', select=True, help="Tanggal outlet setor uang penjualan ke Pusat.",
                track_visibility='onchange', copy=False, readonly=True, states={'draft':[('readonly',False)]}),
        'period_id': fields.many2one('account.period', 'Period', required=True, track_visibility='onchange',
                readonly=True, states={'draft': [('readonly', False)]}),
        'warehouse_id': fields.many2one('stock.warehouse', 'Outlet', change_default=1, track_visibility='onchange',
                readonly=True, states={'draft': [('readonly', False)]}),
        'amount': fields.float('Deposit Amount', digits_compute=dp.get_precision('Account'),
                required=True, track_visibility='onchange', readonly=True, states={'draft':[('readonly',False)]}),
        'user_id': fields.many2one('res.users', 'User'),
        'currency_id': fields.many2one('res.currency', 'Currency'),
        'move_id': fields.many2one('account.move', 'Account Entry', copy=False, readonly=True,
                states={'draft':[('readonly',False)]}),
        'move_ids': fields.related('move_id', 'line_id', type='one2many', relation='account.move.line',
                string='Journal Items', readonly=True),
        'invoice_id': fields.many2one('account.invoice', 'Invoice', readonly=True, states={'draft':[('readonly',False)]}),
        'src_move_line_id': fields.many2one('account.move.line', 'Source Move Line'),
        'state': fields.selection([('draft', 'Draft'), ('validated', 'Validated'), ('cancel', 'Cancelled')],
                'Status', readonly=True, copy=False, track_visibility='onchange'),
        'amount_provisi': fields.float('Provision Fee', digits_compute=dp.get_precision('Account'),
                track_visibility='onchange', readonly=True, states={'draft':[('readonly',False)]}),
    }

    _defaults = {
        'name': '/',
        'user_id': lambda self, cr, uid, context: uid,
        'date': fields.date.context_today,
        'state': 'draft'
    }

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        if vals.get('name', '/') == '/':
            vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'seq_setoran_outlet', context=context) or '/'
        return super(setoran_outlet_pusat, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        acc_move_obj = self.pool.get('account.move')
        acc_inv_obj = self.pool.get('account.invoice')
        curr_obj = self.pool.get('res.currency')
        currency = self.pool.get('res.users').browse(cr, uid, uid).company_id.currency_id
        for setoran in self.browse(cr, uid, ids, context=context):
            if setoran.invoice_id and (vals.get('amount', False) or vals.get('amount_provisi', False)):
                tag_error = _("")
                total_card = 0.0
                for inv in acc_inv_obj.browse(cr, uid, [setoran.invoice_id.id], context=context):
                    for payment in inv.payment_ids:
                        for pay_mv_line in acc_move_obj.browse(cr, uid, [payment.move_id.id], context=context):
                            for mv_line in pay_mv_line.line_id:
                                if (setoran.journal_id.related_journal_id.id==mv_line.journal_id.id) and \
                                        (not curr_obj.is_zero(cr, uid, currency, mv_line.credit)):
                                    total_card += mv_line.credit
                                    tag_error = _(mv_line.journal_id.name)

                if ('amount' in vals) and ('amount_provisi' not in vals):      #vals['amount'] > 0.0:
                    if (vals['amount'] + setoran.amount_provisi) > total_card: #Cash Deposit
                        raise osv.except_osv(_('Incorrect!'),
                                _("The total amount of cash deposit [type:%s] is not equal POS Data.\n[Result: %s <> %.2f]") %
                                             (tag_error,(vals['amount'] + setoran.amount_provisi),total_card))
                elif ('amount_provisi' in vals) and ('amount' not in vals):    #vals['amount_provisi'] > 0.0:
                    if (vals['amount_provisi'] + setoran.amount) > total_card:
                        raise osv.except_osv(_('Incorrect!'),
                                _("The total amount of cash deposit [type:%s] is not equal POS Data.\n[Result: %s <> %.2f]") % (
                                             (tag_error,vals['amount_provisi'] + setoran.amount), total_card))
                else:
                    if ('amount' in vals) and ('amount_provisi' in vals) and \
                            (vals['amount'] + vals['amount_provisi']) > total_card:
                        raise osv.except_osv(_('Incorrect!'),
                                _("The total amount of cash deposit [type:%s] is not equal POS Data.\n[Result: %s <> %.2f]") % (
                                             (tag_error,vals['amount_provisi'] + vals['amount']), total_card))
        return super(setoran_outlet_pusat, self).write(cr, uid, ids, vals, context=context)

    def onchange_date(self, cr, uid, ids, date, context=None):
        if context is None:
            context ={}
        res = {'value': {}}
        ctx = context.copy()
        company_id = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id
        ctx.update({'company_id': company_id, 'account_period_prefer_normal': True})
        pids = self.pool.get('account.period').find(cr, uid, date, context=ctx)
        if pids:
            res['value'].update({'period_id':pids[0]})
        return res

    def unlink(self, cr, uid, ids, context=None):
        for setoran in self.browse(cr, uid, ids, context=context):
            if setoran.move_id:
                self.pool.get('account.move').unlink(cr, uid, [setoran.move_id.id], context=context)
        return super(setoran_outlet_pusat, self).unlink(cr, uid, ids, context=context)

    def action_validate(self, cr, uid, ids, context=None):
        cur_obj = self.pool.get('res.currency')
        move_pool = self.pool.get('account.move')
        aml_obj = self.pool.get('account.move.line')
        for setoran in self.browse(cr, uid, ids, context=context):
            if setoran.move_id:
                for line in setoran.move_id.line_id:
                    if setoran.journal_id.code in ('STRCS','STRDV') or setoran.journal_id.name in (
                            'Setoran Cash','Setoran Delivery'):
                        if (not cur_obj.is_zero(cr, uid, setoran.currency_id, line.debit)):
                            aml_obj.write(cr, uid, [line.id], {'debit': setoran.amount}, context=context)
                        elif (not cur_obj.is_zero(cr, uid, setoran.currency_id, line.credit)):
                            aml_obj.write(cr, uid, [line.id], {'credit': setoran.amount}, context=context)
                    else:
                        if line.account_id.code=='282000.00.05':
                            aml_obj.write(cr, uid, [line.id], {'debit': setoran.amount_provisi}, context=context)
                        elif line.account_id.code=='111140.00.02':
                            aml_obj.write(cr, uid, [line.id], {
                                'credit': (setoran.amount_provisi + setoran.amount)}, context=context)
                        else:
                            if (not cur_obj.is_zero(cr, uid, setoran.currency_id, line.debit)):
                                aml_obj.write(cr, uid, [line.id], {'debit': setoran.amount}, context=context)
                            elif (not cur_obj.is_zero(cr, uid, setoran.currency_id, line.credit)):
                                aml_obj.write(cr, uid, [line.id], {'credit': setoran.amount}, context=context)
                move_pool.post(cr, uid, [setoran.move_id.id], context=context)
        return self.write(cr, uid, ids, {'state': 'validated'}, context=context)

    def action_cancel_setoran(self, cr, uid, ids, context=None):
        for setoran in self.browse(cr, uid, ids, context=context):
            if setoran.move_id:
                self.pool.get('account.move').button_cancel(cr, uid, [setoran.move_id.id], context=context)
        return self.write(cr, uid, ids, {'state': 'cancel'}, context=context)

    def action_set_to_draft(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'draft'}, context=context)


