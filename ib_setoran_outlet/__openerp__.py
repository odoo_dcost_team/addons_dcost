## -*- coding: utf-8 -*-
## © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
{
    "name": "Daily Cash Deposit",
    "version": "1.0",
    "category": "Custom Module",
    "website": "http://ibrohimbinladin.wordpress.com",
    "author": "Ibrohim Binladin | +62838-7190-9782 | ibradiiin@gmail.com",
    "description": """
Setoran Tunai Harian dari Outlet [Store] ke Kantor Pusat D'Cost.
================================================================

Setoran Tunai ini dilakukan berdasarkan Jumlah Setoran dari PIC Outlet ke Departemen Finance
(Pusat) sesuai dengan Data Penjualan Harian yang ada di Sistem POS.

""",
    # "description": """ Setoran Penjualan Harian dari Outlet/Store ke Kantor Pusat DCost """,
    "init_xml": [],
    "application": True,
    "installable": True,
    "depends": [
        "account",
        "ib_sale_dcost",
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/sequence.xml",
        "wizard/wizard_view.xml",
        "views/view.xml",
    ],
    #"views/sequence_view.xml",
    "demo_xml": [],
    "active": False,
    'auto_install': False,
}
