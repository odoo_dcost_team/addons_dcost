import ast
import time
from openerp import netsvc
import urllib2
from itertools import groupby
from datetime import date, datetime, timedelta
import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv, orm
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools import float_compare
from openerp.tools.translate import _
from openerp import tools, SUPERUSER_ID



class StockPicking(osv.osv):
    _inherit = 'stock.picking'
    _columns = {
        'requirement_id': fields.many2one('material.requirement', 'Requirement Reference', ondelete='cascade'),
    }


class account_move(osv.osv):
    _inherit = 'account.move'
    #_inherit = ["mail.thread", "ir.needaction_mixin"]
    
    _columns = {
        'hutang_id': fields.many2one('hutang.senlog', 'Hutang Senlog', ondelete='cascade'),
        'piutang_id': fields.many2one('piutang.senlog', 'Piutang Senlog', ondelete='cascade'),
    }


class ResPartner(osv.osv):
    _inherit = 'res.partner'
    _columns = {
        'ongkir': fields.float('Ongkos Kirim'),
    }

class WorkOrder(osv.osv):
    _inherit = 'work.order'
    _columns = {
        'requirement_id': fields.many2one('material.requirement', 'Requirement Reference', ondelete='cascade'),
    }
    

class PurchaseRequisition(osv.osv):
    _inherit = 'purchase.requisition'
    _columns = {
        'requirement_id': fields.many2one('material.requirement', 'Requirement Reference', ondelete='cascade'),
    }
    

    def _prepare_purchase_order(self, cr, uid, requisition, supplier, context=None):
        supplier_pricelist = supplier.property_product_pricelist_purchase and supplier.property_product_pricelist_purchase.id or False
        picking_type_id = self.pool.get("purchase.order")._get_picking_in(cr, uid, context=context)
        picking_type_in = self.pool.get('res.users').browse(cr, uid, uid).warehouse_id.in_type_id.id or picking_type_id
        picktype = self.pool.get("stock.picking.type").browse(cr, uid, picking_type_in, context=context)
        
        return {
            'origin': requisition.name,
            'date_order': requisition.date_end or fields.datetime.now(),
            'partner_id': supplier.id,
            'pricelist_id': supplier_pricelist,
            'location_id': picktype.default_location_dest_id.id,
            'company_id': requisition.company_id.id,
            'fiscal_position': supplier.property_account_position and supplier.property_account_position.id or False,
            'requisition_id': requisition.id,
            'notes': requisition.description,
            'picking_type_id': picking_type_in,
        }


class MaterialRequirement(osv.osv):
    _name = 'material.requirement'
    _columns = {
        'name': fields.char('Reference', size=64, required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'type': fields.selection([('log', 'Senlog'), ('kit', 'Senkit')], 'Type', required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'plan_id': fields.many2one('production.plan', 'MPS', readonly=True, required=True, select=True, domain="[('state', '=', 'done'), ('mrp', '!=', True), ('type', '=', type)]", states={'draft': [('readonly', False)]}),
        'user_id': fields.many2one('res.users', 'Responsible', states={'draft': [('readonly', False)]}, select=True, track_visibility='onchange'),
        'pricelist_id': fields.many2one('product.pricelist', 'Pricelist', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'date': fields.date('Date', required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'notes': fields.text('Notes'),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('cancel', 'Cancel'),
            ('approve', 'Approve'),
            ('done', 'Done'),
            ], 'Plan State', readonly=True, select=True),
        'requirement_line': fields.one2many('material.requirement.line', 'requirement_id', 'Product Plan'),
        'material_line': fields.one2many('material.plan.line', 'requirement_id', 'Material Plan', readonly=True),
        'production_line': fields.one2many('work.order', 'requirement_id', 'Work Order', readonly=True),
        'requisition_line': fields.one2many('purchase.requisition', 'requirement_id', 'Purchase Requisition', readonly=True),
        'invoice_line': fields.one2many('piutang.senlog', 'requirement_id', 'Piutang Senlog Senkit', readonly=True),
        'picking_line': fields.one2many('stock.picking', 'requirement_id', 'Internal Transfer', readonly=True),
    }

    _defaults = {
                 'name': lambda self, cr, uid, context={}: self.pool.get('ir.sequence').get(cr, uid, 'material.requirement'),
                 'state' : 'draft',
                 'type': 'log',
                 'user_id': lambda obj, cr, uid, context: uid,
                 'date': lambda *a: time.strftime('%Y-%m-%d'),
    }
    
    _order = 'name desc'
    
    def user_id_change(self, cr, uid, ids, idp):
        res = {}                       
        res['user_id'] = uid        
        return {'value': res}

    def unlink(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids, context={})[0]
        if val.state != 'draft':
            raise osv.except_osv(('Perhatian !'), ('MRP tidak bisa dihapus pada state \'%s\'!') % (val.state,))
        return super(MaterialRequirement, self).unlink(cr, uid, ids, context=context)

    def material_draft(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'draft'})
        for x in self.browse(cr, uid, ids)[0].requirement_line:
            self.pool.get('material.requirement.line').write(cr, uid, [x.id], {'state': 'draft'})    
        return True                               
    
    def material_cancel(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'cancel'})
        for x in self.browse(cr, uid, ids)[0].requirement_line:
            self.pool.get('material.requirement.line').write(cr, uid, [x.id], {'state': 'cancel'})    
        return True                                  
         
    def material_confirm(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'approve'})
        for x in self.browse(cr, uid, ids)[0].requirement_line:
            self.pool.get('material.requirement.line').write(cr, uid, [x.id], {'state': 'approve'})    
        return True

    def compute(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        bom_obj = self.pool.get('mrp.bom')
        plan_obj = self.pool.get('material.plan.line')
        
        if not val.material_line:
            raw = []
            for x in val.requirement_line:
                results = self.get_mrp(cr, uid, ids, x.product_uom.id, x.name.id, x.plan, x.name.partner_ref) 
                raw += results

                for r in results:
                    hasil = self.get_mrp(cr, uid, ids, r['product_uom'], r['product_id'], r['product_qty'], r['name'])
                    raw += hasil
                
            data = {}
            for p in raw:
                data[p['product_id']] = {'product_qty': [], 'product_uom': p['product_uom']} 
            for p in raw:
                data[p['product_id']]['product_qty'].append(p['product_qty'])
             
            for i in data:  
                sup = 'buy'
                cek = bom_obj._bom_find(cr, uid, data[i]['product_uom'], product_id=i, properties=[], context=context)
                if cek:
                    sup = 'mo'
                plan_obj.create(cr, uid, {
                                          'requirement_id': val.id,
                                          'name': i,
                                          'bom_id': cek,
                                          'plan': sum(data[i]['product_qty']),
                                          'product_uom': data[i]['product_uom'],
                                          'supply': sup
            })

        return True
               
    def get_mrp(self, cr, uid, ids, puo, pid, qty, nama):
        bom_obj = self.pool.get('mrp.bom')
        uom_obj = self.pool.get('product.uom')
        product_obj = self.pool.get('product.product')
        
        results = [[], []]
        bid = bom_obj._bom_find(cr, uid, puo, product_id=pid, properties=[], context={})        
        if bid:
            bom_point = bom_obj.browse(cr, uid, bid)
            produk = product_obj.browse(cr, uid, pid)
            
            factor = uom_obj._compute_qty(cr, uid, puo, qty, bom_point.product_uom.id)
            results = bom_obj._bom_explode(cr, uid, bom_point, produk, factor/bom_point.product_qty, properties=[], routing_id=False, context={})
            
        return results[0] 
        
    def material_validate(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        
        obj_work = self.pool.get('work.order')
        obj_dds = self.pool.get('production.plan')
        obj_picking = self.pool.get('stock.picking')
        obj_move = self.pool.get('stock.move')
        obj_piutang = self.pool.get('piutang.senlog')
        obj_piutang_line = self.pool.get('piutang.senlog.line')
        obj_hutang = self.pool.get('hutang.senlog')
        obj_hutang_line = self.pool.get('hutang.senlog.line')
        obj_requisition = self.pool.get('purchase.requisition')
        obj_requisition_line = self.pool.get('purchase.requisition.line')
        
        req = []; harga = {}
        
        for l in val.requirement_line:
            harga[l.name.id] = l.price
            self.pool.get('material.requirement.line').write(cr, uid, [l.id], {'state': 'done'})    
        
        for s in val.material_line:
            if s.supply == 'mo':
                line = [(0,0, u) for u in obj_work.bom_change(cr, uid, ids, s.name.id, s.plan, s.bom_id.id)['value']['material_lines']] 
                obj_work.create(cr, uid, {
                                        'requirement_id': val.id,
                                        'date': val.date,
                                        'product_id': s.name.id,
                                        'product_qty': s.plan,
                                        'product_uom': s.product_uom.id,
                                        'bom_id': s.bom_id.id,
                                        'warehouse_id': val.user_id.warehouse_id.id,
                                        'material_lines': line 
                })
            else:
                req.append(s)
        
        for n in val.requirement_line:
            cek = self.pool.get('mrp.bom')._bom_find(cr, uid, n.product_uom.id, product_id=n.name.id, properties=[], context=context)
            if cek:
                line = [(0,0, p) for p in obj_work.bom_change(cr, uid, ids, n.name.id, n.plan, cek)['value']['material_lines']]
                obj_work.create(cr, uid, {
                                        'requirement_id': val.id,
                                        'date': val.date,
                                        'product_id': n.name.id,
                                        'product_qty': n.plan,
                                        'product_uom': n.product_uom.id,
                                        'bom_id': cek,
                                        'warehouse_id': val.user_id.warehouse_id.id,
                                        'material_lines': line
                })
            else:
                req.append(n)
        
        if req:
            rid = obj_requisition.create(cr, uid, {
                                                'requirement_id': val.id,
                                                'exclusive': 'multiple',
                                                'multiple_rfq_per_supplier': True,
                                                'origin': val.name
            })
                                
            for e in req:
                obj_requisition_line.create(cr, uid, {
                                                    'product_id': e.name.id,
                                                    'product_qty': e.plan,
                                                    'product_uom_id': e.product_uom.id,
                                                    'requisition_id': rid
                })
            

        for x in val.plan_id.stock_ids:
            total = 0
            goods_id = obj_picking.create(cr,uid, {
                                                'origin': x.name,
                                                'requirement_id': val.id,
                                                'partner_id': x.user_id.warehouse_id.partner_id.id,
                                                'type_wo': 'fg',
                                                'picking_type_id': val.user_id.warehouse_id.int_type_id.id,
                                                'move_type': 'one',
                                                'date': val.date,
                                                'company_id': 1})
            
            invoice_id = obj_piutang.create(cr,uid, {
                                                'origin': x.name,
                                                'date': val.date,
                                                'journal_id': 1,
                                                'payment_id': 214,
                                                'account_id': 308,
                                                'requirement_id': val.id,
                                                'user_id': val.user_id.id,
                                                'partner_id': x.user_id.warehouse_id.partner_id.id,
                                                'tipe': val.type,
                                                'company_id': x.user_id.company_id.id})

            tagihan_id = obj_hutang.create(cr,uid, {
                                                'origin': x.name,
                                                'date': val.date,
                                                'journal_id': 2,
                                                'payment_id': 214,
                                                'account_id': 329,
                                                'user_id': val.user_id.id,
                                                'partner_id': x.user_id.warehouse_id.partner_id.id,
                                                'tipe': val.type,
                                                'company_id': x.user_id.company_id.id})

            for i in x.stock_line:
                if harga.has_key(i.product_id.id):
                    obj_move.create(cr,uid, {
                                            'name': i.product_id.partner_ref,
                                            'date': val.date,
                                            'product_id': i.product_id.id,
                                            'product_uom': i.product_uom.id,
                                            'product_uom_qty': i.product_qty,
                                            'location_id': val.user_id.warehouse_id.lot_stock_id.id,
                                            'location_dest_id': 10,
                                            'picking_id': goods_id,
                                            'origin': val.name,
                                            'company_id': x.user_id.company_id.id})
                
                    total += i.product_qty*harga[i.product_id.id]
                    obj_piutang_line.create(cr,uid, {
                                            'name': i.product_id.partner_ref,
                                            'piutang_id': invoice_id,
                                            'account_id': 585,
                                            'product_id': i.product_id.id,
                                            'product_uom': i.product_uom.id,
                                            'product_qty': i.product_qty,
                                            'price_unit': harga[i.product_id.id],
                                            })          
                 
                    obj_hutang_line.create(cr,uid, {
                                            'name': i.product_id.partner_ref,
                                            'hutang_id': tagihan_id,
                                            'account_id': 308,
                                            'product_id': i.product_id.id,
                                            'product_uom': i.product_uom.id,
                                            'product_qty': i.product_qty,
                                            'price_unit': harga[i.product_id.id],
                                            })          
                 
            ongkir = x.user_id.warehouse_id.partner_id.ongkir
            if ongkir:
                total += ongkir
                obj_piutang_line.create(cr,uid, {
                                        'name': 'Ongkos Kirim',
                                        'account_id': 329,
                                        'product_qty': 1,
                                        'price_unit': ongkir,
                                        'piutang_id': invoice_id})
             
             
                obj_hutang_line.create(cr,uid, {
                                        'name': 'Ongkos Kirim',
                                        'account_id': 308,
                                        'product_qty': 1,
                                        'price_unit': ongkir,
                                        'hutang_id': tagihan_id})
             
            obj_piutang.write(cr, uid, [invoice_id], {'total': total})
            obj_picking.action_confirm(cr, uid, [goods_id], context=context)
            obj_picking.force_assign(cr, uid, [goods_id], context=context) 
                
        obj_dds.write(cr, uid, [val.plan_id.id], {'mrp': True})
        self.write(cr, uid, ids, {'state': 'done'})
        return True 

    def plan_change(self, cr, uid, ids, plan, pricelist): 
        data = []; harga = {}
        val = self.pool.get('production.plan').browse(cr, uid, plan)
        
        if pricelist:
            for i in self.pool.get('product.pricelist').browse(cr, uid, pricelist).version_id[0].items_id:
                harga[i.product_id.id] = i.price_surcharge
         
        for x in val.plan_line:
            data.append({
                'name': x.product_id.id,
                'product_uom': x.product_uom.id,
                'plan': x.plan,
                'price': harga.get(x.product_id.id, 0.0) 
            })
                 
        return {'value': {'requirement_line': data}}


class MaterialRequirementLine(osv.osv):
    _name = 'material.requirement.line' 
    _columns = {
        'requirement_id': fields.many2one('material.requirement', 'Requirement Reference', required=True, ondelete='cascade', select=True),
        'name': fields.many2one('product.product', 'Product', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'price': fields.float('Unit Price', states={'done': [('readonly', True)]}),
        'plan': fields.float('Requirement', required=True, readonly=True, digits_compute=dp.get_precision('Product UoM'), states={'draft': [('readonly', False)]}),
        'product_uom': fields.many2one('product.uom', 'UoM', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'state': fields.selection([('draft', 'Draft'), ('cancel', 'Cancel'), ('approve', 'Approve'), ('done', 'Done')], 'State', readonly=True, select=True),
    }
    
    _defaults = {'state' : 'draft'}

    
    def product_id_change(self, cr, uid, ids, pid):
        res = {}
        product = self.pool.get('product.product').browse(cr, uid, pid)
        if pid:
            res['name'] = product.id
            res['product_uom'] = product.uom_id.id
        return {'value': res}



class MaterialPlanLine(osv.osv):
    _name = 'material.plan.line'
    _columns = {
        'requirement_id': fields.many2one('material.requirement', 'Requirement Reference', required=True, ondelete='cascade', select=True),
        'name': fields.many2one('product.product', 'Product'),
        'plan': fields.float('Requirement', digits_compute=dp.get_precision('Product UoM')),
        'product_uom': fields.many2one('product.uom', 'UoM'),
        'bom_id': fields.many2one('mrp.bom', 'BoM'),
        'supply': fields.selection([('buy', 'Buy'), ('mo', 'Manufacture')], 'Supply Method'),
    }

class PiutangSenlog(osv.osv):
    _name = 'piutang.senlog'
    
    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {'total': 0.0}
            val = 0.0
            for line in order.piutang_line:
                val += line.price_subtotal
            res[order.id]['total'] = val
        return res
    
    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('piutang.senlog.line').browse(cr, uid, ids, context=context):
            result[line.piutang_id.id] = True
        return result.keys()

    _columns = {
        'name': fields.char('Reference', size=64, required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'origin': fields.char('Origin', size=64, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'date': fields.date('Date', required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'journal_id': fields.many2one('account.journal', 'Journal', domain="[('type', '=', 'sale'), ('company_id', '=', company_id)]", required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'payment_id': fields.many2one('account.journal', 'Payment', domain="[('type', 'in', ('cash', 'bank')), ('company_id', '=', company_id)]", required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'account_id': fields.many2one('account.account', 'Account', domain="[('type', '!=', 'view'), ('company_id', '=', company_id)]", required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'user_id': fields.many2one('res.users', 'User', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'stock_id': fields.many2one('stock.order', 'Stock Order', readonly=True, states={'draft': [('readonly', False)]}),
        'partner_id': fields.many2one('res.partner', 'Customer', required=True, readonly=True, domain=[('customer', '=', True)], states={'draft': [('readonly', False)]}),
        'requirement_id': fields.many2one('material.requirement', 'Material Requirement', readonly=True, states={'draft': [('readonly', False)]}),
        'tipe': fields.selection([('log', 'Senlog'), ('kit', 'Senkit')], 'Sentral', readonly=True),
        'company_id': fields.many2one('res.company', 'Company', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'piutang_line': fields.one2many('piutang.senlog.line', 'piutang_id', 'Piutang Senlog', readonly=True, states={'draft': [('readonly', False)]}),
        'journal_line': fields.one2many('account.move', 'piutang_id', 'Piutang Senlog'),
        'total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Total',
            store={
                'piutang.senlog': (lambda self, cr, uid, ids, c={}: ids, ['piutang_line'], 10),
                'piutang.senlog.line': (_get_order, ['price_unit', 'product_qty'], 10),
            }, multi='sums', help="The total amount."),

        'state': fields.selection([
            ('draft', 'Draft'),
            ('cancel', 'Cancel'),
            ('approve', 'Approve'),
            ('done', 'Done'),
            ], 'State', readonly=True, select=True),
    }

    _defaults = {
                 'name': lambda self, cr, uid, context={}: self.pool.get('ir.sequence').get(cr, uid, 'piutang.senlog'),
                 'state' : 'draft',
                 'tipe': 'log',
                 'user_id': lambda obj, cr, uid, context: uid,
                 'date': lambda *a: time.strftime('%Y-%m-%d'),
    }

    def user_id_change(self, cr, uid, ids, idp):
        res = {}
        cid = self.pool.get('res.users').browse(cr, uid, uid).company_id.id
        jid = self.pool.get('account.journal').search(cr, uid, [('type', '=', 'sale'), ('company_id', '=', cid)])
        if jid:
            res['journal_id'] = jid[0]
        res['user_id'] = uid
        res['company_id'] = cid
        return {'value': res}

    def unlink(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids, context={})[0]
        if val.state != 'draft':
            raise osv.except_osv(('Perhatian !'), ('Piutang tidak bisa dihapus pada state \'%s\'!') % (val.state,))
        return super(PiutangSenlog, self).unlink(cr, uid, ids, context=context)

    def piutang_draft(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'draft'})
        return True                               
    
    def piutang_cancel(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'cancel'})
        return True                                  
         
    def piutang_confirm(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'approve'})
        return True
  
    def piutang_validate(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        obj_move = self.pool.get('account.move')
        obj_move_line = self.pool.get('account.move.line')
    
        mid = obj_move.create(cr,uid, {'piutang_id': val.id, 'ref' : val.name, 'journal_id': val.journal_id.id, 'company_id': val.company_id.id})

        obj_move_line.create(cr,uid, {
                                'name': val.name,
                                'partner_id': val.partner_id.id,
                                'account_id': val.account_id.id,
                                'date_maturity': val.date,
                                'move_id': mid,
                                'debit': val.total,
                                'credit': 0})

#         mad = obj_move.create(cr,uid, {'piutang_id': val.id, 'ref' : val.name, 'journal_id': val.payment_id.id, 'company_id': val.company_id.id})
#         obj_move_line.create(cr,uid, {
#                                 'name': val.name,
#                                 'partner_id': val.partner_id.id,
#                                 'account_id': val.payment_id.default_debit_account_id.id,
#                                 'date_maturity': val.date,
#                                 'move_id': mad,
#                                 'debit': val.total,
#                                 'credit': 0})
# 
#         obj_move_line.create(cr,uid, {
#                                 'name': val.name,
#                                 'partner_id': val.partner_id.id,
#                                 'account_id': val.account_id.id,
#                                 'date_maturity': val.date,
#                                 'move_id': mad,
#                                 'debit': 0,
#                                 'credit': val.total})
        
        for x in val.piutang_line :    
            obj_move_line.create(cr,uid, {
                                    'name': x.name,
                                    'partner_id': val.partner_id.id,
                                    'account_id': x.account_id.id,
                                    'analytic_account_id': x.account_analytic_id.id,
                                    'date_maturity': val.date,
                                    'move_id': mid,
                                    'debit': 0,
                                    'credit': x.price_subtotal})
        

        self.write(cr, uid, ids, {'state': 'done'})
        return True 

class PiutangSenlogLine(osv.osv):
    _name = 'piutang.senlog.line'
    
    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            price = line.price_unit * line.product_qty
            res[line.id] = price
        return res

    _columns = {
        'piutang_id': fields.many2one('piutang.senlog', 'Piutang Senlog', required=True, ondelete='cascade', select=True),
        'product_id': fields.many2one('product.product', 'Product'),
        'name': fields.char('Description', size=64, required=True),
        'product_qty': fields.float('Quantity', digits_compute=dp.get_precision('Product Unit of Measure')),
        'price_unit': fields.float('Price Unit', digits_compute=dp.get_precision('Product Price')),
        'price_subtotal': fields.function(_amount_line, string='Subtotal', digits_compute= dp.get_precision('Account')),
        'product_uom': fields.many2one('product.uom', 'UoM'),
        'partner_id': fields.related('piutang_id', 'partner_id', type='many2one', relation="res.partner", string="Customer", store=True, readonly=True),
        'date': fields.related('piutang_id', 'date', type='date', string="Date", store=True, readonly=True),
        'origin': fields.related('piutang_id', 'origin', type='char', string="Origin", store=True, readonly=True),
        'account_id': fields.many2one('account.account', 'Account', required=True),
        'account_analytic_id': fields.many2one('account.analytic.account', 'Analytic Account', domain=[('type', '!=', 'view')]),
        
    }
    
    def product_id_change(self, cr, uid, ids, product):
        res = {}
        product_obj = self.pool.get('product.product')
        if product:
            res['name'] = product_obj.name_get(cr, uid, [product])[0][1]
            res['product_uom'] = product_obj.browse(cr, uid, product).uom_id.id
        return {'value': res}



class HutangSenlog(osv.osv):
    _name = 'hutang.senlog'
    
    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {'total': 0.0}
            val = 0.0
            for line in order.hutang_line:
                val += line.price_subtotal
            res[order.id]['total'] = val
        return res
    
    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('hutang.senlog.line').browse(cr, uid, ids, context=context):
            result[line.hutang_id.id] = True
        return result.keys()

    _columns = {
        'name': fields.char('Reference', size=64, required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'origin': fields.char('Origin', size=64, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'date': fields.date('Date', required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'journal_id': fields.many2one('account.journal', 'Journal', domain="[('type', '=', 'sale'), ('company_id', '=', company_id)]", required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'payment_id': fields.many2one('account.journal', 'Payment', domain="[('type', 'in', ('cash', 'bank')), ('company_id', '=', company_id)]", required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'account_id': fields.many2one('account.account', 'Account', domain="[('type', '!=', 'view'), ('company_id', '=', company_id)]", required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'user_id': fields.many2one('res.users', 'User', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'stock_id': fields.many2one('stock.order', 'Stock Order', readonly=True, states={'draft': [('readonly', False)]}),
        'partner_id': fields.many2one('res.partner', 'Customer', required=True, readonly=True, domain=[('customer', '=', True)], states={'draft': [('readonly', False)]}),
        'requirement_id': fields.many2one('material.requirement', 'Material Requirement', readonly=True, states={'draft': [('readonly', False)]}),
        'tipe': fields.selection([('log', 'Senlog'), ('kit', 'Senkit')], 'Sentral', readonly=True),
        'company_id': fields.many2one('res.company', 'Company', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'hutang_line': fields.one2many('hutang.senlog.line', 'hutang_id', 'Hutang Senlog', readonly=True, states={'draft': [('readonly', False)]}),
        'journal_line': fields.one2many('account.move', 'hutang_id', 'Hutang Senlog'),
        'total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Total',
            store={
                'hutang.senlog': (lambda self, cr, uid, ids, c={}: ids, ['hutang_line'], 10),
                'hutang.senlog.line': (_get_order, ['price_unit', 'product_qty'], 10),
            }, multi='sums', help="The total amount."),

        'state': fields.selection([
            ('draft', 'Draft'),
            ('cancel', 'Cancel'),
            ('approve', 'Approve'),
            ('done', 'Done'),
            ], 'State', readonly=True, select=True),
    }

    _defaults = {
                 'name': lambda self, cr, uid, context={}: self.pool.get('ir.sequence').get(cr, uid, 'hutang.senlog'),
                 'state' : 'draft',
                 'tipe': 'log',
                 'user_id': lambda obj, cr, uid, context: uid,
                 'date': lambda *a: time.strftime('%Y-%m-%d'),
    }

    def user_id_change(self, cr, uid, ids, idp):
        res = {}
        cid = self.pool.get('res.users').browse(cr, uid, uid).company_id.id
        jid = self.pool.get('account.journal').search(cr, uid, [('type', '=', 'purchase'), ('company_id', '=', cid)])
        if jid:
            res['journal_id'] = jid[0]
        res['user_id'] = uid
        res['company_id'] = cid
        return {'value': res}

    def unlink(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids, context={})[0]
        if val.state != 'draft':
            raise osv.except_osv(('Perhatian !'), ('Hutang tidak bisa dihapus pada state \'%s\'!') % (val.state,))
        return super(HutangSenlog, self).unlink(cr, uid, ids, context=context)

    def hutang_draft(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'draft'})
        return True                               
    
    def hutang_cancel(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'cancel'})
        return True                                  
         
    def hutang_confirm(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'approve'})
        return True
  
    def hutang_validate(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        obj_move = self.pool.get('account.move')
        obj_move_line = self.pool.get('account.move.line')
    
        mid = obj_move.create(cr,uid, {'hutang_id': val.id, 'ref' : val.name, 'journal_id': val.journal_id.id, 'company_id': val.company_id.id})

        obj_move_line.create(cr,uid, {
                                'name': val.name,
                                'partner_id': val.partner_id.id,
                                'account_id': val.account_id.id,
                                'date_maturity': val.date,
                                'move_id': mid,
                                'debit': val.total,
                                'credit': 0})

#         mad = obj_move.create(cr,uid, {'hutang_id': val.id, 'ref' : val.name, 'journal_id': val.payment_id.id, 'company_id': val.company_id.id})
#         obj_move_line.create(cr,uid, {
#                                 'name': val.name,
#                                 'partner_id': val.partner_id.id,
#                                 'account_id': val.payment_id.default_debit_account_id.id,
#                                 'date_maturity': val.date,
#                                 'move_id': mad,
#                                 'debit': val.total,
#                                 'credit': 0})
# 
#         obj_move_line.create(cr,uid, {
#                                 'name': val.name,
#                                 'partner_id': val.partner_id.id,
#                                 'account_id': val.account_id.id,
#                                 'date_maturity': val.date,
#                                 'move_id': mad,
#                                 'debit': 0,
#                                 'credit': val.total})
        
        for x in val.hutang_line :    
            obj_move_line.create(cr,uid, {
                                    'name': x.name,
                                    'partner_id': val.partner_id.id,
                                    'account_id': x.account_id.id,
                                    'date_maturity': val.date,
                                    'move_id': mid,
                                    'debit': 0,
                                    'credit': x.price_subtotal})
        

        self.write(cr, uid, ids, {'state': 'done'})
        return True 

class HutangSenlogLine(osv.osv):
    _name = 'hutang.senlog.line'
    
    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            price = line.price_unit * line.product_qty
            res[line.id] = price
        return res

    _columns = {
        'hutang_id': fields.many2one('hutang.senlog', 'Hutang Senlog', required=True, ondelete='cascade', select=True),
        'product_id': fields.many2one('product.product', 'Product'),
        'name': fields.char('Description', size=64, required=True),
        'product_qty': fields.float('Quantity', digits_compute=dp.get_precision('Product Unit of Measure')),
        'price_unit': fields.float('Price Unit', digits_compute=dp.get_precision('Product Price')),
        'price_subtotal': fields.function(_amount_line, string='Subtotal', digits_compute= dp.get_precision('Account')),
        'product_uom': fields.many2one('product.uom', 'UoM'),
        'account_id': fields.many2one('account.account', 'Account', required=True),
        'account_analytic_id': fields.many2one('account.analytic.account', 'Analytic Account', domain=[('type', '!=', 'view')]),
        
    }
    
    def product_id_change(self, cr, uid, ids, product):
        res = {}
        product_obj = self.pool.get('product.product')
        if product:
            res['name'] = product_obj.name_get(cr, uid, [product])[0][1]
            res['product_uom'] = product_obj.browse(cr, uid, product).uom_id.id
        return {'value': res}

#         <record model="ir.actions.act_window" id="action_piutang_senlog_senkit_form">
#             <field name="name">Piutang Senlog Senkit</field>
#             <field name="res_model">account.invoice</field>
#             <field name="view_type">form</field>
#             <field name="view_mode">tree,form,calendar,graph</field>
#             <field name="view_id" eval="False"/>
#             <field name="domain">[('type','=','out_invoice'), ('tipe','=','logkit')]</field>
#             <field name="context">{'default_type':'out_invoice', 'type':'out_invoice', 'default_tipe':'logkit', 'tipe':'logkit', 'journal_type': 'sale'}</field>
#             <field name="search_view_id" ref="account.view_account_invoice_filter"/>
#         </record>            
# 
# 
#         <record id="action_invoice_tree1_view1_senlog" model="ir.actions.act_window.view">
#             <field eval="1" name="sequence"/>
#             <field name="view_mode">tree</field>
#             <field name="act_window_id" ref="action_piutang_senlog_senkit_form"/>
#         </record>
# 
#         <record id="action_invoice_tree1_view2_senlog" model="ir.actions.act_window.view">
#             <field eval="2" name="sequence"/>
#             <field name="view_mode">form</field>
#             <field name="view_id" ref="account.invoice_form"/>
#             <field name="act_window_id" ref="action_piutang_senlog_senkit_form"/>
#         </record>
# 
# 
#         <menuitem name="Piutang Senlog Senkit" parent="account.menu_finance_receivables" id="menu_piutang_senlog_senkit" action="action_piutang_senlog_senkit_form"/>
# 
# 
# 
#         <record model="ir.actions.act_window" id="action_hutang_senlog_senkit_form">
#             <field name="name">Hutang Senlog Senkit</field>
#             <field name="res_model">account.invoice</field>
#             <field name="view_type">form</field>
#             <field name="view_mode">tree,form,calendar,graph</field>
#             <field eval="False" name="view_id"/>
#             <field name="domain">[('type','=','in_invoice')]</field>
#             <field name="context">{'default_type': 'in_invoice', 'type': 'in_invoice', 'journal_type': 'purchase'}</field>
#             <field name="search_view_id" ref="account.view_account_invoice_filter"/>
#         </record>            
# 
#         <menuitem name="Hutang Senlog Senkit" parent="account.menu_finance_payables" id="menu_hutang_senlog_senkit" action="action_hutang_senlog_senkit_form"/>

