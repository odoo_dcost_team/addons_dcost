import StringIO
import xlwt
import base64
import csv
import time
import tempfile
import cStringIO
from datetime import datetime
from lxml import etree
from dateutil import parser
from openerp import models, api
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.addons.report_xls.report_xls import report_xls
import openerp.addons.decimal_precision as dp





class account_journal(osv.osv):
    _inherit = "account.journal"
    _columns = {
                'analytic_id' : fields.many2one('account.analytic.account', 'Default Analytic', domain=[('type','=', 'normal')]),
    }
    
class account_bank_statement(osv.osv):
    _inherit = "account.bank.statement"

    def _prepare_move_line_vals(self, cr, uid, st_line, move_id, debit, credit, currency_id=False, amount_currency=False, account_id=False, partner_id=False, context=None):
        acc_id = account_id or st_line.account_id.id
        cur_id = currency_id or st_line.statement_id.currency.id
        par_id = partner_id or (((st_line.partner_id) and st_line.partner_id.id) or False)
        return {
            'name': st_line.name,
            'date': st_line.date,
            'ref': st_line.ref,
            'move_id': move_id,
            'partner_id': par_id,
            'account_id': acc_id,
            'credit': credit,
            'debit': debit,
            'analytic_account_id': st_line.statement_id.journal_id.analytic_id.id or False,
            'statement_id': st_line.statement_id.id,
            'journal_id': st_line.statement_id.journal_id.id,
            'period_id': st_line.statement_id.period_id.id,
            'currency_id': amount_currency and cur_id,
            'amount_currency': amount_currency,
        }


class laporan_accounting(osv.osv_memory):
    _name = "laporan.accounting"
    _columns = {
                'company_id' : fields.many2one('res.company', 'Company', required=True),
                'chart_account_id' : fields.many2one('account.account', 'CoA', domain="[('parent_id','=',False), ('company_id','=',company_id)]", required=True),
                'fiscalyear_id' : fields.many2one('account.fiscalyear', 'Fiscal Year', required=True),
                'filter': fields.selection((('period','Period'), ('date','Date')), 'Filter by', required=True),
                'tipe': fields.selection((('tb','Trial Balance'), ('bs','Balance Sheet'), ('pl','Profit Loss')), 'Type', required=True),
                
                'period_to' : fields.many2one('account.period', 'Period To'),
                'period_from' : fields.many2one('account.period', 'Period From'),
                
                'date_start': fields.date('Start date'),
                'date_stop': fields.date('End date'),
                
                'name': fields.char('File Name', 254),
                'data_file': fields.binary('File'),
    }
    
    _defaults = {
                 'tipe' : 'bs',
                 'fiscalyear_id' : 1,
                 'company_id' : 1,
                 'chart_account_id': 1,
                 'filter' : 'period',
    }
    
    def company_change(self, cr, uid, ids, company_id):
        return  {'value': {'chart_account_id':False, 'fiscalyear_id': False, 'period_from': False, 'period_to': False}}  

    def eksport_excel(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        lines = []; data = ''; dari = '-'; ke = '-'
        obj_move = self.pool.get('account.move')
        account_obj = self.pool.get('account.account')
        obj_move_line = self.pool.get('account.move.line')
        
        if val.filter == 'period':   
            dari = val.period_from.name; ke = val.period_to.name
        else:
            dari = val.date_start; ke = val.date_stop
        
        saldo = {}
        if val.filter == 'period':
            hid = obj_move.search(cr, uid, [('period_id', '>=', val.period_from.id), ('period_id', '<=', val.period_to.id)])
        elif val.filter == 'date':
            hid = obj_move.search(cr, uid, [('date', '>=', val.date_start), ('date', '<=', val.date_stop)])
         
        judul = 'TB_%s_%s_%s.xls' % (val.company_id.name, val.period_from.name, val.period_to.name)
        acc = val.chart_account_id
        if val.tipe == 'pl':
            judul = 'PL_%s_%s_%s.xls' % (val.company_id.name, val.period_from.name, val.period_to.name)
            acc = val.chart_account_id.child_id[1]
        elif val.tipe == 'bs':
            judul = 'BS_%s_%s_%s.xls' % (val.company_id.name, val.period_from.name, val.period_to.name)
            acc = val.chart_account_id.child_id[0]
        
        account_ids = account_obj._get_children_and_consol(cr, uid, [acc.id])  
        anak = account_obj.search(cr, uid, [('id', 'in', account_ids), ('type', '!=', 'view')])
        for x in anak:
            mid = obj_move_line.search(cr, uid, [('move_id', 'in', hid), ('account_id', '=', x)])
            if mid:
                mad = obj_move_line.browse(cr, uid, mid)            
                saldo[x] = {'debit': sum([i.debit for i in mad]), 'credit': sum([i.credit for i in mad])}
            else:
                saldo[x] = {'debit': 0, 'credit': 0}
                                   
        gran = {}; buyut = {}
        ortu = [f.parent_id.id for f in account_obj.browse(cr, uid, anak)]
        for o in account_obj.browse(cr, uid, ortu):
            try:
                gran[o.parent_id.id] = []
                saldo[o.id] = {'debit': sum([saldo[u.id]['debit'] for u in o.child_id]), 'credit': sum([saldo[u.id]['credit'] for u in o.child_id])}
            except Exception, e :
                raise osv.except_osv(('Perhatian !'), ("Susunan hirarki CoA %s salah ! ") % (o.name))
            
        for g in account_obj.browse(cr, uid, gran.keys()):
            if all (k in saldo for k in [j.id for j in g.child_id]): 
                saldo[g.id] = {'debit': sum([saldo[h.id]['debit'] for h in g.child_id]), 'credit': sum([saldo[h.id]['credit'] for h in g.child_id])}
                   
        for q in account_obj.browse(cr, uid, gran.keys()):
            if all (s in saldo for s in [n.id for n in q.child_id]): 
                saldo[q.id] = {'debit': sum([saldo[c.id]['debit'] for c in q.child_id]), 'credit': sum([saldo[c.id]['credit'] for c in q.child_id])}
                   
        for p in account_obj.browse(cr, uid, gran.keys()):
            if p.parent_id.parent_id:
                buyut[p.parent_id.id] = []
            if all (m in saldo for m in [z.id for z in p.child_id]):
                saldo[p.id] = {'debit': sum([saldo[e.id]['debit'] for e in p.child_id]), 'credit': sum([saldo[e.id]['credit'] for e in p.child_id])}
                  
        for y in account_obj.browse(cr, uid, buyut.keys()):
            if not saldo.has_key(y.id):
                if all (w in saldo for w in [r.id for r in y.child_id]):
                    saldo[y.id] = {'debit': sum([saldo[b.id]['debit'] for b in y.child_id]), 'credit': sum([saldo[b.id]['credit'] for b in y.child_id])}
            
        for v in account_obj.browse(cr, uid, buyut.keys()):
            if not saldo.has_key(v.id):
                if all (w in saldo for w in [r.id for r in v.child_id]):
                    saldo[v.id] = {'debit': sum([saldo[t.id]['debit'] for t in v.child_id]), 'credit': sum([saldo[t.id]['credit'] for t in v.child_id])}
        

        if val.tipe == 'tb':
            account_ids.remove(account_ids[0])
            data = 'sep=;\nAccount Name;Debit;Credit;Balance'
            for s in account_obj.browse(cr, uid, account_ids):
                if s.type == 'view':
                    lines.append({
                        'name': '['+ s.code +']' + ' ' + s.name,
                        'debit': saldo[s.id]['debit'],
                        'credit': saldo[s.id]['credit'],
                        'balance': saldo[s.id]['debit'] - saldo[s.id]['credit'],
                    })
         
            for n in lines:
                d = [str(n['name']),
                     str(n['debit']),
                     str(n['credit']), 
                     str(n['balance'])]  
                data += '\n' + ';'.join(d)
                                   
            out = base64.b64encode(data.encode('ascii',errors='ignore'))
            self.write(cr, uid, ids, {'data_file':out, 'name':judul}, context=context)
   
        else:
            book = xlwt.Workbook()
            sheet = book.add_sheet("Sheet 1")
       
            style = xlwt.easyxf('font: bold 1;')
            style_judul = xlwt.easyxf('font: bold 1, name Arial, height 300;')
            style_gray = xlwt.easyxf('font: bold 1, name Arial, height 250; pattern: pattern solid, fore_colour gray50;')
            
            style_header = xlwt.easyxf('font: bold 1, name Calibri, height 210; pattern: pattern solid, fore_colour yellow;')
            style_header_dec = xlwt.easyxf('font: bold 1, name Calibri, height 210; pattern: pattern solid, fore_colour yellow;', num_format_str=report_xls.decimal_format)
            style_total = xlwt.easyxf('font: bold 1, name Calibri, height 210; pattern: pattern solid, fore_colour blue;')
            style_total_dec = xlwt.easyxf('font: bold 1, name Calibri, height 210; pattern: pattern solid, fore_colour blue;', num_format_str=report_xls.decimal_format)
            
            _xs = report_xls.xls_styles
            aml_cell_format = _xs['borders_all']
            style_dec = xlwt.easyxf(aml_cell_format + _xs['right'], num_format_str=report_xls.decimal_format)
                    
            zero_col = sheet.col(0)
            zero_col.width = 256 * 3
            
            first_col = sheet.col(1)
            first_col.width = 256 * 3
            
            second_col = sheet.col(2)
            second_col.width = 256 * 60
            
            amount_col = sheet.col(3)
            amount_col.width = 256 * 30
            
            sheet.write(0, 0, 'DCOST', style)
            sheet.write(1, 0, 'SEA FOOD RESTAURANT', style)
            
            sheet.write(4, 0, 'From : ' + dari, style)
            sheet.write(5, 0, 'To : ' + ke, style)
    
            if val.tipe == 'bs':
                sheet.write(2, 0, 'LAPORAN NERACA KOMPILASI', style)
    
                nine_row = sheet.row(9)
                nine_row.height_mismatch = True
                nine_row.height = 256 * 3
                
                limaenam_row = sheet.row(56)
                limaenam_row.height_mismatch = True
                limaenam_row.height = 256 * 3
                
                tujuhenam_row = sheet.row(76)
                tujuhenam_row.height_mismatch = True
                tujuhenam_row.height = 256 * 3           
                
                sheet.write(9, 0, acc.child_id[0].name, style_judul)
                l = 10
                for c in acc.child_id[0].child_id:
                    sheet.write(l, 1, c.name, style_gray)
                    sheet.write(l, 2, '', style_gray)
                    sheet.write(l, 3, '', style_gray)
                    l += 1
                    
                    for t in c.child_id:
                        sheet.write(l, 2, t.name, style)
                        sheet.write(l, 3, saldo[t.id]['debit'] - saldo[t.id]['credit'], style_dec)
                        l += 1
                    
                    sheet.write(l, 1, 'Total ' + c.name, style_header)
                    sheet.write(l, 2, '', style_header)
                    sheet.write(l, 3, saldo[c.id]['debit'] - saldo[c.id]['credit'], style_header_dec)
                    l += 2
                
                sheet.write(l, 0, 'TOTAL ' + acc.child_id[0].name, style_total)
                sheet.write(l, 1, '', style_total)
                sheet.write(l, 2, '', style_total)
                sheet.write(l, 3, saldo[acc.child_id[0].id]['debit'] - saldo[acc.child_id[0].id]['credit'], style_total_dec)
                l += 2
                
                sheet.write(l, 0, acc.child_id[1].name, style_judul)
                l += 1
                for i in acc.child_id[1].child_id:
                    sheet.write(l, 1, i.name, style_gray)
                    sheet.write(l, 2, '', style_gray)
                    sheet.write(l, 3, '', style_gray)
                    l += 1
                    
                    for k in i.child_id:
                        sheet.write(l, 2, k.name, style)
                        sheet.write(l, 3, saldo[k.id]['debit'] - saldo[k.id]['credit'], style_dec)
                        l += 1
                    
                    sheet.write(l, 1, 'Total ' + i.name, style_header)
                    sheet.write(l, 2, '', style_header)
                    sheet.write(l, 3, saldo[i.id]['debit'] - saldo[i.id]['credit'], style_header_dec)
                    l += 2
                        
                sheet.write(l, 0, 'TOTAL ' + acc.child_id[1].name, style_total)
                sheet.write(l, 1, '', style_total)
                sheet.write(l, 2, '', style_total)
                sheet.write(l, 3, saldo[acc.child_id[1].id]['debit'] - saldo[acc.child_id[1].id]['credit'], style_total_dec)
                l += 2
                
                sheet.write(l, 0, acc.child_id[2].name, style_judul)
                l += 1
                for e in acc.child_id[2].child_id:
                    sheet.write(l, 1, e.name, style)
                    sheet.write(l, 3, saldo[e.id]['debit'] - saldo[e.id]['credit'], style_dec)
                    l += 1
                
                l += 1
                sheet.write(l, 0, 'TOTAL ' + acc.child_id[2].name, style_total)
                sheet.write(l, 1, '', style_total)
                sheet.write(l, 2, '', style_total)
                sheet.write(l, 3, saldo[acc.child_id[2].id]['debit'] - saldo[acc.child_id[2].id]['credit'], style_total_dec)        
    
            else:
                sheet.write(2, 0, 'LAPORAN PROFIT LOSS KOMPILASI', style)
    
                nine_row = sheet.row(9)
                nine_row.height_mismatch = True
                nine_row.height = 256 * 3
                
                tujuhbelas_row = sheet.row(17)
                tujuhbelas_row.height_mismatch = True
                tujuhbelas_row.height = 256 * 3
                 
                tigalapan_row = sheet.row(38)
                tigalapan_row.height_mismatch = True
                tigalapan_row.height = 256 * 3           
                
                sheet.write(9, 0, acc.child_id[0].name, style_judul)
                l = 10
                for c in acc.child_id[0].child_id:
                    sheet.write(l, 1, c.name, style_gray)
                    sheet.write(l, 2, '', style_gray)
                    sheet.write(l, 3, '', style_gray)
                    l += 1
                    
                    for t in c.child_id:
                        sheet.write(l, 2, t.name, style)
                        sheet.write(l, 3, saldo[t.id]['debit'] - saldo[t.id]['credit'], style_dec)
                        l += 1
                    
                    sheet.write(l, 1, 'Total ' + c.name, style_header)
                    sheet.write(l, 2, '', style_header)
                    sheet.write(l, 3, saldo[c.id]['debit'] - saldo[c.id]['credit'], style_header_dec)
                    l += 2
                
                sheet.write(l, 0, 'TOTAL ' + acc.child_id[0].name, style_total)
                sheet.write(l, 1, '', style_total)
                sheet.write(l, 2, '', style_total)
                sheet.write(l, 3, saldo[acc.child_id[0].id]['debit'] - saldo[acc.child_id[0].id]['credit'], style_total_dec)
                l += 2
                
                sheet.write(l, 0, acc.child_id[1].name, style_judul)
                l += 1
                for i in acc.child_id[1].child_id:
                    sheet.write(l, 1, i.name, style)
                    sheet.write(l, 3, saldo[i.id]['debit'] - saldo[i.id]['credit'], style_dec)
                    l += 1
                
                l += 1        
                sheet.write(l, 0, 'TOTAL ' + acc.child_id[1].name, style_total)
                sheet.write(l, 1, '', style_total)
                sheet.write(l, 2, '', style_total)
                sheet.write(l, 3, saldo[acc.child_id[1].id]['debit'] - saldo[acc.child_id[1].id]['credit'], style_total_dec)
                l += 1
                
                sheet.write(l, 0, acc.child_id[2].name, style_judul)
                l += 1
                for e in acc.child_id[2].child_id:
                    sheet.write(l, 1, e.name, style_gray)
                    sheet.write(l, 2, '', style_gray)
                    sheet.write(l, 3, '', style_gray)
                    l += 1
                    
                    for h in e.child_id:
                        sheet.write(l, 2, h.name, style)
                        sheet.write(l, 3, saldo[h.id]['debit'] - saldo[h.id]['credit'], style_dec)
                        l += 1
                    
                    sheet.write(l, 1, 'Total ' + e.name, style_header)
                    sheet.write(l, 2, '', style_header)
                    sheet.write(l, 3, saldo[e.id]['debit'] - saldo[e.id]['credit'], style_header_dec)
                    l += 2
                
                sheet.write(l, 0, 'TOTAL ' + acc.child_id[2].name, style_total)
                sheet.write(l, 1, '', style_total)
                sheet.write(l, 2, '', style_total)
                sheet.write(l, 3, saldo[acc.child_id[2].id]['debit'] - saldo[acc.child_id[2].id]['credit'], style_total_dec)        
                    
            file_data = StringIO.StringIO()
            i = book.save(file_data)
                
            out = base64.encodestring(file_data.getvalue())
            self.write(cr, uid, ids, {'data_file': out, 'name': judul}, context=context)
 
        view_rec = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'siu_ledger', 'view_wizard_laporan_accounting')
        view_id = view_rec[1] or False
                
        return {
            'view_type': 'form',
            'view_id' : [view_id],
            'view_mode': 'form',
            'res_id': val.id,
            'res_model': 'laporan.accounting',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
    




class laporan_analytic(osv.osv_memory):
    _name = "laporan.analytic"
    _columns = {
                'company_id' : fields.many2one('res.company', 'Company', required=True),
                'chart_analytic_id' : fields.many2one('account.analytic.account', 'Outlet', domain=[('parent_id','=',False)], required=True),
                'fiscalyear_id' : fields.many2one('account.fiscalyear', 'Fiscal Year', required=True),
                
                'filter': fields.selection((('period','Period'), ('date','Date')), 'Filter by', required=True),
                
                'period_to' : fields.many2one('account.period', 'Period To'),
                'period_from' : fields.many2one('account.period', 'Period From'),
                
                'date_start': fields.date('Start date'),
                'date_stop': fields.date('End date'),
                
                'name': fields.char('File Name', 254),
                'data_file': fields.binary('File'),
    }
    
    _defaults = {
                 'fiscalyear_id' : 1,
                 'company_id' : 1,
                 'filter' : 'period',
    }

    def eksport_excel(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        obj_move = self.pool.get('account.move')
        obj_move_line = self.pool.get('account.move.line')
        
        saldo = {}
        if val.filter == 'period':
            hid = obj_move.search(cr, uid, [('period_id', '>=', val.period_from.id), ('period_id', '<=', val.period_to.id)])
            awal = obj_move.search(cr, uid, [('period_id', '<', val.period_from.id)])
        elif val.filter == 'date':
            hid = obj_move.search(cr, uid, [('date', '>=', val.date_start), ('date', '<=', val.date_stop)])
            awal = obj_move.search(cr, uid, [('date', '<', val.date_stop)])
        
        judul = '%s_%s.xls' % ('Analytic', val.chart_analytic_id.name)
        data = 'sep=;\nAnalytic Name;Opening;Debit;Credit;Balance'

        lines = []
        analytic_obj = self.pool.get('account.analytic.account')
        
        account_ids = self._get_children_and_consol(cr, uid, [val.chart_analytic_id.id])
        account_ids.remove(account_ids[0])
        
        anak = analytic_obj.search(cr, uid, [('id', 'in', account_ids), ('type', '!=', 'view')])
        for x in anak:
            mid = obj_move_line.search(cr, uid, [('move_id', 'in', hid), ('analytic_account_id', '=', x)])
            if mid:
                mad = obj_move_line.browse(cr, uid, mid)
                saldo[x] = {'debit': sum([i.debit for i in mad]), 'credit': sum([i.credit for i in mad])}
            else:
                saldo[x] = {'debit': 0, 'credit': 0}
                
            walid = obj_move_line.search(cr, uid, [('move_id', 'in', awal), ('analytic_account_id', '=', x)])
            if walid:
                walad = obj_move_line.browse(cr, uid, walid)
                saldo[x]['open'] = sum([i.debit for i in walad]) - sum([i.credit for i in walad])
            else:
                saldo[x]['open'] = 0
            
                
        gran = {}; buyut = {}
        ortu = [f.parent_id.id for f in analytic_obj.browse(cr, uid, anak)]
        for o in analytic_obj.browse(cr, uid, ortu):
            try:
                gran[o.parent_id.id] = []
                saldo[o.id] = {'open': sum([saldo[u.id]['open'] for u in o.child_ids]), 'debit': sum([saldo[u.id]['debit'] for u in o.child_ids]), 'credit': sum([saldo[u.id]['credit'] for u in o.child_ids])}
            except  Exception, e :
                raise osv.except_osv(('Perhatian !'), ("Susunan hirarki CoAA %s salah ! ") % (o.name))
          
        for g in analytic_obj.browse(cr, uid, gran.keys()):
            if all (k in saldo for k in [j.id for j in g.child_ids]): 
                saldo[g.id] = {'open': sum([saldo[h.id]['open'] for h in g.child_ids]), 'debit': sum([saldo[h.id]['debit'] for h in g.child_ids]), 'credit': sum([saldo[h.id]['credit'] for h in g.child_ids])}
            
        for q in analytic_obj.browse(cr, uid, gran.keys()):
            if all (s in saldo for s in [n.id for n in q.child_ids]): 
                saldo[q.id] = {'open': sum([saldo[c.id]['open'] for c in q.child_ids]), 'debit': sum([saldo[c.id]['debit'] for c in q.child_ids]), 'credit': sum([saldo[c.id]['credit'] for c in q.child_ids])}
            
        for p in analytic_obj.browse(cr, uid, gran.keys()):
            if p.parent_id.parent_id:
                buyut[p.parent_id.id] = []
            if all (m in saldo for m in [z.id for z in p.child_ids]):
                saldo[p.id] = {'open': sum([saldo[e.id]['open'] for e in p.child_ids]), 'debit': sum([saldo[e.id]['debit'] for e in p.child_ids]), 'credit': sum([saldo[e.id]['credit'] for e in p.child_ids])}
        
        for y in analytic_obj.browse(cr, uid, buyut.keys()):
            if not saldo.has_key(y.id):
                if all (w in saldo for w in [r.id for r in y.child_ids]):
                    saldo[y.id] = {'open': sum([saldo[b.id]['open'] for b in y.child_ids]), 'debit': sum([saldo[b.id]['debit'] for b in y.child_ids]), 'credit': sum([saldo[b.id]['credit'] for b in y.child_ids])}
        
        for v in analytic_obj.browse(cr, uid, buyut.keys()):
            if not saldo.has_key(v.id):
                if all (w in saldo for w in [r.id for r in v.child_ids]):
                    saldo[v.id] = {'open': sum([saldo[t.id]['open'] for t in v.child_ids]), 'debit': sum([saldo[t.id]['debit'] for t in v.child_ids]), 'credit': sum([saldo[t.id]['credit'] for t in v.child_ids])}
                
        for acc in analytic_obj.browse(cr, uid, account_ids):
            if acc.type == 'view':
                lines.append({
                    'name': '['+ acc.code +']' + ' ' + acc.name,
                    'open': saldo[acc.id]['open'],
                    'debit': saldo[acc.id]['debit'],
                    'credit': saldo[acc.id]['credit'],
                    'balance': saldo[acc.id]['open'] + saldo[acc.id]['debit'] - saldo[acc.id]['credit'],
                })
     
        for x in lines:
            d = [str(x['name']),
                 str(x['open']),
                 str(x['debit']),
                 str(x['credit']), 
                 str(x['balance'])]  
            data += '\n' + ';'.join(d)
                               
        out = base64.b64encode(data.encode('ascii',errors='ignore'))
        self.write(cr, uid, ids, {'data_file':out, 'name':judul}, context=context)
   
        view_rec = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'siu_ledger', 'view_wizard_laporan_analytic')
        view_id = view_rec[1] or False
                  
        return {
            'view_type': 'form',
            'view_id' : [view_id],
            'view_mode': 'form',
            'res_id': val.id,
            'res_model': 'laporan.analytic',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }


    def _get_children_and_consol(self, cr, uid, ids, context=None):
        analytic_obj = self.pool.get('account.analytic.account')
        ids2 = analytic_obj.search(cr, uid, [('parent_id', 'child_of', ids)], context=context)
        ids3 = []
        for rec in analytic_obj.browse(cr, uid, ids2, context=context):
            for child in rec.child_ids:
                ids3.append(child.id)
             
        return ids2



class laporan_pivot(osv.osv):
    _name = "laporan.pivot"
    _columns = {
                'company_id' : fields.many2one('res.company', 'Company', required=True),
                'chart_analytic_id' : fields.many2many('account.analytic.account', 'pivot_rel', 'pivot_id', 'analytic_id', 'Outlet', domain=[('parent_id','=',False)]),
                'fiscalyear_id' : fields.many2one('account.fiscalyear', 'Fiscal Year', required=True),
                
                'period_to' : fields.many2one('account.period', 'Period To', required=True),
                'period_from' : fields.many2one('account.period', 'Period From', required=True),
                
                'fiscalyear_lapiaza_id' : fields.many2one('account.fiscalyear', 'Fiscal Year', domain=[('company_id','=',3)]),
                'fiscalyear_senen_id' : fields.many2one('account.fiscalyear', 'Fiscal Year', domain=[('company_id','=',4)]),
                'fiscalyear_kemang_id' : fields.many2one('account.fiscalyear', 'Fiscal Year', domain=[('company_id','=',5)]),
                'fiscalyear_puri_id' : fields.many2one('account.fiscalyear', 'Fiscal Year', domain=[('company_id','=',6)]),
                'fiscalyear_suka_id' : fields.many2one('account.fiscalyear', 'Fiscal Year', domain=[('company_id','=',7)]),
                'fiscalyear_banjar_id' : fields.many2one('account.fiscalyear', 'Fiscal Year', domain=[('company_id','=',8)]),
                'fiscalyear_graha_id' : fields.many2one('account.fiscalyear', 'Fiscal Year', domain=[('company_id','=',9)]),
                
                'period_to_lapiaza' : fields.many2one('account.period', 'Period To'),
                'period_from_lapiaza' : fields.many2one('account.period', 'Period From'),
                'period_to_senen' : fields.many2one('account.period', 'Period To'),
                'period_from_senen' : fields.many2one('account.period', 'Period From'),
                'period_to_kemang' : fields.many2one('account.period', 'Period To'),
                'period_from_kemang' : fields.many2one('account.period', 'Period From'),
                'period_to_puri' : fields.many2one('account.period', 'Period To'),
                'period_from_puri' : fields.many2one('account.period', 'Period From'),
                'period_to_suka' : fields.many2one('account.period', 'Period To'),
                'period_from_suka' : fields.many2one('account.period', 'Period From'),
                'period_to_banjar' : fields.many2one('account.period', 'Period To'),
                'period_from_banjar' : fields.many2one('account.period', 'Period From'),
                'period_to_graha' : fields.many2one('account.period', 'Period To'),
                'period_from_graha' : fields.many2one('account.period', 'Period From'),
                
                'coa_lapiaza_id' : fields.many2one('account.account', 'CoA', domain=[('company_id','=',3), ('parent_id','=',False)]),
                'coa_senen_id' : fields.many2one('account.account', 'CoA', domain=[('company_id','=',4), ('parent_id','=',False)]),
                'coa_kemang_id' : fields.many2one('account.account', 'CoA', domain=[('company_id','=',5), ('parent_id','=',False)]),
                'coa_puri_id' : fields.many2one('account.account', 'CoA', domain=[('company_id','=',6), ('parent_id','=',False)]),
                'coa_suka_id' : fields.many2one('account.account', 'CoA', domain=[('company_id','=',7), ('parent_id','=',False)]),
                'coa_banjar_id' : fields.many2one('account.account', 'CoA', domain=[('company_id','=',8), ('parent_id','=',False)]),
                'coa_graha_id' : fields.many2one('account.account', 'CoA', domain=[('company_id','=',9), ('parent_id','=',False)]),
                
                'name': fields.char('File Name', 254),
                'data_file': fields.binary('File'),
    }
    
    _defaults = {
                 'fiscalyear_id' : 1,
                 'company_id' : 1,
                 'fiscalyear_lapiaza_id' : 6,
                 'fiscalyear_senen_id' : 2,
                 'fiscalyear_kemang_id' : 5,
                 'fiscalyear_puri_id' : 7,
                 'fiscalyear_suka_id' : 8,
                 'fiscalyear_banjar_id' : 3,
                 'fiscalyear_graha_id' : 4,
                 
    }

    def eksport_excel(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        self.account_obj = self.pool.get('account.account')
        self.obj_move = self.pool.get('account.move')
        self.obj_move_line = self.pool.get('account.move.line')
        analytic_obj = self.pool.get('account.analytic.account')
        
        judul = 'Pivot_Analytic.xls'
        self.data = 'sep=;\nAnalytic Name'
        
        if val.coa_lapiaza_id:
            if not val.period_from_lapiaza or not val.period_to_lapiaza:
                raise osv.except_osv(('Perhatian !'), ("Period Lapiaza harus diisi !"))
          
        if val.coa_senen_id:
            if not val.period_from_senen or not val.period_to_senen:
                raise osv.except_osv(('Perhatian !'), ("Period Senen harus diisi !"))
          
        if val.coa_kemang_id:
            if not val.period_from_kemang or not val.period_to_kemang:
                raise osv.except_osv(('Perhatian !'), ("Period Kemang harus diisi !"))
          
        if val.coa_puri_id:
            if not val.period_from_puri or not val.period_to_puri:
                raise osv.except_osv(('Perhatian !'), ("Period Puri harus diisi !"))
          
        if val.coa_suka_id:
            if not val.period_from_suka or not val.period_to_suka:
                raise osv.except_osv(('Perhatian !'), ("Period Sukajadi harus diisi !"))
          
        if val.coa_banjar_id:
            if not val.period_from_banjar or not val.period_to_banjar:
                raise osv.except_osv(('Perhatian !'), ("Period Banjarmasin harus diisi !"))
          
        if val.coa_graha_id:
            if not val.period_from_graha or not val.period_to_graha:
                raise osv.except_osv(('Perhatian !'), ("Period Graha harus diisi !"))
          
        
        hid = self.obj_move.search(cr, uid, [('period_id', '>=', val.period_from.id), ('period_id', '<=', val.period_to.id)])
        
        if val.chart_analytic_id:
            child = analytic_obj.search(cr, uid, [('id', 'in', self._get_children_and_consol(cr, uid, [val.chart_analytic_id[0].child_ids[1].id])), ('type', '!=', 'view')])
            for o in analytic_obj.browse(cr, uid, child):
                self.data += ';' + o.name
                    
        for i in val.chart_analytic_id:
            account_ids = self._get_children_and_consol(cr, uid, [i.child_ids[1].id])
            anak = analytic_obj.search(cr, uid, [('id', 'in', account_ids), ('type', '!=', 'view')])
            baris = '\n%s' % (i.name)
            for x in anak:
                mid = self.obj_move_line.search(cr, uid, [('move_id', 'in', hid), ('analytic_account_id', '=', x)])
                if mid:
                    mad = self.obj_move_line.browse(cr, uid, mid)
                    baris += ';' + str(sum([i.debit for i in mad])-sum([i.credit for i in mad]))
                else:
                    baris += ';' + str(0.0)
            self.data += '\n' + baris
        
        if val.coa_lapiaza_id:
            self.bikin_header(cr, uid, ids, val.coa_lapiaza_id)
        elif val.coa_senen_id:
            self.bikin_header(cr, uid, ids, val.coa_senen_id)
        elif val.coa_kemang_id:
            self.bikin_header(cr, uid, ids, val.coa_kemang_id)
        elif val.coa_puri_id:
            self.bikin_header(cr, uid, ids, val.coa_puri_id)
        elif val.coa_suka_id:
            self.bikin_header(cr, uid, ids, val.coa_suka_id)
        elif val.coa_banjar_id:
            self.bikin_header(cr, uid, ids, val.coa_banjar_id)
        elif val.coa_graha_id:
            self.bikin_header(cr, uid, ids, val.coa_graha_id)
        
        
        if val.coa_lapiaza_id:
            self.tambah_data(cr, uid, ids, val.coa_lapiaza_id, val.period_from_lapiaza.id, val.period_to_lapiaza.id)
        if val.coa_senen_id:
            self.tambah_data(cr, uid, ids, val.coa_senen_id, val.period_from_senen.id, val.period_to_senen.id)
        if val.coa_kemang_id:
            self.tambah_data(cr, uid, ids, val.coa_kemang_id, val.period_from_kemang.id, val.period_to_kemang.id)
        if val.coa_puri_id:
            self.tambah_data(cr, uid, ids, val.coa_puri_id, val.period_from_puri.id, val.period_to_puri.id)
        if val.coa_suka_id:
            self.tambah_data(cr, uid, ids, val.coa_suka_id, val.period_from_suka.id, val.period_to_suka.id)
        if val.coa_banjar_id:
            self.tambah_data(cr, uid, ids, val.coa_banjar_id, val.period_from_banjar.id, val.period_to_banjar.id)
        if val.coa_graha_id:
            self.tambah_data(cr, uid, ids, val.coa_graha_id, val.period_from_graha.id, val.period_to_graha.id)
            
                    
        out = base64.b64encode(self.data.encode('ascii',errors='ignore'))
        self.write(cr, uid, ids, {'data_file':out, 'name':judul}, context=context)
   
        view_rec = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'siu_ledger', 'view_wizard_laporan_pivot')
        view_id = view_rec[1] or False
                  
        return {
            'view_type': 'form',
            'view_id' : [view_id],
            'view_mode': 'form',
            'res_id': val.id,
            'res_model': 'laporan.pivot',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }


    def _get_children_and_consol(self, cr, uid, ids, context=None):
        analytic_obj = self.pool.get('account.analytic.account')
        ids2 = analytic_obj.search(cr, uid, [('parent_id', 'child_of', ids)], context=context)
        ids3 = []
        for rec in analytic_obj.browse(cr, uid, ids2, context=context):
            for child in rec.child_ids:
                ids3.append(child.id)             
        return ids2
    
    def bikin_header(self, cr, uid, ids, coa):
        self.data += '\n'
        parent = self.account_obj.search(cr, uid, [('id', 'in', self.account_obj._get_children_and_consol(cr, uid, [coa.child_id[1].id])), ('type', '!=', 'view')])
        for o in self.account_obj.browse(cr, uid, parent):
            self.data += ';' + o.name

    def tambah_data(self, cr, uid, ids, coa, dari, sampai):
        hid = self.obj_move.search(cr, uid, [('period_id', '>=', dari), ('period_id', '<=', sampai)])
        acc = self.account_obj._get_children_and_consol(cr, uid, [coa.child_id[1].id])
        anak = self.account_obj.search(cr, uid, [('id', 'in', acc), ('type', '!=', 'view')])
        baris = '\n%s' % (coa.name)
        for a in anak:
            mid = self.obj_move_line.search(cr, uid, [('move_id', 'in', hid), ('account_id', '=', a)])
            if mid:
                mad = self.obj_move_line.browse(cr, uid, mid)
                baris += ';' + str(sum([i.debit for i in mad])-sum([i.credit for i in mad]))
            else:
                baris += ';' + str(0.0)
        self.data += '\n' + baris
            


        

#         <record model="ir.ui.view" id="view_account_form_saldo">
#             <field name="name">account.account.form.saldo</field>
#             <field name="model">account.account</field>
#             <field name="inherit_id" ref="account.view_account_form" />
#             <field name="arch" type="xml">
#                 <field name="company_id" position="after">
#                     <group string="Saldo Mutasi" colspan="4" col="4">
#                         <field name="open_2015_debit"/>
#                         <field name="open_2015_credit"/>
#                         <field name="1_2015_debit"/>
#                         <field name="1_2015_credit"/>
#                         <field name="2_2015_debit"/>
#                         <field name="2_2015_credit"/>
#                         <field name="3_2015_debit"/>
#                         <field name="3_2015_credit"/>
#                         <field name="4_2015_debit"/>
#                         <field name="4_2015_credit"/>
#                         <field name="5_2015_debit"/>
#                         <field name="5_2015_credit"/>
#                         <field name="6_2015_debit"/>
#                         <field name="6_2015_credit"/>
#                         <field name="7_2015_debit"/>
#                         <field name="7_2015_credit"/>
#                         <field name="8_2015_debit"/>
#                         <field name="8_2015_credit"/>
#                         <field name="9_2015_debit"/>
#                         <field name="9_2015_credit"/>
#                         <field name="10_2015_debit"/>
#                         <field name="10_2015_credit"/>
#                         <field name="11_2015_debit"/>
#                         <field name="11_2015_credit"/>
#                         <field name="12_2015_debit"/>
#                         <field name="12_2015_credit"/>
#                     </group>                        
#                 </field>
#             </field>
#         </record>
        


# class account_account(osv.osv):
#     _inherit = "account.account"
#     _columns = {
#         'open_2015_debit': fields.float('Debit Opening 2015', digits_compute=dp.get_precision('Account')),
#         'open_2015_credit': fields.float('Credit Opening 2015', digits_compute=dp.get_precision('Account')),
#         
#         '1_2015_debit': fields.float('Debit Januari 2015', digits_compute=dp.get_precision('Account')),
#         '1_2015_credit': fields.float('Credit Januari 2015', digits_compute=dp.get_precision('Account')),
#         
#         '2_2015_debit': fields.float('Debit Februari 2015', digits_compute=dp.get_precision('Account')),
#         '2_2015_credit': fields.float('Credit Februari 2015', digits_compute=dp.get_precision('Account')),
#         
#         '3_2015_debit': fields.float('Debit Maret 2015', digits_compute=dp.get_precision('Account')),
#         '3_2015_credit': fields.float('Credit Maret 2015', digits_compute=dp.get_precision('Account')),
#         
#         '4_2015_debit': fields.float('Debit April 2015', digits_compute=dp.get_precision('Account')),
#         '4_2015_credit': fields.float('Credit April 2015', digits_compute=dp.get_precision('Account')),
#         
#         '5_2015_debit': fields.float('Debit Mei 2015', digits_compute=dp.get_precision('Account')),
#         '5_2015_credit': fields.float('Credit Mei 2015', digits_compute=dp.get_precision('Account')),
#         
#         '6_2015_debit': fields.float('Debit Juni 2015', digits_compute=dp.get_precision('Account')),
#         '6_2015_credit': fields.float('Credit Juni 2015', digits_compute=dp.get_precision('Account')),
#         
#         '7_2015_debit': fields.float('Debit Juli 2015', digits_compute=dp.get_precision('Account')),
#         '7_2015_credit': fields.float('Credit Juli 2015', digits_compute=dp.get_precision('Account')),
#         
#         '8_2015_debit': fields.float('Debit Agustus 2015', digits_compute=dp.get_precision('Account')),
#         '8_2015_credit': fields.float('Credit Agustus 2015', digits_compute=dp.get_precision('Account')),
#         
#         '9_2015_debit': fields.float('Debit September 2015', digits_compute=dp.get_precision('Account')),
#         '9_2015_credit': fields.float('Credit September 2015', digits_compute=dp.get_precision('Account')),
#         
#         '10_2015_debit': fields.float('Debit Oktober 2015', digits_compute=dp.get_precision('Account')),
#         '10_2015_credit': fields.float('Credit Oktober 2015', digits_compute=dp.get_precision('Account')),
#         
#         '11_2015_debit': fields.float('Debit November 2015', digits_compute=dp.get_precision('Account')),
#         '11_2015_credit': fields.float('Credit November 2015', digits_compute=dp.get_precision('Account')),
#         
#         '12_2015_debit': fields.float('Debit Desember 2015', digits_compute=dp.get_precision('Account')),
#         '12_2015_credit': fields.float('Credit Desember 2015', digits_compute=dp.get_precision('Account')),
#     }
# 
#     def update_saldo(self, cr, uid, ids=False, context=None):
#         print '############################################### MULAI UPDATE SALDO ACCOUNT ###############################################'
#         print ids
#         
#         obj_move = self.pool.get('account.move')
#         obj_account = self.pool.get('account.account')
#         obj_move_line = self.pool.get('account.move.line')
#            
#         anak = obj_account.search(cr, uid, [('company_id', '=', 1), ('type', '!=', 'view')])
#         
#         hid_open = obj_move.search(cr, uid, [('period_id', '>=', 1), ('period_id', '<=', 1)])
#         hid_1 = obj_move.search(cr, uid, [('period_id', '>=', 2), ('period_id', '<=', 2)])
#         hid_2 = obj_move.search(cr, uid, [('period_id', '>=', 3), ('period_id', '<=', 3)])
#         hid_3 = obj_move.search(cr, uid, [('period_id', '>=', 4), ('period_id', '<=', 4)])
#         hid_4 = obj_move.search(cr, uid, [('period_id', '>=', 5), ('period_id', '<=', 5)])
#         hid_5 = obj_move.search(cr, uid, [('period_id', '>=', 6), ('period_id', '<=', 6)])
#         hid_6 = obj_move.search(cr, uid, [('period_id', '>=', 7), ('period_id', '<=', 7)])
#         hid_7 = obj_move.search(cr, uid, [('period_id', '>=', 8), ('period_id', '<=', 8)])
#         hid_8 = obj_move.search(cr, uid, [('period_id', '>=', 9), ('period_id', '<=', 9)])
#         hid_9 = obj_move.search(cr, uid, [('period_id', '>=', 10), ('period_id', '<=', 10)])
#         hid_10 = obj_move.search(cr, uid, [('period_id', '>=', 11), ('period_id', '<=', 11)])
#         hid_11 = obj_move.search(cr, uid, [('period_id', '>=', 12), ('period_id', '<=', 12)])
#         hid_12 = obj_move.search(cr, uid, [('period_id', '>=', 13), ('period_id', '<=', 13)])
#         
#         for x in anak:
#             mid_open = obj_move_line.search(cr, uid, [('move_id', 'in', hid_open), ('account_id', '=', x)])
#             mid_1 = obj_move_line.search(cr, uid, [('move_id', 'in', hid_1), ('account_id', '=', x)])
#             mid_2 = obj_move_line.search(cr, uid, [('move_id', 'in', hid_2), ('account_id', '=', x)])
#             mid_3 = obj_move_line.search(cr, uid, [('move_id', 'in', hid_3), ('account_id', '=', x)])
#             mid_4 = obj_move_line.search(cr, uid, [('move_id', 'in', hid_4), ('account_id', '=', x)])
#             mid_5 = obj_move_line.search(cr, uid, [('move_id', 'in', hid_5), ('account_id', '=', x)])
#             mid_6 = obj_move_line.search(cr, uid, [('move_id', 'in', hid_6), ('account_id', '=', x)])
#             mid_7 = obj_move_line.search(cr, uid, [('move_id', 'in', hid_7), ('account_id', '=', x)])
#             mid_8 = obj_move_line.search(cr, uid, [('move_id', 'in', hid_8), ('account_id', '=', x)])
#             mid_9 = obj_move_line.search(cr, uid, [('move_id', 'in', hid_9), ('account_id', '=', x)])
#             mid_10 = obj_move_line.search(cr, uid, [('move_id', 'in', hid_10), ('account_id', '=', x)])
#             mid_11 = obj_move_line.search(cr, uid, [('move_id', 'in', hid_11), ('account_id', '=', x)])
#             mid_12 = obj_move_line.search(cr, uid, [('move_id', 'in', hid_12), ('account_id', '=', x)])
#             
#             if mid_open:
#                 mad_open = obj_move_line.browse(cr, uid, mid_open)
#                 obj_account.write(cr, uid, [x], {'open_2015_debit': sum([i.debit for i in mad_open]), 'open_2015_credit': sum([i.credit for i in mad_open])})
#             
#             if mid_1:
#                 mad_1 = obj_move_line.browse(cr, uid, mid_1)
#                 obj_account.write(cr, uid, [x], {'1_2015_debit': sum([i.debit for i in mad_1]), '1_2015_credit': sum([i.credit for i in mad_1])})
#             
#             if mid_2:
#                 mad_2 = obj_move_line.browse(cr, uid, mid_2)
#                 obj_account.write(cr, uid, [x], {'2_2015_debit': sum([i.debit for i in mad_2]), '2_2015_credit': sum([i.credit for i in mad_2])})
#                          
#             if mid_3:
#                 mad_3 = obj_move_line.browse(cr, uid, mid_3)
#                 obj_account.write(cr, uid, [x], {'3_2015_debit': sum([i.debit for i in mad_3]), '3_2015_credit': sum([i.credit for i in mad_3])})
#                          
#             if mid_4:
#                 mad_4 = obj_move_line.browse(cr, uid, mid_4)
#                 obj_account.write(cr, uid, [x], {'4_2015_debit': sum([i.debit for i in mad_4]), '4_2015_credit': sum([i.credit for i in mad_4])})
#                          
#             if mid_5:
#                 mad_5 = obj_move_line.browse(cr, uid, mid_5)
#                 obj_account.write(cr, uid, [x], {'5_2015_debit': sum([i.debit for i in mad_5]), '5_2015_credit': sum([i.credit for i in mad_5])})
#                          
#             if mid_6:
#                 mad_6 = obj_move_line.browse(cr, uid, mid_6)
#                 obj_account.write(cr, uid, [x], {'6_2015_debit': sum([i.debit for i in mad_6]), '6_2015_credit': sum([i.credit for i in mad_6])})
#                          
#             if mid_7:
#                 mad_7 = obj_move_line.browse(cr, uid, mid_7)
#                 obj_account.write(cr, uid, [x], {'7_2015_debit': sum([i.debit for i in mad_7]), '7_2015_credit': sum([i.credit for i in mad_7])})
#                          
#             if mid_8:
#                 mad_8 = obj_move_line.browse(cr, uid, mid_8)
#                 obj_account.write(cr, uid, [x], {'8_2015_debit': sum([i.debit for i in mad_8]), '8_2015_credit': sum([i.credit for i in mad_8])})
#                          
#             if mid_9:
#                 mad_9 = obj_move_line.browse(cr, uid, mid_9)
#                 obj_account.write(cr, uid, [x], {'9_2015_debit': sum([i.debit for i in mad_9]), '9_2015_credit': sum([i.credit for i in mad_9])})
#                          
#             if mid_10:
#                 mad_10 = obj_move_line.browse(cr, uid, mid_10)
#                 obj_account.write(cr, uid, [x], {'10_2015_debit': sum([i.debit for i in mad_10]), '10_2015_credit': sum([i.credit for i in mad_10])})
#                          
#             if mid_11:
#                 mad_11 = obj_move_line.browse(cr, uid, mid_11)
#                 obj_account.write(cr, uid, [x], {'11_2015_debit': sum([i.debit for i in mad_11]), '11_2015_credit': sum([i.credit for i in mad_11])})
#                          
#             if mid_12:
#                 mad_12 = obj_move_line.browse(cr, uid, mid_12)
#                 obj_account.write(cr, uid, [x], {'12_2015_debit': sum([i.debit for i in mad_12]), '12_2015_credit': sum([i.credit for i in mad_12])})
#         
#         print '############################################### SELESAI UPDATE SALDO ACCOUNT ###############################################'

    
#     def eksport_excel(self, cr, uid, ids, context=None):
#         val = self.browse(cr, uid, ids)[0]
#         judul = ''; data = ''
#             
#         obj_journal = self.pool.get('account.journal')
#         jids = obj_journal.search(cr, uid, [])
#         chart = {1:1, 3:1473, 4:2066, 5:1178, 6:1771, 7:2361, 8:588, 9:883}
#          
#         if val.type == 'BS':
#             laporan = [4, 'Balance Sheet']
#         elif val.type == 'PL':
#             laporan = [1, 'Profit and Loss']
#         
#         datas = {}
#         if val.filter == 'date':            
#             datas={
#                 'form': 
#                         {
#                          'period_to_cmp': False, 
#                          'chart_account_id': chart[val.company_id.id], 
#                          'period_from_cmp': False, 
#                          'account_report_id': laporan, 
#                          'period_to': False, 
#                          'date_to_cmp': False, 
#                          'fiscalyear_id': val.fiscalyear_id.id, 
#                          'periods': [], 
#                          'id': 1, 
#                          'fiscalyear_id_cmp': False, 
#                          'date_from': val.date_start, 
#                          'used_context': 
#                                         {
#                                          'lang': 'en_US', 
#                                          'chart_account_id': chart[val.company_id.id], 
#                                          'date_from': val.date_start, 
#                                          'journal_ids': jids, 
#                                          'state': 'posted', 
#                                          'date_to': val.date_stop, 
#                                          'fiscalyear': val.fiscalyear_id.id
#                                          }, 
#                          'period_from': False, 
#                          'label_filter': False, 
#                          'filter_cmp': 'filter_no', 
#                          'enable_filter': False, 
#                          'journal_ids': jids, 
#                          'date_to': val.date_stop, 
#                          'comparison_context': 
#                                                 {
#                                                  'state': 'posted', 
#                                                  'chart_account_id': chart[val.company_id.id], 
#                                                  'journal_ids': jids, 
#                                                  'fiscalyear': False
#                                                  }, 
#                          'filter': 'filter_date', 
#                          'date_from_cmp': False, 
#                          'debit_credit': True, 
#                          'target_move': 'posted'
#                          }
#             }
#         
#         elif val.filter == 'period':
#             datas={
#                 'form': {
#                             'period_to_cmp': False, 
#                             'chart_account_id': chart[val.company_id.id], 
#                             'period_from_cmp': False, 
#                             'account_report_id': laporan, 
#                             'period_to': val.period_to.id, 
#                             'date_to_cmp': False, 
#                             'fiscalyear_id': val.fiscalyear_id.id, 
#                             'periods': [], 
#                             'id': 1, 
#                             'fiscalyear_id_cmp': False, 
#                             'date_from': False, 
#                             'used_context': {
#                                               'lang': 'en_US', 
#                                               'chart_account_id': chart[val.company_id.id], 
#                                               'period_to': val.period_to.id,
#                                               'journal_ids': jids, 
#                                               'period_from': val.period_from.id, 
#                                               'state': 'posted', 
#                                               'fiscalyear': val.fiscalyear_id.id
#                                             }, 
#                           'period_from': val.period_from.id, 
#                           'label_filter': False, 
#                           'filter_cmp': 'filter_no', 
#                           'enable_filter': False, 
#                           'journal_ids': jids, 
#                           'date_to': False, 
#                           'comparison_context': {
#                                                   'state': 'posted', 
#                                                   'chart_account_id': chart[val.company_id.id], 
#                                                   'journal_ids': jids, 
#                                                   'fiscalyear': False
#                                                   }, 
#                           'filter': 'filter_period', 
#                           'date_from_cmp': False, 
#                           'debit_credit': True, 
#                           'target_move': 'posted'
#                           }
#                 }
#         
#         
#         judul = '%s_%s.xls' % (val.type, val.company_id.name)
#         data = 'sep=;\nAccount Name;Debit;Credit;Balance'
# 
#         lines = self.get_lines(cr, uid, ids, datas)
#         for x in lines:
#             d = [str(x['name']),
#                  str(x['debit']),
#                  str(x['credit']), 
#                  str(x['balance'])]  
#             data += '\n' + ';'.join(d)
#                             
#         out = base64.b64encode(data.encode('ascii',errors='ignore'))
#         self.write(cr, uid, ids, {'data_file':out, 'name':judul}, context=context)
# 
#         view_rec = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'siu_ledger', 'view_wizard_laporan_accounting')
#         view_id = view_rec[1] or False
#                
#         return {
#             'view_type': 'form',
#             'view_id' : [view_id],
#             'view_mode': 'form',
#             'res_id': val.id,
#             'res_model': 'laporan.accounting',
#             'type': 'ir.actions.act_window',
#             'target': 'new',
#         }
# 
#     def get_lines(self, cr, uid, ids, data):
#         lines = []
#         account_obj = self.pool.get('account.account')
#         currency_obj = self.pool.get('res.currency')
#         ids2 = self.pool.get('account.financial.report')._get_children_by_order(cr, uid, [data['form']['account_report_id'][0]], context=data['form']['used_context'])
#         for report in self.pool.get('account.financial.report').browse(cr, uid, ids2, context=data['form']['used_context']):
#             account_ids = []
#             if report.display_detail == 'no_detail':
#                 continue
#             if report.type == 'accounts' and report.account_ids:
#                 account_ids = account_obj._get_children_and_consol(cr, uid, [x.id for x in report.account_ids]) 
#             elif report.type == 'account_type' and report.account_type_ids:
#                 account_ids = account_obj.search(cr, uid, [('user_type','in', [x.id for x in report.account_type_ids])])
#             if account_ids:
#                 for account in account_obj.browse(cr, uid, account_ids, context=data['form']['used_context']):
#                     if report.display_detail == 'detail_flat' and account.type == 'view':
#                         continue
#                     flag = False
#                     vals = {
#                         'name': account.code + ' ' + account.name,
#                         'balance':  account.balance != 0 and account.balance * report.sign or account.balance,
#                         'type': 'account',
#                         'level': report.display_detail == 'detail_with_hierarchy' and min(account.level + 1,6) or 6,
#                         'account_type': account.type,
#                     }
#                     if data['form']['debit_credit']:
#                         vals['debit'] = account.debit
#                         vals['credit'] = account.credit
#                     if not currency_obj.is_zero(cr, uid, account.company_id.currency_id, vals['balance']):
#                         flag = True
#                     if data['form']['enable_filter']:
#                         vals['balance_cmp'] = account_obj.browse(cr, uid, account.id, context=data['form']['comparison_context']).balance * report.sign or 0.0
#                         if not currency_obj.is_zero(cr, uid, account.company_id.currency_id, vals['balance_cmp']):
#                             flag = True
#                     if flag:
#                         if account.type == 'view':
#                             lines.append(vals)
#         return lines












            

    
#     def eksport_excel(self, cr, uid, ids, context=None):
#         val = self.browse(cr, uid, ids)[0]
#         judul = ''; data = ''
#             
#         obj_journal = self.pool.get('account.analytic.journal')
#         jids = obj_journal.search(cr, uid, [])
#         
#         datas = {}
#         if val.filter == 'date':            
#             datas={
#                      'date_from': val.date_start, 
#                      'journal_ids': jids, 
#                      'state': 'posted', 
#                      'date_to': val.date_stop, 
#                      'fiscalyear': val.fiscalyear_id.id
#             }
#         
#         elif val.filter == 'period':
#             datas={
#                       'period_to': val.period_to.id,
#                       'journal_ids': jids, 
#                       'period_from': val.period_from.id, 
#                       'state': 'posted', 
#                       'fiscalyear': val.fiscalyear_id.id
#                 }
#         
#         
#         judul = '%s_%s.xls' % ('Analytic', val.chart_analytic_id.name)
#         data = 'sep=;\nAnalytic Name;Debit;Credit;Balance'
# 
#         lines = []
#         analytic_obj = self.pool.get('account.analytic.account')
#         account_ids = self._get_children_and_consol(cr, uid, [val.chart_analytic_id.id])
#         for account in analytic_obj.browse(cr, uid, account_ids, context=datas):
#             if account.type == 'view':
#                 lines.append({
#                     'name': '['+account.code+']' + ' ' + account.name,
#                     'balance':  account.balance,
#                     'credit': account.credit,
#                     'debit': account.debit,
#                 })
#         lines.remove(lines[0])    
#         for x in lines:
#             d = [str(x['name']),
#                  str(x['debit']),
#                  str(x['credit']), 
#                  str(x['balance'])]  
#             data += '\n' + ';'.join(d)
#                             
#         out = base64.b64encode(data.encode('ascii',errors='ignore'))
#         self.write(cr, uid, ids, {'data_file':out, 'name':judul}, context=context)
# 
#         view_rec = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'siu_ledger', 'view_wizard_laporan_analytic')
#         view_id = view_rec[1] or False
#                
#         return {
#             'view_type': 'form',
#             'view_id' : [view_id],
#             'view_mode': 'form',
#             'res_id': val.id,
#             'res_model': 'laporan.analytic',
#             'type': 'ir.actions.act_window',
#             'target': 'new',
#         }

