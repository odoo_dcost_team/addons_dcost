import re
import time
from openerp.osv import fields, osv
from datetime import datetime
from openerp.report import report_sxw

class ReportStatus(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        super(ReportStatus, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'urut': self.urut,
            'koma': self.koma,
            'tot': self.tot,
            'get_baris': self.get_baris,
            'pencetak': self.pencetak,
        })
        
        self.no = 0
        self.total = 0
        self.re_digits_nondigits = re.compile(r'\d+|\D+')
                           
    def urut(self):
        self.no += 1
        return self.no

    def pencetak(self):
        from pytz import timezone
        date = datetime.now(timezone('Asia/Jakarta')).strftime("%d/%m/%Y at %H:%M:%S")
        user = self.pool.get('res.users').browse(self.cr, self.uid, self.uid)
        
        return date, user.name
             
    def get_basedon(self, form):
        data = self.pool.get(form['model']).browse(self.cr, self.uid, [form['form']['id']])
        return data
    
    
    def get_baris(self, line, mrp):
        val = []; harga = {}
        
        for i in mrp:
            harga[i.name.id] = i.price
        
        for x in line:
            self.total += x.product_uom_qty*harga[x.product_id.id]
            val.append({
                        'nama': x.product_id.partner_ref,
                        'qty': x.product_uom_qty,
                        'uom': x.product_uom.name,
                        'harga': harga[x.product_id.id],
                        'subtotal': x.product_uom_qty*harga[x.product_id.id],
                        })
            
        return val
    
    def tot(self):
        return self.total
    
    def koma(self, format, value):
        parts = self.re_digits_nondigits.findall(format % (value,))
        for i in xrange(len(parts)):
            s = parts[i]
            if s.isdigit():
                parts[i] = self.commafy(s)
                break
        return ''.join(parts)
        
    def commafy(self, s):
        r = []
        for i, c in enumerate(reversed(s)):
            if i and (not (i % 3)):
                r.insert(0, ',')
            r.insert(0, c)
        return ''.join(r)


report_sxw.report_sxw('report.print.deliver', 'stock.picking', 'addons/siu_report/report/print_delivery.rml', parser=ReportStatus, header=False)

