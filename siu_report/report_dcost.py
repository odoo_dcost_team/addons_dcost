import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv, orm


class hpp_direct(osv.osv):
    _name = 'hpp.direct'
    _columns = {
        'name': fields.selection([('date', 'Date'), ('period', 'Period')], 'Filter', required=True, select=True),
        'pricelist_id': fields.many2one('product.pricelist.version', 'Pricelist', required=True, select=True),
        'company_id': fields.many2one('res.company', 'Company', required=True),
        'period_to' : fields.many2one('account.period', 'Period To', domain="[('company_id', '=', company_id)]", select=True),
        'period_from' : fields.many2one('account.period', 'Period From', domain="[('company_id', '=', company_id)]", select=True),
        'date_start': fields.date('Start date', select=True),
        'date_stop': fields.date('End date', select=True),
        'direct_line': fields.one2many('hpp.direct.line', 'hpp_id', 'Direct Cost Line', readonly=True),
        'price_line': fields.one2many('hpp.price.line', 'hpp_id', 'Price List', readonly=True)
    }

    _defaults = {
                 'name': 'period'
    }


    def compute(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        wh_obj = self.pool.get('stock.warehouse')
        mbo_obj = self.pool.get('mrp.bom')
        pro_obj = self.pool.get('product.product')
        hdl_obj = self.pool.get('hpp.direct.line')
        pdl_obj = self.pool.get('hpp.price.line')
        ppl_obj = self.pool.get('hpp.product.line')
        
        
        pid = pro_obj.search(cr, uid, [('sale_ok', '=', True)])[:5]
        bid = mbo_obj.search(cr, uid, [('product_tmpl_id', 'in', pid)])
        
        wh = wh_obj.search(cr, uid, [('partner_id', '!=', False), ('company_id', '=', val.company_id.id)])
        for w in wh :
            ada = hdl_obj.search(cr, uid, [('hpp_id', '=', val.id), ('name', '=', w)])
            if not ada:
                wid = hdl_obj.create(cr, uid, {'hpp_id': val.id, 'name': w, 'total': 0})
                for b in mbo_obj.browse(cr, uid, bid):
                    ppl_obj.create(cr, uid, {'line_id': wid, 'name': b.product_tmpl_id.id, 'product_qty': 1, 'cost': 100, 'uom_id': b.product_uom.id,})
            
        harga = {}
        for i in val.pricelist_id.items_id:
            if i.product_id.id:
                adi = pdl_obj.search(cr, uid, [('hpp_id', '=', val.id), ('name', '=', i.product_id.id)])
                if not adi:
                    harga[i.product_id.id] = i.price_surcharge
                    pdl_obj.create(cr, uid, {'hpp_id': val.id, 'name': i.product_id.id, 'cost': i.price_surcharge, 'uom_id': i.product_id.uom_id.id,})
            

        

        return True


class hpp_price_line(osv.osv):
    _name = 'hpp.price.line' 
    _columns = {
        'hpp_id': fields.many2one('hpp.direct', 'HPP Direct', required=True, ondelete='cascade', select=True),
        'name': fields.many2one('product.product', 'Product', required=True, select=True),
        'cost': fields.float('Unit Price', required=True, select=True, digits_compute=dp.get_precision('Account')),
        'uom_id': fields.many2one('product.uom', 'UoM', required=True),
    }



class hpp_direct_line(osv.osv):
    _name = 'hpp.direct.line'
    
    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {'total': 0.0}
            val = 0.0
            for line in order.product_line:
                val += line.subtotal
            res[order.id]['total'] = val
        return res
    
    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('hpp.product.line').browse(cr, uid, ids, context=context):
            result[line.line_id.id] = True
        return result.keys()

     
    _columns = {
        'hpp_id': fields.many2one('hpp.direct', 'HPP Direct', required=True, ondelete='cascade', select=True),
        'name': fields.many2one('stock.warehouse', 'Outlet', required=True, select=True),
        'product_line': fields.one2many('hpp.product.line', 'line_id', 'Direct Line', readonly=True),
        'total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Total',
            store={
                'hpp.direct.line': (lambda self, cr, uid, ids, c={}: ids, ['product_line'], 10),
                'hpp.product.line': (_get_order, ['cost', 'product_qty'], 10)}, multi='sums')
    }



class hpp_product_line(osv.osv):
    _name = 'hpp.product.line'
    
    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = line.cost * line.product_qty
        return res
    
 
    _columns = {
        'line_id': fields.many2one('hpp.direct.line', 'Direct Line', required=True, ondelete='cascade', select=True),
        'name': fields.many2one('product.product', 'Product', required=True, select=True),
        'uom_id': fields.many2one('product.uom', 'UoM', required=True),
        'product_qty': fields.float('Quantity', required=True, select=True, digits_compute=dp.get_precision('Product UoM')),
        'cost': fields.float('Unit Price', required=True, select=True, digits_compute=dp.get_precision('Account')),
        'subtotal': fields.function(_amount_line, string='Subtotal', digits_compute= dp.get_precision('Account')),
        
    }






class hpp_indirect(osv.osv):
    _name = 'hpp.indirect'
    _columns = {
        'name': fields.selection([('date', 'Date'), ('period', 'Period')], 'Filter', required=True, select=True),
        'company_id': fields.many2one('res.company', 'Company', required=True),
        'period_to' : fields.many2one('account.period', 'Period To', domain="[('company_id', '=', company_id)]", select=True),
        'period_from' : fields.many2one('account.period', 'Period From', domain="[('company_id', '=', company_id)]", select=True),
        'date_start': fields.date('Start date', select=True),
        'date_stop': fields.date('End date', select=True),
        'indirect_line': fields.one2many('hpp.indirect.line', 'hpp_id', 'Indirect Cost Line', readonly=True)
    }

    _defaults = {
                 'name': 'period'
    }



    def compute(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        wh_obj = self.pool.get('stock.warehouse')
        hdl_obj = self.pool.get('hpp.indirect.line')
        
        
        wh = wh_obj.search(cr, uid, [('partner_id', '!=', False), ('company_id', '=', val.company_id.id)])
        for w in wh :
            ada = hdl_obj.search(cr, uid, [('hpp_id', '=', val.id), ('name', '=', w)])
            if not ada:
                hdl_obj.create(cr, uid, {'hpp_id': val.id, 'name': w, 'saldo_awal': 10, 'senlog': 10, 'supplier': 10, 'omzet': 10, 'saldo_akhir': 10, 'hpp': 10})
                        

        return True


class hpp_indirect_line(osv.osv):
    _name = 'hpp.indirect.line'
    _columns = {
        'hpp_id': fields.many2one('hpp.indirect', 'HPP Indirect', required=True, ondelete='cascade', select=True),
        'name': fields.many2one('stock.warehouse', 'Outlet', required=True, select=True),
        'saldo_awal': fields.float('Saldo Awal', required=True, select=True, digits_compute=dp.get_precision('Account')),
        'senlog': fields.float('Senlog', required=True, select=True, digits_compute=dp.get_precision('Account')),
        'supplier': fields.float('Supplier', required=True, select=True, digits_compute=dp.get_precision('Account')),
        'omzet': fields.float('Omzet', required=True, select=True, digits_compute=dp.get_precision('Account')),
        'saldo_akhir': fields.float('Saldo Akhir', required=True, select=True, digits_compute=dp.get_precision('Account')),
        'hpp': fields.float('HPP', required=True, select=True, digits_compute=dp.get_precision('Account')),
        
    }




# class hpp_direct(osv.osv):
#     _name = 'hpp.direct'
#     _columns = {
#         'name': fields.selection([('date', 'Date'), ('period', 'Period')], 'Filter', required=True, select=True),
#         'pricelist_id': fields.many2one('product.pricelist.version', 'Pricelist', required=True, select=True),
#         'company_id': fields.many2one('res.company', 'Company', required=True),
#         'period_to' : fields.many2one('account.period', 'Period To', domain="[('company_id', '=', company_id)]", select=True),
#         'period_from' : fields.many2one('account.period', 'Period From', domain="[('company_id', '=', company_id)]", select=True),
#         'date_start': fields.date('Start date', select=True),
#         'date_stop': fields.date('End date', select=True),
#     }



#         if not val.material_line:
#             raw = []
#             for x in val.requirement_line:
#                 results = self.get_mrp(cr, uid, ids, x.product_uom.id, x.name.id, x.plan, x.name.partner_ref) 
#                 raw += results
# 
#                 for r in results:
#                     hasil = self.get_mrp(cr, uid, ids, r['product_uom'], r['product_id'], r['product_qty'], r['name'])
#                     raw += hasil
#                 
#             data = {}
#             for p in raw:
#                 data[p['product_id']] = {'product_qty': [], 'product_uom': p['product_uom']} 
#             for p in raw:
#                 data[p['product_id']]['product_qty'].append(p['product_qty'])
#              
#             for i in data:  
#                 sup = 'buy'
#                 cek = bom_obj._bom_find(cr, uid, data[i]['product_uom'], product_id=i, properties=[], context=context)
#                 if cek:
#                     sup = 'mo'
#                 plan_obj.create(cr, uid, {
#                                           'requirement_id': val.id,
#                                           'name': i,
#                                           'bom_id': cek,
#                                           'plan': sum(data[i]['product_qty']),
#                                           'product_uom': data[i]['product_uom'],
#                                           'supply': sup
#             })